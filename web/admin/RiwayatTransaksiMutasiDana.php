<?php
include ('akses.php');
$fitur_id = 23;
include ('login/lock-menu.php');

//kode jadi untuk server log
include ('../library/tgl-indo.php');
$DateTime = date('Y-m');

include('../library/config.php');
date_default_timezone_set('Asia/Jakarta');

if(@$_REQUEST['KeywordCabang']!=null){
	$cabang = @$_REQUEST['KeywordCabang'];
}else{
	$cabang = $login_cabang;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <?php include 'view/title.php' ?>

    <!-- Bootstrap Core CSS -->
    <link href="komponen/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="komponen/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="komponen/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="komponen/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="komponen/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda Yakin Untuk Mengganti Kurir Ini ?")
			if (answer == true){
				window.location = "MasterKurir.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
	
	<!-- Datepcker -->
	<link rel="stylesheet" href="../library/Datepicker/dist/css/default/zebra_datepicker.min.css" type="text/css">
	
	<!-- Select 2 Skin -->
	<link rel="stylesheet" href="../library/select2-master/dist/css/select2.css"/>
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
		<?php include 'view/menu.php' ; ?>
		</nav>
		
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Riwayat Transaksi Mutasi Dana</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
						<!-- Nav tabs -->
						<ul class="nav nav-pills">
							<li <?php if(@$id==null){echo 'class="active"';} ?>><a href="#home-pills" data-toggle="tab">Data Mutasi Dana</a></li>
							<?php if(@$id!=null){
								echo '<li class="active"><a href="#tambah-user" data-toggle="tab">Detil Order</a></li>';
							} ?>
						</ul>
						<div class="tab-content">
                            <div class="tab-pane fade <?php if(@$id==null){echo 'in active';} ?>" id="home-pills">
							<form method="post" action="">
								<div class="form-group col-lg-4 col-lg-offset-5">
								<?php if($IsServer=='1'){ ?>	
									<select name="KeywordCabang" class="form-control" required>	
										<?php
											echo '<option value="">-- Cabang Perusahaan --</option>';
											$menu = mysqli_query($koneksi,"SELECT a.*,b.NamaProvinsi,c.NamaKab FROM mstcabang a JOIN mstprovinsi b ON a.KodeProvinsi=b.KodeProvinsi JOIN mstkabupaten c ON a.KodeKab=c.KodeKab ORDER BY a.KodeCabang ASC");
											while($kode = mysqli_fetch_array($menu)){
												if($kode['KodeCabang']==$cabang){
													echo "<option value=\"".$kode['KodeCabang']."\" selected >".$kode['NamaProvinsi']." - ".$kode['NamaKab']."</option>\n";
												}else{
													echo "<option value=\"".$kode['KodeCabang']."\">".$kode['NamaProvinsi']." - ".$kode['NamaKab']."</option>\n";
												}
												
											}
										?>
									</select>
								<?php } ?>
								</div>
								<div class="form-group input-group col-lg-3">
									<input type="text" name="keyword" class="form-control" id="datepicker" value="<?php if(@$_REQUEST['keyword']==null){echo $DateTime;}else{echo $_REQUEST['keyword'];}; ?>" placeholder="Tanggal Transaksi">
									<span class="input-group-btn">
										<button class="btn btn-large btn-info" type="submit">Cari</button>
									 </span>
								</div>
							</form>
							<!-- ===================================== tabel ==================================== -->
							<div class="table-responsive">
							<table width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal Transaksi</th>
                                        <th>Mutasi Dana Ke-</th>
                                        <th>Nominal</th>
                                        <th>Status Transfer</th>
                                    </tr>
                                </thead>
								<?php
									include '../library/pagination1.php';
									// mengatur variabel reload dan sql
									$kosong=null;
									if(@$_REQUEST['KeywordCabang']!=null OR @$_REQUEST['keyword']!=null){
										// jika ada kata kunci pencarian (artinya form pencarian disubmit dan tidak kosong)pakai ini
										$keyword=$_REQUEST['keyword'];
										$reload = "RiwayatTransaksiMutasiDana.php?pagination=true&keyword=$keyword";
										$sql =  "SELECT a.*,d.NamaProvinsi as ProvTujuan,c.NamaKab as KabTujuan,e.NamaProvinsi as ProvPengirim,f.NamaKab as KabPengirim FROM trpengirimandana a JOIN mstcabang b ON a.CabangPenerima=b.KodeCabang JOIN mstprovinsi d ON b.KodeProvinsi=d.KodeProvinsi JOIN mstkabupaten c ON b.KodeKab=c.KodeKab JOIN mstcabang g ON g.KodeCabang=a.KodeCabang JOIN mstprovinsi e ON g.KodeProvinsi=e.KodeProvinsi JOIN mstkabupaten f ON g.KodeKab=f.KodeKab  
										WHERE (a.KodeCabang='$cabang' OR a.CabangPenerima='$cabang')  AND date_format(TanggalTransaksi, '%Y-%m')='$keyword' ORDER BY a.TanggalTransaksi DESC";
										
										$result = mysqli_query($koneksi,$sql);
									}else{
									//jika tidak ada pencarian pakai ini
										$reload = "RiwayatTransaksiMutasiDana.php?pagination=true";
										$sql = "SELECT a.*,d.NamaProvinsi as ProvTujuan,c.NamaKab as KabTujuan,e.NamaProvinsi as ProvPengirim,f.NamaKab as KabPengirim FROM trpengirimandana a JOIN mstcabang b ON a.CabangPenerima=b.KodeCabang JOIN mstprovinsi d ON b.KodeProvinsi=d.KodeProvinsi JOIN mstkabupaten c ON b.KodeKab=c.KodeKab JOIN mstcabang g ON g.KodeCabang=a.KodeCabang JOIN mstprovinsi e ON g.KodeProvinsi=e.KodeProvinsi JOIN mstkabupaten f ON g.KodeKab=f.KodeKab  
										WHERE a.KodeCabang='$cabang' OR a.CabangPenerima='$cabang' ORDER BY a.TanggalTransaksi DESC";
										$result = mysqli_query($koneksi,$sql);
									}
									
									//pagination config start
									$rpp = 25; // jumlah record per halaman
									$page = intval(@$_GET["page"]);
									if($page<=0) $page = 1;  
									$tcount = mysqli_num_rows($result);
									$tpages = ($tcount) ? ceil($tcount/$rpp) : 1; // total pages, last page number
									$count = 0;
									$i = ($page-1)*$rpp;
									$no_urut = ($page-1)*$rpp;
									//pagination config end				
								?>
                                <tbody>
									<?php
									while(($count<$rpp) && ($i<$tcount)) {
										mysqli_data_seek($result,$i);
										$data = mysqli_fetch_array($result);
									?>
                                    <tr class="odd gradeX">
                                        <td width="50px">
											<?php echo ++$no_urut;?> 
										</td>
										<td>
											<?php 
												echo TanggalIndo($data ['TanggalTransaksi']); echo '<br/>';echo substr($data['TanggalTransaksi'],10,19);
											?>
										</td>
										<td>
											<?php 
												if($data['KodeCabang']=='C0001'){ 
													echo 'Pusat Ke- '; 
													echo "Cabang ".ucwords(strtolower($data['ProvTujuan'])).",".ucwords(strtolower($data['KabTujuan']))."";
													if($cabang=='C0001'){
														$Status = '<font color="red">Keluar</font>';
													}else{
														$Status = '<font color="green">Masuk</font>';
													}
												}else{
													echo "Cabang ".ucwords(strtolower($data['ProvPengirim'])).",".ucwords(strtolower($data['KabPengirim']))." Ke- Pusat";
													if($cabang=='C0001'){
														$Status = '<font color="green">Masuk</font>';
													}else{
														$Status = '<font color="red">Keluar</font>';
													}
												} 
											?>
										</td>
										<td>
											Rp.<?php echo number_format($data['NominalTransfer']);?>,-
										</td>
										<td>
											<?php echo $Status;?>
										</td>
                                    </tr>
									<?php
										$i++; 
										$count++;
									}
									?>
                                </tbody>
                            </table>
							<div><?php echo paginate_one($reload, $page, $tpages); ?></div>
							</div>
							<!-- ===================================== tabel ==================================== -->
							</div>
							
						</div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="komponen/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="komponen/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="komponen/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="komponen/vendor/raphael/raphael.min.js"></script>
    <script src="komponen/vendor/morrisjs/morris.min.js"></script>
    <script src="komponen/data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="komponen/dist/js/sb-admin-2.js"></script>
	<!-- Select 2 -->
	<script src="../library/select2-master/dist/js/select2.min.js"></script>
	<script>
	$(document).ready(function () {
		$("#kota").select2({
		placeholder: "Cari Nama Kurir Pengganti"
		});
	});
	</script>
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>

<!-- DatePicker -->
<script type="text/javascript" src="../library/Datepicker/dist/zebra_datepicker.src.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#datepicker').Zebra_DatePicker({format: 'Y-m'});
	});
</script>
</body>
</html>
