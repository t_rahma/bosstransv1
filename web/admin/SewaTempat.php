<?php
include ('akses.php');
$fitur_id = 13;
include ('login/lock-menu.php');

//kode jadi untuk server log
include ('../library/kode-log-server.php');
include ('../library/tgl-indo.php');
$DateTime = date('Y-m-d H:i:s a');

include('../library/config.php');
date_default_timezone_set('Asia/Jakarta');

@$id 	= base64_decode($_GET['id']);
@$gambar = base64_decode($_GET['gbr']);
@$metode = $_GET['metode'];
/* if($metode=='aktifkan'){
	mysqli_query($koneksi,"UPDATE iklan SET IsAktif='1' WHERE KodeIklan='$id' AND KodeCabang='$login_cabang'");
	mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
	VALUES ('$kode_jadi_log','$DateTime','Hapus Data','Sewa Iklan Set Aktif: $id','$login_id','$login_cabang')");
}elseif($metode=='nonaktifkan'){
	mysqli_query($koneksi,"UPDATE iklan SET IsAktif='0' WHERE KodeIklan='$id' AND KodeCabang='$login_cabang'");
	mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
	VALUES ('$kode_jadi_log','$DateTime','Update Data','Sewa Iklan Set Nonaktif: $id','$login_id','$login_cabang')");
}else */
if($metode=='hapus'){
	mysqli_query($koneksi,"DELETE FROM iklan WHERE KodeIklan='$id' AND KodeCabang='$login_cabang'");
	//update no tampil rm_penjual
	mysqli_query($koneksi,"UPDATE rm_penjual SET NoTampil=NULL WHERE KodeRM='".base64_decode($_GET['rm'])."' AND KodePerson='".base64_decode($_GET['prs'])."'");
	//unlink ("../assets/img/Iklan/$gambar");
	mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
	VALUES ('$kode_jadi_log','$DateTime','Hapus Data','Sewa Tempat : $id','$login_id','$login_cabang')");
}

if(isset($_POST['SimpanData'])){
	//kode iklan
	$year	= date('Y');
	$sql 	= mysqli_query($koneksi,'SELECT RIGHT(KodeIklan,6) AS kode FROM iklan WHERE KodeIklan LIKE "%'.$year.'%" ORDER BY KodeIklan DESC LIMIT 1'); 
	$nums 	= mysqli_num_rows($sql);
	 
	if($nums <> 0)
	 {
	 $data = mysqli_fetch_array($sql);
	 $kode = $data['kode'] + 1;
	 }else
	 {
	 $kode = 1;
	 }
	 
	//mulai bikin kode
	 $bikin_kode = str_pad($kode, 6, "0", STR_PAD_LEFT);
	 $kode_jadi	 = "IKL-".$year."-".$bikin_kode;
	
	$KodeRM 		 = substr(@$_POST['KodeRM'],0,8);
	$KodePerson 	 = substr(@$_POST['KodeRM'],9,16);
	$NamaRumahMakan	 = substr(@$_POST['KodeRM'],26,50);
	
	$BiayaSewa 		 = @$_POST['BiayaSewa'];
	//-------------- Ambil prosentase perusahaan cabang usaha ----------------//
	if($login_cabang!='C0001'){
		$PersenCab=mysqli_query($koneksi,"SELECT ValueData FROM systemsetting WHERE KodeSetting='SET-00019' AND KodeCabang='C0001'");
		$DataPersenCab = mysqli_fetch_assoc($PersenCab);
		//porsi pusat
		$PorsiPusat = ($DataPersenCab['ValueData']/100)*$BiayaSewa;
		$PorsiCabang = $BiayaSewa-$PorsiPusat;
	}else{
		$PorsiPusat = 0;
		$PorsiCabang = $BiayaSewa;
	}
	
	$query = mysqli_query($koneksi,"INSERT INTO iklan(KodeIklan,UserName,NamaIklan,TglMulaiIklan,TglSelesaiIklan, IsAktif,HargaSewa,PorsiPusat,KodeRM,KodePerson,KodeCabang,TipeIklan,NoTampil) VALUES('$kode_jadi','$login_id','$NamaRumahMakan', '".$_POST['TglMulaiIklan']."','".$_POST['TglSelesaiIklan']."','1','$PorsiCabang','$PorsiPusat','$KodeRM','$KodePerson','$login_cabang','RM','".$_POST['NoUrut']."')");
	if($query){
		//update no tampil rm_penjual
		mysqli_query($koneksi,"UPDATE rm_penjual SET NoTampil='".$_POST['NoUrut']."' WHERE KodeRM='$KodeRM' AND KodePerson='$KodePerson'");
		
		mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
		VALUES ('$kode_jadi_log','".date("Y-m-d H:i:s")."','Tambah Data','Sewa Tempat : $NamaRumahMakan, $kode_jadi_log','$login_id','$login_cabang')");
		echo '<script language="javascript">document.location="SewaTempat.php"; </script>';
	}else{
		echo '<script language="javascript">alert("Simpan data gagal !"); document.location="SewaTempat.php"; </script>';
	}
 }

 //===========procedure Update data=============//
 if($metode=='Edit'){
	$edit = mysqli_query($koneksi,"select * from iklan where KodeIklan = '$id' AND KodeCabang='$login_cabang'");
	 while($row = mysqli_fetch_array($edit)){
		$nama_Iklan		 	= $row['NamaIklan'];
		$tgl_mulaiIklan		= $row['TglMulaiIklan'];
		$tgl_selesaiIklan	= $row['TglSelesaiIklan'];
		$biaya_sewa			= $row['HargaSewa'];
		$foto_iklan			= $row['Foto'];
		$kode_menu			= $row['KodeMenu'];
		$kode_rm			= $row['KodeRM'];
		$kode_person		= $row['KodePerson'];
		$tampil_semua		= $row['IsTampilSemua'];
		$no_tampil			= $row['NoTampil'];
	 } 
 }
 
 if(isset($_POST['EditData'])){
	$BiayaSewa = @$_POST['BiayaSewa'];
	//-------------- Ambil prosentase perusahaan cabang usaha ----------------//
	if($login_cabang!='C0001'){
		$PersenCab=mysqli_query($koneksi,"SELECT ValueData FROM systemsetting WHERE KodeSetting='SET-00019' AND KodeCabang='C0001'");
		$DataPersenCab = mysqli_fetch_assoc($PersenCab);
		//porsi pusat
		$PorsiPusat = ($DataPersenCab['ValueData']/100)*$BiayaSewa;
		$PorsiCabang = $BiayaSewa-$PorsiPusat;
	}else{
		$PorsiPusat = 0;
		$PorsiCabang = $BiayaSewa;
	}
	
	$query = mysqli_query($koneksi,"UPDATE iklan SET TglMulaiIklan='".$_POST['TglMulaiIklan']."',TglSelesaiIklan='".$_POST['TglSelesaiIklan']."',HargaSewa='$PorsiCabang',PorsiPusat='$PorsiPusat' WHERE KodeIklan = '$id'");
		if($query){
			mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
			VALUES ('$kode_jadi_log','$DateTime','Edit Data','Sewa Tempat : $id','$login_id','$login_cabang')");
			echo '<script language="javascript">alert("Edit Data berhasil disimpan !"); document.location="SewaTempat.php"; </script>';
		}else{
			echo '<script language="javascript">alert("Edit data gagal !"); document.location="SewaTempat.php?id='.base64_encode($id).'"; </script>';
		}
 }
 
//ambil kode cabang 
$QueryCab = mysqli_query($koneksi,"SELECT * FROM mstcabang WHERE KodeCabang='$login_cabang'");
$RowCab = mysqli_fetch_assoc($QueryCab);
$Prov = $RowCab['KodeProvinsi'];
$Kab = $RowCab['KodeKab'];
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <?php include 'view/title.php' ?>

    <!-- Bootstrap Core CSS -->
    <link href="komponen/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="komponen/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="komponen/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="komponen/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="komponen/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<!-- Datepcker -->
	<link rel="stylesheet" href="../library/Datepicker/dist/css/default/zebra_datepicker.min.css" type="text/css">

	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda Yakin Untuk Menghapus Iklan ini? Iklan yang dihapus tidak dapat dikembalikan lagi!")
			if (answer == true){
				window.location = "AccessLevel.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
	
	<script type="text/javascript">
		function confirmation2() {
			var answer = confirm("Apakah Anda Yakin Mengaktifkan Fitur ini . . . ?")
			if (answer == true){
				window.location = "AccessLevel.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
	
	<script type="text/javascript">
		function confirmation3() {
			var answer = confirm("Apakah Anda Yakin Non Aktifkan Fitur ini . . . ?")
			if (answer == true){
				window.location = "AccessLevel.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
		<?php include 'view/menu.php' ; ?>
		</nav>
		
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Sewa Tempat</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
						<!-- Nav tabs -->
                            <ul class="nav nav-pills">
                                <li <?php if(@$metode!='Edit'){ echo 'class="active"';}?>><a href="#home-pills" data-toggle="tab">Data Sewa Tempat</a></li>
                                <li><a href="#tambah-user" data-toggle="tab">Tambah Sewa Tempat</a></li>
								<?php if(@$metode=='Edit'){echo '<li class="active"><a href="#edit-iklan" data-toggle="tab">Edit Sewa Tempat</a></li>';}?>
                            </ul>
						<div class="tab-content">
                            <div class="tab-pane fade <?php if(@$metode!='Edit'){ echo 'in active';}?>" id="home-pills"><br/>
							<!-- ===================================== tabel ==================================== -->
							<div class="table-responsive">
							<table width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Rumah Makan</th>
                                        <th>No.Tampil</th>
                                        <th>Tanggal Tayang</th>
                                        <th>Harga Sewa</th>
                                        <th>Status Sewa</th>
                                        <th>Author</th>
										<th>Aksi</th>
                                    </tr>
                                </thead>
								<?php
									include '../library/pagination1.php';
									// mengatur variabel reload dan sql
									$kosong=null;
									if(isset($_REQUEST['keyword']) && $_REQUEST['keyword']<>""){
										// jika ada kata kunci pencarian (artinya form pencarian disubmit dan tidak kosong)pakai ini
										$keyword=$_REQUEST['keyword'];
										$reload = "SewaIklan.php?pagination=true&keyword=$keyword";
										$sql =  "SELECT * FROM iklan WHERE KodeCabang='$login_cabang' AND TipeIklan='RM' NamaIklan LIKE '%$keyword%' ORDER BY KodeIklan DESC";
										$result = mysqli_query($koneksi,$sql);
									}else{
									//jika tidak ada pencarian pakai ini
										$reload = "SewaIklan.php?pagination=true";
										$sql =  "SELECT * FROM iklan WHERE KodeCabang='$login_cabang' AND TipeIklan='RM' ORDER BY KodeIklan DESC";
										$result = mysqli_query($koneksi,$sql);
									}
									
									//pagination config start
									$rpp = 30; // jumlah record per halaman
									$page = intval(@$_GET["page"]);
									if($page<=0) $page = 1;  
									$tcount = mysqli_num_rows($result);
									$tpages = ($tcount) ? ceil($tcount/$rpp) : 1; // total pages, last page number
									$count = 0;
									$i = ($page-1)*$rpp;
									$no_urut = ($page-1)*$rpp;
									//pagination config end				
								?>
                                <tbody>
									<?php
									while(($count<$rpp) && ($i<$tcount)) {
										mysqli_data_seek($result,$i);
										$data = mysqli_fetch_array($result);
									?>
                                    <tr class="odd gradeX">
                                        <td width="50px">
											<?php echo ++$no_urut;?> 
										</td>
										<td>
											<strong><?php echo $data ['NamaIklan']; ?></strong>
										</td>
										<td>
											<?php echo $data['NoTampil'];?>
										</td>
                                        <td>
											<?php 
												if(strtotime($DateTime) > strtotime($data['TglSelesaiIklan'])){
													//update iklan tidak aktif
													mysqli_query($koneksi,"UPDATE iklan SET IsAktif='0' WHERE KodeIklan='".$data['KodeIklan']."'");
													mysqli_query($koneksi,"UPDATE rm_penjual SET NoTampil = NULL WHERE KodeRM='".$data['KodeRM']."' AND KodePerson='".$_POST['KodePerson']."' ");
													echo 'Iklan Expired';
												}else{
													echo "".TanggalIndo($data['TglMulaiIklan'])." ".substr($data['TglMulaiIklan'],10,17)."";
													echo ' s/d <br/>'; 
													echo "".TanggalIndo($data['TglSelesaiIklan'])." ".substr($data['TglSelesaiIklan'],10,17)."";
												}
											?>
										</td>
										<td>
											Rp.<?php echo number_format($data['HargaSewa']+$data['PorsiPusat']);?>,-
										</td>
										<td>
											<?php if($data ['IsAktif']=='1'){
												echo '<span class="btn btn-sm btn-success">Aktif</span>';
											}else{
												echo '<span class="btn btn-sm btn-danger">Tidak Aktif</span>';
											} ?>
										</td>
										<td>
											<?php echo $data ['UserName']; ?>
										</td>
										<td width="100px">				
											<a href="SewaTempat.php?id=<?php echo base64_encode($data['KodeIklan']);?>&metode=Edit" title='Edit Iklan'><i class='btn btn-warning btn-sm'><span class='fa fa-pencil'></span></i></a>
											
											<a href="SewaTempat.php?id=<?php echo base64_encode($data['KodeIklan']); ?>&metode=hapus&rm=<?php echo base64_encode($data['KodeRM']); ?>&prs=<?php echo base64_encode($data['KodePerson']); ?>" title='Hapus' onclick='return confirmation()'><i class='btn btn-danger btn-sm'><span class='fa fa-trash'></span></i></a>
										</td>
                                    </tr>
									<?php
										$i++; 
										$count++;
									}
									?>
                                </tbody>
                            </table>
							<div><?php echo paginate_one($reload, $page, $tpages); ?></div>
                            <!-- /.table-responsive -->
							</div>
							<!-- ===================================== tabel ==================================== -->
							</div>
							<div class="tab-pane fade" id="tambah-user">
							<!-- ===================================== input ===================================== -->
							<div class="panel-body">
                            <div class="row">
								<div class="col-lg-6">
									<br/>
									<form method="post" action="">
										<div class="form-group col-lg-12">
											<label>Nama Toko/Rumah Makan</label>
											<select id="KodeNegara" name="KodeRM" class="form-control">	
												<?php
													echo "<option>-- Nama Rumah Makan (Alamat) --</option>";
													$menu = mysqli_query($koneksi,"SELECT KodeRM,KodePerson,NamaRM,AlamatRM FROM rm_penjual WHERE KodeProvinsi='$Prov' AND KodeKab='$Kab' AND NoTampil is null ORDER BY NamaRM ASC");
													while($kode = mysqli_fetch_array($menu)){
														echo "<option value=\"".$kode['KodeRM']."-".$kode['KodePerson']."-".$kode['NamaRM']."\" >".$kode['NamaRM']." (".$kode['AlamatRM'].")</option>\n";
													}
												?>
											</select>
										</div>
										<div class="form-group col-lg-12">
											<label>Biaya Sewa Tempat</label>
											<input type="number" min="0" class="form-control" name="BiayaSewa" required>
										</div>
										<div class="form-group col-lg-6">
											<label>Tanggal Mulai Sewa</label>
											<input type="text" class="form-control" id="datepicker1" name="TglMulaiIklan" required>
										</div>
										<div class="form-group col-lg-6">
											<label>Tanggal Selesai Sewa</label>
											<input type="text" class="form-control" id="datepicker2" name="TglSelesaiIklan" required>
										</div>
										<div class="form-group col-lg-12">
											<label>No Urut Tampil Tersedia</label>
											<p><font color="red">*No.Urut terpilih tidak dapat di ubah/edit lagi.</font></p>
											<select name="NoUrut" class="form-control" required>	
												<?php
													$menu = mysqli_query($koneksi,"SELECT NoTampil FROM rm_penjual WHERE KodeProvinsi='$Prov' AND KodeKab='$Kab' AND NoTampil is not null ORDER BY NoTampil ASC");
													while($kode = mysqli_fetch_array($menu)){
														$NomerTerpakai[] = $kode['NoTampil'];
													}
													
														if($NomerTerpakai[0]!='1' AND $NomerTerpakai[1]!='1' AND $NomerTerpakai[2]!='1' AND $NomerTerpakai[3]!='1' AND $NomerTerpakai[4]!='1'){
															echo '<option value="1"> 1 </option>';
														}
														
														if($NomerTerpakai[0]!='2' AND $NomerTerpakai[1]!='2' AND $NomerTerpakai[2]!='2' AND $NomerTerpakai[3]!='2' AND $NomerTerpakai[4]!='2'){
															echo '<option value="2"> 2 </option>';
														}
														
														if($NomerTerpakai[0]!='3' AND $NomerTerpakai[1]!='3' AND $NomerTerpakai[2]!='3' AND $NomerTerpakai[3]!='3' AND $NomerTerpakai[4]!='3'){
															echo '<option value="3"> 3 </option>';
														}
														
														if($NomerTerpakai[0]!='4' AND $NomerTerpakai[1]!='4' AND $NomerTerpakai[2]!='4' AND $NomerTerpakai[3]!='4' AND $NomerTerpakai[4]!='4'){
															echo '<option value="4"> 4 </option>';
														}
														
														if($NomerTerpakai[0]!='5' AND $NomerTerpakai[1]!='5' AND $NomerTerpakai[2]!='5' AND $NomerTerpakai[3]!='5' AND $NomerTerpakai[4]!='5'){
															echo '<option value="5"> 5 </option>';
														}
												?>
											</select>
										</div>
										<div class="col-lg-12">
											<button type="submit" name="SimpanData" class="btn btn-md btn-primary">Simpan</button>
										</div>
									</form>
									
								</div>
                            </div>
                            <!-- /.row (nested) -->
							</div>
							<!-- ===================================== input ===================================== -->
							</div>
							
							<div class="tab-pane fade <?php if(@$metode=='Edit'){ echo 'in active';}?>" id="edit-iklan">
							<!-- ===================================== Edit ===================================== -->
							<div class="panel-body">
                            <div class="row">
								<div class="col-lg-6">
									<br/>
									<form method="post" action="">
										<div class="form-group col-lg-12">
											<label>Nama Toko/Rumah Makan</label>
											<select class="form-control" disabled>	
												<?php
													echo "<option>-- Nama Rumah Makan (Alamat) --</option>";
													$menu = mysqli_query($koneksi,"SELECT KodeRM,KodePerson,NamaRM,AlamatRM FROM rm_penjual WHERE KodeProvinsi='$Prov' AND KodeKab='$Kab' AND NoTampil is not null ORDER BY NamaRM ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['KodeRM']==$kode_rm AND $kode['KodePerson']==$kode_person){
															echo "<option value=\"".$kode['KodeRM']."-".$kode['KodePerson']."-".$kode['NamaRM']."\" selected>".$kode['NamaRM']." (".$kode['AlamatRM'].")</option>\n";
														}else{
															echo "<option value=\"".$kode['KodeRM']."-".$kode['KodePerson']."-".$kode['NamaRM']."\" >".$kode['NamaRM']." (".$kode['AlamatRM'].")</option>\n";
														}
													}
												?>
											</select>
										</div>
										<div class="form-group col-lg-12">
											<label>Biaya Sewa Iklan</label>
											<input type="number" min="0" class="form-control" name="BiayaSewa" value="<?php echo $biaya_sewa; ?>" required>
										</div>
										<div class="form-group col-lg-6">
											<label>Tanggal Mulai Iklan</label>
											<input type="text" class="form-control" id="datepicker3" value="<?php echo $tgl_mulaiIklan; ?>" name="TglMulaiIklan" required>
										</div>
										<div class="form-group col-lg-6">
											<label>Tanggal Selesai Iklan</label>
											<input type="text" class="form-control" id="datepicker4" value="<?php echo $tgl_selesaiIklan; ?>" name="TglSelesaiIklan" required>
										</div>
										<div class="form-group col-lg-12">
											<label>No Urut Tampil</label>
											<input type="number" class="form-control" value="<?php echo $no_tampil; ?>" disabled>
										</div>
										<div class="col-lg-12">
											<button type="submit" class="btn btn-md btn-block btn-success" name="EditData">Simpan Edit</button>
										</div>
									</form>
								</div>
                            </div>
                            <!-- /.row (nested) -->
							</div>
							<!-- ===================================== Edit ===================================== -->
							</div>
						</div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="komponen/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="komponen/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="komponen/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="komponen/vendor/raphael/raphael.min.js"></script>
    <script src="komponen/vendor/morrisjs/morris.min.js"></script>
    <script src="komponen/data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="komponen/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
	
	<!-- DatePicker -->
<script type="text/javascript" src="../library/Datepicker/dist/zebra_datepicker.src.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#datepicker1').Zebra_DatePicker({format: 'Y-m-d H:i'});
		$('#datepicker2').Zebra_DatePicker({format: 'Y-m-d H:i'});
		$('#datepicker3').Zebra_DatePicker({format: 'Y-m-d H:i'});
		$('#datepicker4').Zebra_DatePicker({format: 'Y-m-d H:i'});
	});
</script>

<!-- membuat dropdown bertingkat -->
<script>
	var htmlobjek;
	$(document).ready(function(){
	  //apabila terjadi event onchange terhadap object <select id=nama_produk>
	  $("#KodeNegara").change(function(){
		var KodeNegara = $("#KodeNegara").val();
		$.ajax({
			url: "upload/ambil-menu.php",
			data: "KodeRM="+KodeNegara,
			cache: false,
			success: function(msg){
				//jika data sukses diambil dari server kita tampilkan
				//di <select id=kota>
				$("#KodeProvinsi").html(msg);
			}
		});
	  });
	});
</script>
<script>
	/* var htmlobjek;
	$(document).ready(function(){
	  //apabila terjadi event onchange terhadap object <select id=nama_produk>
	  $("#KodeNegara2").change(function(){
		var KodeNegara2 = $("#KodeNegara2").val();
		$.ajax({
			url: "upload/ambil-menu.php",
			data: "KodeRM="+KodeNegara2,
			cache: false,
			success: function(msg){
				//jika data sukses diambil dari server kita tampilkan
				//di <select id=kota>
				$("#KodeProvinsi2").html(msg);
			}
		});
	  });
	}); */
</script>
</body>

</html>
