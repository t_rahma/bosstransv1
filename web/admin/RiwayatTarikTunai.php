<?php
include ('akses.php');
$fitur_id = 7;
include ('login/lock-menu.php');
if($IsServer!='1'){
	echo '<script language="javascript">document.location="index.php"; </script>';
}
//kode jadi untuk server log
/* include ('../library/kode-log-server.php');
include ('../library/kode-mstperson.php'); */
include ('../library/tgl-indo.php');
$DateTime = date('Y/m');

include('../library/config.php');
date_default_timezone_set('Asia/Jakarta');

// if(base64_decode($_GET['aksi'])=='Hapus'){
	// $Hapus = mysqli_query($koneksi,"UPDATE dompetperson SET IsVerified='0',UserVerificator='$login_id' WHERE NoTransaksi='".base64_decode($_GET['id'])."' ");
	// if($Hapus){
		// mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName) 
		// VALUES ('$kode_jadi_log','$DateTime','Update Data','Unverified Tarik Tunai Dompet Person : ".base64_decode($_GET['id'])."','$login_id')");
		// echo '<script language="javascript">document.location="TarikTunai.php"; </script>';
	// }else{
		// echo '<script language="javascript">alert("Unverified Gagal!"); document.location="TarikTunai.php"; </script>';
	// }
// }

// if(base64_decode($_GET['aksi'])=='Verifikasi'){
	// $Hapus = mysqli_query($koneksi,"UPDATE dompetperson SET UserVerificator='$login_id' WHERE NoTransaksi='".base64_decode($_GET['id'])."' ");
	// if($Hapus){
		// mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName) 
		// VALUES ('$kode_jadi_log','$DateTime','Update Data','Verified Tarik Tunai Dompet Person : ".base64_decode($_GET['id'])."','$login_id')");
		// echo '<script language="javascript">document.location="TarikTunai.php"; </script>';
	// }else{
		// echo '<script language="javascript">alert("Unverified Gagal!"); document.location="TarikTunai.php"; </script>';
	// }
// }
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <?php include 'view/title.php' ?>

    <!-- Bootstrap Core CSS -->
    <link href="komponen/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="komponen/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="komponen/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="komponen/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="komponen/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Jika Anda verifikasi tarik tunai, pastikan anda sudah mentransfer sejumlah uang ke rekening klien!")
			if (answer == true){
				window.location = "TarikTunai.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
	<script type="text/javascript">
		function Hapusconfirmation() {
			var answer = confirm("Transaksi tarik tunai akan dihapus! ")
			if (answer == true){
				window.location = "TarikTunai.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
	<!-- Datepcker -->
	<link rel="stylesheet" href="../library/Datepicker/dist/css/default/zebra_datepicker.min.css" type="text/css">
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
		<?php include 'view/menu.php' ; ?>
		</nav>
		
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">Riwayat Transaksi Tarik Tunai <?php if($_REQUEST['keyword']!=null){echo TanggalIndo($_REQUEST['keyword']);}else{echo TanggalIndo($DateTime);} ?></h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
							<form method="post" action="">
								<div class="form-group input-group col-lg-3 col-lg-offset-9">
									<input type="text" name="keyword" class="form-control" id="datepicker" value="<?php echo $_REQUEST['keyword']; ?>" placeholder="Tanggal Transaksi">
									<span class="input-group-btn">
										<button class="btn btn-large btn-info" type="submit">Cari</button>
									 </span>
								</div>
							</form>
								<!-- ===================================== tabel ==================================== -->
							<div class="table-responsive">
								<table width="100%" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>No</th>
											<th>Tanggal Transaksi</th>
											<th>Nama</th>
											<th>No.Rekening</th>
											<th>Nominal</th>
											<th>Verifikator</th>
											<th>Status</th>
										</tr>
									</thead>
									<?php
										include '../library/pagination1.php';
										// mengatur variabel reload dan sql
										$kosong=null;
										if(isset($_REQUEST['keyword']) && $_REQUEST['keyword']<>""){
											// jika ada kata kunci pencarian (artinya form pencarian disubmit dan tidak kosong)pakai ini
											$keyword=$_REQUEST['keyword'];
											$reload = "RiwayatTarikTunai.php?pagination=true&keyword=$keyword";
											$sql =  "SELECT a.*,b.NamaPerson,b.KodeBankPerson,b.NoRekPerson,b.AtasNama FROM dompetperson a JOIN mstperson b ON a.KodePerson=b.KodePerson WHERE date_format(a.TanggalTransaksi,'%Y/%m')='$keyword' AND a.UserVerificator is not null AND a.JenisMutasi='Penjualan' ORDER BY a.TanggalTransaksi ASC";
											$result = mysqli_query($koneksi,$sql);
										}else{
										//jika tidak ada pencarian pakai ini
											$reload = "RiwayatTarikTunai.php?pagination=true";
											$sql =  "SELECT a.*,b.NamaPerson,b.KodeBankPerson,b.NoRekPerson,b.AtasNama FROM dompetperson a JOIN mstperson b ON a.KodePerson=b.KodePerson WHERE date_format(a.TanggalTransaksi,'%Y/%m')='$DateTime' AND a.UserVerificator is not null AND a.JenisMutasi='Penjualan' ORDER BY a.TanggalTransaksi DESC";
											$result = mysqli_query($koneksi,$sql);
										}
										
										//pagination config start
										$rpp = 20; // jumlah record per halaman
										$page = intval($_GET["page"]);
										if($page<=0) $page = 1;  
										$tcount = mysqli_num_rows($result);
										$tpages = ($tcount) ? ceil($tcount/$rpp) : 1; // total pages, last page number
										$count = 0;
										$i = ($page-1)*$rpp;
										$no_urut = ($page-1)*$rpp;
										//pagination config end				
									?>
									<tbody>
										<?php
										while(($count<$rpp) && ($i<$tcount)) {
											mysqli_data_seek($result,$i);
											$data = mysqli_fetch_array($result);
										?>
										<tr class="odd gradeX">
											<td width="50px">
												<?php echo ++$no_urut;?> 
											</td>
											<td width="140px">
												<?php echo "".TanggalIndo($data['TanggalTransaksi'])."<br/>".substr($data['TanggalTransaksi'],10,19).""; ?>
											</td>
											<td>
												<strong><?php echo $data ['NamaPerson']; ?></strong>
											</td>
											<td>
												<?php echo "No.Rek : ".$data['NoRekPerson']."<br/>Bank : ".$data['KodeBankPerson']."<br/>a.n : ".$data['AtasNama'].""; ?>
											</td>
											<td>
												Rp.<?php echo number_format($data ['Kredit']); ?>,-
											</td>
											<td>
												<?php echo $data ['UserVerificator']; ?>
											</td>
											<td width="100px">											
												<?php if($data['IsVerified']=='1'){
													echo '<span class="btn btn-success btn-sm">Verified</span>';
												}else{
													echo '<span class="btn btn-danger btn-sm">Unverified</span>';
												}?>
												<!--<a href="TarikTunai.php?id=<?php echo base64_encode($data['NoTransaksi']);?>&aksi=<?php echo base64_encode('Verifikasi');?>" title='Verifikasi' onclick='return confirmation()'><span class='btn btn-warning btn-sm'>Verifikasi</span></a>
												
												<a href="TarikTunai.php?id=<?php echo base64_encode($data['NoTransaksi']);?>&aksi=<?php echo base64_encode('Hapus');?>" title='Hapus' onclick='return Hapusconfirmation()'><span class='btn btn-danger btn-sm'>Unverified</span></a>-->
											</td>
										</tr>
										<?php
											$i++; 
											$count++;
										}
										?>
									</tbody>
								</table>
								<div><?php echo paginate_one($reload, $page, $tpages); ?></div>
								<!-- ===================================== tabel ==================================== -->
							</div>
							</div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="komponen/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="komponen/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="komponen/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="komponen/vendor/raphael/raphael.min.js"></script>
    <script src="komponen/vendor/morrisjs/morris.min.js"></script>
    <script src="komponen/data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="komponen/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>

<!-- DatePicker -->
<script type="text/javascript" src="../library/Datepicker/dist/zebra_datepicker.src.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#datepicker').Zebra_DatePicker({format: 'Y/m'});
	});
</script>
</body>
</html>
