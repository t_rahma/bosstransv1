<!--
Author : Aguzrybudy
Created : Selasa, 19-April-2016
Title : Crud Menggunakan Modal Bootsrap
-->
<?php
 $jenis	=@$_GET['Jenis'];
?>

<div class="modal-dialog">
    <div class="modal-content">
    	<div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">Input Data Slider Baru</h4>
        </div>
        <div class="modal-body">
			<div class="form-group">
				<label>Nama</label>
				<p><strong><?php echo $jenis;?></strong></p>
			
				<label>Tanggal Transaksi</label>
				<p><?php echo $TglTransaksi;?></p>
				
				<?php
					echo '<label>Debet</label>';
					echo '<p>'.number_format($Debet).'</p>';
				?>
				
				<label>Keterangan</label>
				<p><?php echo $ket;?></p>
				
				<label>Bukti Transfer</label>
				<img src="../../../andro/foto_bukti/<?php echo $Foto; ?>" class="img img-responsive img-thumbnail">
			</div>
			<div class="modal-footer">
				<a href="VerifikasiTopup.php?id=<?php echo base64_encode($NoTransaksi);?>&aksi=<?php echo base64_encode('Verifikasi');?>" title='Verifikasi' onclick='return confirmation()'><span class='btn btn-success btn-md'>Verifikasi</span></a>
				
				<button type="reset" class="btn btn-danger"  data-dismiss="modal" aria-hidden="true">Tutup</button>
			</div>
        </div>
	</div>
</div>