<!--
Author : Aguzrybudy
Created : Selasa, 19-April-2016
Title : Crud Menggunakan Modal Bootsrap
-->
<?php
 $KodeRM	=@$_GET['KodeRM'];
 $KodePerson=@$_GET['KodePerson'];
 $NamaRM	=@$_GET['NamaRM'];
 $ket		=@$_GET['Keterangan'];
 $Kategory	=@$_GET['Kategory'];
 $Foto		=@$_GET['Foto'];
 $JamBuka	=@$_GET['JamBuka'];
 $Telp		=@$_GET['Telp'];
 $Alamat	=@$_GET['Alamat'];
?>

<div class="modal-dialog">
    <div class="modal-content">
    	<div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">Detil Verifikasi <?php echo $NamaRM; ?></h4>
        </div>
        <div class="modal-body">
			<div class="form-group">
				<label>Nama Toko/Rumah Makan</label>
				<p><?php echo $NamaRM;?></p>
			
				<label>Kategory</label>
				<p><?php echo $Kategory;?></p>
				
				<label>Jam Operasional</label>
				<p><?php echo $JamBuka;?></p>
				
				<label>No.Telp</label>
				<p><?php echo $Telp;?></p>
				
				<label>Alamat</label>
				<p><?php echo $Alamat;?></p>
				
				<label>Keterangan</label>
				<p><?php echo $ket;?></p>
				
				<label>Foto Rumah Makan</label>
				<img src="../assets/img/FotoRM/<?php echo $Foto; ?>" class="img img-responsive img-thumbnail">
			</div>
			<div class="modal-footer">
				<a href="VerifikasiToko.php?id=<?php echo base64_encode($KodeRM);?>&prs=<?php echo base64_encode($KodePerson);?>&aksi=<?php echo base64_encode('Verifikasi');?>" title='Verifikasi' onclick='return confirmation()'><span class='btn btn-success btn-md'>Verifikasi</span></a>
				
				<a href="VerifikasiToko.php?id=<?php echo base64_encode($KodeRM);?>&prs=<?php echo base64_encode($KodePerson);?>&aksi=<?php echo base64_encode('Hapus');?>" title='Hapus' onclick='return Hapusconfirmation()'><span class='btn btn-warning btn-md'>Unverified</span></a>
				
				<button type="reset" class="btn btn-danger"  data-dismiss="modal" aria-hidden="true">Tutup</button>
			</div>
        </div>
	</div>
</div>