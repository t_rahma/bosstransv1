<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header" >
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php"><?php if($login_cabang=='C0001'){ echo 'SERVER SIDE'; }else{ echo 'CLIENT SIDE'; }?></a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!--<li class="dropdown">
                    <a class="link" class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>Read All Messages</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                </li>-->
              
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="link" class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="SettingUser.php?id=<?php echo base64_encode($login_id);?>"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="../library/logout-admin.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
							<div class="text-center">
								<img src="../assets/img/logo.png" class="navbar-brand-logo" width="70%" alt="" />
							</div>
							<br/>
                            <div class="text-center" style="color:white;">
								<?php if($login_foto==null){
									echo '<img src="../../andro/foto_profil/no_pict.png" class="img img-responsive img-thumbnail" alt="Image User">';
								}else{
									echo '<img src="../../andro/foto_profil/'.$login_foto.'" class="img img-responsive img-thumbnail" alt="Image User" width="50%">';
								}?>
								<br/>Anda masuk sebagai<br/><strong><?php echo $login_session ?></strong>
							</div>
                        </li>
                        <li>
                            <a href="index.php" class="link"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a class="link" href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Master Data<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
								<li>
                                    <a class="link" href="#">Pengguna <span class="fa arrow"></span></a>
                                    <ul class="nav nav-third-level">
                                        <!--<li>
                                            <a class="link" href="MasterPenjual.php">Data Penjual</a>
                                        </li>-->
                                        <li>
                                            <a class="link" href="MasterDriver.php">Data Driver</a>
                                        </li>
										<li>
                                            <a class="link" href="MasterKlien.php">Data Klien</a>
                                        </li>
                                    </ul>
                                    <!-- /.nav-third-level -->
                                </li>
								<?php if($IsServer=='1'){ ?>
								<!--<li>
                                    <a class="link" href="MasterCabang.php">Cabang</a>
                                </li>-->
								<?php } ?>
								<li>
                                    <a class="link" href="SewaIklan.php">Setting Iklan</a>
                                </li>
								<li>
                                    <a class="link" href="SistemSetting.php">Sistem Setting</a>
                                </li>
								
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
						<li>
                            <a href="KotakMasuk.php" class="link"><i class="fa fa-envelope fa-fw"></i> Kotak Masuk</a>
                        </li>
						<li>
                            <a class="link" href="#"><i class="fa fa-globe fa-fw"></i> Konten Web<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
								<li>
                                    <a class="link" href="MasterBeranda.php">Beranda</a>
                                </li>
                                <li>
                                    <a class="link" href="MasterGaleri.php">Gallery</a>
                                </li>
								<li>
                                    <a class="link" href="MasterLayanan.php">Layanan</a>
                                </li>
								<li>
                                    <a class="link" href="MasterSyarat.php">Syarat & Ketentuan</a>
                                </li>
								<li>
                                    <a class="link" href="MasterFaq.php">Faq</a>
                                </li>
                            </ul>
                        </li>
						<li>
                            <a href="LapLabaPerusahaan.php" class="link"><i class="fa fa-line-chart fa-fw"></i> Pendapatan Perusahaan</a>
                        </li>
						
						<!--<li>
                            <a href="PenggantiKurir.php" class="link"><i class="fa fa-bus"></i> Pengganti Kurir</a>
                        </li>
						<li>
                            <a href="SewaIklan.php" class="link"><i class="fa fa-bullhorn"></i> Space Iklan</a>
                        </li>
						<li>
                            <a href="SewaTempat.php" class="link"><i class="fa fa-shopping-cart"></i> Sewa Tempat</a>
                        </li>-->
						<!--<li>
                            <a href="TrOrderOfline.php" class="link"><i class="fa fa-power-off"></i> Order Offline</a>
                        </li>-->
                        <?php if($IsServer=='1'){ ?>
						<li>
                            <a class="link" href="#"><i class="fa fa-credit-card fa-fw"></i> Transaksi Dompet<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a class="link" href="VerifikasiPencairan.php">Pencairan Dompet</a>
                                </li>
                                <li>
                                    <a class="link" href="VerifikasiTopup.php">Topup Dompet</a>
                                </li>
								<li>
                                    <a class="link" href="LaporanDompet.php">Laporan Transaksi Dompet</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
						<!--<?php }else{ ?>
						<li>
                            <a href="TrMutasiDana.php" class="link"><i class="fa fa-money"></i> Transaksi Mutasi Dana</a>
                        </li>
						<?php } ?>
						<li>
                            <a class="link" href="#"><i class="fa fa-bank fa-fw"></i> Transaksi Kas<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a class="link" href="TrKasMasuk.php">Kas Masuk</a>
                                </li>
                                <li>
                                    <a class="link" href="TrKasKeluar.php">Kas Keluar</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        <!--</li>-->
						<!--<li>
                            <a class="link" href="#"><i class="fa fa-file-text-o fa-fw"></i> Riwayat<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <?php if($IsServer=='1'){ ?>
								<li>
                                    <a class="link" href="RiwayatTarikTunai.php">Riwayat Pencairan Dompet</a>
                                </li>
                                <li>
                                    <a class="link" href="RiwayatDepositKoin.php">Riwayat Topup Dompet</a>
                                </li>
								<?php } ?>
								<li>
                                    <a class="link" href="RiwayatTransaksiOrder.php">Riwayat Transaksi Order</a>
                                </li>
								<!--<li>
                                    <a class="link" href="RiwayatTransaksiMutasiDana.php">Riwayat Transaksi Mutasi Dana</a>
                                </li>-->
                            <!--</ul>-->
                            <!-- /.nav-second-level -->
                        <!--</li>-->
						<!--<li>
                            <a class="link" href="#"><i class="fa fa-list fa-fw"></i> Laporan Keuangan<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a class="link" href="LapLabaPerusahaan.php">Lap. Arus Kas</a>
                                </li>
                                <li>
                                    <a class="link" href="LapBulanan.php">Lap. Bulanan</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        <!--</li>-->
						<!--<li>
                            <a class="link" href="#"><i class="fa fa-user fa-fw"></i> Administrator<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a class="link" href="UserLogin.php">User Login</a>
                                </li>
                                <li>
                                    <a class="link" href="AccessLevel.php">Access Level</a>
                                </li>
								<li>
                                    <a class="link" href="ServerLog.php">Server Log</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level 
                        </li>-->
						<li>
                            <a href="UserLogin.php" class="link"><i class="fa fa-users fa-fw"></i> User Login</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>