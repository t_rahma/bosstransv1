 <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.8
    </div>
    <strong>Garuda Delivery&trade; Copyright &copy; 2018 | Powered by Afindo Informatika</strong>
  </footer>