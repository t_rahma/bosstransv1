<?php
include ('akses.php');
/* 
$fitur_id = 28;
include ('update/lock-menu.php'); */
include('../library/config.php');
date_default_timezone_set('Asia/Jakarta');

$id = base64_decode(@$_GET['id']);
$data = base64_decode(@$_GET['data']);
if($IsServer=='1'){
	$KodeCabang = base64_decode(@$_GET['cab']);
}else{
	$KodeCabang = $login_cabang;
} 

if(base64_decode(@$_GET['aksi'])=='NonAktif'){
	$Hapus = mysqli_query($koneksi,"DELETE FROM fiturlevel WHERE LevelID='$id' AND FiturID='$data' AND KodeCabang='$KodeCabang'");
	if($Hapus){
		 echo '<script> window.location="FiturLevel.php?id='.base64_encode($id).'&cab='.base64_encode($KodeCabang).'"</script>';
	}else{
		 echo '<script>alert("Set Hak Akses Gagal"); window.location="FiturLevel.php?id='.base64_encode($id).'&cab='.base64_encode($KodeCabang).'"</script>';
	}
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Bootstrap Core CSS -->
    <link href="komponen/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="komponen/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="komponen/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="komponen/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="komponen/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="komponen/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<?php include 'view/title.php';?>

	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda Yakin Untuk Menonaktifkan Fitur Menu ini . . . ?")
			if (answer == true){
				window.location = "fitur-akses.php?id=<?php echo base64_encode($id);?>";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
		<?php include 'view/menu.php' ; ?>
		</nav>
		
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Fitur Akses</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
						<!-- Nav tabs 
                            <ul class="nav nav-pills">
                                <li class="active"><a href="#home-pills" data-toggle="tab">Data Karyawan</a>
                                </li>
                                <li><a href="#tambah-user" data-toggle="tab">Tambah Karyawan</a>
                                </li>
                            </ul>-->
						<div class="tab-content">
                            <div class="tab-pane fade in active" id="home-pills">
							<!-- ===================================== tabel ==================================== -->
							<div class="col-lg-6">
							<div class="table-responsive">
                            <table width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Menu Akses</th>
                                        <th>Hak Akses</th>
                                    </tr>
                                </thead>
								<?php
									//procedure paging
									include '../library/pagination1.php';
									// mengatur variabel reload dan sql
									$kosong=null;
									if(isset($_REQUEST['keyword']) && $_REQUEST['keyword']<>""){
										// jika ada kata kunci pencarian (artinya form pencarian disubmit dan tidak kosong)pakai ini
										$keyword=$_REQUEST['keyword'];
										$reload = "mst-user.php?pagination=true&keyword=$keyword";
										$sql =  "SELECT * FROM mst_user WHERE judul LIKE '%$keyword%' ORDER BY id_user DESC";
										$result = mysqli_query($koneksi,$sql);
									}else{
									//jika tidak ada pencarian pakai ini
										$reload = "mst-user.php?pagination=true";
										$sql =  "SELECT * FROM serverfitur ";
										if($IsServer=='0'){
											$sql .= " WHERE FiturID!='24' ";
										}
										$sql .= " ORDER BY FiturID ASC ";
										$result = mysqli_query($koneksi,$sql);
									}
									
									//pagination config start
									$rpp = 100; // jumlah record per halaman
									$page = intval(@$_GET["page"]);
									if($page<=0) $page = 1;  
									$tcount = mysqli_num_rows($result);
									$tpages = ($tcount) ? ceil($tcount/$rpp) : 1; // total pages, last page number
									$count = 0;
									$i = ($page-1)*$rpp;
									$no_urut = ($page-1)*$rpp;
									//pagination config end				
								?>
                                <tbody>
								<form action="upload/insert-fitur-akses.php?id=<?php echo base64_encode($id); ?>&cab=<?php echo base64_encode($KodeCabang); ?>" method="post">
									<?php
									while(($count<$rpp) && ($i<$tcount)) {
										mysqli_data_seek($result,$i);
										$data = mysqli_fetch_array($result);
									?>
                                    <tr class="odd gradeX">
                                        <td width="50px">
											<?php echo ++$no_urut;?> 
										</td>
										<td>
											<strong><?php echo $data ['FiturName']; ?></strong>
										</td>
										<td align="center">
											<?php
												$fitur_id = $data ['FiturID'];
												$cek2 = mysqli_query($koneksi,"select * from fiturlevel where LevelID='$id' AND FiturID='$fitur_id' AND KodeCabang='$KodeCabang'");
												$num2 = mysqli_num_rows($cek2);
												if($num2 == 1 ):
											?>
												<a href="FiturLevel.php?id=<?php echo base64_encode($id);?>&data=<?php echo base64_encode($fitur_id);?>&aksi=<?php echo base64_encode('NonAktif');?>&cab=<?php echo base64_encode($KodeCabang);?>" title='Klik tombol untuk menonaktifkan fitur akses menu ini !' onclick='return confirmation()'><i class='btn btn-success btn-sm'><span class='fa fa-check'></span> Akses Verified</i></a>
											<?php else: ?>
												<input type="checkbox" id="cekbox" name="cekbox[]" value="<?php echo $data['FiturID'] ?>"/>
											<?php endif ?>
										</td>
                                    </tr>
									<?php
										$i++; 
										$count++;
									}
									?>
                                </tbody>
								<input type="button" class="btn btn-md btn-warning" onclick="cek(this.form.cekbox)" value="Select All" />&nbsp;
								<input type="button" class="btn btn-md btn-danger" onclick="uncek(this.form.cekbox)" value="Clear All" />&nbsp;
								<input type="submit" class="btn btn-md btn-success" value="Simpan" name="submit" />
								<br/><br/>
								</form>
                            </table>
							
                            <!-- /.table-responsive -->
							</div>
							<!-- ===================================== tabel ==================================== -->
							</div>
							<div class="tab-pane fade" id="tambah-user">
							<!-- ===================================== input ===================================== -->

							<!-- ===================================== input ===================================== -->
							</div>
							</div>
						</div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="komponen/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="komponen/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="komponen/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="komponen/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="komponen/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="komponen/vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="komponen/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
	<script>
		function cek(cekbox){
			for(i=0; i < cekbox.length; i++){
				cekbox[i].checked = true;
			}
		}
		function uncek(cekbox){
			for(i=0; i < cekbox.length; i++){
				cekbox[i].checked = false;
			}
		}
	</script>

</body>

</html>
