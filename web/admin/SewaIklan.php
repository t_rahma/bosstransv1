<?php
include ('akses.php');
$fitur_id = 13;
// include ('login/lock-menu.php');

//kode jadi untuk server log
// include ('../library/kode-log-server.php');
include ('../library/tgl-indo.php');
$DateTime = date('Y-m-d H:i:s a');

include('../library/config.php');
date_default_timezone_set('Asia/Jakarta');
 
@$id 	= base64_decode($_GET['id']);
@$gambar = base64_decode($_GET['gbr']);
@$metode = $_GET['metode'];

if($metode=='aktifkan'){
	mysqli_query($koneksi,"UPDATE iklan SET IsAktif='1' WHERE KodeIklan='$id' AND KodeCabang='$login_cabang'");
	mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
	VALUES ('$kode_jadi_log','$DateTime','Hapus Data','Sewa Iklan Set Aktif: $id','$login_id','$login_cabang')");
}elseif($metode=='nonaktifkan'){
	mysqli_query($koneksi,"UPDATE iklan SET IsAktif='0' WHERE KodeIklan='$id' AND KodeCabang='$login_cabang'");
	mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
	VALUES ('$kode_jadi_log','$DateTime','Update Data','Sewa Iklan Set Nonaktif: $id','$login_id','$login_cabang')");
}elseif($metode=='hapus'){
	mysqli_query($koneksi,"DELETE FROM iklan WHERE KodeIklan='$id' AND KodeCabang='$login_cabang'");
	unlink ("../assets/img/Iklan/$gambar");
	mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
	VALUES ('$kode_jadi_log','$DateTime','Hapus Data','SewaIklan : $id','$login_id','$login_cabang')");
}

if(base64_decode(@$_GET['jenis'])=='Slider' && base64_decode(@$_GET['aksi'])=='Hapus'){
	$id = base64_decode(@$_GET['id']);
	$slider = mysqli_query($koneksi, "update mstslider set IsAktif=0 where ID='$id'");
	//tambahkan log server
 }elseif(base64_decode(@$_GET['jenis'])=='Slider' && base64_decode(@$_GET['aksi'])=='Aktif'){
	$id = base64_decode(@$_GET['id']);
	$slider = mysqli_query($koneksi, "update mstslider set IsAktif=1 where ID='$id'");
	//tambahkan log server
 }

 
	       
 //===========procedure Update data=============//
 if($metode=='Edit'){
	$edit = mysqli_query($koneksi,"select * from iklan where KodeIklan = '$id' AND KodeCabang='$login_cabang'");
	 while($row = mysqli_fetch_array($edit)){
		$nama_Iklan		 	= $row['NamaIklan'];
		$tgl_mulaiIklan		= $row['TglMulaiIklan'];
		$tgl_selesaiIklan	= $row['TglSelesaiIklan'];
		$biaya_sewa			= $row['HargaSewa'];
		$foto_iklan			= $row['Foto'];
		$kode_menu			= $row['KodeMenu'];
		$kode_rm			= $row['KodeRM'];
		$kode_person		= $row['KodePerson'];
		$tampil_semua		= $row['IsTampilSemua'];
	 } 
 }
 
 if(isset($_POST['EditData'])){
	$BiayaSewa = @$_POST['BiayaSewa'];
	//-------------- Ambil prosentase perusahaan cabang usaha ----------------//
	if($login_cabang!='C0001'){
		$PersenCab=mysqli_query($koneksi,"SELECT ValueData FROM systemsetting WHERE KodeSetting='SET-00019' AND KodeCabang='C0001'");
		$DataPersenCab = mysqli_fetch_assoc($PersenCab);
		//porsi pusat
		$PorsiPusat = ($DataPersenCab['ValueData']/100)*$BiayaSewa;
		$PorsiCabang = $BiayaSewa-$PorsiPusat;
	}else{
		$PorsiPusat = 0;
		$PorsiCabang = $BiayaSewa;
	}
	
	$query = mysqli_query($koneksi,"UPDATE iklan SET NamaIklan ='".$_POST['NamaIklan']."',TglMulaiIklan='".$_POST['TglMulaiIklan']."',TglSelesaiIklan='".$_POST['TglSelesaiIklan']."',HargaSewa='$PorsiCabang',PorsiPusat='$PorsiPusat',KodeMenu='".$_POST['KodeMenu']."',KodePerson='".substr($_POST['KodeRM'],9,30)."',KodeRM='".substr($_POST['KodeRM'],0,8)."' WHERE KodeIklan = '$id'");
		if($query){
			mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
			VALUES ('$kode_jadi_log','$DateTime','Edit Data','Sewa Iklan : $id','$login_id','$login_cabang')");
			echo '<script language="javascript">alert("Edit Data berhasil disimpan !"); document.location="SewaIklan.php"; </script>';
		}else{
			echo '<script language="javascript">alert("Edit data gagal !"); document.location="SewaIklan.php?id='.base64_encode($id).'"; </script>';
		}
 }
 
//ambil kode cabang 
$QueryCab = mysqli_query($koneksi,"SELECT * FROM mstcabang WHERE KodeCabang='$login_cabang'");
$RowCab = mysqli_fetch_assoc($QueryCab);
$Prov = $RowCab['KodeProvinsi'];
$Kab = $RowCab['KodeKab'];
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <?php include 'view/title.php' ?>

    <!-- Bootstrap Core CSS -->
    <link href="komponen/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="komponen/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="komponen/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="komponen/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="komponen/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<!-- Datepcker -->
	<link rel="stylesheet" href="../library/Datepicker/dist/css/default/zebra_datepicker.min.css" type="text/css">

	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda Yakin Untuk Menghapus Iklan ini? Iklan yang dihapus tidak dapat dikembalikan lagi!")
			if (answer == true){
				window.location = "AccessLevel.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
	
	<script type="text/javascript">
		function confirmation2() {
			var answer = confirm("Apakah Anda Yakin Mengaktifkan Fitur ini . . . ?")
			if (answer == true){
				window.location = "AccessLevel.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
	
	<script type="text/javascript">
		function confirmation3() {
			var answer = confirm("Apakah Anda Yakin Non Aktifkan Fitur ini . . . ?")
			if (answer == true){
				window.location = "AccessLevel.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
		<?php include 'view/menu.php' ; ?>
		</nav>
		
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Data Iklan</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
						<!-- Nav tabs -->
                            <ul class="nav nav-pills">
                                <li <?php if(@$metode!='Edit'){ echo 'class="active"';}?>><a href="#home-pills" data-toggle="tab">Data Iklan</a></li>
                                <li><a href="#tambah-user" data-toggle="tab">Tambah Iklan</a></li>
								<?php if(@$metode=='Edit'){echo '<li class="active"><a href="#edit-iklan" data-toggle="tab">Edit Iklan</a></li>';}?>
                            </ul>
						<div class="tab-content">
                            <div class="tab-pane fade <?php if(@$metode!='Edit'){ echo 'in active';}?>" id="home-pills"><br/>
							<!-- ===================================== tabel ==================================== -->
							<div class="table-responsive">
							<table width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Gambar</th>
                                        <th>Judul</th>
                                        <th>Deskripsi</th>
                                        <th>Viewer</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
								<?php
									include '../library/pagination1.php';
									// mengatur variabel reload dan sql
									$kosong=null;
									//jika tidak ada pencarian pakai ini
										$reload = "SewaIklan.php?pagination=true";
										$sql =  "SELECT * FROM mstslider ORDER BY Judul DESC";
										$result = mysqli_query($koneksi,$sql);
									
									
									//pagination config start
									$rpp = 30; // jumlah record per halaman
									$page = intval(@$_GET["page"]);
									if($page<=0) $page = 1;  
									$tcount = mysqli_num_rows($result);
									$tpages = ($tcount) ? ceil($tcount/$rpp) : 1; // total pages, last page number
									$count = 0;
									$i = ($page-1)*$rpp;
									$no_urut = ($page-1)*$rpp;
									//pagination config end				
								?>
                                <tbody>
									<?php
									while(($count<$rpp) && ($i<$tcount)) {
										mysqli_data_seek($result,$i);
										$data = mysqli_fetch_array($result);
									?>
                                    <tr class="odd gradeX">
                                        <td width="50px">
											<?php echo ++$no_urut;?> 
										</td>
										<td width = "150px">
										<img src="../../andro/foto_slider/<?php echo $data ['Gambar']; ?>" class="img img-responsive img-thumbnail" width="100px">
										</td>
										<td>
											<strong><?php echo $data ['Judul']; ?></strong>
										</td>
										<td>
											<strong><?php echo $data ['Isi']; ?></strong>
										</td>
										
										<td>
											<?php
											if($data['IsDriver'] == 1 AND $data['IsKlien'] == 1){
												echo '<font color="red">Driver & Klien</font>';
											}elseif($data['IsDriver'] == 1 AND $data['IsKlien'] == 0){
												echo '<font color="orange">Driver</font>';
											}elseif($data['IsDriver'] == 0 AND $data['IsKlien'] == 1){
												echo '<font color="green">Klien</font>';
											}
											?>
										</td>
										<td>
											<?php
											if($data['IsAktif'] == 1){
												if($IsServer=='1'){ ?>
												<a href="SewaIklan.php?id=<?php echo base64_encode($data['ID']);?>&aksi=<?php echo base64_encode('Hapus');?>&jenis=<?php echo base64_encode('Slider');?>" title='NonAktifkan' onclick='return confirmation3()'><span class='btn btn-success btn-sm'>Aktif</span></a>
											<?php }else{
												echo '<i class="btn btn-danger btn-sm disabled"><span class="btn btn-warning btn-sm">Aktif</span></i>';
											}
											}else{
												if($IsServer=='1'){ ?>
												<a href="SewaIklan.php?id=<?php echo base64_encode($data['ID']);?>&aksi=<?php echo base64_encode('Aktif');?>&jenis=<?php echo base64_encode('Slider');?>" title='Aktifkan' onclick='return confirmation2()'><span class='btn btn-warning btn-sm'>NonAktif</span></a>
											<?php }else{
												echo '<i class="btn btn-danger btn-sm disabled"><span class="btn btn-warning btn-sm">NonAktif</span></i>';
											}
											}
											?>
										</td>
                                        										
										<!--<td width="100px">											
											<a href="SewaIklan.php?id=<?php //echo base64_encode($data['KodeIklan']);?>&metode=Edit" title='Edit Iklan'><i class='btn btn-warning btn-sm'><span class='fa fa-pencil'></span></i></a>
											
											<a href="SewaIklan.php?id=<?php //echo base64_encode($data['KodeIklan']); ?>&metode=hapus&gbr=<?php //echo base64_encode($data['Foto']); ?>" title='Hapus' onclick='return confirmation()'><i class='btn btn-danger btn-sm'><span class='fa fa-trash'></span></i></a>
										</td>-->
                                    </tr>
									<?php
										$i++; 
										$count++;
									}
									?>
                                </tbody>
                            </table>
							<div><?php echo paginate_one($reload, $page, $tpages); ?></div>
                            <!-- /.table-responsive -->
							</div>
							<!-- ===================================== tabel ==================================== -->
							</div>
							<div class="tab-pane fade" id="tambah-user">
							<!-- ===================================== input ===================================== -->
							<div class="panel-body">
                            <div class="row">
								<div class="col-lg-6">
									<br/>
									<form id="form1" method="post" enctype="multipart/form-data">
										<div class="form-group col-lg-12">
											<label>Judul Iklan</label>
											<input type="text" class="form-control" name="judul" required>
											<!--<input type="hidden" class="form-control" name="UserName" value="<?php //echo $login_id; ?>">
											<input type="hidden" class="form-control" name="Cabang" value="<?php //echo $login_cabang; ?>">-->
										</div>
										<div class="form-group col-lg-12">
											<label>Deskripsi Iklan</label>
											<input type="text" min="0" class="form-control" name="deskripsi" required>
										</div>
										<div><label>Untuk</label></div>
										<div class="form-group col-lg-6">
											<label><input type="checkbox" name="Driver" value="1"> Driver</label>
										</div>
										<div class="form-group col-lg-6">
											<label><input type="checkbox" name="Klien" value="1" checked> Klien</label>
										</div>
										<!--<div class="form-group col-lg-6">
											<label>Tanggal Selesai Iklan</label>
											<input type="text" class="form-control" id="datepicker2" name="TglSelesaiIklan" required>
										</div>
										<div class="form-group col-lg-12">
											<label>Link Iklan</label>
											<select id="KodeNegara" name="KodeRM" class="form-control">	
												<?php
													// echo "<option>-- Nama Rumah Makan (Alamat) --</option>";
													// $menu = mysqli_query($koneksi,"SELECT KodeRM,KodePerson,NamaRM,AlamatRM FROM rm_penjual WHERE KodeProvinsi='$Prov' AND KodeKab='$Kab' ORDER BY NamaRM ASC");
													// while($kode = mysqli_fetch_array($menu)){
														// echo "<option value=\"".$kode['KodeRM']."-".$kode['KodePerson']."\" >".$kode['NamaRM']." (".$kode['AlamatRM'].")</option>\n";
													// }
												?>
											</select>
											<select id="KodeProvinsi" class="form-control" name="KodeMenu"></select>
											<br/>
											<label class="checkbox-inline">
												<input type="checkbox" name="IsTampilSemua" value="1"> Tampil Semua
											</label>
											<p><font color="red">* Tampil Semua (check), menampilkan iklan saat pengguna belum login & sesudah login dalam aplikasi Garuda Delivery.</font></p>
											<p><font color="red">* Tampil Semua (uncheck), iklan hanya tampil saat pengguna sudah login dalam aplikasi Garuda Delivery.</font></p>
										</div>
										
										<div class="form-group col-lg-12">
										<div class="form-group text-center">
											<label>Gunakan Template Iklan Agar Gambar Proposional <a href="../assets/img/slides/TemplatePromo.png" target="_BLANK"><span class="btn btn-xs btn-success">Download</span></a></label>
										</div>-->
										<font size="2">Upload gambar Dengan Extension .jpeg/.jpg/.png (Max 2 Mb)</font><br/>
											<div class="input-group">
												<label class="input-group-btn">
													<span class="btn btn-danger btn-md">
														Browse&hellip; <input type="file" id="media" name="media" style="display: none;" required>
													</span>
												</label>
												<input type="text" class="form-control input-md" size="40" readonly required>
											</div>
										</div>
										<div class="col-lg-12">
											<br>
											<input type="submit" class="btn btn-md btn-primary" value="Kirim" name="Tambah">
										</div>
									</form>
									<br/>
									<div class="form-group col-lg-12">
										<div class="progress" style="display:none">
											<div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
												<span class="sr-only">0%</span>
											</div>
										</div>
										<div class="msg alert alert-info text-left" style="display:none"></div>
										<div class="clearfix"></div>
									</div>
								</div>
                            </div>
                            <!-- /.row (nested) -->
							</div>
							<!-- ===================================== input ===================================== -->
							</div>
							
							<div class="tab-pane fade <?php if(@$metode=='Edit'){ echo 'in active';}?>" id="edit-iklan">
							<!-- ===================================== Edit ===================================== -->
							<div class="panel-body">
                            <div class="row">
								<div class="col-lg-6">
									<br/>
									<form method="post" action="">
										<div class="form-group col-lg-12">
											<label>Nama Iklan</label>
											<input type="text" class="form-control" name="NamaIklan" value="<?php echo $nama_Iklan; ?>" required>
											<input type="hidden" class="form-control" name="UserName" value="<?php echo $login_id; ?>">
											<input type="hidden" class="form-control" name="Cabang" value="<?php echo $login_cabang; ?>">
										</div>
										<div class="form-group col-lg-12">
											<label>Biaya Sewa Iklan</label>
											<input type="number" min="0" class="form-control" name="BiayaSewa" value="<?php echo $biaya_sewa; ?>" required>
										</div>
										<div class="form-group col-lg-6">
											<label>Tanggal Mulai Iklan</label>
											<input type="text" class="form-control" id="datepicker3" value="<?php echo $tgl_mulaiIklan; ?>" name="TglMulaiIklan" required>
										</div>
										<div class="form-group col-lg-6">
											<label>Tanggal Selesai Iklan</label>
											<input type="text" class="form-control" id="datepicker4" value="<?php echo $tgl_selesaiIklan; ?>" name="TglSelesaiIklan" required>
										</div>
										<div class="form-group col-lg-12">
											<label>Link Iklan</label>
											<select id="KodeNegara2" name="KodeRM" class="form-control">	
												<?php
													echo "<option value=''>-- Nama Rumah Makan (Alamat) --</option>";
													$menu = mysqli_query($koneksi,"SELECT KodeRM,KodePerson,NamaRM,AlamatRM FROM rm_penjual WHERE KodeProvinsi='$Prov' AND KodeKab='$Kab' ORDER BY NamaRM ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['KodeRM']==$kode_rm AND $kode['KodePerson']==$kode_person){
															echo "<option value=\"".$kode['KodeRM']."-".$kode['KodePerson']."\" selected>".$kode['NamaRM']." (".$kode['AlamatRM'].")</option>\n";
														}else{
															echo "<option value=\"".$kode['KodeRM']."-".$kode['KodePerson']."\" >".$kode['NamaRM']." (".$kode['AlamatRM'].")</option>\n";
														}
													}
												?>
											</select>
											<select id="KodeProvinsi2" class="form-control" name="KodeMenu">
												<option value=''>-- Nama Menu (Jenis Menu) --</option>
												<?php
													$menu = mysqli_query($koneksi,"SELECT KodeMenu,NamaMenu,JenisMenu FROM menu_rm WHERE KodeRM ='$kode_rm' AND KodePerson='$kode_person' ORDER BY NamaMenu ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['KodeMenu']==$kode_menu){
															 echo "<option value=\"".$kode['KodeMenu']."\" selected>".$kode['NamaMenu']." (".$kode['JenisMenu'].")</option>\n";
														}else{
															 echo "<option value=\"".$kode['KodeMenu']."\">".$kode['NamaMenu']." (".$kode['JenisMenu'].")</option>\n";
														}
													}
												?>
											</select>
											<br/>
											<label class="checkbox-inline">
												<input type="checkbox" name="IsTampilSemua" value="1" <?php if($tampil_semua=='1'){echo 'checked';}?>> Tampil Semua
											</label>
											<p><font color="red">* Tampil Semua (check), menampilkan iklan saat pengguna belum login & sesudah login dalam aplikasi Garuda Delivery.</font></p>
											<p><font color="red">* Tampil Semua (uncheck), iklan hanya tampil saat pengguna sudah login dalam aplikasi Garuda Delivery.</font></p>
										</div>
										<div class="col-lg-12">
										<button type="submit" class="btn btn-md btn-block btn-success" name="EditData">Simpan Edit</button>
										</div>
									</form>
									<h3>&nbsp;</h3>
									<form id="form2" method="post" action="">
										<div class="form-group col-lg-12">
										<img src="../assets/img/Iklan/<?php echo $foto_iklan; ?>" class="img img-thumbnail"> 
										<div class="form-group text-center">
											<label>Gunakan Template Iklan Agar Gambar Proposional <a href="../assets/img/slides/TemplatePromo.png" target="_BLANK"><span class="btn btn-xs btn-success">Download</span></a></label>
										</div>
										<font size="2">Upload gambar Dengan Extension .jpeg/.jpg/.png (Max 2 Mb)</font><br/>
											<div class="input-group">
												<label class="input-group-btn">
													<span class="btn btn-danger btn-md">
														Browse&hellip; <input type="file" id="media" name="media" style="display: none;" required>
													</span>
												</label>
												<input type="text" class="form-control input-md" size="40" readonly required>
											</div>
										</div>
										<div class="col-lg-12">
											<input type="hidden" name="Status" value="EditData">
											<input type="hidden" name="FotoIklan" value="<?php echo $foto_iklan;?>">
											<input type="hidden" name="KodeIklan" value="<?php echo $id;?>">
											<input type="hidden" name="UserName" value="<?php echo $login_id; ?>">
											<input type="hidden" name="Cabang" value="<?php echo $login_cabang; ?>">
											<input type="submit" class="btn btn-md btn-block btn-primary" value="Upload Gambar">
										</div>
									</form>
									<br/>
									<div class="form-group col-lg-12">
										<div class="progress2" style="display:none">
											<div id="progressBar2" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
												<span class="sr-only">0%</span>
											</div>
										</div>
										<div class="msg2 alert alert-info text-left" style="display:none"></div>
										<div class="clearfix"></div>
									</div>
								</div>
                            </div>
                            <!-- /.row (nested) -->
							</div>
							<!-- ===================================== Edit ===================================== -->
							</div>
						</div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="komponen/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="komponen/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="komponen/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="komponen/vendor/raphael/raphael.min.js"></script>
    <script src="komponen/vendor/morrisjs/morris.min.js"></script>
    <script src="komponen/data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="komponen/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
	 <!-- =============================================Progres Bar=================================-->
	<script type="text/javascript">
	$(document).ready(function() {
		$('#form1').on('submit', function(event){
			event.preventDefault();
			var formData = new FormData($('#form1')[0]);

			$('.msg').hide();
			$('.progress').show();
			
			$.ajax({
				xhr : function() {
					var xhr = new window.XMLHttpRequest();
					xhr.upload.addEventListener('progress', function(e){
						if(e.lengthComputable){
							console.log('Bytes Loaded : ' + e.loaded);
							console.log('Total Size : ' + e.total);
							console.log('Persen : ' + (e.loaded / e.total));
							
							var percent = Math.round((e.loaded / e.total) * 100);
							
							$('#progressBar').attr('aria-valuenow', percent).css('width', percent + '%').text(percent + '%');
						}
					});
					return xhr;
				},
				
				type : 'POST',
				url : 'upload/UploadIklan.php',
				data : formData,
				processData : false,
				contentType : false,
				success : function(response){
					$('#form1')[0].reset();
					$('.progress').hide();
					$('.msg').show();
					if(response == ""){
						alert('File gagal di upload');
					}else{
						var msg = 'Status : ' + response;
						$('.msg').html(msg);
						document.location="SewaIklan.php";
					}
				}
			});
		});
		
		$('#form2').on('submit', function(event){
			event.preventDefault();
			var formData = new FormData($('#form2')[0]);

			$('.msg2').hide();
			$('.progress2').show();
			
			$.ajax({
				xhr : function() {
					var xhr = new window.XMLHttpRequest();
					xhr.upload.addEventListener('progress2', function(e){
						if(e.lengthComputable){
							console.log('Bytes Loaded : ' + e.loaded);
							console.log('Total Size : ' + e.total);
							console.log('Persen : ' + (e.loaded / e.total));
							
							var percent = Math.round((e.loaded / e.total) * 100);
							
							$('#progressBar2').attr('aria-valuenow', percent).css('width', percent + '%').text(percent + '%');
						}
					});
					return xhr;
				},
				
				type : 'POST',
				url : 'upload/UploadIklan.php',
				data : formData,
				processData : false,
				contentType : false,
				success : function(response){
					$('#form2')[0].reset();
					$('.progress2').hide();
					$('.msg2').show();
					if(response == ""){
						alert('File gagal di upload');
					}else{
						var msg = 'Status : ' + response;
						$('.msg2').html(msg);
						//document.location="SewaIklan.php";
					}
				}
			});
		});
	});
	</script>
	
	<script>
	$(function() {
	  $(document).on('change', ':file', function() {
		var input = $(this),
			numFiles = input.get(0).files ? input.get(0).files.length : 1,
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [numFiles, label]);
	  });

	  $(document).ready( function() {
		  $(':file').on('fileselect', function(event, numFiles, label) {

			  var input = $(this).parents('.input-group').find(':text'),
				  log = numFiles > 1 ? numFiles + ' files selected' : label;

			  if( input.length ) {
				  input.val(log);
			  } else {
				  if( log ) alert(log);
			  }

		  });
	  });
	  
	});
	</script>
    <!-- =============================================end Progres Bar=================================-->
	
	<!-- DatePicker -->
<script type="text/javascript" src="../library/Datepicker/dist/zebra_datepicker.src.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#datepicker1').Zebra_DatePicker({format: 'Y-m-d H:i'});
		$('#datepicker2').Zebra_DatePicker({format: 'Y-m-d H:i'});
		$('#datepicker3').Zebra_DatePicker({format: 'Y-m-d H:i'});
		$('#datepicker4').Zebra_DatePicker({format: 'Y-m-d H:i'});
	});
</script>

<!-- membuat dropdown bertingkat -->
<script>
	var htmlobjek;
	$(document).ready(function(){
	  //apabila terjadi event onchange terhadap object <select id=nama_produk>
	  $("#KodeNegara").change(function(){
		var KodeNegara = $("#KodeNegara").val();
		$.ajax({
			url: "upload/ambil-menu.php",
			data: "KodeRM="+KodeNegara,
			cache: false,
			success: function(msg){
				//jika data sukses diambil dari server kita tampilkan
				//di <select id=kota>
				$("#KodeProvinsi").html(msg);
			}
		});
	  });
	});
</script>
<script>
	var htmlobjek;
	$(document).ready(function(){
	  //apabila terjadi event onchange terhadap object <select id=nama_produk>
	  $("#KodeNegara2").change(function(){
		var KodeNegara2 = $("#KodeNegara2").val();
		$.ajax({
			url: "upload/ambil-menu.php",
			data: "KodeRM="+KodeNegara2,
			cache: false,
			success: function(msg){
				//jika data sukses diambil dari server kita tampilkan
				//di <select id=kota>
				$("#KodeProvinsi2").html(msg);
			}
		});
	  });
	});
</script>
<!--<script>
function dcheck() {
    document.getElementById("dvc").checked = true;
	document.getElementById("kc").checked = false;
}
function duncheck() {
    document.getElementById("dvc").checked = false;
	document.getElementById("kc").checked = true;
}
function kcheck() {
    document.getElementById("kc").checked = true;
	document.getElementById("dvc").checked = false;
}
function kuncheck() {
    document.getElementById("kc").checked = false;
	document.getElementById("dvc").checked = true;
}
</script>-->
</body>

</html>
