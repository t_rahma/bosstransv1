<?php
include ('akses.php');
$fitur_id = 18;
include ('login/lock-menu.php');

//kode jadi untuk server log
include ('../library/kode-log-server.php');
include ('../library/tgl-indo.php');
$DateTime = date('Y-m-d H:i:s');

include('../library/config.php');
date_default_timezone_set('Asia/Jakarta');
$id = base64_decode(@$_GET['id']);

if(isset($_POST['Simpan'])){
	$GantiKurir = mysqli_query($koneksi,"UPDATE trordermakanan SET KodePersonKurir='".$_POST['KodePersonKurir']."',KetGantiKurir='".$_POST['Keterangan']."' WHERE NoTrOrder='".$_POST['NoTrOrder']."' AND KodeCabang='$login_cabang'");
	if($GantiKurir){
		mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
		VALUES ('$kode_jadi_log','$DateTime','Update Data','Transaksi Penggantian Kurir : ".$_POST['KurirLama']." ke ".$_POST['KodePersonKurir']." No.Ref ".$_POST['NoTrOrder']."','$login_id','$login_cabang')");
		echo '<script language="javascript">alert("Penggantian Kurir Berhasil!");document.location="PenggantiKurir.php"; </script>';
	}else{
		echo '<script language="javascript"> document.location="PenggantiKurir.php"; </script>';
	}
}

/* if(base64_decode($_GET['aksi'])=='Verifikasi'){
	$Hapus = mysqli_query($koneksi,"UPDATE mstperson SET IsAktif='1' WHERE KodePerson='".base64_decode($_GET['id'])."' ");
	if($Hapus){
		mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName) 
		VALUES ('$kode_jadi_log','$DateTime','Update Data','Master Person : Verifikasi Data Penjual ".base64_decode($_GET['id'])."','$login_id')");
		echo '<script language="javascript">document.location="VerifikasiMasterPenjual.php"; </script>';
	}else{
		echo '<script language="javascript">alert("Hapus Data Gagal!"); document.location="VerifikasiMasterPenjual.php"; </script>';
	}
} */

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <?php include 'view/title.php' ?>

    <!-- Bootstrap Core CSS -->
    <link href="komponen/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="komponen/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="komponen/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="komponen/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="komponen/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda Yakin Untuk Mengganti Kurir Ini ?")
			if (answer == true){
				window.location = "MasterKurir.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
	
	<!-- Datepcker -->
	<link rel="stylesheet" href="../library/Datepicker/dist/css/default/zebra_datepicker.min.css" type="text/css">
	
	<!-- Select 2 Skin -->
	<link rel="stylesheet" href="../library/select2-master/dist/css/select2.css"/>
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
		<?php include 'view/menu.php' ; ?>
		</nav>
		
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Transaksi Penggantian Kurir</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
						<!-- Nav tabs -->
						<ul class="nav nav-pills">
							<li <?php if(@$id==null){echo 'class="active"';} ?>><a href="#home-pills" data-toggle="tab">Data Order</a></li>
							<?php if(@$id!=null){
								echo '<li class="active"><a href="#tambah-user" data-toggle="tab">Ganti Kurir</a></li>';
							} ?>
							
						</ul>
						<div class="tab-content">
                            <div class="tab-pane fade <?php if(@$id==null){echo 'in active';} ?>" id="home-pills">
							<form method="post" action="">
								<div class="form-group input-group col-lg-4 col-lg-offset-8">
									<input type="text" name="keyword" class="form-control" value="<?php echo @$_REQUEST['keyword']; ?>" placeholder="Cari Nama Kurir">
									<span class="input-group-btn">
										<button class="btn btn-large btn-info" type="submit">Cari</button>
									 </span>
								</div>
							</form>
							<!-- ===================================== tabel ==================================== -->
							<div class="table-responsive">
							
							<table width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Kurir</th>
                                        <th>Nama Pembeli</th>
                                        <th>Tanggal Kirim</th>
                                        <th>Pembayaran</th>
                                        <th>Jenis Order</th>
                                        <th>Status</th>
										<th>Aksi</th>
                                    </tr>
                                </thead>
								<?php
									include '../library/pagination1.php';
									// mengatur variabel reload dan sql
									$kosong=null;
									if(isset($_REQUEST['keyword']) && $_REQUEST['keyword']<>""){
										// jika ada kata kunci pencarian (artinya form pencarian disubmit dan tidak kosong)pakai ini
										$keyword=$_REQUEST['keyword'];
										$reload = "PenggantiKurir.php?pagination=true&keyword=$keyword";
										$sql =  "SELECT a.NoTrOrder,a.TglKirim,a.StatusOrder,a.JenisPembayaran,a.KodePersonKurir,b.NamaPerson as Kurir,b.NoHP as HpKurir,c.NamaPerson as Pembeli,c.NoHP as HpPembeli FROM trordermakanan a JOIN mstperson b ON a.KodePersonKurir=b.KodePerson JOIN mstperson c ON a.KodePersonPembeli=c.KodePerson WHERE a.KodeCabang='$login_cabang' AND StatusOrder='ACC' AND b.NamaPerson LIKE '%$keyword%' ORDER BY a.TglKirim ASC";
										
										$result = mysqli_query($koneksi,$sql);
									}else{
									//jika tidak ada pencarian pakai ini
										$reload = "PenggantiKurir.php?pagination=true";
										$sql =  "SELECT a.IsTrCariKurir,a.NoTrOrder,a.TglKirim,a.StatusOrder,a.JenisPembayaran, a.KodePersonKurir, b.NamaPerson as Kurir,b.NoHP as HpKurir,c.NamaPerson as Pembeli,c.NoHP as HpPembeli FROM trordermakanan a JOIN mstperson b ON a.KodePersonKurir=b.KodePerson JOIN mstperson c ON a.KodePersonPembeli=c.KodePerson WHERE a.KodeCabang='$login_cabang' AND StatusOrder='ACC' ORDER BY a.TglKirim ASC";
										$result = mysqli_query($koneksi,$sql);
									}
									
									//pagination config start
									$rpp = 25; // jumlah record per halaman
									$page = intval(@$_GET["page"]);
									if($page<=0) $page = 1;  
									$tcount = mysqli_num_rows($result);
									$tpages = ($tcount) ? ceil($tcount/$rpp) : 1; // total pages, last page number
									$count = 0;
									$i = ($page-1)*$rpp;
									$no_urut = ($page-1)*$rpp;
									//pagination config end				
								?>
                                <tbody>
									<?php
									while(($count<$rpp) && ($i<$tcount)) {
										mysqli_data_seek($result,$i);
										$data = mysqli_fetch_array($result);
									?>
                                    <tr class="odd gradeX">
                                        <td width="50px">
											<?php echo ++$no_urut;?> 
										</td>
										<td>
											<strong><?php echo $data ['Kurir']; ?></strong><br/>
											Hp.<?php echo $data['HpKurir'];?>
										</td>
                                        <td>
											<?php echo $data ['Pembeli']; ?><br/>
											Hp.<?php echo $data['HpPembeli'];?>
										</td>
										<td>
											<?php echo TanggalIndo($data ['TglKirim']); echo '<br/>';echo substr($data['TglKirim'],10,19);?>
										</td>
										<td>
											<?php echo $data ['JenisPembayaran']; ?>
										</td>
										<td>
											<?php
											if($data['IsTrCariKurir']=='1'){
												echo 'Pengiriman Barang';
											}else{
												echo 'Order & Kirim Makanan';
											}
											?>
											
										</td>
										<td>
											<?php echo $data['StatusOrder']; ?>
										</td>
										<td>										
											<a href="PenggantiKurir.php?id=<?php echo base64_encode($data['NoTrOrder']);?>&kr=<?php echo base64_encode($data['KodePersonKurir']);?>&aksi=<?php echo base64_encode('Ganti');?>" title='Klik untuk mengganti kurir' onclick='return confirmation()'><span class='btn btn-warning btn-sm'>Ganti Kurir</span></a>
										</td>
                                    </tr>
									<?php
										$i++; 
										$count++;
									}
									?>
                                </tbody>
                            </table>
							<div><?php echo paginate_one($reload, $page, $tpages); ?></div>
							</div>
							<!-- ===================================== tabel ==================================== -->
							</div>
							<div class="tab-pane fade <?php if(@$id!=null){echo 'in active';} ?>" id="tambah-user">
							<!-- ===================================== input ===================================== -->
								<div class="panel-body">
									<div class="row">
										<h2>Penggantian Kurir</h2>
										<div class="col-lg-6">
											<form method="post" action="">
												<div class="form-group">
													<input name="NoTrOrder" type="hidden" value="<?php echo $id; ?>">
													<input name="KurirLama" type="hidden" value="<?php echo base64_decode($_GET['kr']); ?>">
													<select id="kota" name="KodePersonKurir" class="form-control" style="width: 100% !important;" required>
													<option value=""></option>
													<?php
														$menu = mysqli_query($koneksi,"SELECT KodePerson,NamaPerson FROM mstperson WHERE IsKurir='1' AND IsAktif='1' AND KodeCabang='$login_cabang' ORDER BY NamaPerson ASC");
														while($kode = mysqli_fetch_array($menu)){
															echo "<option value=\"".$kode['KodePerson']."\" >".$kode['NamaPerson']."</option>\n";
														}
													?>
													</select>
												</div>
												<div class="form-group">
													<textarea class="form-control" placeholder="Keterangan Penggantian Kurir" name="Keterangan" required></textarea>
												</div>
												<button type="submit" class="btn btn-info btn-md" name="Simpan">Simpan Penggantian Kurir</button>
											</form>
										</div>
									</div>
								</div>
							<!-- ===================================== input ===================================== -->
							</div>
						</div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="komponen/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="komponen/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="komponen/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="komponen/vendor/raphael/raphael.min.js"></script>
    <script src="komponen/vendor/morrisjs/morris.min.js"></script>
    <script src="komponen/data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="komponen/dist/js/sb-admin-2.js"></script>
	<!-- Select 2 -->
	<script src="../library/select2-master/dist/js/select2.min.js"></script>
	<script>
	$(document).ready(function () {
		$("#kota").select2({
		placeholder: "Cari Nama Kurir Pengganti"
		});
	});
	</script>
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>

<!-- DatePicker -->
<script type="text/javascript" src="../library/Datepicker/dist/zebra_datepicker.src.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#datepicker').Zebra_DatePicker();
	});
</script>
</body>
</html>
