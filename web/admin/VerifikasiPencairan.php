<?php
include ('akses.php');
$fitur_id = 5;
// include ('login/lock-menu.php');
if($IsServer!='1'){
	echo '<script language="javascript">document.location="index.php"; </script>';
}
//kode jadi untuk server log
// include ('../library/kode-log-server.php');
include ('../library/kode-mstperson.php');
include ('../library/tgl-indo.php');
$DateTime = date('Y-m-d H:i:s');

include('../library/config.php');
date_default_timezone_set('Asia/Jakarta');

if(base64_decode(@$_GET['aksi'])=='Hapus'){
	$Hapus = mysqli_query($koneksi,"UPDATE dompetperson SET IsVerified='1',UserVerificator='$login_id' WHERE NoTransaksi='".base64_decode($_GET['id'])."' ");
	// if($Hapus){
		// mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName) 
		// VALUES ('$kode_jadi_log','$DateTime','Update Data','Unverified Penjualan G Koin Dompet Person : ".base64_decode($_GET['id'])."','$login_id')");
		// echo '<script language="javascript">document.location="VerifikasiPencairan.php"; </script>';
	// }else{
		// echo '<script language="javascript">alert("Unverified Gagal!"); document.location="VerifikasiPencairan.php"; </script>';
	// }
}

if(base64_decode(@$_GET['aksi'])=='Verifikasi'){
	$Hapus = mysqli_query($koneksi,"UPDATE dompetperson SET IsVerified='1',UserVerificator='$login_id' WHERE NoTransaksi='".base64_decode($_GET['id'])."' ");
	// if($Hapus){
		// mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName) 
		// VALUES ('$kode_jadi_log','$DateTime','Update Data','Verified Penjualan G Koin Dompet Person : ".base64_decode($_GET['id'])."','$login_id')");
		// echo '<script language="javascript">document.location="VerifikasiPencairan.php"; </script>';
	// }else{
		// echo '<script language="javascript">alert("Unverified Gagal!"); document.location="VerifikasiPencairan.php"; </script>';
	// }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <?php include 'view/title.php' ?>

    <!-- Bootstrap Core CSS -->
    <link href="komponen/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="komponen/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="komponen/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="komponen/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="komponen/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Jika Anda verifikasi tarik tunai, pastikan anda sudah mentransfer sejumlah uang ke rekening klien!")
			if (answer == true){
				window.location = "VerifikasiPencairan.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
	<script type="text/javascript">
		function Hapusconfirmation() {
			var answer = confirm("Transaksi tarik tunai akan dihapus! ")
			if (answer == true){
				window.location = "VerifikasiPencairan.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
	<!-- Datepcker -->
	<link rel="stylesheet" href="../library/Datepicker/dist/css/default/zebra_datepicker.min.css" type="text/css">
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
		<?php include 'view/menu.php' ; ?>
		</nav>
		
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Transaksi Pencairan Saldo Dompet</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
							<form method="post" action="">
								<div class="form-group input-group col-lg-4 col-lg-offset-8">
									<input type="text" name="keyword" class="form-control" value="<?php echo @$_REQUEST['keyword']; ?>" placeholder="Cari Nama">
									<span class="input-group-btn">
										<button class="btn btn-large btn-info" type="submit">Cari</button>
									 </span>
								</div>
							</form>
								<!-- ===================================== tabel ==================================== -->
							<div class="table-responsive">
								<table width="100%" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>No</th>
											<th>Tanggal Transaksi</th>
											<th>Nama</th>
											<th>No.Rekening</th>
											<th>Nominal</th>
											<th>Konfirmasi</th>
										</tr>
									</thead>
									<?php
										include '../library/pagination1.php';
										// mengatur variabel reload dan sql
										$kosong=null;
										if(isset($_REQUEST['keyword']) && $_REQUEST['keyword']<>""){
											// jika ada kata kunci pencarian (artinya form pencarian disubmit dan tidak kosong)pakai ini
											$keyword=$_REQUEST['keyword'];
											$reload = "VerifikasiPencairan.php?pagination=true&keyword=$keyword";
											$sql =  "SELECT a.*,b.NamaPerson,b.NoRekPerson,b.AtasNama, c.nama_bank as KodeBankPerson FROM dompetperson a JOIN mstperson b ON a.KodePerson=b.KodePerson JOIN mstbank c ON b.KodeBankPerson = c.id_bank WHERE a.UserVerificator ='Menunggu Verifikasi' AND a.IsVerified='0' AND a.Keterangan='TARIK' AND b.NamaPerson LIKE '%$keyword%' ORDER BY a.TanggalTransaksi ASC";
											$result = mysqli_query($koneksi,$sql);
										}else{
										//jika tidak ada pencarian pakai ini
											$reload = "VerifikasiPencairan.php?pagination=true";
											$sql =  "SELECT a.*,b.NamaPerson,b.NoRekPerson,b.AtasNama, c.nama_bank as KodeBankPerson FROM dompetperson a JOIN mstperson b ON a.KodePerson=b.KodePerson JOIN mstbank c ON b.KodeBankPerson = c.id_bank WHERE a.UserVerificator ='Menunggu Verifikasi' AND a.IsVerified='0' AND a.Keterangan='TARIK' ORDER BY a.TanggalTransaksi ASC";
											$result = mysqli_query($koneksi,$sql);
										}
										
										//pagination config start
										$rpp = 25; // jumlah record per halaman
										$page = intval(@$_GET["page"]);
										if($page<=0) $page = 1;  
										$tcount = mysqli_num_rows($result);
										$tpages = ($tcount) ? ceil($tcount/$rpp) : 1; // total pages, last page number
										$count = 0;
										$i = ($page-1)*$rpp;
										$no_urut = ($page-1)*$rpp;
										//pagination config end				
									?>
									<tbody>
										<?php
										while(($count<$rpp) && ($i<$tcount)) {
											mysqli_data_seek($result,$i);
											$data = mysqli_fetch_array($result);
										?>
										<tr class="odd gradeX">
											<td width="50px">
												<?php echo ++$no_urut;?> 
											</td>
											<td width="140px">
												<?php echo "".TanggalIndo($data['TanggalTransaksi'])."<br/>".substr($data['TanggalTransaksi'],10,19).""; ?>
											</td>
											<td>
												<strong><?php echo $data ['NamaPerson']; ?></strong>
											</td>
											<td>
												<?php echo "No.Rek : ".$data['NoRekPerson']."<br/>Bank : ".$data['KodeBankPerson']."<br/>a.n : ".$data['AtasNama'].""; ?>
											</td>
											<td>
												Rp.<?php echo number_format($data ['Kredit']); ?>,-
											</td>
											<td width="180px">											
												<a href="VerifikasiPencairan.php?id=<?php echo base64_encode($data['NoTransaksi']);?>&aksi=<?php echo base64_encode('Verifikasi');?>" title='Verifikasi' onclick='return confirmation()'><span class='btn btn-warning btn-sm'>Verifikasi</span></a>
												
												<a href="VerifikasiPencairan.php?id=<?php echo base64_encode($data['NoTransaksi']);?>&aksi=<?php echo base64_encode('Hapus');?>" title='Hapus' onclick='return Hapusconfirmation()'><span class='btn btn-danger btn-sm'>Unverified</span></a>
											</td>
										</tr>
										<?php
											$i++; 
											$count++;
										}
										?>
									</tbody>
								</table>
								<div><?php echo paginate_one($reload, $page, $tpages); ?></div>
								<!-- ===================================== tabel ==================================== -->
							</div>
							</div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="komponen/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="komponen/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="komponen/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="komponen/vendor/raphael/raphael.min.js"></script>
    <script src="komponen/vendor/morrisjs/morris.min.js"></script>
    <script src="komponen/data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="komponen/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>

<!-- DatePicker -->
<script type="text/javascript" src="../library/Datepicker/dist/zebra_datepicker.src.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#datepicker').Zebra_DatePicker();
	});
</script>

<script>
	var htmlobjek;
	$(document).ready(function(){
	  //apabila terjadi event onchange terhadap object <select id=nama_produk>
	 $("#KodeProvinsi").change(function(){
		var KodeProvinsi = $("#KodeProvinsi").val();
		$.ajax({
			url: "../library/Dropdown/ambil-kabupaten.php",
			data: "KodeProvinsi="+KodeProvinsi,
			cache: false,
			success: function(msg){
				$("#KodeKabupaten").html(msg);
			}
		});
	  });
	  
	  $("#KodeKabupaten").change(function(){
		var KodeKabupaten = $("#KodeKabupaten").val();
		$.ajax({
			url: "../library/Dropdown/ambil-kecamatan.php",
			data: "KodeKabupaten="+KodeKabupaten,
			cache: false,
			success: function(msg){
				$("#KodeKecamatan").html(msg);
			}
		});
	  });
	  
	  $("#KodeKecamatan").change(function(){
		var KodeKecamatan = $("#KodeKecamatan").val();
		$.ajax({
			url: "../library/Dropdown/ambil-desa.php",
			data: "KodeKecamatan="+KodeKecamatan,
			cache: false,
			success: function(msg){
				$("#KodeDesa").html(msg);
			}
		});
	  });
	  
	});
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#form1').on('submit', function(event){
			event.preventDefault();
			var formData = new FormData($('#form1')[0]);

			$('.msg').hide();
			$('.progress').show();
			
			$.ajax({
				type : 'POST',
				url : 'SimpanPendaftaran.php',
				data : formData,
				processData : false,
				contentType : false,
				success : function(response){
					//$('#form1')[0].reset();
					$('.progress').hide();
					$('.msg').show();
					if(response == "Maaf Username sudah ada, silahkan ganti Username Anda !"){
						var msg = response;
						$('.msg').html(msg);
					}else if(response === "Maaf Email sudah ada, silahkan ganti Email Anda !"){
						var msg = response;
						$('.msg').html(msg);
					}else if(response === "Maaf Kode Sponsor Tidak Valid!"){
						var msg = response;
						$('.msg').html(msg);
					}else if(response === "Proses Pendaftaran gagal !"){
						var msg = response;
						$('.msg').html(msg);
					}else if(response === "Maaf Password Anda Salah!"){
						var msg = response;
						$('.msg').html(msg);
					}else{
						$('#form1')[0].reset();
						var msg = response;
						$('.msg').html(msg);
						//alert(msg);document.location="Library.php";
					}
				}
			});
		});
	});
</script>
</body>
</html>
