<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Bosstrans</title>
	<link rel="shortcut icon" href="../../../andro/img/bosstrans.png">

    <!-- Bootstrap Core CSS -->
    <link href="../komponen/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../komponen/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../komponen/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../komponen/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../komponen/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<style>
		.panel-green > .panel-heading {
			border-color: #5cb85c;
			color: white;
			background-color: #141212;
		}
		
		.panel-body{
			background-color: #141212;
		}
	</style>
</head>

<body>

	<div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-green">
                    <div class="panel-heading">
						<div class="text-center">
							<img src="../../../img/bosstrans.png" class="img" width="75%">
						</div>
                    </div>
                    <div class="panel-body">
                        <form role="form" method="post" id="edit" action="../../library/proses-login.php" enctype="multipart/form-data">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Username" name="username" type="text" required>
                                    <input class="form-control" placeholder="Username" name="is_aktif" type="hidden" value="1">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" required>
                                </div>
								<!--<div class="form-group">
									<select name="KodeCabang" class="form-control" required>	
										<?php
											// include ('../../library/config.php');
											// echo "<option value=''>--- Cabang ---</option>";
											// $menu = mysqli_query($koneksi,"SELECT a.*,b.NamaProvinsi,c.NamaKab FROM mstcabang a JOIN mstprovinsi b ON a.KodeProvinsi=b.KodeProvinsi JOIN mstkabupaten c ON a.KodeKab=c.KodeKab ORDER BY a.KodeCabang ASC");
											// while($kode = mysqli_fetch_array($menu)){
												// echo "<option value=\"".$kode['KodeCabang']."\" >".$kode['NamaProvinsi']." - ".$kode['NamaKab']."</option>\n";
											// }
										?>
									</select>
								</div>-->
                                <!--<div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me">Remember Me
                                    </label>
                                </div>-->
                                <!-- Change this to a button or input when using this as a form -->
                                <button type="submit" name="submit" class="btn btn-lg btn-success btn-block">Login</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="../komponen/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../komponen/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../komponen/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../komponen/vendor/raphael/raphael.min.js"></script>
    <script src="../komponen/vendor/morrisjs/morris.min.js"></script>
    <script src="../komponen/data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../komponen/dist/js/sb-admin-2.js"></script>

</body>

</html>
