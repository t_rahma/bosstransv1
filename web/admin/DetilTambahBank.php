<!--
Author : Aguzrybudy
Created : Selasa, 19-April-2016
Title : Crud Menggunakan Modal Bootsrap
-->
<?php
include ('akses.php');
?>
<div class="modal-dialog">
    <div class="modal-content">
    	<div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">Input Data Bank Baru</h4>
        </div>
        <div class="modal-body">
			<div class="form-group">
				<div class="tab-pane fade <?php if(@$id==null){echo 'in active';} ?>" id="home-pills">
				<form method="post" action="">
						<div class="form-group col-lg-12">
							<select id="KodeBank" name="KodeBank" class="form-control" required>	
								<?php
									echo "<option value=''>--- Bank ---</option>";
									$menu = mysqli_query($koneksi,"SELECT * FROM mstbank");
									while($kode = mysqli_fetch_array($menu)){
										echo "<option value=\"".$kode['id_bank']."\" >".$kode['nama_bank']."</option>\n";
									}
								?>
							</select>
						</div>
						<div class="form-group col-lg-12">
							<input type="number" name="norek" class="form-control" placeholder="Nomor Rekening">
						</div>
						<div class="form-group col-lg-12">
							<input type="text" name="atasnama" class="form-control"  placeholder="Atasnama">
						</div>
						<div class="text-right">
							<button type="submit" name="submit_bank" class="btn btn-info">Submit</button>
						</div>
				</form>
				</div>
			</div>
			<div class="modal-footer">
								
			</div>
        </div>
	</div>
</div>