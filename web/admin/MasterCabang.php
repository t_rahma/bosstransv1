<?php
include ('akses.php');
$fitur_id = 3;
include ('login/lock-menu.php');
//cek jika bukan server maka kembali ke index
if($IsServer!='1'){
	echo '<script language="javascript">document.location="index.php"; </script>';
}

//kode jadi untuk server log
$DateTime = date('Y-m-d H:i:s');

include('../library/config.php');
date_default_timezone_set('Asia/Jakarta');

if(base64_decode(@$_GET['aksi'])=='Hapus'){
	include ('../library/kode-log-server.php');
	$Hapus = mysqli_query($koneksi,"DELETE FROM mstcabang WHERE KodeCabang='".base64_decode($_GET['id'])."' ")OR DIE('<script language="javascript">alert("Hapus Data Gagal, Cabang ini sudah digunakan di berbagai transaksi!"); document.location="MasterCabang.php"; </script>');
	if($Hapus){
		mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
		VALUES ('$kode_jadi_log','$DateTime','Delete Data','Master Cabang : ".base64_decode($_GET['id'])."','$login_id','$login_cabang')");
		echo '<script language="javascript">document.location="MasterCabang.php"; </script>';
	}else{
		echo '<script language="javascript">alert("Hapus Data Gagal!"); document.location="MasterCabang.php"; </script>';
	}
}

if(isset($_POST['Simpan'])){
	include ('../library/kode-log-server.php');
	//apakah provinsi dan kab ada yang sama atau tidak
	$CekDataSama = mysqli_query($koneksi,"SELECT KodeCabang FROM mstcabang SET KodeProvinsi='".$_POST['KodeProvinsi']."' AND KodeKab='".$_POST['KodeKabupaten']."'");
	if(mysqli_num_rows($CekDataSama) > 0){
		//gagal
		echo '<script language="javascript">alert("Cabang sudah ada!"); document.location="MasterCabang.php"; </script>';
	}else{
		//membuat id dompetperson
		$sql 	 = mysqli_query($koneksi,'SELECT RIGHT(KodeCabang,4) AS kode FROM mstcabang ORDER BY KodeCabang DESC LIMIT 1');  
		$num	 = mysqli_num_rows($sql);

		if($num <> 0){
			$data = mysqli_fetch_array($sql);
			$kode = $data['kode'] + 1;
		}else{
			$kode = 1;
		}

		//mulai bikin kode
		$bikin_kode_cab = str_pad($kode, 4, "0", STR_PAD_LEFT);
		$kode_jadi_cab	 = "C".$bikin_kode_cab;
		
		$Simpan = mysqli_query($koneksi,"INSERT INTO mstcabang (KodeCabang,KodeProvinsi,KodeKab)VALUES('$kode_jadi_cab','".$_POST['KodeProvinsi']."','".$_POST['KodeKabupaten']."')");
		if($Simpan){
			//create sistem setting sesuai cabang usaha
			$QuerySetting=mysqli_query($koneksi,"SELECT * FROM systemsetting WHERE KodeCabang='$login_cabang' ORDER BY KodeSetting ASC");
			while($DataSetting = mysqli_fetch_array($QuerySetting)){			
				mysqli_query($koneksi,"INSERT INTO systemsetting (KodeSetting,KodeCabang,Title,ValueData,ValueData2,ValueData3,ValueData4)VALUES ('".$DataSetting['KodeSetting']."','$kode_jadi_cab','".$DataSetting['Title']."','".$DataSetting['ValueData']."','".$DataSetting['ValueData2']."','".$DataSetting['ValueData3']."','".$DataSetting['ValueData4']."')");
			}
			
			//simpan log server
			mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
			VALUES ('$kode_jadi_log','$DateTime','Tambah Data','Master Cabang : $kode_jadi_cab','$login_id','$login_cabang')");
			echo '<script language="javascript">document.location="MasterCabang.php"; </script>';
		}else{
			echo '<script language="javascript">alert("Tambah Data Gagal!"); document.location="MasterCabang.php"; </script>';
		}
	}
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <?php include 'view/title.php' ?>

    <!-- Bootstrap Core CSS -->
    <link href="komponen/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="komponen/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="komponen/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="komponen/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="komponen/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda Yakin Untuk Menghapus Data . . . ?")
			if (answer == true){
				window.location = "MasterKurir.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
	
	<!-- Datepcker -->
	<link rel="stylesheet" href="../library/Datepicker/dist/css/default/zebra_datepicker.min.css" type="text/css">
</head>

<body>
    <div id="wrapper">
        <!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
		<?php include 'view/menu.php' ; ?>
		</nav>
		
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Master Cabang</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
						<!-- Nav tabs -->
						<ul class="nav nav-pills">
							<li <?php if(@$id==null){echo 'class="active"';} ?>><a href="#home-pills" data-toggle="tab">Data Cabang</a></li>
							<li><a href="#tambah-user" data-toggle="tab">Tambah Cabang</a></li>
						</ul>
						<div class="tab-content">
                            <div class="tab-pane fade <?php if(@$id==null){echo 'in active';} ?>" id="home-pills">
							<br/>
							<!--<form method="post" action="">
								<div class="form-group input-group col-lg-4 col-lg-offset-8">
									<input type="text" name="keyword" class="form-control" value="<?php echo @$_REQUEST['keyword']; ?>" placeholder="Cari Nama Klien">
									<span class="input-group-btn">
										<button class="btn btn-large btn-info" type="submit">Cari</button>
									 </span>
								</div>
							</form>-->
							<!-- ===================================== tabel ==================================== -->
							<div class="table-responsive">
							<table width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode Cabang</th>
                                        <th>Nama Provinsi</th>
                                        <th>Nama Kab/Kota</th>
										<!--<th>Aksi</th>-->
                                    </tr>
                                </thead>
								<?php
									include '../library/pagination1.php';
									// mengatur variabel reload dan sql
									$kosong=null;
									if(isset($_REQUEST['keyword']) && $_REQUEST['keyword']<>""){
										// jika ada kata kunci pencarian (artinya form pencarian disubmit dan tidak kosong)pakai ini
										$keyword=$_REQUEST['keyword'];
										$reload = "MasterKlien.php?pagination=true&keyword=$keyword";
										$sql =  "SELECT * FROM mstperson WHERE NamaPerson LIKE '%$keyword%' AND IsPembeli='1' AND IsAktif='1' ORDER BY NamaPerson ASC";
										$result = mysqli_query($koneksi,$sql);
									}else{
									//jika tidak ada pencarian pakai ini
										$reload = "MasterCabang.php?pagination=true";
										$sql =  "SELECT a.KodeCabang,b.NamaProvinsi,c.NamaKab FROM mstcabang a JOIN mstprovinsi b ON a.KodeProvinsi=b.KodeProvinsi JOIN mstkabupaten c ON a.KodeKab=c.KodeKab ORDER BY a.KodeCabang ASC";
										$result = mysqli_query($koneksi,$sql);
									}
									
									//pagination config start
									$rpp = 10; // jumlah record per halaman
									$page = intval(@$_GET["page"]);
									if($page<=0) $page = 1;  
									$tcount = mysqli_num_rows($result);
									$tpages = ($tcount) ? ceil($tcount/$rpp) : 1; // total pages, last page number
									$count = 0;
									$i = ($page-1)*$rpp;
									$no_urut = ($page-1)*$rpp;
									//pagination config end				
								?>
                                <tbody>
									<?php
									while(($count<$rpp) && ($i<$tcount)) {
										mysqli_data_seek($result,$i);
										$data = mysqli_fetch_array($result);
									?>
                                    <tr class="odd gradeX">
                                        <td width="50px">
											<?php echo ++$no_urut;?> 
										</td>
										<td>
											<strong><?php echo $data ['KodeCabang']; ?></strong>
										</td>
										<td>
											<?php echo $data ['NamaProvinsi']; ?>
										</td>
                                        <td>
											<?php echo $data ['NamaKab']; ?>
										</td>
										<!--<td width="100px">											
											<a href="MasterCabang.php?id=<?php echo base64_encode($data['KodeCabang']);?>&aksi=<?php echo base64_encode('Hapus');?>" title='Hapus' onclick='return confirmation()'><i class='btn btn-danger btn-sm'><span class='fa fa-trash'></span></i></a>
										</td>-->
                                    </tr>
									<?php
										$i++; 
										$count++;
									}
									?>
                                </tbody>
                            </table>
							<div><?php echo paginate_one($reload, $page, $tpages); ?></div>
                            <!-- /.table-responsive -->
							
							</div>
							<!-- ===================================== tabel ==================================== -->
							</div>
							<div class="tab-pane fade" id="tambah-user">
							<!-- ===================================== input ===================================== -->
								<div class="panel-body">
									<div class="row">
										<form method="post" name='input' action="">
											<div class="col-lg-6">
												<label>Provinsi</label>
												<div class="form-group">
													<select id="KodeProvinsi" name="KodeProvinsi" class="form-control" required>	
														<?php
															echo "<option value=''>--- Provinsi ---</option>";
															$menu = mysqli_query($koneksi,"SELECT * FROM mstprovinsi ORDER BY NamaProvinsi ASC");
															while($kode = mysqli_fetch_array($menu)){
																echo "<option value=\"".$kode['KodeProvinsi']."\" >".$kode['NamaProvinsi']."</option>\n";
															}
														?>
													</select>
												</div>
												
												<label>Kab/Kota</label>
												<div class="form-group">
													<select id="KodeKabupaten" name="KodeKabupaten" class="form-control" required>
													<option value=''>--- Kab/Kota ---</option>
													</select>
												</div>
												
												<button type="submit" name="Simpan" class="btn btn-info">Simpan</button>
											</div>
										</form>
									</div>
								</div>
							<!-- ===================================== input ===================================== -->
							</div>
						</div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="komponen/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="komponen/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="komponen/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="komponen/vendor/raphael/raphael.min.js"></script>
    <script src="komponen/vendor/morrisjs/morris.min.js"></script>
    <script src="komponen/data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="komponen/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>

<!-- DatePicker -->
<script type="text/javascript" src="../library/Datepicker/dist/zebra_datepicker.src.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#datepicker').Zebra_DatePicker();
	});
</script>

<script>
	var htmlobjek;
	$(document).ready(function(){
	  //apabila terjadi event onchange terhadap object <select id=nama_produk>
	 $("#KodeProvinsi").change(function(){
		var KodeProvinsi = $("#KodeProvinsi").val();
		$.ajax({
			url: "../library/Dropdown/ambil-kabupaten.php",
			data: "KodeProvinsi="+KodeProvinsi,
			cache: false,
			success: function(msg){
				$("#KodeKabupaten").html(msg);
			}
		});
	  });
	  
	  $("#KodeKabupaten").change(function(){
		var KodeKabupaten = $("#KodeKabupaten").val();
		$.ajax({
			url: "../library/Dropdown/ambil-kecamatan.php",
			data: "KodeKabupaten="+KodeKabupaten,
			cache: false,
			success: function(msg){
				$("#KodeKecamatan").html(msg);
			}
		});
	  });
	  
	  $("#KodeKecamatan").change(function(){
		var KodeKecamatan = $("#KodeKecamatan").val();
		$.ajax({
			url: "../library/Dropdown/ambil-desa.php",
			data: "KodeKecamatan="+KodeKecamatan,
			cache: false,
			success: function(msg){
				$("#KodeDesa").html(msg);
			}
		});
	  });
	  
	});
</script>
</body>
</html>
