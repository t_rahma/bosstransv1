<?php
include ('akses.php');
$fitur_id = 14;
include ('login/lock-menu.php');
date_default_timezone_set('Asia/Jakarta');

//kode jadi untuk server log
include ('../library/tgl-indo.php');
include('../library/config.php');
$DateTime = date('Y-m-d H:i:s a');
$BulanIni = date('Y/m');

@$id 	= base64_decode($_GET['id']);
@$metode = $_GET['metode'];

if($metode=='hapus'){
	include ('../library/kode-log-server.php');
	mysqli_query($koneksi,"DELETE FROM dompetperusahaan WHERE NoTransaksi='$id' AND KodeCabang='$login_cabang'");
	mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
	VALUES ('$kode_jadi_log','$DateTime','Hapus Data','Transaksi Kas Masuk : $id','$login_id','$login_cabang')");
}

//ambil prosentase bagi hasil
$QuerySetting = mysqli_query($koneksi,"SELECT ValueData FROM systemsetting WHERE KodeSetting='SET-00019' AND KodeCabang='$login_cabang'");
$DataSetting = mysqli_fetch_assoc($QuerySetting);

if(isset($_POST['SimpanData'])){
	include ('../library/kode-DompetPerusahaan.php');
	include ('../library/kode-log-server.php');
	if($IsServer=='1'){
		$PorsiPusat = 0;
		$PorsiCabang = $_POST['NominalMasuk'];
	}else{
		$PorsiPusat = ($DataSetting['ValueData']/100)*$_POST['NominalMasuk'];
		$PorsiCabang = $_POST['NominalMasuk']-$PorsiPusat;
	}
	
	$query = mysqli_query($koneksi,"INSERT INTO dompetperusahaan (NoTransaksi,TanggalTransaksi,Debet,Keterangan,UserName,KodeCabang, IsTrOffline,PorsiPusat)VALUES('$kode_jadi_bank_perusahaan','$DateTime','$PorsiCabang','".$_POST['Keterangan']."','$login_id','$login_cabang','1','$PorsiPusat')");
		if($query){
			mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
			VALUES ('$kode_jadi_log','$DateTime','Tambah Data','Transaksi Order Offline: $kode_jadi_bank_perusahaan','$login_id','$login_cabang')");
			echo '<script language="javascript">document.location="TrOrderOfline.php"; </script>';
		}else{
			echo '<script language="javascript">alert("Simpan data gagal !"); document.location="TrOrderOfline.php"; </script>';
		}
 }
	       
 //===========procedure Update data=============//
 if($metode=='Edit'){
	$edit = mysqli_query($koneksi,"select * from dompetperusahaan where NoTransaksi = '$id' AND KodeCabang='$login_cabang'");
	 while($row = mysqli_fetch_array($edit)){
		$debet		 	= $row['Debet']+$row['PorsiPusat'];
		$ket			= $row['Keterangan'];
	 } 
 }
 
 if(isset($_POST['EditData'])){
	include ('../library/kode-log-server.php');
	
	if($IsServer=='1'){
		$PorsiPusat = 0;
		$PorsiCabang = $_POST['NominalMasuk'];
	}else{
		$PorsiPusat = ($DataSetting['ValueData']/100)*$_POST['NominalMasuk'];
		$PorsiCabang = $_POST['NominalMasuk']-$PorsiPusat;
	}
	
	$query = mysqli_query($koneksi,"UPDATE dompetperusahaan SET Debet ='$PorsiCabang',PorsiPusat='$PorsiPusat',Keterangan='".$_POST['Keterangan']."',UserName='$login_id' WHERE NoTransaksi = '$id' AND KodeCabang='$login_cabang'");
		if($query){
			mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
			VALUES ('$kode_jadi_log','$DateTime','Edit Data','Transaksi Order Offline: $id','$login_id','$login_cabang')");
			echo '<script language="javascript"> document.location="TrOrderOfline.php"; </script>';
		}else{
			echo '<script language="javascript">alert("Edit data gagal !"); document.location="TrOrderOfline.php?id='.base64_encode($id).'"; </script>';
		}
 }
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <?php include 'view/title.php' ?>

    <!-- Bootstrap Core CSS -->
    <link href="komponen/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="komponen/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="komponen/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="komponen/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="komponen/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<!-- Datepcker -->
	<link rel="stylesheet" href="../library/Datepicker/dist/css/default/zebra_datepicker.min.css" type="text/css">

	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda Yakin Untuk Menghapus Transaksi ini? Transaksi yang dihapus tidak dapat dikembalikan lagi!")
			if (answer == true){
				window.location = "AccessLevel.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
	
	
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
		<?php include 'view/menu.php' ; ?>
		</nav>
		
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Transaksi Order Offline <?php if(@$_REQUEST['keyword']!=null){echo TanggalIndo(@$_REQUEST['keyword']);}else{echo TanggalIndo($BulanIni);} ?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
						<!-- Nav tabs -->
                            <ul class="nav nav-pills">
                                <li <?php if(@$metode!='Edit'){ echo 'class="active"';}?>><a href="#home-pills" data-toggle="tab">Data Order Offline</a></li>
                                <li><a href="#tambah-user" data-toggle="tab">Tambah Data</a></li>
								<?php if(@$metode=='Edit'){echo '<li class="active"><a href="#edit-iklan" data-toggle="tab">Edit Kas Masuk</a></li>';}?>
                            </ul>
						<div class="tab-content">
                            <div class="tab-pane fade <?php if(@$metode!='Edit'){ echo 'in active';}?>" id="home-pills">
							<!-- ===================================== tabel ==================================== -->
							<form method="post" action="">
								<div class="form-group input-group col-lg-3 col-lg-offset-9">
									<input type="text" name="keyword" class="form-control" id="datepicker" value="<?php echo @$_REQUEST['keyword']; ?>" placeholder="Tanggal Transaksi">
									<span class="input-group-btn">
										<button class="btn btn-large btn-info" type="submit">Cari</button>
									 </span>
								</div>
							</form>
							<div class="table-responsive">
							<table width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal Transaksi</th>
                                        <th>Nominal</th>
                                        <th>Keterangan</th>
                                        <th>Author</th>
										<th>Aksi</th>
                                    </tr>
                                </thead>
								<?php
									include '../library/pagination1.php';
									// mengatur variabel reload dan sql
									$kosong=null;
									if(isset($_REQUEST['keyword']) && $_REQUEST['keyword']<>""){
										// jika ada kata kunci pencarian (artinya form pencarian disubmit dan tidak kosong)pakai ini
										$keyword=$_REQUEST['keyword'];
										$reload = "TrOrderOfline.php?pagination=true&keyword=$keyword";
										$sql =  "SELECT * FROM dompetperusahaan WHERE KodeCabang='$login_cabang' AND date_format(TanggalTransaksi,'%Y/%m')='$keyword' AND IsTrOffline='1' ORDER BY TanggalTransaksi ASC";
										$result = mysqli_query($koneksi,$sql);
									}else{
									//jika tidak ada pencarian pakai ini
										$reload = "TrOrderOfline.php?pagination=true";
										$sql =  "SELECT * FROM dompetperusahaan WHERE KodeCabang='$login_cabang' AND date_format(TanggalTransaksi,'%Y/%m')='$BulanIni' AND IsTrOffline='1' ORDER BY TanggalTransaksi ASC";
										$result = mysqli_query($koneksi,$sql);
									}
									
									//pagination config start
									$rpp = 30; // jumlah record per halaman
									$page = intval(@$_GET["page"]);
									if($page<=0) $page = 1;  
									$tcount = mysqli_num_rows($result);
									$tpages = ($tcount) ? ceil($tcount/$rpp) : 1; // total pages, last page number
									$count = 0;
									$i = ($page-1)*$rpp;
									$no_urut = ($page-1)*$rpp;
									//pagination config end				
								?>
                                <tbody>
									<?php
									while(($count<$rpp) && ($i<$tcount)) {
										mysqli_data_seek($result,$i);
										$data = mysqli_fetch_array($result);
									?>
                                    <tr class="odd gradeX">
                                        <td width="50px">
											<?php echo ++$no_urut;?> 
										</td>
										<td>
											<strong><?php echo "".TanggalIndo($data['TanggalTransaksi'])."<br/>".substr($data['TanggalTransaksi'],10,17).""; ?></strong>
										</td>
                                        <td>
											Rp.<?php echo number_format($data['Debet']+$data['PorsiPusat']);?>,-
										</td>
										<td>
											<?php echo $data['Keterangan'];?>
										</td>
										<td>
											<?php echo $data ['UserName']; ?>
										</td>
										<td width="100px">				
											<?php if($data['IsKirimUang']!=1 AND $data['UserName']!=null){ ?>
											<a href="TrOrderOfline.php?id=<?php echo base64_encode($data['NoTransaksi']);?>&metode=Edit" title='Edit Iklan'><i class='btn btn-warning btn-sm'><span class='fa fa-pencil'></span></i></a>
											
											<a href="TrOrderOfline.php?id=<?php echo base64_encode($data['NoTransaksi']); ?>&metode=hapus" title='Hapus' onclick='return confirmation()'><i class='btn btn-danger btn-sm'><span class='fa fa-trash'></span></i></a>
											<?php }else{
												echo "<i class='btn btn-warning btn-sm' disabled><span class='fa fa-pencil'></span></i>";
												echo '&nbsp;';
												echo "<i class='btn btn-danger btn-sm' disabled><span class='fa fa-trash'></span></i>";
												
											} ?>
											
										</td>
                                    </tr>
									<?php
										$i++; 
										$count++;
									}
									?>
                                </tbody>
                            </table>
							<div><?php echo paginate_one($reload, $page, $tpages); ?></div>
                            <!-- /.table-responsive -->
							</div>
							<!-- ===================================== tabel ==================================== -->
							</div>
							<div class="tab-pane fade" id="tambah-user">
							<!-- ===================================== input ===================================== -->
							<div class="panel-body">
                            <div class="row">
								<div class="col-lg-6">
									<form method="post" action="">
										<div class="form-group col-lg-12">
											<label>Nominal Transaksi</label>
											<input type="number" min="0" class="form-control" name="NominalMasuk" required>
										</div>	
										<div class="form-group col-lg-12">
											<label>Keterangan</label>
											<textarea class="form-control" name="Keterangan" required></textarea>
										</div>
										<div class="col-lg-12">
											<button type="submit" class="btn btn-md btn-primary" name="SimpanData">Simpan</button>
										</div>
									</form>
								</div>
                            </div>
                            <!-- /.row (nested) -->
							</div>
							<!-- ===================================== input ===================================== -->
							</div>
							
							<div class="tab-pane fade <?php if(@$metode=='Edit'){ echo 'in active';}?>" id="edit-iklan">
							<!-- ===================================== Edit ===================================== -->
							<div class="panel-body">
                            <div class="row">
								<div class="col-lg-6">
									<form method="post" action="">
										<div class="form-group col-lg-12">
											<label>Nominal Transaksi</label>
											<input type="number" min="0" class="form-control" name="NominalMasuk"  value="<?php echo $debet; ?>" required>
										</div>	
										<div class="form-group col-lg-12">
											<label>Keterangan</label>
											<textarea class="form-control" name="Keterangan" required><?php echo $ket; ?></textarea>
										</div>
										<div class="col-lg-12">
											<button type="submit" class="btn btn-md btn-primary" name="EditData">Simpan Edit</button>
										</div>
									</form>
								</div>
                            </div>
                            <!-- /.row (nested) -->
							</div>
							<!-- ===================================== Edit ===================================== -->
							</div>
						</div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="komponen/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="komponen/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="komponen/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="komponen/vendor/raphael/raphael.min.js"></script>
    <script src="komponen/vendor/morrisjs/morris.min.js"></script>
    <script src="komponen/data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="komponen/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
	
	<!-- DatePicker -->
<script type="text/javascript" src="../library/Datepicker/dist/zebra_datepicker.src.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#datepicker').Zebra_DatePicker({format: 'Y/m'});
		$('#datepicker2').Zebra_DatePicker({format: 'Y-m-d H:i'});
		$('#datepicker3').Zebra_DatePicker({format: 'Y-m-d H:i'});
		$('#datepicker4').Zebra_DatePicker({format: 'Y-m-d H:i'});
	});
</script>
</body>

</html>
