<!--
Author : Aguzrybudy
Created : Selasa, 19-April-2016
Title : Crud Menggunakan Modal Bootsrap
-->
<?php
 $NoPol		=@$_GET['NoPol'];
 $Foto1		=@$_GET['Foto1'];
 $Foto2		=@$_GET['Foto2'];
 $Foto3		=@$_GET['Foto3'];
 $Foto4		=@$_GET['Foto4'];
 $Stnk		=@$_GET['Stnk'];
 $KodePerson=@$_GET['KodePerson'];
 $Ktp=@$_GET['Ktp'];
 $Sim=@$_GET['Sim'];
 $Skck=@$_GET['Skck'];
 $Detail=@$_GET['Detail'];
?>

<div class="modal-dialog">
    <div class="modal-content">
    	<div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">Detil Data Driver</h4>
        </div>
        <div class="modal-body">
			<div class="form-group">
				<label>Nomor Polisi</label>
				<p><strong><?php echo $NoPol;?></strong></p>
				
				<label>Foto KTP</label>
				<img src="../../andro/foto_ktp/<?php echo $Ktp; ?>" class="img img-responsive img-thumbnail">
				
				<p><label>Foto SIM</label>
				<img src="../../andro/foto_sim/<?php echo $Sim; ?>" class="img img-responsive img-thumbnail"></p>
				
				<p><label>Foto SKCK</label>
				<img src="../../andro/foto_skck/<?php echo $Skck; ?>" class="img img-responsive img-thumbnail"></p>
				
				<p><label>Foto STNK</label>
				<img src="../../andro/foto_stnk/<?php echo $Stnk; ?>" class="img img-responsive img-thumbnail"></p>
			
				<p><label>Foto 1</label>
				<img src="../../andro/foto_kendaraan/<?php echo $Foto1; ?>" class="img img-responsive img-thumbnail"></p>
				
				<p><label>Foto 2</label>
				<img src="../../andro/foto_kendaraan/<?php echo $Foto2; ?>" class="img img-responsive img-thumbnail"></p>
				
				<p><label>Foto 3</label>
				<img src="../../andro/foto_kendaraan/<?php echo $Foto3; ?>" class="img img-responsive img-thumbnail"></p>
				
				<label>Foto 4</label>
				<img src="../../andro/foto_kendaraan/<?php echo $Foto4; ?>" class="img img-responsive img-thumbnail">
				
				
			</div>
			<?php 
				if($Detail == 'verifikasi'){
					?>
					<div class="modal-footer">
					<a href="VerifikasiDriver.php?id=<?php echo base64_encode($KodePerson);?>&aksi=<?php echo base64_encode('Verifikasi');?>" title='Verifikasi' onclick='return confirmation()'><span class='btn btn-success btn-md'>Verifikasi</span></a>
				
					<button type="reset" class="btn btn-danger"  data-dismiss="modal" aria-hidden="true">Tutup</button>
					</div>
					<?php
				}
			?>
			
        </div>
	</div>
</div>