<?php
include ('akses.php');
$fitur_id = 10;
//cek apakah user mempunyai hak akses menu ini
// include ('login/lock-menu.php');
//kode jadi untuk server log
// include ('../library/kode-log-server.php');
$DateTime = date('Y-m-d H:i:s');

include('../library/config.php');
date_default_timezone_set('Asia/Jakarta');

$UserName			= @$_POST['UserName'];
$ActualName		 	= @$_POST['ActualName'];
$Address		 	= @$_POST['Address'];
$Phone			 	= @$_POST['Phone'];
//$HPNo			 	= $_POST['HPNo'];
$Email			 	= @$_POST['Email'];
$LevelID		 	= @$_POST['LevelID'];
$IsAktif		 	= 1;

/* if($IsServer=='1'){
	$InputCabang= @$_POST['KodeCabang']; //dari form input
	$KodeCabang = base64_decode(@$_GET['cab']);
}else{
	$InputCabang= $login_cabang;
	$KodeCabang = $login_cabang;
}  */
$InputCabang = "C0001";
$KodeCabang = "C0001";
if(isset($_POST['submit'])){
	$UserPsw		 	= base64_encode($_POST['UserPsw']);
	//cek apakah username ada yang sama atau tidak
	$cek2 = mysqli_query($koneksi,"select UserName from userlogin where UserName='$UserName' AND KodeCabang='$InputCabang'");
	$num2 = mysqli_num_rows($cek2);
	if($num2 == 1 ){
		echo '<script type="text/javascript">alert("Maaf username sudah ada, silahkan ganti username anda" );document.location="UserLogin.php";</script>';
	}else{
		$query = mysqli_query($koneksi,"INSERT into userlogin (UserName,UserPsw,ActualName,Address,Phone,Email,LevelID,KodeCabang,IsAktif) 
		VALUES ('$UserName','$UserPsw','$ActualName','$Address','$Phone','$Email','$LevelID','$InputCabang','$IsAktif')");
		if($query){
			/* mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
			VALUES ('$kode_jadi_log','$DateTime','Tambah Data','Tambah User Login : $UserName','$login_id','$login_cabang')"); */
			echo '<script language="javascript">document.location="UserLogin.php";</script>';
		}else{
			echo '<script language="javascript">alert("Input data gagal !"); document.location="UserLogin.php"; </script>';
		}
	}
}
 
 //===========procedure Update data=============//
 $id = base64_decode(@$_GET['id']);
 $edit = mysqli_query($koneksi,"select * from userlogin where UserName = '$id' AND KodeCabang='$KodeCabang'");
 while($row = mysqli_fetch_array($edit)){
	$username 	= $row['UserName'];
	$userpsw	= $row['UserPsw'];
	$nama		= $row['ActualName'];
	$alamat		= $row['Address'];
	$no_hp		= $row['Phone'];
	$email		= $row['Email'];
	$level_id	= $row['LevelID'];
	$is_aktif	= $row['IsAktif'];
	$foto		= $row['Foto'];
 }
 
 if(isset($_POST['submit_edit'])){
    $UserPsw		 	= base64_encode($_POST['UserPsw']);
	$query = mysqli_query($koneksi,"UPDATE userlogin SET UserPsw ='$UserPsw',ActualName='$ActualName',Address='$Address',Phone='$Phone',IsAktif='$IsAktif',Email='$Email' WHERE UserName = '$id' AND KodeCabang='$KodeCabang'");
		if($query){
			/* mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
			VALUES ('$kode_jadi_log','$DateTime','Edit Data','Edit User Login : $id:$UserPsw:$ActualName:$Address:$Phone:$LevelID:$Email','$login_id','$login_cabang')"); */
			echo '<script language="javascript">alert("Edit Data berhasil disimpan !"); document.location="UserLogin.php"; </script>';
		}else{
			echo '<script language="javascript">alert("Edit data gagal !"); document.location="UserLogin.php?id='.base64_encode($id).'"; </script>';
		}
 }
 
 //===================== Delete User =======================
if(base64_decode(@$_GET['aksi'])=='Hapus'){
	$query = "UPDATE userlogin SET IsAktif =b'0' WHERE UserName ='$id' AND KodeCabang='$KodeCabang'";  
	$sql= mysqli_query($koneksi,$query);  
	if($sql){
		/* mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
		VALUES ('$kode_jadi_log','$DateTime','Delete Data','User Login : $id','$login_id','$login_cabang')"); */
		
		echo '<script language="javascript">document.location="UserLogin.php"; </script>';
	}
}

if(@$_REQUEST['keyword']!=null){
	$cabang = @$_REQUEST['keyword'];
}else{
	$cabang = $login_cabang;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <?php include 'view/title.php' ?>

    <!-- Bootstrap Core CSS -->
    <link href="komponen/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="komponen/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="komponen/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="komponen/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="komponen/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<!-- css progress upload gambar   --> 
	<link href="../css/style.css" rel="stylesheet">
	
	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda Yakin Untuk Menghapus Data . . . ?")
			if (answer == true){
				window.location = "AccessLevel.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
	
	<script type="text/javascript">
		function confirmation2() {
			var answer = confirm("Apakah Anda Yakin Mengaktifkan Fitur ini . . . ?")
			if (answer == true){
				window.location = "AccessLevel.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
	
	<script type="text/javascript">
		function confirmation3() {
			var answer = confirm("Apakah Anda Yakin Non Aktifkan Fitur ini . . . ?")
			if (answer == true){
				window.location = "AccessLevel.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
		<?php include 'view/menu.php' ; ?>
		</nav>
		
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">User Login</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
						<!-- Nav tabs -->
                            <ul class="nav nav-pills">
                                <li <?php if($id==null){echo 'class="active"';} ?>><a href="#home-pills" data-toggle="tab">Data User Login</a>
                                </li>
                                <li><a href="#tambah-user" data-toggle="tab">Tambah Data</a>
                                </li>
								<li <?php if($id!=null){echo 'class="active"';} ?>><a href="#edit-user" data-toggle="tab">Edit Data</a>
                                </li>
                            </ul>
						<div class="tab-content">
                            <div class="tab-pane fade <?php if($id==null){echo 'in active';} ?>" id="home-pills">
							<!-- ===================================== tabel ==================================== -->
							<div class="table-responsive col-lg-12">
							<h2>Data User Login</h2>
								<?php if(@$IsServer=='1'){ ?>
								<!--<form method="post" action="">
									<div class="form-group input-group col-lg-5 col-lg-offset-7">
										<select name="keyword" class="form-control" required>	
											
											<?php
												echo '<option value="">-- Cabang Perusahaan --</option>';
												$menu = mysqli_query($koneksi,"SELECT a.*,b.NamaProvinsi,c.NamaKab FROM mstcabang a JOIN mstprovinsi b ON a.KodeProvinsi=b.KodeProvinsi JOIN mstkabupaten c ON a.KodeKab=c.KodeKab ORDER BY a.KodeCabang ASC");
												while($kode = mysqli_fetch_array($menu)){
													if($kode['KodeCabang']==$cabang){
														echo "<option value=\"".$kode['KodeCabang']."\" selected >".$kode['NamaProvinsi']." - ".$kode['NamaKab']."</option>\n";
													}else{
														echo "<option value=\"".$kode['KodeCabang']."\">".$kode['NamaProvinsi']." - ".$kode['NamaKab']."</option>\n";
													}
													
												}
											?>
										</select>
										<span class="input-group-btn">
											<button class="btn btn-large btn-info" type="submit">Cari</button>
										</span>
									</div>
								</form>-->
								<?php } ?>
							<table width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Alamat</th>
                                        <th>Telp</th>
                                        <th>Email</th>
                                        <th>Username</th>
                                        <!--<th>Level ID</th>-->
										<th>Aksi</th>
                                    </tr>
                                </thead>
								<?php
									include '../library/pagination1.php';
									// mengatur variabel reload dan sql
									$kosong=null;
									if(isset($_REQUEST['keyword']) && $_REQUEST['keyword']<>""){
										// jika ada kata kunci pencarian (artinya form pencarian disubmit dan tidak kosong)pakai ini
										$keyword=$_REQUEST['keyword'];
										$reload = "UserLogin.php?pagination=true&keyword=$keyword";
										//$sql =  "SELECT a.* , b.LevelName FROM userlogin a JOIN accesslevel b ON (a.LevelID,a.KodeCabang) = (b.LevelID,b.KodeCabang) WHERE a.IsAktif='1' AND a.UserName!='afindo' AND a.KodeCabang='$cabang'";
										$result = mysqli_query($koneksi,$sql);
									}else{
									//jika tidak ada pencarian pakai ini
										$reload = "UserLogin.php?pagination=true";
										//$sql =  "SELECT a.* , b.LevelName FROM userlogin a JOIN accesslevel b ON (a.LevelID,a.KodeCabang) = (b.LevelID,b.KodeCabang) WHERE a.IsAktif='1' AND a.UserName!='afindo' AND a.KodeCabang='$login_cabang'";
										$sql = "SELECT * FROM userlogin WHERE IsAktif='1'";
										$result = mysqli_query($koneksi,$sql);
									}
									
									//pagination config start
									$rpp = 10; // jumlah record per halaman
									$page = intval(@$_GET["page"]);
									if($page<=0) $page = 1;  
									$tcount = mysqli_num_rows($result);
									$tpages = ($tcount) ? ceil($tcount/$rpp) : 1; // total pages, last page number
									$count = 0;
									$i = ($page-1)*$rpp;
									$no_urut = ($page-1)*$rpp;
									//pagination config end				
								?>
                                <tbody>
									<?php
									while(($count<$rpp) && ($i<$tcount)) {
										mysqli_data_seek($result,$i);
										$data = mysqli_fetch_array($result);
									?>
                                    <tr class="odd gradeX">
                                        <td width="50px">
											<?php echo ++$no_urut;?> 
										</td>
										<td>
											<strong><?php echo $data ['ActualName']; ?></strong>
										</td>
										<td>
											<?php echo $data ['Address']; ?>
										</td>
										<td>
											<?php echo $data ['Phone']; ?>
										</td>
										<td>
											<?php echo $data ['Email']; ?>
										</td>
										<td>
											<?php echo $data ['UserName']; ?>
										</td>
										<!--<td>
											<?php //echo $data ['LevelName']; ?>
										</td>-->
                                        <td width="100px">
											<a href="UserLogin.php?id=<?php echo base64_encode($data['UserName']);?>&cab=<?php echo base64_encode($data['KodeCabang']);?>" title='Edit'><i class='btn btn-warning btn-sm'><span class='fa fa-edit'></span></i></a> 
											
											<?php
											if($data ['UserName'] != $login_id){
												echo '<a href="UserLogin.php?id='.base64_encode($data['UserName']).'&aksi='.base64_encode('Hapus').'&cab='.base64_encode($data['KodeCabang']).'" title="Hapus" onclick="return confirmation()"><i class="btn btn-danger btn-sm"><span class="fa fa-trash"></span></i></a>';
											}
											?>
										</td>
                                    </tr>
									<?php
										$i++; 
										$count++;
									}
									?>
                                </tbody>
                            </table>
							<div><?php echo paginate_one($reload, $page, $tpages); ?></div>
                            <!-- /.table-responsive -->
							
							</div>
							<!-- ===================================== tabel ==================================== -->
							</div>
							<div class="tab-pane fade" id="tambah-user">
							<!-- ===================================== input ===================================== -->
							<div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
									<h2>Tambah User Login </h2>
										<form role="form" method="post" id="edit"  enctype="multipart/form-data">
											<div class="form-group">
												<label>Nama*</label>
												<input type="text" class="form-control" name="ActualName" required>
											</div>
											<div class="form-group">
												<label>Alamat*</label>
												<input type="text" class="form-control" name="Address" required>
											</div>
											<div class="form-group">
												<label>Phone*</label>
												<input type="text" class="form-control" name="Phone" required>
											</div>
											<div class="form-group">
												<label>Email*</label>
												<input type="email" class="form-control" name="Email" required>
											</div>
											<div class="form-group">
												<label>Username*</label>
												<input type="text" class="form-control" name="UserName" required>
											</div>
											<div class="form-group">
												<label>Password*</label>
												<input type="password" class="form-control" name="UserPsw" required>
											</div>
											<!--<?php if($IsServer=='1'){?>
											<div class="form-group">
												<label>Cabang Perusahaan*</label>
												<select id="KodeCabang" name="KodeCabang" class="form-control" required>	
													<?php
														echo '<option value="">-- Cabang Perusahaan --</option>';
														$menu = mysqli_query($koneksi,"SELECT a.*,b.NamaProvinsi,c.NamaKab FROM mstcabang a JOIN mstprovinsi b ON a.KodeProvinsi=b.KodeProvinsi JOIN mstkabupaten c ON a.KodeKab=c.KodeKab ORDER BY a.KodeCabang ASC");
														while($kode = mysqli_fetch_array($menu)){
															echo "<option value=\"".$kode['KodeCabang']."\">".$kode['NamaProvinsi']." - ".$kode['NamaKab']."</option>\n";
														}
													?>
												</select>
											</div>
											<div class="form-group">
												<label>Level ID*</label>
												<select id="LevelID" name="LevelID" class="form-control" required>	
													<?php
														/* $menu = mysqli_query($koneksi,"SELECT * FROM accesslevel WHERE IsAktif='1'");
														while($kode = mysqli_fetch_array($menu)){
															echo "<option value=\"".$kode['LevelID']."\" >".$kode['LevelName']."</option>\n";
														} */
													?>
												</select>
											</div>
											<?php }else{ ?>
											<div class="form-group">
												<label>Level ID*</label>
												<select id="LevelID" name="LevelID" class="form-control" required>	
													<?php
														$menu = mysqli_query($koneksi,"SELECT * FROM accesslevel WHERE IsAktif='1' AND KodeCabang='$login_cabang'");
														while($kode = mysqli_fetch_array($menu)){
															echo "<option value=\"".$kode['LevelID']."\" >".$kode['LevelName']."</option>\n";
														}
													?>
												</select>
											</div>
											<?php } ?>-->
											<button type="submit" name="submit" value="simpan" class="btn btn-info">Simpan</button>
											<button type="reset" name="reset" value="reset" class="btn btn-danger">Reset</button>
										</form>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
							</div>
							<!-- ===================================== input ===================================== -->
							</div>
							<!-- ===================================== Start Edit ===================================== -->
							<div class="tab-pane fade  <?php if($id!=null){echo 'in active';} ?>" id="edit-user">
							<div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
								<!-- kondisi jika $id == null maka tambah data -->
								<?php if($id==null):?>
									<h2>Edit User Login </h2>
										<form role="form" method="post" id="edit"  enctype="multipart/form-data">
											<div class="form-group">
												<label>Nama*</label>
												<input type="text" class="form-control" name="ActualName" disabled>
											</div>
											<div class="form-group">
												<label>Alamat*</label>
												<input type="text" class="form-control" name="Address" disabled>
											</div>
											<div class="form-group">
												<label>Phone*</label>
												<input type="text" class="form-control" name="Phone" disabled>
											</div>
											<div class="form-group">
												<label>Email*</label>
												<input type="email" class="form-control" name="Email" disabled>
											</div>
											<div class="form-group">
												<label>Username*</label>
												<input type="text" class="form-control" name="UserName" disabled>
											</div>
											<div class="form-group">
												<label>Password*</label>
												<input type="password" class="form-control" name="UserPsw" disabled>
											</div>
											<button type="submit" name="submit" value="simpan" class="btn btn-info" disabled>Simpan</button>
											<button type="reset" name="reset" value="reset" class="btn btn-danger" disabled>Reset</button>
										</form>
								<?php elseif($id!=null):?>
									<h2>Edit Data </h2>
										<form role="form" method="post" id="edit"  enctype="multipart/form-data">
											<div class="form-group">
												<label>Nama*</label>
												<input type="text" class="form-control" name="ActualName" value="<?php echo $nama; ?>" required>
											</div>
											<div class="form-group">
												<label>Alamat*</label>
												<input type="text" class="form-control" name="Address" value="<?php echo $alamat; ?>" required>
											</div>
											<div class="form-group">
												<label>Phone*</label>
												<input type="text" class="form-control" name="Phone" value="<?php echo $no_hp; ?>" required>
											</div>
											<div class="form-group">
												<label>Email*</label>
												<input type="email" class="form-control" name="Email" value="<?php echo $email; ?>" required>
											</div>
											<div class="form-group">
												<label>Password*</label>
												<input type="password" class="form-control" name="UserPsw" value="<?php echo base64_decode($userpsw); ?>" required>
											</div>
											<!--<div class="form-group">
												<label>Level ID*</label>
												<select name="LevelID" class="form-control" required>	
													<?php
														$menu = mysqli_query($koneksi,"SELECT * FROM accesslevel WHERE IsAktif='1' AND KodeCabang='$KodeCabang'");
														while($kode = mysqli_fetch_array($menu)){
															if($kode['LevelID']==$level_id){
																echo "<option value=\"".$kode['LevelID']."\" selected >".$kode['LevelName']."</option>\n";
															}else{
																echo "<option value=\"".$kode['LevelID']."\" >".$kode['LevelName']."</option>\n";
															}
															
														}
													?>
												</select>
											</div>-->
											<button type="submit" name="submit_edit" value="simpan" class="btn btn-info">Simpan</button>
											<button type="reset" name="reset" value="reset" class="btn btn-danger">Reset</button>
										</form>
								<?php endif?>
                                </div>
                                <div class="col-lg-6"  <?php if($id==null){echo 'style="visibility: hidden;"';} ?>>
									<h2>&nbsp;</h2>
									<div class="text-center">
										<?php if($foto==null){
											echo '<img src="../assets/img/no-image.jpg" class="img img-responsive img-thumbnail" alt="Image User">';
										}else{
											echo '<img src="../assets/img/FotoProfil/'.$foto.'" class="img img-responsive img-thumbnail" alt="Image User">';
										}?>
									</div>
									<div id="upload-wrapper">
										<div align="center">
											<h3>Upload Foto Profil</h3>
											<label>Upload gambar dengan type .jpg dengan ukuran max 3 mb</label>
											<form action="upload/processupload-userlogin.php?id=<?php echo base64_encode($id);?>&foto=<?php echo base64_encode($foto);?>&cab=<?php echo base64_encode($KodeCabang);?>" onSubmit="return false" method="post" enctype="multipart/form-data" id="MyUploadForm">
												<input name="ImageFile" id="imageInput" type="file" /><br/>
												<input type="submit" class="btn btn-info" id="submit-btn" value="Upload Foto" />
												<img src="../assets/img/ajax-loader.gif" id="loading-img" style="display:none;" alt="Please Wait"/>
											</form>
											<div id="progressbox" style="display:none;"><div id="progressbar"></div ><div id="statustxt">0%</div></div>
											<div align="center" id="output"></div>
										</div>
									</div>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
							</div>
							</div>
							<!-- ===================================== End Edit ===================================== -->
						</div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="komponen/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="komponen/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="komponen/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="komponen/vendor/raphael/raphael.min.js"></script>
    <script src="komponen/vendor/morrisjs/morris.min.js"></script>
    <script src="komponen/data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="komponen/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
	
	<!-- =============================================Progres Bar=================================-->
	<script type="text/javascript" src="../assets/js/jquery.form.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function() { 

		var progressbox     = $('#progressbox');
		var progressbar     = $('#progressbar');
		var statustxt       = $('#statustxt');
		var completed       = '0%';
		
		var options = { 
				target:   '#output',   // target element(s) to be updated with server response 
				beforeSubmit:  beforeSubmit,  // pre-submit callback 
				uploadProgress: OnProgress,
				success:       afterSuccess,  // post-submit callback 
				resetForm: true        // reset the form after successful submit 
			}; 
			
		 $('#MyUploadForm').submit(function() { 
				$(this).ajaxSubmit(options);  			
				// return false to prevent standard browser submit and page navigation 
				return false; 
			});
		
	//when upload progresses	
	function OnProgress(event, position, total, percentComplete)
	{
		//Progress bar
		progressbar.width(percentComplete + '%') //update progressbar percent complete
		statustxt.html(percentComplete + '%'); //update status text
		if(percentComplete>50)
			{
				statustxt.css('color','#fff'); //change status text to white after 50%
			}
	}

	//after succesful upload
	function afterSuccess()
	{
		$('#submit-btn').show(); //hide submit button
		$('#loading-img').hide(); //hide submit button

	}

	//function to check file size before uploading.
	function beforeSubmit(){
		//check whether browser fully supports all File API
	   if (window.File && window.FileReader && window.FileList && window.Blob)
		{

			if( !$('#imageInput').val()) //check empty input filed
			{
				$("#output").html("Masukkan gambar terlebih dahulu!");
				return false
			}
			
			var fsize = $('#imageInput')[0].files[0].size; //get file size
			var ftype = $('#imageInput')[0].files[0].type; // get file type
			
			//allow only valid image file types 
			switch(ftype)
			{
				case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
					break;
				default:
					$("#output").html("<b>"+ftype+"</b> File tidak didukung!");
					return false
			}
			
			//Allowed file size is less than 3 MB (1048576)
			if(fsize>3048576) 
			{
				$("#output").html("<b>"+bytesToSize(fsize) +"</b> File gambar terlalu besar! <br />Maksimal ukuran gambar 3 MB.");
				return false
			}
			
			//Progress bar
			progressbox.show(); //show progressbar
			progressbar.width(completed); //initial value 0% of progressbar
			statustxt.html(completed); //set status text
			statustxt.css('color','#000'); //initial color of status text

					
			$('#submit-btn').hide(); //hide submit button
			$('#loading-img').show(); //hide submit button
			$("#output").html("");  
		}
		else
		{
			//Output error to older unsupported browsers that doesn't support HTML5 File API
			$("#output").html("Browser tidak mendukung fitur ini, tolong upgrade browser anda!");
			return false;
		}
	}

	//function to format bites bit.ly/19yoIPO
	function bytesToSize(bytes) {
	   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	   if (bytes == 0) return '0 Bytes';
	   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
	}

	}); 
	</script>
	<script>
		var htmlobjek;
		$(document).ready(function(){
		  //apabila terjadi event onchange terhadap object <select id=nama_produk>
		 $("#KodeCabang").change(function(){
			var KodeCabang = $("#KodeCabang").val();
			$.ajax({
				url: "../library/Dropdown/ambil-LevelID.php",
				data: "KodeCabang="+KodeCabang,
				cache: false,
				success: function(msg){
					$("#LevelID").html(msg);
				}
			});
		  });
		});
	</script>
</body>

</html>
