<?php include 'akses.php';
$QueryProfil = mysqli_query($koneksi,"SELECT * FROM mstperson WHERE KodePerson='$login_id'");
$DataProfil = mysqli_fetch_assoc($QueryProfil);
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<?php //include 'title.php';?>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="" />
<meta name="author" content="http://bootstraptaste.com" />
<!-- css -->
<link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
<link href="../assets/css/fancybox/jquery.fancybox.css" rel="stylesheet">
<link href="../assets/css/jcarousel.css" rel="stylesheet" />
<link href="../assets/css/flexslider.css" rel="stylesheet" />
<link href="../assets/css/style.css" rel="stylesheet" />
<!-- Datepcker -->
<link rel="stylesheet" href="../library/Datepicker/dist/css/default/zebra_datepicker.min.css" type="text/css">
<!-- Theme skin -->
<link href="../assets/skins/default.css" rel="stylesheet" />

<!-- =======================================================
    Theme Name: Moderna
    Theme URL: https://bootstrapmade.com/free-bootstrap-template-corporate-moderna/
    Author: BootstrapMade
    Author URL: https://bootstrapmade.com
======================================================= -->

</head>
<body>
<div id="wrapper">
	<!-- end header -->	
	<section id="content">
		<div class="row">
			<div class="container col-lg-12">				
				<div class="text-center">
				<h3>Profil</h3>
				<?php 
				if($DataProfil['Foto']==null){
					echo '<img src="../assets/img/no-image.jpg" class="img img-circle">';					
				}else{
					echo '<img src="../assets/img/FotoProfil/thumb_'.$DataProfil['Foto'].'" class="img img-circle">';
				}
				?>
				</div>
				<form action="SimpanFotoProfil.php" onSubmit="return false" method="post" enctype="multipart/form-data" id="MyUploadForm">
					<label>Upload Foto Profil</label>
					<input type="hidden" name="FotoProfil" value="<?php echo $DataProfil['Foto']; ?>">
					<input name="ImageFile" id="imageInput" type="file" class="form-control"/><br/>
					<input type="submit" class="btn btn-theme btn-block" id="submit-btn" value="Upload" />
					<img src="../assets/img/ajax-loader.gif" id="loading-img" style="display:none;" alt="Please Wait"/>
				</form>
				<div id="progressbox" style="display:none;"><div id="progressbar"></div ><div id="statustxt">0%</div></div>
				<div align="center" id="output"></div>
				<hr/>
				<form method="post" id="form1" name='input' action="">
					<fieldset>
						<div class="form-group">
							<label>Nama</label>
							<input class="form-control" placeholder="Nama Lengkap" value="<?php echo $DataProfil['NamaPerson'];?>" name="NamaPerson" type="text" required>
						</div>
						<div class="form-group">
							<label>Tanggal Lahir</label>
							<input class="form-control" id="datepicker" placeholder="Tanggal Lahir (YYYY-MM-DD)" value="<?php echo $DataProfil['TglLahir'];?>" name="TglLahir" type="text" data-zdp_readonly_element="false" required>
						</div>
						<div class="form-group">
							<label>Nomor KTP</label>
							<input class="form-control" placeholder="Nomor KTP" value="<?php echo $DataProfil['NoID_KTP'];?>" name="NomorKTP" type="text" required>
						</div>
						<div class="form-group">
							<label>Nomor Handphone</label>
							<input class="form-control" placeholder="Nomor Handphone" value="<?php echo $DataProfil['NoHP'];?>" name="NoHP" type="text" required>
						</div>
						<div class="form-group">
							<label>Email</label>
							<input class="form-control" placeholder="Email" value="<?php echo $DataProfil['AlamatEmail'];?>" name="Email" type="email" required>
						</div>
						<div class="form-group">
							<label>Username</label>
							<input class="form-control" placeholder="Username" value="<?php echo $DataProfil['UserName'];?>" name="Username" type="text" readonly>
						</div>
						<div class="form-group">
							<label>Password</label>
							<input class="form-control" placeholder="Password" value="<?php echo base64_decode($DataProfil['Password']);?>" name="Password" type="password" required>
						</div>
						<div class="form-group">
							<label>Konfirmasi Password</label>
							<input class="form-control" placeholder="Konfirmasi Password" value="<?php echo base64_decode($DataProfil['Password']);?>" name="KonfirmasiPassword" type="password" required>
						</div><hr/>
						<div class="form-group">
							<label>Kode Bank</label>
							<input class="form-control" placeholder="Bank" value="<?php echo $DataProfil['KodeBankPerson'];?>" name="KodeBank" type="text" required>
						</div>
						<div class="form-group">
							<label>No. Rekening</label>
							<input class="form-control" placeholder="Nomor Rekening" value="<?php echo $DataProfil['NoRekPerson'];?>" name="NoRek" type="text" required>
						</div>
						<div class="form-group">
							<label>Atas Nama Rekening</label>
							<input class="form-control" placeholder="Atas Nama Rekening" value="<?php echo $DataProfil['AtasNama'];?>" name="AtasNamaRek" type="text" required>
						</div>
						<div class="form-group">
							<label>Provinsi</label>
							<select id="KodeProvinsi" name="KodeProvinsi" class="form-control" required>	
								<?php
									echo "<option value=''>--- Provinsi ---</option>";
									$menu = mysqli_query($koneksi,"SELECT * FROM mstprovinsi ORDER BY NamaProvinsi ASC");
									while($kode = mysqli_fetch_array($menu)){
										if($kode['KodeProvinsi']==$DataProfil['KodeProvinsi']){
											echo "<option value=\"".$kode['KodeProvinsi']."\" selected >".$kode['NamaProvinsi']."</option>\n";
										}else{
											echo "<option value=\"".$kode['KodeProvinsi']."\" >".$kode['NamaProvinsi']."</option>\n";
										}
									}
								?>
							</select>
						</div>
						<div class="form-group">
							<label>Kab/Kota</label>
							<select id="KodeKabupaten" name="KodeKabupaten" class="form-control" required>
								<option value=''>--- Kab/Kota ---</option>
								<?php
									$menu = mysqli_query($koneksi,"SELECT * FROM mstkabupaten WHERE KodeProvinsi='".$DataProfil['KodeProvinsi']."' ORDER BY NamaKab ASC");
									while($kode = mysqli_fetch_array($menu)){
										if($kode['KodeKab']==$DataProfil['KodeKab']){
											echo "<option value=\"".$kode['KodeKab']."\" selected >".$kode['NamaKab']."</option>\n";
										}else{
											echo "<option value=\"".$kode['KodeKab']."\" >".$kode['NamaKab']."</option>\n";
										}
									}
								?>
							</select>
						</div>
						<div class="form-group">
							<label>Kecamatan</label>
							<select id="KodeKecamatan" class="form-control" name="KodeKecamatan" required>
								<option value=''>--- Kecamatan ---</option>
								<?php
									$menu = mysqli_query($koneksi,"SELECT * FROM mstkecamatan WHERE KodeKab='".$DataProfil['KodeKab']."' ORDER BY NamaKec ASC");
									while($kode = mysqli_fetch_array($menu)){
										if($kode['KodeKec']==$DataProfil['KodeKec']){
											echo "<option value=\"".$kode['KodeKec']."\" selected >".$kode['NamaKec']."</option>\n";
										}else{
											echo "<option value=\"".$kode['KodeKec']."\" >".$kode['NamaKec']."</option>\n";
										}
									}
								?>
							</select>
						</div>
						<div class="form-group">
							<label>Desa</label>
							<select id="KodeDesa" class="form-control" name="KodeDesa" required>
								<option value=''>--- Desa ---</option>
								<?php
									$menu = mysqli_query($koneksi,"SELECT * FROM mstdesa WHERE KodeKec='".$DataProfil['KodeKec']."' ORDER BY NamaDesa ASC");
									while($kode = mysqli_fetch_array($menu)){
										if($kode['KodeDesa']==$DataProfil['KodeDesa']){
											echo "<option value=\"".$kode['KodeDesa']."\" selected >".$kode['NamaDesa']."</option>\n";
										}else{
											echo "<option value=\"".$kode['KodeDesa']."\" >".$kode['NamaDesa']."</option>\n";
										}
									}
								?>
							</select>
						</div>
						<div class="form-group">
							<label>Alamat Lengkap</label>
							<input class="form-control" placeholder="Alamat Lengkap" value="<?php echo $DataProfil['AlamatLengkap'];?>" name="AlamatLengkap" id="Alamat" type="text" required>
						</div>
						<hr/>
						<!-- ------------------------- Start Map Cari Lokasi ---------------------------------- -->			
						<div class="col-lg-12">
							<p><font color="red">Tandai lokasi rumah Anda dengan akurat.</font></p>
							<div id="mapcanvas" style="height:350px;width:100%;margin-bottom:30px;"></div>
							<input readonly="readonly" name="Latitude" type="hidden" class="form-control input-lg" id="lat">
							<input readonly="readonly" name="Longitude" type="hidden" class="form-control input-lg" id="long" >
							<script>
								function initMap() {
								  var latawal = <?php echo $DataProfil['Koor_Lat']; ?>;
								  var longawal = <?php echo $DataProfil['Koor_Long']; ?>;
								  var map = new google.maps.Map(document.getElementById('mapcanvas'), {
									zoom: 16,
									center: {lat: latawal, lng: longawal}
								  });

								  var marker = new google.maps.Marker({
										map: map,
										position: {lat: latawal, lng: longawal},
										draggable: true,
										title:"Lokasi Anda!"
									  });
								  google.maps.event.addListener(marker, 'drag', function() {
									  updateMarkerPosition(marker.getPosition());
								  }); 
								  var geocoder = new google.maps.Geocoder();

								  var typingTimer;                //timer identifier
								  var doneTypingInterval = 3000;  //time in ms (5 seconds)
								  document.getElementById('Alamat').addEventListener('click', function() {
									clearTimeout(typingTimer);
									if (document.getElementById('Alamat').value) {
										typingTimer = setTimeout(doneTyping, doneTypingInterval);
									}
								  });
								  
								  document.getElementById('KodeKecamatan').addEventListener('change', function() {
									geocodeAddress(geocoder, map, marker);
								  });

								  function doneTyping () {
									geocodeAddress(geocoder, map, marker);
								  }
								}
								function updateMarkerPosition(latlon) {
								  document.getElementById('lat').value = [latlon.lat()]
								  document.getElementById('long').value = [latlon.lng()]
								} 
								function geocodeAddress(geocoder, resultsMap, resultsMarker) {
								  var alamat = document.getElementById('Alamat').value;
								  var kab = document.getElementById('KodeKecamatan').options[document.getElementById('KodeKecamatan').selectedIndex].text;
								  var alamatLengkap = alamat + ' ' + kab;
								  geocoder.geocode({'address': alamatLengkap}, function(results, status) {
									if (status === 'OK') {
									  resultsMap.setCenter(results[0].geometry.location);
									  resultsMarker.setPosition(results[0].geometry.location);
									  updateMarkerPosition(results[0].geometry.location);
									  google.maps.event.addListener(resultsMarker, 'drag', function() {
										  updateMarkerPosition(resultsMarker.getPosition());
									  }); 
									}
								  });
								}
							  </script>
							  <?php include '../library/Maps.php';?>
						</div>
						<!-- ------------------------- End Map Cari Lokasi ---------------------------------- -->	
						<!--<button type="submit" name="submit" class="btn btn-theme btn-block">Daftar</button>-->
						<input type="submit" class="btn btn-block btn-theme" value="Simpan">
					</fieldset>
				</form>
				<div class="msg alert alert-info text-left" style="display:none"></div>
				<div class="clearfix"></div>
			</div>
		</div>
	</section>
	<?php //include 'footer.php'; ?>
</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../assets/js/jquery.js"></script>
<script src="../assets/js/jquery.easing.1.3.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/jquery.fancybox.pack.js"></script>
<script src="../assets/js/jquery.fancybox-media.js"></script>
<script src="../assets/js/google-code-prettify/prettify.js"></script>
<script src="../assets/js/portfolio/jquery.quicksand.js"></script>
<script src="../assets/js/portfolio/setting.js"></script>
<script src="../assets/js/jquery.flexslider.js"></script>
<script src="../assets/js/animate.js"></script>
<script src="../assets/js/custom.js"></script>
<!-- DatePicker -->
<script type="text/javascript" src="../library/Datepicker/dist/zebra_datepicker.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#datepicker').Zebra_DatePicker();
	});
</script>

<script>
	var htmlobjek;
	$(document).ready(function(){
	  //apabila terjadi event onchange terhadap object <select id=nama_produk>
	 $("#KodeProvinsi").change(function(){
		var KodeProvinsi = $("#KodeProvinsi").val();
		$.ajax({
			url: "../library/Dropdown/ambil-kabupaten.php",
			data: "KodeProvinsi="+KodeProvinsi,
			cache: false,
			success: function(msg){
				$("#KodeKabupaten").html(msg);
			}
		});
	  });
	  
	  $("#KodeKabupaten").change(function(){
		var KodeKabupaten = $("#KodeKabupaten").val();
		$.ajax({
			url: "../library/Dropdown/ambil-kecamatan.php",
			data: "KodeKabupaten="+KodeKabupaten,
			cache: false,
			success: function(msg){
				$("#KodeKecamatan").html(msg);
			}
		});
	  });
	  
	  $("#KodeKecamatan").change(function(){
		var KodeKecamatan = $("#KodeKecamatan").val();
		$.ajax({
			url: "../library/Dropdown/ambil-desa.php",
			data: "KodeKecamatan="+KodeKecamatan,
			cache: false,
			success: function(msg){
				$("#KodeDesa").html(msg);
			}
		});
	  });
	  
	});
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#form1').on('submit', function(event){
		event.preventDefault();
		var formData = new FormData($('#form1')[0]);

		$('.msg').hide();
		$('.progress').show();
		
		$.ajax({
			type : 'POST',
			url : 'SimpanProfil.php',
			data : formData,
			processData : false,
			contentType : false,
			success : function(response){
				//$('#form1')[0].reset();
				$('.progress').hide();
				$('.msg').show();
				if(response == "Maaf Username sudah ada, silahkan ganti Username Anda !"){
					var msg = response;
					$('.msg').html(msg);
				}else if(response === "Maaf Email sudah ada, silahkan ganti Email Anda !"){
					var msg = response;
					$('.msg').html(msg);
				}else if(response === "Maaf Kode Sponsor Tidak Valid!"){
					var msg = response;
					$('.msg').html(msg);
				}else if(response === "Proses Edit Data gagal !"){
					var msg = response;
					$('.msg').html(msg);
				}else if(response === "Maaf Password Anda Salah!"){
					var msg = response;
					$('.msg').html(msg);
				}else{
					//$('#form1')[0].reset();
					var msg = response;
					$('.msg').html(msg);
					//alert(msg);document.location="Library.php";
				}
			}
		});
	});
});
</script>

<!-- =============================================Progres Bar=================================-->
<script type="text/javascript" src="../assets/js/jquery.form.min.js"></script>
<script type="text/javascript">
$(document).ready(function() { 

	var progressbox     = $('#progressbox');
	var progressbar     = $('#progressbar');
	var statustxt       = $('#statustxt');
	var completed       = '0%';
	
	var options = { 
			target:   '#output',   // target element(s) to be updated with server response 
			beforeSubmit:  beforeSubmit,  // pre-submit callback 
			uploadProgress: OnProgress,
			success:       afterSuccess,  // post-submit callback 
			resetForm: true        // reset the form after successful submit 
		}; 
		
	 $('#MyUploadForm').submit(function() { 
			$(this).ajaxSubmit(options);  			
			// return false to prevent standard browser submit and page navigation 
			return false; 
		});
	
//when upload progresses	
function OnProgress(event, position, total, percentComplete)
{
	//Progress bar
	progressbar.width(percentComplete + '%') //update progressbar percent complete
	statustxt.html(percentComplete + '%'); //update status text
	if(percentComplete>50)
		{
			statustxt.css('color','#fff'); //change status text to white after 50%
		}
}

//after succesful upload
function afterSuccess()
{
	$('#submit-btn').show(); //hide submit button
	$('#loading-img').hide(); //hide submit button

}

//function to check file size before uploading.
function beforeSubmit(){
	//check whether browser fully supports all File API
   if (window.File && window.FileReader && window.FileList && window.Blob)
	{

		if( !$('#imageInput').val()) //check empty input filed
		{
			$("#output").html("Masukkan gambar terlebih dahulu!");
			return false
		}
		
		var fsize = $('#imageInput')[0].files[0].size; //get file size
		var ftype = $('#imageInput')[0].files[0].type; // get file type
		
		//allow only valid image file types 
		switch(ftype)
		{
			case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
				break;
			default:
				$("#output").html("<b>"+ftype+"</b> File tidak didukung!");
				return false
		}
		
		//Allowed file size is less than 3 MB (1048576)
		if(fsize>3048576) 
		{
			$("#output").html("<b>"+bytesToSize(fsize) +"</b> File gambar terlalu besar! <br />Maksimal ukuran gambar 3 MB.");
			return false
		}
		
		//Progress bar
		progressbox.show(); //show progressbar
		progressbar.width(completed); //initial value 0% of progressbar
		statustxt.html(completed); //set status text
		statustxt.css('color','#000'); //initial color of status text

				
		$('#submit-btn').hide(); //hide submit button
		$('#loading-img').show(); //hide submit button
		$("#output").html("");  
	}
	else
	{
		//Output error to older unsupported browsers that doesn't support HTML5 File API
		$("#output").html("Browser tidak mendukung fitur ini, tolong upgrade browser anda!");
		return false;
	}
}

//function to format bites bit.ly/19yoIPO
function bytesToSize(bytes) {
   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
   if (bytes == 0) return '0 Bytes';
   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}

}); 
</script>
</body>
</html>