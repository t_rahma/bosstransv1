<?php
include ('akses.php');
$fitur_id = 22;
include ('login/lock-menu.php');
date_default_timezone_set('Asia/Jakarta');

if(base64_decode(@$_GET['aksi'])=='MutasiDana'){
	//membuat id dompetperson
	$year_perusahaan	 = date('Y');
	$sql_perusahaan 	 = mysqli_query($koneksi,'SELECT RIGHT(NoTrPengiriman,6) AS kode FROM trpengirimandana WHERE NoTrPengiriman LIKE "%'.$year_perusahaan.'%" ORDER BY NoTrPengiriman DESC LIMIT 1');  
	$num_perusahaan	 = mysqli_num_rows($sql_perusahaan);
	 
	if($num_perusahaan <> 0){
	 $data_perusahaan = mysqli_fetch_array($sql_perusahaan);
	 $kode_perusahaan = $data_perusahaan['kode'] + 1;
	 }else{
	 $kode_perusahaan = 1;
	 }
	 
	//mulai bikin kode
	 $bikin_kode_bank_perusahaan = str_pad($kode_perusahaan, 6, "0", STR_PAD_LEFT);
	 $kode_jadi_bank_perusahaan	 = "KRM-".$year_perusahaan."-".$bikin_kode_bank_perusahaan;
	 
	 include ('../library/kode-log-server.php');
	 $DateTime = date('Y-m-d H:i:s');
	 
	 $InsertData = mysqli_query($koneksi,"INSERT INTO trpengirimandana (NoTrPengiriman,KodeCabang,TanggalTransaksi,CabangPenerima, NominalTransfer) VALUES ('$kode_jadi_bank_perusahaan','$login_cabang','$DateTime','".base64_decode($_GET['cab'])."','".base64_decode($_GET['tr'])."')");
	 if($InsertData){
		 //update uang yang ada di bulan tersebut
		 if($IsServer=='1'){
			 //update trorder makanan
			 mysqli_query($koneksi,"UPDATE trordermakanan SET IsKirimUang='1',NoTrPengiriman='$kode_jadi_bank_perusahaan' WHERE (StatusOrder='Selesai' OR StatusOrder='Dikirim') AND date_format(TglKirim, '%Y-%m')='".base64_decode($_GET['tgl'])."' AND KodeCabang='".base64_decode($_GET['cab'])."' AND IsKirimUang is null");
			 
			 //update dompet perusahaan
			 mysqli_query($koneksi,"UPDATE dompetperusahaan SET IsKirimUang='1',NoTrPengiriman='$kode_jadi_bank_perusahaan' WHERE date_format(TanggalTransaksi, '%Y-%m')='".base64_decode($_GET['tgl'])."' AND KodeCabang='".base64_decode($_GET['cab'])."' AND UserName is null AND IsKirimUang is null");
		 }else{
			 //update iklan
			mysqli_query($koneksi,"UPDATE iklan SET IsKirimUang='1', NoTrPengiriman='$kode_jadi_bank_perusahaan' WHERE date_format(TglMulaiIklan, '%Y-%m')='".base64_decode($_GET['tgl'])."' AND KodeCabang='$login_cabang' AND IsKirimUang is null");
			
			 //update dompet perusahaan
			mysqli_query($koneksi,"UPDATE dompetperusahaan SET IsKirimUang='1', NoTrPengiriman='$kode_jadi_bank_perusahaan' WHERE date_format(TanggalTransaksi, '%Y-%m')='".base64_decode($_GET['tgl'])."' AND KodeCabang='$login_cabang' AND IsTrOffline='1' AND IsKirimUang is null");
		 }
		 
		 mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
		VALUES ('$kode_jadi_log','$DateTime','Update Data','Transaksi Mutasi Dana : Dari Cabang $login_cabang Ke ".base64_decode($_GET['cab'])." Ref.$kode_jadi_bank_perusahaan ','$login_id','$login_cabang')");
		echo '<script language="javascript">document.location="TrMutasiDana.php"; </script>';
	 }
}

if(isset($_POST['cari'])){
	$_SESSION['KeywordCabang'] 	= $_POST['KeywordCabang'];
	$_SESSION['keyword'] 		= $_POST['keyword'];
}

if(@$_SESSION['KeywordCabang']!=null){
	$cabang 	= @$_SESSION['KeywordCabang'];
	$DateTime 	= @$_SESSION['keyword'];
}else{
	if($IsServer=='1'){
		$cabang 	= '';
	}else{
		$cabang = 'C0001';
	}
	$DateTime 	= date('Y');
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <?php include 'view/title.php' ?>

    <!-- Bootstrap Core CSS -->
    <link href="komponen/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="komponen/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="komponen/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="komponen/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="komponen/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda sudah transfer dana ke cabang ini?")
			if (answer == true){
				window.location = "TrMutasiDana.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
	<!-- Datepcker -->
	<link rel="stylesheet" href="../library/Datepicker/dist/css/default/zebra_datepicker.min.css" type="text/css">
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
		<?php include 'view/menu.php' ; ?>
		</nav>
		
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Transaksi Mutasi Dana</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
							<div class="col-lg-6">
								<label>Cabang</label>
								<form method="post">
									<div class="form-group col-lg-8">
										<select name="KeywordCabang" class="form-control" required>	
											<?php
												if($IsServer=='1'){
													echo '<option value="">-- Cabang Perusahaan --</option>';
													$menu = mysqli_query($koneksi,"SELECT a.*,b.NamaProvinsi,c.NamaKab FROM mstcabang a JOIN mstprovinsi b ON a.KodeProvinsi=b.KodeProvinsi JOIN mstkabupaten c ON a.KodeKab=c.KodeKab AND KodeCabang!='C0001' ORDER BY a.KodeCabang ASC");
													while($kode = mysqli_fetch_array($menu)){
														if($kode['KodeCabang']==$cabang){
															echo "<option value=\"".$kode['KodeCabang']."\" selected >".$kode['NamaProvinsi']." - ".$kode['NamaKab']."</option>\n";
														}else{
															echo "<option value=\"".$kode['KodeCabang']."\">".$kode['NamaProvinsi']." - ".$kode['NamaKab']."</option>\n";
														}
													}
												}else{
													$menu = mysqli_query($koneksi,"SELECT a.*,b.NamaProvinsi,c.NamaKab FROM mstcabang a JOIN mstprovinsi b ON a.KodeProvinsi=b.KodeProvinsi JOIN mstkabupaten c ON a.KodeKab=c.KodeKab AND KodeCabang='C0001' ORDER BY a.KodeCabang ASC");
													while($kode = mysqli_fetch_array($menu)){
														echo "<option value=\"".$kode['KodeCabang']."\" selected>SERVER PUSAT</option>\n";
													}
												}
												
											?>
										</select>
									</div>
									<div class="form-group input-group col-lg-4">
										<input type="text" name="keyword" class="form-control" id="datepicker" value="<?php echo $DateTime;?>" placeholder="Tanggal Transaksi">
										<span class="input-group-btn">
											<button class="btn btn-large btn-info" type="submit" name="cari">Check</button>
										 </span>
									</div>
								</form>
								<label>Jumlah fisik dana yang belum disetorkan</label>
								<div class="table-responsive">
									<table width="100%" class="table table-hover">
										<tbody>
											<?php 
											$no=1;
											$BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
											for($i=0;$i<12;$i++){ 
												//tanggal jika 1-9 maka tambah 0
												$nomor = $no++;
												if($nomor<10){
													$bulan = $DateTime."-0".$nomor;
												}else{
													$bulan = $DateTime."-".$nomor;
												}
												
												if($IsServer=='1'){
													$Query = mysqli_query($koneksi,"SELECT SUM(debet) as Total FROM ( 
													SELECT SUM(PorsiPerusahaan) as debet FROM trordermakanan WHERE (StatusOrder='Selesai' OR StatusOrder='Dikirim') AND date_format(TglKirim, '%Y-%m')='$bulan' AND KodeCabang='$cabang' AND IsKirimUang is null 
													UNION ALL 
													SELECT SUM(Debet) as debet FROM dompetperusahaan WHERE date_format(TanggalTransaksi, '%Y-%m')='$bulan' AND KodeCabang='$cabang' AND UserName is null AND IsKirimUang is null ) as Q1");
													$DataSaldoAwal = mysqli_fetch_assoc($Query);
												}else{
													$Query = mysqli_query($koneksi,"SELECT SUM(debet) as Total FROM (
													SELECT SUM(PorsiPusat) as debet FROM iklan WHERE date_format(TglMulaiIklan, '%Y-%m')='$bulan' AND KodeCabang='$login_cabang' AND IsKirimUang is null
													UNION ALL
													SELECT SUM(PorsiPusat) as debet FROM dompetperusahaan WHERE date_format(TanggalTransaksi, '%Y-%m')='$bulan' AND KodeCabang='$login_cabang' AND IsTrOffline='1' AND IsKirimUang is null) as Q1");
													$DataSaldoAwal = mysqli_fetch_assoc($Query);
												}
												
												if($DataSaldoAwal['Total']!=null){
											?>
												<tr>
													<td><?php echo $BulanIndo[$i]; echo ' '; echo $DateTime;?></td>
													<td align="center">Rp. <?php echo number_format($DataSaldoAwal['Total']); ?>,-</td>
													<td align="center">
														<a href="TrMutasiDana.php?cab=<?php echo base64_encode($cabang);?>&tr=<?php echo base64_encode($DataSaldoAwal['Total']);?>&tgl=<?php echo base64_encode($bulan)?>&aksi=<?php echo base64_encode('MutasiDana');?>" onclick='return confirmation()'><span class="btn btn-md btn-danger">Mutasi Dana</span>
													</td>
												</tr>
											<?php 
												}
											}
											?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="komponen/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="komponen/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="komponen/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="komponen/vendor/raphael/raphael.min.js"></script>
    <script src="komponen/vendor/morrisjs/morris.min.js"></script>
    <script src="komponen/data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="komponen/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>

<!-- DatePicker -->
<script type="text/javascript" src="../library/Datepicker/dist/zebra_datepicker.src.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#datepicker').Zebra_DatePicker({format: 'Y'});
	});
</script>
</body>
</html>
