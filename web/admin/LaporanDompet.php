<?php
include ('akses.php');
include ('../library/tgl-indo.php');
$DateTime = date('Y-m');
date_default_timezone_set('Asia/Jakarta');

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <?php include 'view/title.php' ?>

    <!-- Bootstrap Core CSS -->
    <link href="komponen/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="komponen/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="komponen/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="komponen/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="komponen/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Jika Anda verifikasi tarik tunai, pastikan anda sudah mentransfer sejumlah uang ke rekening klien!")
			if (answer == true){
				window.location = "TarikTunai.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
	<script type="text/javascript">
		function Hapusconfirmation() {
			var answer = confirm("Transaksi tarik tunai akan dihapus! ")
			if (answer == true){
				window.location = "TarikTunai.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
	<!-- Datepcker -->
	<link rel="stylesheet" href="../library/Datepicker/dist/css/default/zebra_datepicker.min.css" type="text/css">
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
		<?php include 'view/menu.php' ; ?>
		</nav>
		
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">Laporan Transaksi Dompet
					<?php if(@$_REQUEST['Tanggal1']==null AND @$_REQUEST['Tanggal2']==null){ echo 'Bulan ini'; } ?></h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
							<form method="post" action="">
								<div class="form-group col-lg-3 ">
									<input type="text" name="keyword" class="form-control" value="<?php echo @$_REQUEST['keyword']; ?>" placeholder="Cari Nama">
								</div>
								<div class="form-group col-lg-3 ">
									<input type="text" name="Tanggal1" class="form-control" id="datepicker2" value="<?php echo @$_REQUEST['Tanggal1']; ?>" placeholder="Tanggal Awal">
								</div>
								<div class="form-group input-group col-lg-3">
									<input type="text" name="Tanggal2" class="form-control" id="datepicker" value="<?php echo @$_REQUEST['Tanggal2']; ?>" placeholder="Tanggal Akhir">
									
									<span class="input-group-btn">
										<button class="btn btn-large btn-info" type="submit">Cari</button>
									 </span>
								</div>
							</form>
							<!-- ===================================== tabel ==================================== -->
							<div class="table-responsive">
								<table width="100%" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>No</th>
											<th>No.Transaksi</th>
											<th>Tanggal Transaksi</th>
											<th>Nama</th>
											<th>Jenis Transaksi</th>
											<th>Nominal</th>
										</tr>
									</thead>
									<?php
										include '../library/pagination1.php';
										// mengatur variabel reload dan sql
										$kosong=null;
										if(@$_REQUEST['Tanggal1']!=null AND @$_REQUEST['Tanggal2']!=null AND @$_REQUEST['keyword']==null){
											// jika ada kata kunci pencarian (artinya form pencarian disubmit dan tidak kosong)pakai ini
											$reload = "Dompet.php?pagination=true";
											$sql =  "SELECT a.NoTransaksi,a.TanggalTransaksi, a.Keterangan, IF(a.Debet <> 0, a.Debet, a.Kredit) AS value, b.NamaPerson as NamaKlien FROM dompetperson a JOIN mstperson b ON a.KodePerson=b.KodePerson  WHERE a.IsVerified=1 AND date_format(a.TanggalTransaksi, '%Y-%m-%d') BETWEEN '".$_REQUEST['Tanggal1']."' AND '".$_REQUEST['Tanggal2']."' ";
											
											
											$sql.= " ORDER BY a.NoTransaksi ASC";
											$result = mysqli_query($koneksi,$sql);
										}elseif(@$_REQUEST['keyword']!=null AND @$_REQUEST['Tanggal1']!=null AND @$_REQUEST['Tanggal2']!=null){
										//jika tidak ada pencarian pakai ini
											$reload = "Dompet.php?pagination=true";
											$sql =  "SELECT a.NoTransaksi,a.TanggalTransaksi, a.Keterangan, IF(a.Debet <> 0, a.Debet, a.Kredit) AS value, b.NamaPerson as NamaKlien FROM dompetperson a JOIN mstperson b ON a.KodePerson=b.KodePerson  WHERE b.NamaPerson LIKE '%".$_REQUEST['keyword']."%' AND a.IsVerified=1 AND date_format(a.TanggalTransaksi, '%Y-%m-%d') BETWEEN '".$_REQUEST['Tanggal1']."' AND '".$_REQUEST['Tanggal2']."' ";
											
											
											$sql.= " ORDER BY a.NoTransaksi ASC";
											$result = mysqli_query($koneksi,$sql);
										}elseif(@$_REQUEST['keyword']!=null AND @$_REQUEST['Tanggal1']==null AND @$_REQUEST['Tanggal2']==null){
										//jika tidak ada pencarian pakai ini
											$reload = "Dompet.php?pagination=true";
											$sql =  "SELECT a.NoTransaksi,a.TanggalTransaksi, a.Keterangan, IF(a.Debet <> 0, a.Debet, a.Kredit) AS value, b.NamaPerson as NamaKlien FROM dompetperson a JOIN mstperson b ON a.KodePerson=b.KodePerson  WHERE b.NamaPerson LIKE '%".$_REQUEST['keyword']."%' AND a.IsVerified=1  ";
											
											
											$sql.= " ORDER BY a.NoTransaksi ASC";
											$result = mysqli_query($koneksi,$sql);
										}else{
										//jika tidak ada pencarian pakai ini
											$reload = "Dompet.php?pagination=true";
											$sql =  "SELECT a.NoTransaksi,a.TanggalTransaksi, a.Keterangan, IF(a.Debet <> 0, a.Debet, a.Kredit) AS value, b.NamaPerson as NamaKlien FROM dompetperson a JOIN mstperson b ON a.KodePerson=b.KodePerson  WHERE a.IsVerified=1 AND date_format(a.TanggalTransaksi, '%Y-%m')='$DateTime' ORDER BY a.TanggalTransaksi ASC";
											$result = mysqli_query($koneksi,$sql);
										}
										
										//pagination config start
										$rpp = 100000; // jumlah record per halaman
										$page = intval(@$_GET["page"]);
										if($page<=0) $page = 1;  
										$tcount = mysqli_num_rows($result);
										$tpages = ($tcount) ? ceil($tcount/$rpp) : 1; // total pages, last page number
										$count = 0;
										$i = ($page-1)*$rpp;
										$no_urut = ($page-1)*$rpp;
										//pagination config end				
									?>
									<tbody>
										<?php
										while(($count<$rpp) && ($i<$tcount)) {
											mysqli_data_seek($result,$i);
											$data = mysqli_fetch_array($result);
										?>
										<tr class="odd gradeX">
											<td width="20px">
												<?php echo ++$no_urut;?> 
											</td>
											<td>
												<?php echo $data['NoTransaksi']; ?>
											</td>
											<td>
												<?php echo TanggalIndo($data['TanggalTransaksi']); ?>
												<?php echo substr($data['TanggalTransaksi'],11,8); ?>
											</td>
											<td>
												<?php echo $data['NamaKlien']; ?>
											</td>
											<td>
												<?php echo $data['Keterangan']; ?>
											</td>
											<td align="right">
												<?php
													$pendapatan[] = $data['value'];
													echo number_format($data['value']);
												?>,-
											</td>
										</tr>
										<?php
											$i++; 
											$count++;
										}
										
										// if(@$pendapatan==null){
											// echo '<tr class="odd gradeX">
													// <td colspan="6" align="center">Tidak Ada Data</td>
													// </tr>';
										// }else{
											// $Total = array_sum($pendapatan);
											// echo '
											// <tr class="odd gradeX">
												// <td colspan="5" align="center">
													// <strong>Total Pendapatan</strong>
												// </td>
												// <td align="right">
													// <strong>'.number_format($Total).',-</strong>
												// </td>
											// </tr>
											// ';
										// }
										?>
										
										
									</tbody>
								</table>
								
								<!-- ===================================== tabel ==================================== -->
							</div>
							</div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="komponen/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="komponen/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="komponen/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="komponen/vendor/raphael/raphael.min.js"></script>
    <script src="komponen/vendor/morrisjs/morris.min.js"></script>
    <script src="komponen/data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="komponen/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>

<!-- DatePicker -->
<script type="text/javascript" src="../library/Datepicker/dist/zebra_datepicker.src.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#datepicker').Zebra_DatePicker({format: 'Y-m-d'});
		$('#datepicker2').Zebra_DatePicker({format: 'Y-m-d'});
	});
</script>
</body>
</html>
