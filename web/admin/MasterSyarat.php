<?php
include ('akses.php');
/* $fitur_id = 13;
include ('login/lock-menu.php'); */

//kode jadi untuk server log
//include ('../library/kode-log-server.php');
include ('../library/tgl-indo.php');
$DateTime = date('Y-m-d H:i:s a');

include('../library/config.php');
date_default_timezone_set('Asia/Jakarta');

//Edit Data
$Tampil = mysqli_query($koneksi,"SELECT * FROM kontenweb WHERE JenisKonten='S&K' AND KodeKonten='KNT-0000001'");
$Data = mysqli_fetch_assoc($Tampil);

if(isset($_POST['EditData'])){
	$Edit = mysqli_query($koneksi,"UPDATE kontenweb SET Judul='".$_POST['Judul']."',Isi='".$_POST['Isi']."',UserName='$login_id' WHERE JenisKonten='S&K' AND KodeKonten='KNT-0000001' ");
	if($Edit){
		echo '<script language="javascript">document.location="MasterSyarat.php"; </script>';
	}else{
		echo '<script language="javascript">alert("Edit Data Gagal !"); document.location="MasterSyarat.php"; </script>';
	}
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <?php include 'view/title.php' ?>

    <!-- Bootstrap Core CSS -->
    <link href="komponen/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="komponen/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="komponen/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="komponen/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="komponen/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<!-- Datepcker -->
	<link rel="stylesheet" href="../library/Datepicker/dist/css/default/zebra_datepicker.min.css" type="text/css">
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
		<?php include 'view/menu.php' ; ?>
		</nav>
		
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Syarat & Ketentuan</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
						<!-- Nav tabs -->
                            <ul class="nav nav-pills">
                                <li class="active"><a href="#home-pills" data-toggle="tab">Edit S & K</a></li>
                            </ul>
							<div class="tab-content">
								<div class="tab-pane fade in active" id="home-pills"><br/>
									<form method="post" action="">
										<div class="form-group col-lg-6">
											<label>Judul</label>
											<input type="text" class="form-control" name="Judul" value="<?php echo $Data['Judul']; ?>" readonly>
										</div>
										<div class="form-group col-lg-12">
											<label>Isi</label>
											<textarea class="ckeditor" name="Isi" class="form-control" rows="4"><?php echo $Data['Isi']; ?></textarea>
										</div>
										<div class="col-lg-3">
											<button type="submit" class="btn btn-md btn-block btn-warning" name="EditData">Simpan Edit</button>
										</div>
									</form>
								</div>
							</div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="komponen/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="komponen/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="komponen/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="komponen/vendor/raphael/raphael.min.js"></script>
    <script src="komponen/vendor/morrisjs/morris.min.js"></script>
    <script src="komponen/data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="komponen/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
		<!-- ckeditor JS -->
   <script type="text/javascript" src="../library/ckeditor/ckeditor.js"></script>
	<!-- DatePicker -->
<script type="text/javascript" src="../library/Datepicker/dist/zebra_datepicker.src.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#datepicker1').Zebra_DatePicker({format: 'Y-m-d H:i'});
		$('#datepicker2').Zebra_DatePicker({format: 'Y-m-d H:i'});
		$('#datepicker3').Zebra_DatePicker({format: 'Y-m-d H:i'});
		$('#datepicker4').Zebra_DatePicker({format: 'Y-m-d H:i'});
	});
</script>

<!-- membuat dropdown bertingkat -->
<script>
	var htmlobjek;
	$(document).ready(function(){
	  //apabila terjadi event onchange terhadap object <select id=nama_produk>
	  $("#KodeNegara").change(function(){
		var KodeNegara = $("#KodeNegara").val();
		$.ajax({
			url: "upload/ambil-menu.php",
			data: "KodeRM="+KodeNegara,
			cache: false,
			success: function(msg){
				//jika data sukses diambil dari server kita tampilkan
				//di <select id=kota>
				$("#KodeProvinsi").html(msg);
			}
		});
	  });
	});
</script>
<script>
	/* var htmlobjek;
	$(document).ready(function(){
	  //apabila terjadi event onchange terhadap object <select id=nama_produk>
	  $("#KodeNegara2").change(function(){
		var KodeNegara2 = $("#KodeNegara2").val();
		$.ajax({
			url: "upload/ambil-menu.php",
			data: "KodeRM="+KodeNegara2,
			cache: false,
			success: function(msg){
				//jika data sukses diambil dari server kita tampilkan
				//di <select id=kota>
				$("#KodeProvinsi2").html(msg);
			}
		});
	  });
	}); */
</script>
</body>

</html>
