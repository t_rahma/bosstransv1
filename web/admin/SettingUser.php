<?php
include ('akses.php');
//$fitur_id = 1;
//cek apakah user mempunyai hak akses menu ini
//include ('login/lock-menu.php');
//kode jadi untuk server log
// include ('../library/kode-log-server.php');
$DateTime = date('Y-m-d H:i:s a');

include('../library/config.php');
date_default_timezone_set('Asia/Jakarta');

$UserName			= @$_POST['UserName'];
$UserPsw		 	= base64_encode(@$_POST['UserPsw']);
$ActualName		 	= @$_POST['ActualName'];
$Address		 	= @$_POST['Address'];
$Phone			 	= @$_POST['Phone'];
//$HPNo			 	= $_POST['HPNo'];
$Email			 	= @$_POST['Email'];
$IsAktif		 	= 1;
 
 //===========procedure Update data=============//
 $id = base64_decode($_GET['id']);
 $edit = mysqli_query($koneksi,"select * from userlogin where UserName = '$id' AND KodeCabang='$login_cabang'");
 while($row = mysqli_fetch_array($edit)){
	$username 	= $row['UserName'];
	$userpsw	= $row['UserPsw'];
	$nama		= $row['ActualName'];
	$alamat		= $row['Address'];
	$no_hp		= $row['Phone'];
	$email		= $row['Email'];
	$is_aktif	= $row['IsAktif'];
	$foto		= $row['Foto'];
 }
 
 if(isset($_POST['submit_edit'])){
	$query = mysqli_query($koneksi,"UPDATE userlogin SET UserPsw ='$UserPsw',ActualName='$ActualName',Address='$Address',Phone='$Phone',IsAktif='$IsAktif',Email='$Email' WHERE UserName = '$id' AND KodeCabang='$login_cabang'");
		if($query){
			// mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
			// VALUES ('$kode_jadi_log','$DateTime','Edit Data','Edit User Login : $id:$UserPsw:$ActualName:$Address:$Phone:$Email','$login_id','$login_cabang')");
			echo '<script language="javascript">alert("Edit Data berhasil disimpan !"); document.location="SettingUser.php?id='.base64_encode($id).'"; </script>';
		}else{
			echo '<script language="javascript">alert("Edit data gagal !"); document.location="SettingUser.php?id='.base64_encode($id).'"; </script>';
		}
 }
 
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <?php include 'view/title.php' ?>

    <!-- Bootstrap Core CSS -->
    <link href="komponen/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="komponen/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="komponen/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="komponen/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="komponen/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<!-- css progress upload gambar   --> 
	<link href="../css/style.css" rel="stylesheet">
	
	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda Yakin Untuk Menghapus Data . . . ?")
			if (answer == true){
				window.location = "AccessLevel.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
	
	<script type="text/javascript">
		function confirmation2() {
			var answer = confirm("Apakah Anda Yakin Mengaktifkan Fitur ini . . . ?")
			if (answer == true){
				window.location = "AccessLevel.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
	
	<script type="text/javascript">
		function confirmation3() {
			var answer = confirm("Apakah Anda Yakin Non Aktifkan Fitur ini . . . ?")
			if (answer == true){
				window.location = "AccessLevel.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
		<?php include 'view/menu.php' ; ?>
		</nav>
		
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">User Profile</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
						<!-- Nav tabs 
                            <ul class="nav nav-pills">
                                <li <?php if($id==null){echo 'class="active"';} ?>><a href="#home-pills" data-toggle="tab">Data User Login</a>
                                </li>
                                <li><a href="#tambah-user" data-toggle="tab">Tambah Data</a>
                                </li>
								<li <?php if($id!=null){echo 'class="active"';} ?>><a href="#edit-user" data-toggle="tab">Edit Data</a>
                                </li>
                            </ul>-->
						<div class="tab-content">
							<!-- ===================================== Start Edit ===================================== -->
							<div class="tab-pane fade  <?php if($id!=null){echo 'in active';} ?>" id="edit-user">
							<div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
								<!-- kondisi jika $id == null maka tambah data -->
								<?php if($id==null):?>
									<h2>Edit User Login </h2>
										<form role="form" method="post" id="edit"  enctype="multipart/form-data">
											<div class="form-group">
												<label>Nama*</label>
												<input type="text" class="form-control" name="ActualName" disabled>
											</div>
											<div class="form-group">
												<label>Alamat*</label>
												<input type="text" class="form-control" name="Address" disabled>
											</div>
											<div class="form-group">
												<label>Phone*</label>
												<input type="text" class="form-control" name="Phone" disabled>
											</div>
											<div class="form-group">
												<label>Email*</label>
												<input type="email" class="form-control" name="Email" disabled>
											</div>
											<div class="form-group">
												<label>Username*</label>
												<input type="text" class="form-control" name="UserName" disabled>
											</div>
											<div class="form-group">
												<label>Password*</label>
												<input type="password" class="form-control" name="UserPsw" disabled>
											</div>
											<button type="submit" name="submit" value="simpan" class="btn btn-info" disabled>Simpan</button>
											<button type="reset" name="reset" value="reset" class="btn btn-danger" disabled>Reset</button>
										</form>
								<?php elseif($id!=null):?>
										<form role="form" method="post" id="edit"  enctype="multipart/form-data">
											<div class="form-group">
												<label>Nama*</label>
												<input type="text" class="form-control" name="ActualName" value="<?php echo $nama; ?>" required>
											</div>
											<div class="form-group">
												<label>Alamat*</label>
												<input type="text" class="form-control" name="Address" value="<?php echo $alamat; ?>" required>
											</div>
											<div class="form-group">
												<label>Phone*</label>
												<input type="text" class="form-control" name="Phone" value="<?php echo $no_hp; ?>" required>
											</div>
											<div class="form-group">
												<label>Email*</label>
												<input type="email" class="form-control" name="Email" value="<?php echo $email; ?>" required>
											</div>
											<div class="form-group">
												<label>Password*</label>
												<input type="password" class="form-control" name="UserPsw" value="<?php echo base64_decode($userpsw); ?>" required>
											</div>
											<button type="submit" name="submit_edit" value="simpan" class="btn btn-info">Simpan</button>
											<button type="reset" name="reset" value="reset" class="btn btn-danger">Reset</button>
										</form>
								<?php endif?>
                                </div>
                                <div class="col-lg-6"  <?php if($id==null){echo 'style="visibility: hidden;"';} ?>>
									<h2>&nbsp;</h2>
									<div class="text-center">
										<div class="text-center">
										<?php if($foto==null){
											echo '<img src="../../andro/img/no_image.jpg" class="img img-responsive img-thumbnail" alt="Image User" width="50%">';
										}else{
											echo '<img src="../../andro/foto_profil/'.$foto.'" class="img img-responsive img-thumbnail" alt="Image User" width="50%">';
										}?>
										</div>
									</div>
									<div id="upload-wrapper">
										<div align="center">
											<h3>Upload Foto Profil</h3>
											<label>Upload gambar dengan type .jpg dengan ukuran max 3 mb</label>
											<form action="upload/processupload-userlogin.php?id=<?php echo base64_encode($id);?>&foto=<?php echo base64_encode($foto);?>" onSubmit="return false" method="post" enctype="multipart/form-data" id="MyUploadForm">
												<input name="ImageFile" id="imageInput" type="file" /><br/>
												<input type="submit" class="btn btn-info" id="submit-btn" value="Upload Foto" />
												<img src="../../andro/img/ajax-loader.gif" id="loading-img" style="display:none;" alt="Please Wait"/>
											</form>
											<div id="progressbox" style="display:none;"><div id="progressbar"></div ><div id="statustxt">0%</div></div>
											<div align="center" id="output"></div>
										</div>
									</div>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
							</div>
							</div>
							<!-- ===================================== End Edit ===================================== -->
						</div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="komponen/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="komponen/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="komponen/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="komponen/vendor/raphael/raphael.min.js"></script>
    <script src="komponen/vendor/morrisjs/morris.min.js"></script>
    <script src="komponen/data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="komponen/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
	
	<!-- =============================================Progres Bar=================================-->
	<script type="text/javascript" src="../assets/js/jquery.form.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function() { 

		var progressbox     = $('#progressbox');
		var progressbar     = $('#progressbar');
		var statustxt       = $('#statustxt');
		var completed       = '0%';
		
		var options = { 
				target:   '#output',   // target element(s) to be updated with server response 
				beforeSubmit:  beforeSubmit,  // pre-submit callback 
				uploadProgress: OnProgress,
				success:       afterSuccess,  // post-submit callback 
				resetForm: true        // reset the form after successful submit 
			}; 
			
		 $('#MyUploadForm').submit(function() { 
				$(this).ajaxSubmit(options);  			
				// return false to prevent standard browser submit and page navigation 
				return false; 
			});
		
	//when upload progresses	
	function OnProgress(event, position, total, percentComplete)
	{
		//Progress bar
		progressbar.width(percentComplete + '%') //update progressbar percent complete
		statustxt.html(percentComplete + '%'); //update status text
		if(percentComplete>50)
			{
				statustxt.css('color','#fff'); //change status text to white after 50%
			}
	}

	//after succesful upload
	function afterSuccess()
	{
		$('#submit-btn').show(); //hide submit button
		$('#loading-img').hide(); //hide submit button

	}

	//function to check file size before uploading.
	function beforeSubmit(){
		//check whether browser fully supports all File API
	   if (window.File && window.FileReader && window.FileList && window.Blob)
		{

			if( !$('#imageInput').val()) //check empty input filed
			{
				$("#output").html("Masukkan gambar terlebih dahulu!");
				return false
			}
			
			var fsize = $('#imageInput')[0].files[0].size; //get file size
			var ftype = $('#imageInput')[0].files[0].type; // get file type
			
			//allow only valid image file types 
			switch(ftype)
			{
				case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
					break;
				default:
					$("#output").html("<b>"+ftype+"</b> File tidak didukung!");
					return false
			}
			
			//Allowed file size is less than 3 MB (1048576)
			if(fsize>3048576) 
			{
				$("#output").html("<b>"+bytesToSize(fsize) +"</b> File gambar terlalu besar! <br />Maksimal ukuran gambar 3 MB.");
				return false
			}
			
			//Progress bar
			progressbox.show(); //show progressbar
			progressbar.width(completed); //initial value 0% of progressbar
			statustxt.html(completed); //set status text
			statustxt.css('color','#000'); //initial color of status text

					
			$('#submit-btn').hide(); //hide submit button
			$('#loading-img').show(); //hide submit button
			$("#output").html("");  
		}
		else
		{
			//Output error to older unsupported browsers that doesn't support HTML5 File API
			$("#output").html("Browser tidak mendukung fitur ini, tolong upgrade browser anda!");
			return false;
		}
	}

	//function to format bites bit.ly/19yoIPO
	function bytesToSize(bytes) {
	   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	   if (bytes == 0) return '0 Bytes';
	   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
	}

	}); 
	</script>
</body>

</html>
