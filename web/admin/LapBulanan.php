<?php
include ('akses.php');
$fitur_id = 17;
include ('login/lock-menu.php');

//kode jadi untuk server log
/* include ('../library/kode-log-server.php');
include ('../library/kode-mstperson.php'); */
include ('../library/tgl-indo.php');
$DateTime = date('Y-m');

include('../library/config.php');
date_default_timezone_set('Asia/Jakarta');

// if(base64_decode($_GET['aksi'])=='Hapus'){
	// $Hapus = mysqli_query($koneksi,"UPDATE dompetperson SET IsVerified='0',UserVerificator='$login_id' WHERE NoTransaksi='".base64_decode($_GET['id'])."' ");
	// if($Hapus){
		// mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName) 
		// VALUES ('$kode_jadi_log','$DateTime','Update Data','Unverified Tarik Tunai Dompet Person : ".base64_decode($_GET['id'])."','$login_id')");
		// echo '<script language="javascript">document.location="TarikTunai.php"; </script>';
	// }else{
		// echo '<script language="javascript">alert("Unverified Gagal!"); document.location="TarikTunai.php"; </script>';
	// }
// }

// if(base64_decode($_GET['aksi'])=='Verifikasi'){
	// $Hapus = mysqli_query($koneksi,"UPDATE dompetperson SET UserVerificator='$login_id' WHERE NoTransaksi='".base64_decode($_GET['id'])."' ");
	// if($Hapus){
		// mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName) 
		// VALUES ('$kode_jadi_log','$DateTime','Update Data','Verified Tarik Tunai Dompet Person : ".base64_decode($_GET['id'])."','$login_id')");
		// echo '<script language="javascript">document.location="TarikTunai.php"; </script>';
	// }else{
		// echo '<script language="javascript">alert("Unverified Gagal!"); document.location="TarikTunai.php"; </script>';
	// }
// }
if(@$_REQUEST['KeywordCabang']!=null){
	$cabang = @$_REQUEST['KeywordCabang'];
}else{
	$cabang = $login_cabang;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <?php include 'view/title.php' ?>

    <!-- Bootstrap Core CSS -->
    <link href="komponen/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="komponen/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="komponen/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="komponen/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="komponen/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Jika Anda verifikasi tarik tunai, pastikan anda sudah mentransfer sejumlah uang ke rekening klien!")
			if (answer == true){
				window.location = "TarikTunai.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
	<script type="text/javascript">
		function Hapusconfirmation() {
			var answer = confirm("Transaksi tarik tunai akan dihapus! ")
			if (answer == true){
				window.location = "TarikTunai.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
	<!-- Datepcker -->
	<link rel="stylesheet" href="../library/Datepicker/dist/css/default/zebra_datepicker.min.css" type="text/css">
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
		<?php include 'view/menu.php' ; ?>
		</nav>
		
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">Laporan Bulan <?php if(@$_REQUEST['keyword']!=null){echo TanggalIndo($_REQUEST['keyword']);}else{echo TanggalIndo($DateTime);} ?></h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
							<form method="post" action="">
								<div class="form-group col-lg-4 col-lg-offset-5">
								<?php if($IsServer=='1'){ ?>	
									<select name="KeywordCabang" class="form-control" required>	
										<?php
											echo '<option value="">-- Cabang Perusahaan --</option>';
											$menu = mysqli_query($koneksi,"SELECT a.*,b.NamaProvinsi,c.NamaKab FROM mstcabang a JOIN mstprovinsi b ON a.KodeProvinsi=b.KodeProvinsi JOIN mstkabupaten c ON a.KodeKab=c.KodeKab ORDER BY a.KodeCabang ASC");
											while($kode = mysqli_fetch_array($menu)){
												if($kode['KodeCabang']==$cabang){
													echo "<option value=\"".$kode['KodeCabang']."\" selected >".$kode['NamaProvinsi']." - ".$kode['NamaKab']."</option>\n";
												}else{
													echo "<option value=\"".$kode['KodeCabang']."\">".$kode['NamaProvinsi']." - ".$kode['NamaKab']."</option>\n";
												}
												
											}
										?>
									</select>
								<?php } ?>
								</div>
								<div class="form-group input-group col-lg-3">
									<input type="text" name="keyword" class="form-control" id="datepicker" value="<?php if(@$_REQUEST['keyword']==null){echo $DateTime;}else{echo $_REQUEST['keyword'];}; ?>" placeholder="Tanggal Transaksi">
									<span class="input-group-btn">
										<button class="btn btn-large btn-info" type="submit">Cari</button>
									 </span>
								</div>
							</form>
							<!-- ===================================== tabel ==================================== -->
							<div class="table-responsive">
								<table width="100%" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>No</th>
											<th>Tanggal</th>
											<th>Keterangan</th>
											<th>Debet</th>
											<!--<th>Kredit</th>-->
											<!--<th>Saldo</th>-->
											<th>Status Dana</th>
										</tr>
									</thead>
									<?php
										include '../library/pagination1.php';
										// mengatur variabel reload dan sql
										$kosong=null;
										if(@$_REQUEST['KeywordCabang']!=null OR @$_REQUEST['keyword']!=null){
											// jika ada kata kunci pencarian (artinya form pencarian disubmit dan tidak kosong)pakai ini
											$keyword=$_REQUEST['keyword'];
											$reload = "Dompet.php?pagination=true&keyword=$keyword";
											$sql =  "SELECT NoTransaksi,TanggalTransaksi,(Debet+PorsiPusat) as Debet,Kredit,Keterangan, IsKirimUang,KodeCabang,UserName FROM dompetperusahaan WHERE date_format(TanggalTransaksi, '%Y-%m')='$keyword' AND KodeCabang='$cabang' AND IsTrOffline='1'
											UNION ALL 
											SELECT NoTrOrder,TglKirim as TanggalTransaksi,(PorsiPerusahaan+PorsiPusat) as Debet,null as Kredit,null as Keterangan,IsKirimUang,KodeCabang,null as UserName FROM trordermakanan WHERE (StatusOrder='Selesai' OR StatusOrder='Dikirim') AND date_format(TglKirim, '%Y-%m')='$keyword' AND KodeCabang='$cabang' 
											UNION ALL 
											SELECT KodeIklan,TglMulaiIklan as TanggalTransaksi,(HargaSewa+PorsiPusat) as Debet,null as Kredit,NamaIklan as Keterangan,IsKirimUang,KodeCabang,null as UserName FROM iklan WHERE date_format(TglMulaiIklan, '%Y-%m')='$keyword' AND KodeCabang='$cabang'  ";
											if($cabang=='C0001'){
												$sql.=" UNION ALL
											SELECT NoTransaksi,TanggalTransaksi,PorsiPusat as Debet,Kredit,Keterangan, IsKirimUang,KodeCabang, UserName FROM dompetperusahaan WHERE date_format(TanggalTransaksi, '%Y-%m')='$keyword' AND KodeCabang!='$cabang' AND IsTrOffline='1'
											UNION ALL 
											SELECT NoTrOrder,TglKirim as TanggalTransaksi,PorsiPusat as Debet,null as Kredit,null as Keterangan,IsKirimUang,KodeCabang,null as UserName FROM trordermakanan WHERE (StatusOrder='Selesai' OR StatusOrder='Dikirim') AND date_format(TglKirim, '%Y-%m')='$keyword' AND KodeCabang!='$cabang'
											UNION ALL 
											SELECT KodeIklan,TglMulaiIklan as TanggalTransaksi,PorsiPusat as Debet,null as Kredit,NamaIklan as Keterangan,IsKirimUang,KodeCabang,null as UserName FROM iklan WHERE date_format(TglMulaiIklan, '%Y-%m')='$keyword' AND KodeCabang!='$cabang' ";
											}
											
											$sql.= " ORDER BY TanggalTransaksi ASC";
											$result = mysqli_query($koneksi,$sql);
										}else{
										//jika tidak ada pencarian pakai ini
											$reload = "Dompet.php?pagination=true";
											$sql =  "SELECT NoTransaksi,TanggalTransaksi,(Debet+PorsiPusat) as Debet,Kredit, Keterangan, IsKirimUang,KodeCabang,UserName FROM dompetperusahaan WHERE date_format(TanggalTransaksi, '%Y-%m')='$DateTime' AND KodeCabang='$cabang' AND IsTrOffline='1'
											UNION ALL 
											SELECT NoTrOrder,TglKirim as TanggalTransaksi,(PorsiPerusahaan+PorsiPusat) as Debet,null as Kredit,null as Keterangan,IsKirimUang,KodeCabang,null as UserName FROM trordermakanan WHERE (StatusOrder='Selesai' OR StatusOrder='Dikirim') AND date_format(TglKirim, '%Y-%m')='$DateTime' AND KodeCabang='$cabang'
											UNION ALL 
											SELECT KodeIklan,TglMulaiIklan as TanggalTransaksi,(HargaSewa+PorsiPusat) as Debet,null as Kredit,NamaIklan as Keterangan,IsKirimUang,KodeCabang,null as UserName FROM iklan WHERE date_format(TglMulaiIklan, '%Y-%m')='$DateTime' AND KodeCabang='$cabang' ";
											if($cabang=='C0001'){
												$sql.=" UNION ALL
											SELECT NoTransaksi,TanggalTransaksi,PorsiPusat as Debet,Kredit,Keterangan, IsKirimUang,KodeCabang, UserName FROM dompetperusahaan WHERE date_format(TanggalTransaksi, '%Y-%m')='$DateTime' AND KodeCabang!='$cabang' AND IsTrOffline='1'
											UNION ALL 
											SELECT NoTrOrder,TglKirim as TanggalTransaksi,PorsiPusat as Debet,null as Kredit,null as Keterangan,IsKirimUang,KodeCabang,null as UserName FROM trordermakanan WHERE (StatusOrder='Selesai' OR StatusOrder='Dikirim') AND date_format(TglKirim, '%Y-%m')='$DateTime' AND KodeCabang!='$cabang'
											UNION ALL 
											SELECT KodeIklan,TglMulaiIklan as TanggalTransaksi,PorsiPusat as Debet,null as Kredit,NamaIklan as Keterangan,IsKirimUang,KodeCabang,null as UserName FROM iklan WHERE date_format(TglMulaiIklan, '%Y-%m')='$DateTime' AND KodeCabang!='$cabang' ";
											}
											
											$sql.= " ORDER BY TanggalTransaksi ASC";
											
											//$sql =  "SELECT NoTrOrder,TanggalOrder,PorsiPerusahaan FROM trordermakanan WHERE StatusOrder='Selesai' AND date_format(TanggalOrder, '%Y/%m')='$DateTime' ORDER BY TanggalOrder ASC";
											$result = mysqli_query($koneksi,$sql);
										}
										
										//pagination config start
										$rpp = 10000; // jumlah record per halaman
										$page = intval(@$_GET["page"]);
										if($page<=0) $page = 1;  
										$tcount = mysqli_num_rows($result);
										$tpages = ($tcount) ? ceil($tcount/$rpp) : 1; // total pages, last page number
										$count = 0;
										$i = ($page-1)*$rpp;
										$no_urut = ($page-1)*$rpp;
										//pagination config end				
									?>
									<tbody>
									<!-- --------------------------------- Saldo Bulan Lalu ---------------------------->
										<?php 
											/* if(@$_REQUEST['keyword']==NULL){
												$Querybln_lalu ="SELECT SUM(Debet) as debet,SUM(Kredit) as kredit FROM dompetperusahaan WHERE date_format(TanggalTransaksi, '%Y-%m')<'$DateTime' AND KodeCabang='$cabang'
												UNION ALL 
												SELECT SUM(PorsiPerusahaan) as debet,null as kredit FROM trordermakanan WHERE (StatusOrder='Selesai' OR StatusOrder='Dikirim') AND date_format(TglKirim, '%Y-%m')<'$DateTime' AND KodeCabang='$cabang'
												UNION ALL 
												SELECT SUM(HargaSewa) as debet,null as kredit FROM iklan WHERE date_format(TglMulaiIklan, '%Y-%m')<'$DateTime' AND KodeCabang='$cabang' ";
												if($cabang=='C0001'){
													$Querybln_lalu.= " UNION ALL
													SELECT SUM(PorsiPusat) as debet,null as kredit FROM trordermakanan WHERE (StatusOrder='Selesai' OR StatusOrder='Dikirim') AND date_format(TglKirim, '%Y-%m')<'$DateTime' AND KodeCabang!='$cabang'
													UNION ALL 
													SELECT SUM(PorsiPusat) as debet,null as kredit FROM iklan WHERE date_format(TglMulaiIklan, '%Y-%m')<'$DateTime' AND KodeCabang!='$cabang' ";
												}
												$bln_lalu = mysqli_query($koneksi,$Querybln_lalu);
											}else{
												$Querybln_lalu ="SELECT SUM(Debet) as debet,SUM(Kredit) as kredit FROM dompetperusahaan WHERE date_format(TanggalTransaksi, '%Y-%m')<'".$_REQUEST['keyword']."' AND KodeCabang='$cabang'
												UNION ALL 
												SELECT SUM(PorsiPerusahaan) as debet,null as kredit FROM trordermakanan WHERE (StatusOrder='Selesai' OR StatusOrder='Dikirim') AND date_format(TglKirim, '%Y-%m')<'".$_REQUEST['keyword']."' AND KodeCabang='$cabang'
												UNION ALL 
												SELECT SUM(HargaSewa) as debet,null as kredit FROM iklan WHERE date_format(TglMulaiIklan, '%Y-%m')<'".$_REQUEST['keyword']."' AND KodeCabang='$cabang' ";
												if($cabang=='C0001'){
													$Querybln_lalu.= " UNION ALL 
													SELECT SUM(PorsiPusat) as debet,null as kredit FROM trordermakanan WHERE (StatusOrder='Selesai' OR StatusOrder='Dikirim') AND date_format(TglKirim, '%Y-%m')<'".$_REQUEST['keyword']."' AND KodeCabang!='$cabang'
													UNION ALL 
													SELECT SUM(PorsiPusat) as debet,null as kredit FROM iklan WHERE date_format(TglMulaiIklan, '%Y-%m')<'".$_REQUEST['keyword']."' AND KodeCabang!='$cabang' ";
												}
												$bln_lalu = mysqli_query($koneksi,$Querybln_lalu);
											}
											while($DataSaldoAwal = mysqli_fetch_assoc($bln_lalu)){
												$DebetAwal[] = $DataSaldoAwal['debet'];
												$KreditAwal[] = $DataSaldoAwal['kredit'];
											}
											$saldo_awal = array_sum($DebetAwal) - array_sum($KreditAwal); */
										?>
									<!-- ------------------------------ end Saldo Bulan Lalu ---------------------------->
										<?php
										while(($count<$rpp) && ($i<$tcount)) {
											mysqli_data_seek($result,$i);
											$data = mysqli_fetch_array($result);
										?>
										<tr class="odd gradeX">
											<td width="20px">
												<?php echo ++$no_urut;?> 
											</td>
											<td width="130px">
												<?php echo TanggalIndo($data['TanggalTransaksi']); ?><br/> 
												<?php echo substr($data['TanggalTransaksi'],11,8); ?>
											</td>
											<td>
												<?php 
													if(substr($data['NoTransaksi'],0,2)=='TR'){ //tr order makanan
														echo 'Pendapatan Perusahaan Transaksi Kurir & Penjual No.Ref : '.$data['NoTransaksi'].'-'.$data['KodeCabang'].'';
													}elseif(substr($data['NoTransaksi'],0,2)=='BA'){ //kode bank perusahaan
														echo 'Transaksi Order Offline : '.$data['Keterangan'].'-'.$data['KodeCabang'].'';
													}elseif(substr($data['NoTransaksi'],0,2)=='IK'){ //iklan
														echo 'Pendapatan Perusahaan Sewa Iklan No.Ref : '.$data['NoTransaksi'].'-'.$data['KodeCabang'].'';
													}
												?>
											</td>
											<td>
												G <?php
													if($data['Debet']==NULL){
														$step_6='0';
													}else{
														$step_6		= $data['Debet'];
														$tot_debet[] = $step_6;
													}
													echo number_format($step_6);
												?>
											</td>
											<!--<td>
												G <?php
													if($data['Kredit']==NULL){
														$step_3='0';
													}else{
														$step_3		= $data['Kredit'];
														$tot_kredit[] = $step_3;
													}
													echo number_format($step_3);
												?>
											</td>
											<td>														
												G <?php 
													$saldo_awal = ($saldo_awal + $step_6)-$step_3;
													echo number_format($saldo_awal);
												?>
											</td>-->
											<td>
												<?php 
												if($cabang=='C0001'){
													if($data['IsKirimUang']!=1 AND $data['KodeCabang']!='C0001' AND substr($data['NoTransaksi'],0,2)=='IK'){ 
														echo '<font color="red">Cabang</font>';
													}elseif($data['IsKirimUang']!=1 AND $data['KodeCabang']!='C0001' AND substr($data['NoTransaksi'],0,2)=='BA'){
														echo '<font color="red">Cabang</font>';
													}else{
														echo 'Pusat';
													}
												}else{
													if($data['IsKirimUang']!=1 AND substr($data['NoTransaksi'],0,2)=='BA' AND $data['UserName']==null){ 
														echo '<font color="red">Pusat</font>';
													}elseif($data['IsKirimUang']!=1 AND substr($data['NoTransaksi'],0,2)=='TR'){
														echo '<font color="red">Pusat</font>';
													}else{
														echo 'Cabang';
													}
												}
												?>
											</td>
										</tr>
										<?php
											$i++; 
											$count++;
										}
										?>
										<tr class="odd gradeX">
											<td colspan='3'>
												<strong>Total Pendapatan Bulan Ini</strong>
											</td>
											<td colspan='3' align="right">
												<strong>G <?php
												if(@$tot_debet==null){
													$TampilDebet ='0';
												}else{
													$TampilDebet = array_sum($tot_debet);
												}
												echo number_format($TampilDebet);
												?></strong> 
											</td>
										</tr>
										<!--<tr class="odd gradeX">
											<td colspan='3'>
												<strong>Total Pengeluaran Bulan Ini</strong>
											</td>
											<td colspan='3' align="right">
												<strong>G <?php
												if(@$tot_kredit==null){
													$TampilKredit ='0';
												}else{
													$TampilKredit = array_sum($tot_kredit);
												}
												echo number_format($TampilKredit);
												?></strong> 
											</td>
										</tr>
										<tr class="odd gradeX">
											<td colspan='3'>
												<strong>Laba Bersih</strong>
											</td>
											<td colspan='3' align="right">
												<strong>G <?php $LabaPerusahaan =$TampilDebet-$TampilKredit; echo number_format($LabaPerusahaan);?></strong> 
											</td>
										</tr>-->
										<!--<tr class="odd gradeX">
											<td colspan='5'>
												<strong>Total Saldo Bulan ini</strong>
											</td>
											<td>
												<strong>G <?php 
													//$saldo_bln_ini =$totalMasuk - $totalKeluar;
													echo number_format($saldo_awal);
												?></strong>
											</td>
										</tr>-->
										
									</tbody>
								</table>
							</div>
							<?php if($IsServer==1 AND $cabang=='C0001'){ ?>
								<?php
								//prosen afindo
								if(@$_REQUEST['keyword']!=null){
									if(strtotime(@$_REQUEST['keyword']) > strtotime($DateTime)){
										$PorsiAfindo = 0;
										$PorsiPerusahaan = 0;
									}else{
										$PorsiAfindo = (10/100)*$TampilDebet;
										$PorsiPerusahaan = $TampilDebet-$PorsiAfindo;
									}
								}else{
									$PorsiAfindo = (10/100)*$TampilDebet;
									$PorsiPerusahaan = $TampilDebet-$PorsiAfindo;
								}
								?>
								<div class="col-lg-6 col-md-6">
									<div class="panel panel-red">
										<div class="panel-heading">
											<div class="row">
												<div class="col-lg-4">
													<img src="komponen/LogoAfindo2.png" class="img img-responsive">
												</div>
												<div class="col-xs-8 text-right">
													<div class="huge">G <?php echo number_format($PorsiAfindo); ?></div>
													<div>Porsi (10%) Afindo Informatika<br/> Bulan <?php if(@$_REQUEST['keyword']!=null){echo TanggalIndo($_REQUEST['keyword']);}else{echo TanggalIndo($DateTime);} ?></div>
												</div>
											</div>
										</div>
									</div>
								</div>
								
								<div class="col-lg-6 col-md-6">
									<div class="panel panel-green">
										<div class="panel-heading">
											<div class="row">
												<div class="col-xs-4">
													<img src="../assets/img/logo.png" class="img img-responsive">
												</div>
												<div class="col-xs-8 text-right">
													<div class="huge">G <?php echo number_format($PorsiPerusahaan); ?></div>
													<div>Porsi Garuda Delivery Bulan <?php if(@$_REQUEST['keyword']!=null){echo TanggalIndo($_REQUEST['keyword']);}else{echo TanggalIndo($DateTime);} ?></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							<?php }else{ ?>
								<?php
								//prosen pusat
								if(@$_REQUEST['keyword']!=null){
									if(strtotime(@$_REQUEST['keyword']) > strtotime($DateTime)){
										$PorsiCabang = 0;
										$PorsiPusat = 0;
									}else{
										$PorsiPusatBulanIni= mysqli_query($koneksi,"SELECT SUM(PorsiPusat) as debet FROM trordermakanan WHERE (StatusOrder='Selesai' OR StatusOrder='Dikirim') AND date_format(TglKirim, '%Y-%m')='".$_REQUEST['keyword']."' AND KodeCabang='$cabang'
										UNION ALL 
										SELECT SUM(PorsiPusat) as debet FROM iklan WHERE date_format(TglMulaiIklan, '%Y-%m')='".$_REQUEST['keyword']."' AND KodeCabang='$cabang'
										UNION ALL
										SELECT PorsiPusat as Debet FROM dompetperusahaan WHERE date_format(TanggalTransaksi, '%Y-%m')='".$_REQUEST['keyword']."' AND KodeCabang='$cabang' AND IsTrOffline='1'");
										while($DataPorsiBulanIni = mysqli_fetch_assoc($PorsiPusatBulanIni)){
											$PorsiBulanIni[] = $DataPorsiBulanIni['debet'];
										}
										
										$PorsiCabang = $TampilDebet-array_sum($PorsiBulanIni);
										$PorsiPusat = $TampilDebet-$PorsiCabang;
									}
								}else{
									$PorsiPusatBulanIni= mysqli_query($koneksi,"SELECT SUM(PorsiPusat) as debet FROM trordermakanan WHERE (StatusOrder='Selesai' OR StatusOrder='Dikirim') AND date_format(TglKirim, '%Y-%m')='$DateTime' AND KodeCabang='$cabang'
										UNION ALL 
										SELECT SUM(PorsiPusat) as debet FROM iklan WHERE date_format(TglMulaiIklan, '%Y-%m')='$DateTime' AND KodeCabang='$cabang'
										UNION ALL
										SELECT PorsiPusat as Debet FROM dompetperusahaan WHERE date_format(TanggalTransaksi, '%Y-%m')='$DateTime' AND KodeCabang='$cabang' AND IsTrOffline='1'");
										while($DataPorsiBulanIni = mysqli_fetch_assoc($PorsiPusatBulanIni)){
											$PorsiBulanIni[] = $DataPorsiBulanIni['debet'];
										}
										
										$PorsiCabang = $TampilDebet-array_sum($PorsiBulanIni);
										$PorsiPusat = $TampilDebet-$PorsiCabang;
								}
								?>
								
								<div class="col-lg-6 col-md-6">
									<div class="panel panel-primary">
										<div class="panel-heading">
											<div class="row">
												<div class="col-xs-4">
													<img src="../assets/img/logo.png" class="img img-responsive">
												</div>
												<div class="col-xs-8 text-right">
													<div class="huge">G <?php echo number_format($PorsiPusat); ?></div>
													<div>Porsi Pusat Garuda Delivery Bulan <?php if(@$_REQUEST['keyword']!=null){echo TanggalIndo($_REQUEST['keyword']);}else{echo TanggalIndo($DateTime);} ?></div>
												</div>
											</div>
										</div>
									</div>
								</div>
								
								<div class="col-lg-6 col-md-6">
									<div class="panel panel-green">
										<div class="panel-heading">
											<div class="row">
												<div class="col-xs-4">
													<img src="../assets/img/logo.png" class="img img-responsive">
												</div>
												<div class="col-xs-8 text-right">
													<div class="huge">G <?php echo number_format($PorsiCabang); ?></div>
													<div>Porsi Cabang Garuda Delivery Bulan <?php if(@$_REQUEST['keyword']!=null){echo TanggalIndo($_REQUEST['keyword']);}else{echo TanggalIndo($DateTime);} ?></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							<?php } ?>
								<!-- ===================================== tabel ==================================== -->
							</div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="komponen/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="komponen/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="komponen/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="komponen/vendor/raphael/raphael.min.js"></script>
    <script src="komponen/vendor/morrisjs/morris.min.js"></script>
    <script src="komponen/data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="komponen/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>

<!-- DatePicker -->
<script type="text/javascript" src="../library/Datepicker/dist/zebra_datepicker.src.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#datepicker').Zebra_DatePicker({format: 'Y-m'});
	});
</script>
</body>
</html>
