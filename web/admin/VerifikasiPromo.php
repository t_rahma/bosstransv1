<?php
include ('akses.php');
$fitur_id = 19;
include ('login/lock-menu.php');

//kode jadi untuk server log
include ('../library/kode-log-server.php');
include ('../library/tgl-indo.php');
$DateTime = date('Y-m-d H:i:s');

include('../library/config.php');
date_default_timezone_set('Asia/Jakarta');
$id = base64_decode(@$_GET['rm']);

//ambil kode cabang 
$QueryCab = mysqli_query($koneksi,"SELECT * FROM mstcabang WHERE KodeCabang='$login_cabang'");
$RowCab = mysqli_fetch_assoc($QueryCab);
$Prov = $RowCab['KodeProvinsi'];
$Kab = $RowCab['KodeKab'];
/* if(isset($_POST['Simpan'])){
	$GantiKurir = mysqli_query($koneksi,"UPDATE trordermakanan SET KodePersonKurir='".$_POST['KodePersonKurir']."',KetGantiKurir='".$_POST['Keterangan']."' WHERE NoTrOrder='".$_POST['NoTrOrder']."'");
	if($GantiKurir){
		mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName) 
		VALUES ('$kode_jadi_log','$DateTime','Update Data','Transaksi Penggantian Kurir : ".$_POST['KurirLama']." ke ".$_POST['KodePersonKurir']." No.Ref ".$_POST['NoTrOrder']."','$login_id')");
		echo '<script language="javascript">alert("Penggantian Kurir Berhasil!");document.location="PenggantiKurir.php"; </script>';
	}else{
		echo '<script language="javascript"> document.location="PenggantiKurir.php"; </script>';
	}
} */

if(base64_decode(@$_GET['aksi'])=="Ganti"){
	$AmbilData = mysqli_query($koneksi,"SELECT a.*,b.NamaProvinsi,c.NamaKab,d.NamaKec,e.NamaDesa FROM rm_penjual a JOIN mstprovinsi b ON a.KodeProvinsi=b.KodeProvinsi JOIN mstkabupaten c ON a.KodeKab=c.KodeKab JOIN mstkecamatan d ON a.KodeKec=d.KodeKec JOIN mstdesa e ON a.KodeDesa=e.KodeDesa WHERE a.KodeProvinsi='$Prov' AND a.KodeKab='$Kab' AND a.KodeRM='".base64_decode($_GET['rm'])."' AND a.KodePerson='".base64_decode($_GET['prs'])."'");
	$RowData = mysqli_fetch_array($AmbilData);
}

if(base64_decode(@$_GET['aksi'])=="VerifikasiPromo"){
	$VerifikasiMenu = mysqli_query($koneksi,"UPDATE menu_rm SET IsVerifiedDiskon='1' WHERE KodeRM='".base64_decode($_GET['rm'])."' AND KodePerson='".base64_decode($_GET['prs'])."' AND KodeMenu='".base64_decode($_GET['mn'])."'");
	
	mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
		VALUES ('$kode_jadi_log','$DateTime','Update Data','Transaksi Verifikasi Promo No.Ref ".base64_decode($_GET['mn'])." - ".base64_decode($_GET['rm'])." - ".base64_decode($_GET['prs'])."','$login_id','$login_cabang')");
	
	echo '<script language="javascript">document.location="VerifikasiPromo.php?rm='.$_GET['rm'].'&prs='.$_GET['prs'].'=&aksi='.base64_encode('Ganti').'"; </script>';
}

if(base64_decode(@$_GET['aksi'])=="UnVerifikasiPromo"){
	$VerifikasiMenu = mysqli_query($koneksi,"UPDATE menu_rm SET IsVerifiedDiskon=null,HargaDiskon=null,TglMulaiDiskon=null, TglSelesaiDiskon=null WHERE KodeRM='".base64_decode($_GET['rm'])."' AND KodePerson='".base64_decode($_GET['prs'])."' AND KodeMenu='".base64_decode($_GET['mn'])."'");
	
	mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
		VALUES ('$kode_jadi_log','$DateTime','Update Data','Transaksi Unverifikasi Promo No.Ref ".base64_decode($_GET['mn'])." - ".base64_decode($_GET['rm'])." - ".base64_decode($_GET['prs'])."','$login_id','$login_cabang')");
	
	echo '<script language="javascript">document.location="VerifikasiPromo.php?rm='.$_GET['rm'].'&prs='.$_GET['prs'].'=&aksi='.base64_encode('Ganti').'"; </script>';
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <?php include 'view/title.php' ?>

    <!-- Bootstrap Core CSS -->
    <link href="komponen/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="komponen/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="komponen/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="komponen/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="komponen/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda Yakin Untuk Mengganti Kurir Ini ?")
			if (answer == true){
				window.location = "MasterKurir.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
	
	<!-- Datepcker -->
	<link rel="stylesheet" href="../library/Datepicker/dist/css/default/zebra_datepicker.min.css" type="text/css">
	
	<!-- Select 2 Skin -->
	<link rel="stylesheet" href="../library/select2-master/dist/css/select2.css"/>
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
		<?php include 'view/menu.php' ; ?>
		</nav>
		
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Verifikasi Promo Penjual</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
						<!-- Nav tabs -->
						<ul class="nav nav-pills">
							<li <?php if(@$id==null){echo 'class="active"';} ?>><a href="#home-pills" data-toggle="tab">Data Penjual</a></li>
							<?php if(@$id!=null){
								echo '<li class="active"><a href="#tambah-user" data-toggle="tab">Verifikasi Promo</a></li>';
							} ?>
							
						</ul>
						<div class="tab-content">
                            <div class="tab-pane fade <?php if(@$id==null){echo 'in active';} ?>" id="home-pills">
							<form method="post" action="">
								<div class="form-group input-group col-lg-4 col-lg-offset-8">
									<input type="text" name="keyword" class="form-control" value="<?php echo @$_REQUEST['keyword']; ?>" placeholder="Cari Nama Penjual / Warung">
									<span class="input-group-btn">
										<button class="btn btn-large btn-info" type="submit">Cari</button>
									 </span>
								</div>
							</form>
							<!-- ===================================== tabel ==================================== -->
							<div class="table-responsive">
							<table width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Pemilik/Penjual</th>
                                        <th>Nama Tempat/Warung</th>
                                        <td align="center">Jumlah Permintaan Promo</td>
										<td align="center">Aksi</td>
                                    </tr>
                                </thead>
								<?php
									include '../library/pagination1.php';
									// mengatur variabel reload dan sql
									$kosong=null;
									if(isset($_REQUEST['keyword']) && $_REQUEST['keyword']<>""){
										// jika ada kata kunci pencarian (artinya form pencarian disubmit dan tidak kosong)pakai ini
										$keyword=$_REQUEST['keyword'];
										$reload = "VerifikasiPromo.php?pagination=true&keyword=$keyword";
										$sql =  "SELECT a.KodeRM,a.KodePerson,a.NamaRM,a.TelpRM,b.NamaPerson,b.NoHP,COUNT(c.KodeMenu) as 
												JumlahPromo FROM 
												rm_penjual a JOIN mstperson b ON a.KodePerson=b.KodePerson JOIN menu_rm c ON (a.KodePerson,a.KodeRM)=(c.KodePerson,c.KodeRM) WHERE a.KodeProvinsi='$Prov' AND a.KodeKab='$Kab' AND c.IsVerifiedDiskon='0' AND b.NamaPerson LIKE '%$keyword%' || a.NamaRM LIKE '%$keyword%' GROUP BY a.KodeRM ORDER BY a.NamaRM ASC";
										$result = mysqli_query($koneksi,$sql);
									}else{
									//jika tidak ada pencarian pakai ini
										$reload = "PenggantiKurir.php?pagination=true";
										$sql =  "SELECT a.KodeRM,a.KodePerson,a.NamaRM,a.TelpRM,b.NamaPerson,b.NoHP,COUNT(c.KodeMenu) as 
												JumlahPromo FROM rm_penjual a JOIN mstperson b ON a.KodePerson=b.KodePerson JOIN menu_rm c ON (a.KodePerson,a.KodeRM)=(c.KodePerson,c.KodeRM) WHERE a.KodeProvinsi='$Prov' AND a.KodeKab='$Kab' AND c.IsVerifiedDiskon='0' GROUP BY a.KodeRM ORDER BY a.NamaRM ASC";
										$result = mysqli_query($koneksi,$sql);
									}
									
									//pagination config start
									$rpp = 25; // jumlah record per halaman
									$page = intval(@$_GET["page"]);
									if($page<=0) $page = 1;  
									$tcount = mysqli_num_rows($result);
									$tpages = ($tcount) ? ceil($tcount/$rpp) : 1; // total pages, last page number
									$count = 0;
									$i = ($page-1)*$rpp;
									$no_urut = ($page-1)*$rpp;
									//pagination config end				
								?>
                                <tbody>
									<?php
									while(($count<$rpp) && ($i<$tcount)) {
										mysqli_data_seek($result,$i);
										$data = mysqli_fetch_array($result);
									?>
                                    <tr class="odd gradeX">
                                        <td width="50px">
											<?php echo ++$no_urut;?> 
										</td>
										<td>
											<strong><?php echo $data ['NamaPerson']; ?></strong><br/>
											(Hp.<?php echo $data['NoHP'];?>)
										</td>
                                        <td>
											<?php echo $data ['NamaRM']; ?><br/>
											(Hp.<?php echo $data['TelpRM'];?>)
										</td>
										<td align="center">
											<font color="red"><?php echo $data['JumlahPromo'];?> Menu</font>
										</td>
										<td align="center">										
											<a href="VerifikasiPromo.php?rm=<?php echo base64_encode($data['KodeRM']);?>&prs=<?php echo base64_encode($data['KodePerson']);?>&aksi=<?php echo base64_encode('Ganti');?>" title='Klik untuk verifikasi promo'><span class='btn btn-warning btn-sm'>Verifikasi</span></a>
										</td>
                                    </tr>
									<?php
										$i++; 
										$count++;
									}
									?>
                                </tbody>
                            </table>
							<div><?php echo paginate_one($reload, $page, $tpages); ?></div>
							</div>
							<!-- ===================================== tabel ==================================== -->
							</div>
							<div class="tab-pane fade <?php if(@$id!=null){echo 'in active';} ?>" id="tambah-user">
							<!-- ===================================== input ===================================== -->
								<div class="panel-body">
									<div class="row">
										<div class="col-lg-12">
											<h2><span><?php echo ucwords($RowData['NamaRM']);?></span></h2>
											<article>
												<div class="post-slider col-lg-6">
													<img src="../assets/img/FotoRM/<?php echo $RowData['Foto'];?>" class="img img-responsive img-thumbnail" />
												</div>
												<p><?php echo $RowData['Keterangan'];?></p><br/>
												
												<label>Kategory</label>
												<p><?php echo $RowData['KategoryRM'];?></p>
												
												<label>Jam Buka-Tutup</label>
												<p><?php echo "".substr($RowData['JamBuka'],0,5)." - ".substr($RowData['JamTutup'],0,5)."";?></p>
												
												<label>Telp.</label>
												<p><?php echo $RowData['TelpRM'];?></p>
												
												<label>ALamat</label>
												<p><?php echo "".$RowData['AlamatRM']." ".$RowData['Jalan']." ".$RowData['Dusun'].""; ?></p>
												<p><?php echo "Ds.".$RowData['NamaDesa'].", Kec.".$RowData['NamaKec'].", Kab.".$RowData['NamaKab'].", Prov.".$RowData['NamaProvinsi'].""; ?></p>
											</article>
										</div>
										<div class="col-lg-12"><br/>
											<div class="table-responsive">
												<table width="100%" class="table table-striped table-bordered table-hover">
													<thead>
														<tr>
															<th>No</th>
															<th>Nama Menu</th>
															<th>Harga</th>
															<th>Harga Promo</th>
															<th>Tanggal Promo</th>
															<td align="center">Aksi</td>
														</tr>
													</thead>
													<tbody>
														<?php
														$nomer=1;
														$QueryMenuRM = mysqli_query($koneksi,"SELECT * FROM menu_rm WHERE KodeRM='".base64_decode($_GET['rm'])."' AND KodePerson='".base64_decode($_GET['prs'])."' AND IsVerifiedDiskon='0' ORDER BY NamaMenu ASC");
														while($DataMenu=mysqli_fetch_array($QueryMenuRM)){
														?>
														<tr class="odd gradeX">
															<td width="50px">
																<?php echo $nomer++;?> 
															</td>
															<td>
																<strong><?php echo $DataMenu ['NamaMenu']; ?></strong>
															</td>
															<td>
																G <?php echo number_format($DataMenu ['Harga']); ?>
															</td>
															<td>
																<font color="red">G <?php echo number_format($DataMenu['HargaDiskon']);?></font>
															</td>
															<td>
																<?php
																echo '<p>'.TanggalIndo($DataMenu['TglMulaiDiskon']).' '.substr($DataMenu['TglMulaiDiskon'],11,19).' s/d '.TanggalIndo($DataMenu['TglSelesaiDiskon']).' '.substr($DataMenu['TglSelesaiDiskon'],11,19).'</p>';
																?>
															</td>
															<td align="center">										
																<a href="VerifikasiPromo.php?rm=<?php echo base64_encode($DataMenu['KodeRM']);?>&prs=<?php echo base64_encode($DataMenu['KodePerson']);?>&mn=<?php echo base64_encode($DataMenu['KodeMenu']);?>&aksi=<?php echo base64_encode('VerifikasiPromo');?>" title='Klik untuk verifikasi promo'><span class='btn btn-info btn-sm'>Verifikasi</span></a>
																
																<a href="VerifikasiPromo.php?rm=<?php echo base64_encode($DataMenu['KodeRM']);?>&prs=<?php echo base64_encode($DataMenu['KodePerson']);?>&mn=<?php echo base64_encode($DataMenu['KodeMenu']);?>&aksi=<?php echo base64_encode('UnVerifikasiPromo');?>" title='Klik untuk unverifikasi promo'><span class='btn btn-danger btn-sm'>Unverifikasi</span></a>
															</td>
														</tr>
														<?php } ?>
													</tbody>
												</table>
											</div>	
										</div>	
									</div>
								</div>
							<!-- ===================================== input ===================================== -->
							</div>
						</div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="komponen/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="komponen/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="komponen/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="komponen/vendor/raphael/raphael.min.js"></script>
    <script src="komponen/vendor/morrisjs/morris.min.js"></script>
    <script src="komponen/data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="komponen/dist/js/sb-admin-2.js"></script>
	<!-- Select 2 -->
	<script src="../library/select2-master/dist/js/select2.min.js"></script>
	<script>
	$(document).ready(function () {
		$("#kota").select2({
		placeholder: "Cari Nama Kurir Pengganti"
		});
	});
	</script>
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>

<!-- DatePicker -->
<script type="text/javascript" src="../library/Datepicker/dist/zebra_datepicker.src.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#datepicker').Zebra_DatePicker();
	});
</script>
</body>
</html>
