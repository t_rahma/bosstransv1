<?php
include ('akses.php');
$fitur_id = 3;
include ('login/lock-menu.php');

//kode jadi untuk server log
include ('../library/kode-log-server.php');
$DateTime = date('Y-m-d H:i:s a');

include('../library/config.php');
date_default_timezone_set('Asia/Jakarta');

$KodeProvinsi		= $_POST['KodeProvinsi'];
$NamaProvinsi	 	= $_POST['NamaProvinsi'];
$Keterangan		 	= $_POST['Keterangan'];
$KodeNegara		 	= $_POST['KodeNegara'];

if(isset($_POST['submit'])){
	$query = mysqli_query($koneksi,"INSERT into mstprovinsi (KodeProvinsi,NamaProvinsi,Keterangan,KodeNegara) 
	VALUES ('$KodeProvinsi','$NamaProvinsi','$Keterangan','$KodeNegara')");
	if($query){
		mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName) 
		VALUES ('$kode_jadi_log','$DateTime','Tambah Data','Master Provinsi : $KodeProvinsi:$NamaProvinsi:$Keterangan:$KodeNegara','$login_id')");
		echo '<script language="javascript">alert("Data berhasil disimpan !"); document.location="MasterProvinsi.php"; </script>';
	}else{
		echo '<script language="javascript">alert("Input data gagal !"); document.location="MasterProvinsi.php"; </script>';
	}
}
	       
//membuat id negara
$sql = mysqli_query($koneksi,'SELECT RIGHT(KodeProvinsi,6) AS kode FROM mstprovinsi ORDER BY KodeProvinsi DESC LIMIT 1');  
$nums = mysqli_num_rows($sql);
 
if($nums <> 0)
 {
 $data = mysqli_fetch_array($sql);
 $kode = $data['kode'] + 1;
 }else
 {
 $kode = 1;
 }
 
//mulai bikin kode
 $bikin_kode = str_pad($kode, 6, "0", STR_PAD_LEFT);
 $kode_jadi = $bikin_kode;
 
 //===========procedure Update data=============//
 $id = base64_decode($_GET['id']);
 $edit = mysqli_query($koneksi,"select * from mstprovinsi where KodeProvinsi = '$id'");
 while($row = mysqli_fetch_array($edit)){
	$kode_provinsi 	= $row['KodeProvinsi'];
	$kode_negara 	= $row['KodeNegara'];
	$nama_provinsi	= $row['NamaProvinsi'];
	$ket			= $row['Keterangan'];
 }
 
 if(isset($_POST['submit_edit'])){
	$query = mysqli_query($koneksi,"UPDATE mstprovinsi SET NamaProvinsi ='$NamaProvinsi',Keterangan='$Keterangan',KodeNegara='$KodeNegara' WHERE KodeProvinsi = '$id'");
		if($query){
			mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName) 
			VALUES ('$kode_jadi_log','$DateTime','Edit Data','Master Provinsi : $kode_provinsi:$NamaProvinsi:$Keterangan:$KodeNegara','$login_id')");
			echo '<script language="javascript">alert("Edit Data berhasil disimpan !"); document.location="MasterProvinsi.php"; </script>';
		}else{
			echo '<script language="javascript">alert("Edit data gagal !"); document.location="MasterProvinsi.php?id='.base64_encode($id).'"; </script>';
		}
 }
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <?php include 'view/title.php' ?>

    <!-- Bootstrap Core CSS -->
    <link href="komponen/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="komponen/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="komponen/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="komponen/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="komponen/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda Yakin Untuk Menghapus Data . . . ?")
			if (answer == true){
				window.location = "AccessLevel.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
	
	<script type="text/javascript">
		function confirmation2() {
			var answer = confirm("Apakah Anda Yakin Mengaktifkan Fitur ini . . . ?")
			if (answer == true){
				window.location = "AccessLevel.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
	
	<script type="text/javascript">
		function confirmation3() {
			var answer = confirm("Apakah Anda Yakin Non Aktifkan Fitur ini . . . ?")
			if (answer == true){
				window.location = "AccessLevel.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
		<?php include 'view/menu.php' ; ?>
		</nav>
		
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Master Provinsi</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
						<!-- Nav tabs -->
                            <ul class="nav nav-pills">
                                <li <?php if($id==null){echo 'class="active"';} ?>><a href="#home-pills" data-toggle="tab">Data Provinsi</a>
                                </li>
                                <li><a href="#tambah-user" data-toggle="tab">Tambah Data</a>
                                </li>
								<li <?php if($id!=null){echo 'class="active"';} ?>><a href="#edit-user" data-toggle="tab">Edit Data</a>
                                </li>
                            </ul>
						<div class="tab-content">
                            <div class="tab-pane fade <?php if($id==null){echo 'in active';} ?>" id="home-pills">
							<!-- ===================================== tabel ==================================== -->
							<div class="table-responsive col-lg-9">
							<h2>Data Provinsi</h2>
							<div class="col-lg-offset-8 text-right">
								<form method="post" action="MasterProvinsi.php">
									<div class="form-group input-group">						
										<input type="text" name="keyword" class="form-control" placeholder="Masukkan Nama Provinsi" value="<?php echo $_REQUEST['keyword']; ?>">
											<span class="input-group-btn">
												<button class="btn btn-info" type="submit">Cari</button>
											</span>
									</div>
								</form>
							</div>
							<table width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Provinsi</th>
                                        <th>Nama Negara</th>
                                        <th>Keterangan</th>
										<th>Aksi</th>
                                    </tr>
                                </thead>
								<?php
									include '../library/pagination1.php';
									// mengatur variabel reload dan sql
									$kosong=null;
									if(isset($_REQUEST['keyword']) && $_REQUEST['keyword']<>""){
										// jika ada kata kunci pencarian (artinya form pencarian disubmit dan tidak kosong)pakai ini
										$keyword=$_REQUEST['keyword'];
										$reload = "MasterProvinsi.php?pagination=true&keyword=$keyword";
										//$sql =  "SELECT * FROM mstprovinsi WHERE NamaProvinsi LIKE '%$keyword%' ORDER BY  DESC";
										$sql =  "SELECT mstprovinsi.* , mstnegara.NamaNegara
												FROM mstprovinsi INNER JOIN mstnegara 
												ON mstprovinsi.KodeNegara = mstnegara.KodeNegara
												WHERE mstprovinsi.NamaProvinsi LIKE '%$keyword%' ORDER BY KodeProvinsi DESC";
										$result = mysqli_query($koneksi,$sql);
									}else{
									//jika tidak ada pencarian pakai ini
										$reload = "MasterProvinsi.php?pagination=true";
										//$sql =  "SELECT * FROM mstprovinsi ORDER BY KodeProvinsi DESC";
										$sql =  "SELECT mstprovinsi.* , mstnegara.NamaNegara
												FROM mstprovinsi INNER JOIN mstnegara 
												ON mstprovinsi.KodeNegara = mstnegara.KodeNegara
												ORDER BY KodeProvinsi DESC";
										$result = mysqli_query($koneksi,$sql);
									}
									
									//pagination config start
									$rpp = 10; // jumlah record per halaman
									$page = intval($_GET["page"]);
									if($page<=0) $page = 1;  
									$tcount = mysqli_num_rows($result);
									$tpages = ($tcount) ? ceil($tcount/$rpp) : 1; // total pages, last page number
									$count = 0;
									$i = ($page-1)*$rpp;
									$no_urut = ($page-1)*$rpp;
									//pagination config end				
								?>
                                <tbody>
									<?php
									while(($count<$rpp) && ($i<$tcount)) {
										mysqli_data_seek($result,$i);
										$data = mysqli_fetch_array($result);
									?>
                                    <tr class="odd gradeX">
                                        <td width="50px">
											<?php echo ++$no_urut;?> 
										</td>
										<td>
											<strong><?php echo $data ['NamaProvinsi']; ?></strong>
										</td>
                                        <td>
											<?php echo $data ['NamaNegara']; ?>
										</td>
										<td>
											<?php echo $data ['Keterangan']; ?>
										</td>
										<td width="100px">
											<a href="MasterProvinsi.php?id=<?php echo base64_encode($data['KodeProvinsi']);?>" title='Edit'><i class='btn btn-warning btn-sm'><span class='fa fa-edit'></span></i></a> 
											
											<a href="delete/delete-MasterProvinsi.php?id=<?php echo base64_encode($data['KodeProvinsi']);?>&user=<?php echo base64_encode($login_id); ?>" title='Hapus' onclick='return confirmation()'><i class='btn btn-danger btn-sm'><span class='fa fa-trash'></span></i></a>
										</td>
                                    </tr>
									<?php
										$i++; 
										$count++;
									}
									?>
                                </tbody>
                            </table>
							<div><?php echo paginate_one($reload, $page, $tpages); ?></div>
                            <!-- /.table-responsive -->
							
							</div>
							<!-- ===================================== tabel ==================================== -->
							</div>
							<div class="tab-pane fade" id="tambah-user">
							<!-- ===================================== input ===================================== -->
							<div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
									<h2>Tambah Data Provinsi </h2>
										<form role="form" method="post" id="edit"  enctype="multipart/form-data">
											<div class="form-group">
												<input type="hidden" class="form-control" name="KodeProvinsi" value="<?php echo (int)$kode_jadi;?>" readonly>
											</div>
											<div class="form-group">
												<label>Nama Provinsi</label>
												<input type="text" class="form-control" name="NamaProvinsi" required>
											</div>
											<div class="form-group">
												<label>Nama Negara</label>
												<select name="KodeNegara" class="form-control" required>	
													<?php
														$menu = mysqli_query($koneksi,"SELECT * FROM mstnegara GROUP by NamaNegara");
														while($kode = mysqli_fetch_array($menu)){
															echo "<option value=\"".$kode['KodeNegara']."\" >".$kode['NamaNegara']."</option>\n";
														}
													?>
												</select>
											</div>
											<div class="form-group">
												<label>Keterangan</label>
												<input type="text" class="form-control" name="Keterangan">
											</div>
											<button type="submit" name="submit" value="simpan" class="btn btn-info">Simpan</button>
											<button type="reset" name="reset" value="reset" class="btn btn-danger">Reset</button>
										</form>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
							</div>
							<!-- ===================================== input ===================================== -->
							</div>
							<!-- ===================================== Start Edit ===================================== -->
							<div class="tab-pane fade  <?php if($id!=null){echo 'in active';} ?>" id="edit-user">
							<div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
								<!-- kondisi jika $id == null maka tambah data -->
								<?php if($id==null):?>
									<h2>Edit Data Provinsi </h2>
										<form role="form" method="post" id="edit"  enctype="multipart/form-data">
											<div class="form-group">
												<label>Nama Provinsi</label>
												<input type="text" class="form-control" disabled>
											</div>
											<div class="form-group">
												<label>Nama Negara</label>
												<input type="text" class="form-control" disabled>
											</div>
											<div class="form-group">
												<label>Keterangan</label>
												<input type="text" class="form-control" disabled>
											</div>
											<button type="submit" name="submit" value="simpan" class="btn btn-info" disabled>Simpan</button>
											<button type="reset" name="reset" value="reset" class="btn btn-danger" disabled>Reset</button>
										</form>
								<?php elseif($id!=null):?>
									<h2>Edit Data Negara </h2>
										<form role="form" method="post" id="edit"  enctype="multipart/form-data">
											<div class="form-group">
												<label>Nama Provinsi</label>
												<input type="text" class="form-control" name="NamaProvinsi" value="<?php echo $nama_provinsi; ?>" required>
											</div>
											<div class="form-group">
												<label>NamaNegara</label>
												<select name="KodeNegara" class="form-control" required>	
													<?php
														$menu = mysqli_query($koneksi,"SELECT * FROM mstnegara GROUP by NamaNegara");
														while($kode = mysqli_fetch_array($menu)){
															if($kode['KodeNegara']==$kode_negara){
																echo "<option value=\"".$kode['KodeNegara']."\" selected >".$kode['NamaNegara']."</option>\n";
															}else{
																echo "<option value=\"".$kode['KodeNegara']."\" >".$kode['NamaNegara']."</option>\n";
															}
															
														}
													?>
												</select>
											</div>
											<div class="form-group">
												<label>Keterangan</label>
												<input type="text" class="form-control" name="Keterangan" value="<?php echo $ket; ?>">
											</div>
											<button type="submit" name="submit_edit" value="simpan" class="btn btn-info">Simpan</button>
											<button type="reset" name="reset" value="reset" class="btn btn-danger">Reset</button>
										</form>
								<?php endif?>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
							</div>
							</div>
							<!-- ===================================== End Edit ===================================== -->
						</div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="komponen/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="komponen/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="komponen/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="komponen/vendor/raphael/raphael.min.js"></script>
    <script src="komponen/vendor/morrisjs/morris.min.js"></script>
    <script src="komponen/data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="komponen/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>

</body>

</html>
