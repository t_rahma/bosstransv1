<?php
include ('akses.php');
/* $fitur_id = 13;
include ('login/lock-menu.php'); */

//kode jadi untuk server log
//include ('../library/kode-log-server.php');
include ('../library/tgl-indo.php');
$DateTime = date('Y-m-d H:i:s a');

include('../library/config.php');
date_default_timezone_set('Asia/Jakarta');

//Edit Data
if(@$_GET['metode']=='Edit'){
	$Tampil = mysqli_query($koneksi,"SELECT * FROM kontenweb WHERE JenisKonten='FAQ' AND KodeKonten='".base64_decode($_GET['id'])."'");
	$Data = mysqli_fetch_assoc($Tampil);
}

if(@$_GET['metode']=='Hapus'){
	$Hapus= mysqli_query($koneksi,"DELETE FROM kontenweb WHERE KodeKonten='".base64_decode($_GET['id'])."' AND JenisKonten='FAQ'");
	if($Hapus){
		echo '<script language="javascript">document.location="MasterFaq.php"; </script>';
	}else{
		echo '<script language="javascript">alert("Hapus Data Gagal !"); document.location="MasterFaq.php"; </script>';
	}
}

if(isset($_POST['SimpanData'])){
	$sql 	 = mysqli_query($koneksi,'SELECT RIGHT(KodeKonten,7) AS kode FROM kontenweb WHERE JenisKonten="FAQ" ORDER BY KodeKonten DESC LIMIT 1');  
	$num	 = mysqli_num_rows($sql);
	 
	if($num <> 0)
	 {
	 $data = mysqli_fetch_array($sql);
	 $kode = $data['kode'] + 1;
	 }else
	 {
	 $kode = 1;
	 }
	 
	//mulai bikin kode
	 $bikin_kode = str_pad($kode, 7, "0", STR_PAD_LEFT);
	 $kode_jadi	 = "KNT-".$bikin_kode;
	
	$Simpan = mysqli_query($koneksi,"INSERT INTO kontenweb (KodeKonten,Judul,Isi,UserName,JenisKonten)VALUES('$kode_jadi','".$_POST['Judul']."','".$_POST['Isi']."','$login_id','FAQ')");
	if($Simpan){
		echo '<script language="javascript">document.location="MasterFaq.php"; </script>';
	}else{
		echo '<script language="javascript">alert("Simpan Data Gagal !"); document.location="MasterFaq.php"; </script>';
	}
}

if(isset($_POST['EditData'])){
	$Edit = mysqli_query($koneksi,"UPDATE kontenweb SET Judul='".$_POST['Judul']."',Isi='".$_POST['Isi']."',UserName='$login_id' WHERE KodeKonten='".$_POST['KodeKonten']."' AND JenisKonten='FAQ' ");
	if($Edit){
		echo '<script language="javascript">document.location="MasterFaq.php"; </script>';
	}else{
		echo '<script language="javascript">alert("Edit Data Gagal !"); document.location="MasterFaq.php"; </script>';
	}
}

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <?php include 'view/title.php' ?>

    <!-- Bootstrap Core CSS -->
    <link href="komponen/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="komponen/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="komponen/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="komponen/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="komponen/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<!-- Datepcker -->
	<link rel="stylesheet" href="../library/Datepicker/dist/css/default/zebra_datepicker.min.css" type="text/css">
	
	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda Yakin Untuk Menghapus Data ?")
			if (answer == true){
				window.location = "MasterFaq.php";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
	</script>
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
		<?php include 'view/menu.php' ; ?>
		</nav>
		
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">FAQ</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
						<!-- Nav tabs -->
                            <ul class="nav nav-pills">
                                <li <?php if(@$_GET['metode']!='Edit'){ echo 'class="active"';}?>><a href="#home-pills" data-toggle="tab">Data FAQ</a></li>
                                
								<li><a href="#tambah-iklan" data-toggle="tab">Tambah Data</a></li>
								
								<?php if(@$_GET['metode']=='Edit'){echo '<li class="active"><a href="#edit-iklan" data-toggle="tab">Edit Data</a></li>';}?>
                            </ul>
						<div class="tab-content">
                            <div class="tab-pane fade <?php if(@$_GET['metode']!='Edit'){ echo 'in active';}?>" id="home-pills"><br/>
							<!-- ===================================== tabel ==================================== -->
							<form method="post" action="">
								<div class="form-group input-group col-lg-4 col-lg-offset-8">
									<input type="text" name="keyword" class="form-control" value="<?php echo @$_REQUEST['keyword']; ?>" placeholder="Pencarian...">
									<span class="input-group-btn">
										<button class="btn btn-large btn-warning" type="submit">Cari</button>
									 </span>
								</div>
							</form>
							
							<div class="table-responsive">
							<table width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Pertanyaan</th>
                                        <th>Jawaban</th>
                                        <th>Author</th>
										<th>Aksi</th>
                                    </tr>
                                </thead>
								<?php
									include '../library/pagination1.php';
									// mengatur variabel reload dan sql
									$kosong=null;
									if(isset($_REQUEST['keyword']) && $_REQUEST['keyword']<>""){
										// jika ada kata kunci pencarian (artinya form pencarian disubmit dan tidak kosong)pakai ini
										$keyword=$_REQUEST['keyword'];
										$reload = "MasterFaq.php?pagination=true&keyword=$keyword";
										$sql =  "SELECT * FROM kontenweb WHERE JenisKonten='FAQ' AND Judul LIKE '%$keyword%' ORDER BY KodeKonten ASC";
										$result = mysqli_query($koneksi,$sql);
									}else{
									//jika tidak ada pencarian pakai ini
										$reload = "MasterFaq.php?pagination=true";
										$sql =  "SELECT * FROM kontenweb WHERE JenisKonten='FAQ' ORDER BY KodeKonten ASC";
										$result = mysqli_query($koneksi,$sql);
									}
									
									//pagination config start
									$rpp = 10; // jumlah record per halaman
									$page = intval(@$_GET["page"]);
									if($page<=0) $page = 1;  
									$tcount = mysqli_num_rows($result);
									$tpages = ($tcount) ? ceil($tcount/$rpp) : 1; // total pages, last page number
									$count = 0;
									$i = ($page-1)*$rpp;
									$no_urut = ($page-1)*$rpp;
									//pagination config end				
								?>
                                <tbody>
									<?php
									while(($count<$rpp) && ($i<$tcount)) {
										mysqli_data_seek($result,$i);
										$data = mysqli_fetch_array($result);
									?>
                                    <tr class="odd gradeX">
                                        <td width="50px">
											<?php echo ++$no_urut;?> 
										</td>
										<td>
											<strong><?php echo $data ['Judul']; ?></strong>
										</td>
										<td>
											<?php echo substr($data['Isi'],0,600);?>
										</td>
										<td>
											<?php echo $data ['UserName']; ?>
										</td>
										<td width="100px">				
											<a href="MasterFaq.php?id=<?php echo base64_encode($data['KodeKonten']);?>&metode=Edit" title='Edit'><i class='btn btn-warning btn-sm'><span class='fa fa-pencil'></span></i></a>
											
											<a href="MasterFaq.php?id=<?php echo base64_encode($data['KodeKonten']);?>&metode=Hapus" title='Hapus' onclick="return confirmation()"><i class='btn btn-danger btn-sm'><span class='fa fa-trash'></span></i></a>
										</td>
                                    </tr>
									<?php
										$i++; 
										$count++;
									}
									?>
                                </tbody>
                            </table>
							<div><?php echo paginate_one($reload, $page, $tpages); ?></div>
                            <!-- /.table-responsive -->
							</div>
							<!-- ===================================== tabel ==================================== -->
							</div>
							
							<div class="tab-pane fade" id="tambah-iklan">
							<!-- ===================================== Edit ===================================== -->
							<div class="panel-body">
                            <div class="row">
								<form method="post" action="">
									<div class="form-group col-lg-12">
										<label>Pertanyaan</label>
										<input type="text" class="form-control" name="Judul" required>
									</div>
									
									<div class="form-group col-lg-12">
										<label>Jawaban</label>
										<textarea class="ckeditor" name="Isi" class="form-control" rows="4"></textarea>
									</div>
									<div class="col-lg-3">
										<button type="submit" class="btn btn-md btn-block btn-warning" name="SimpanData">Simpan</button>
									</div>
								</form>
                            </div>
                            <!-- /.row (nested) -->
							</div>
							<!-- ===================================== Edit ===================================== -->
							</div>
							
							<div class="tab-pane fade <?php if(@$_GET['metode']=='Edit'){ echo 'in active';}?>" id="edit-iklan">
							<!-- ===================================== Edit ===================================== -->
							<div class="panel-body">
                            <div class="row">
								<div class="col-lg-12">
									<br/>
									<form method="post" action="">
										<div class="form-group col-lg-6">
											<label>Pertanyaan</label>
											<input type="text" class="form-control" name="Judul" value="<?php echo $Data['Judul']; ?>" required>
											<input type="hidden" class="form-control" name="KodeKonten" value="<?php echo $Data['KodeKonten']; ?>" required>
										</div>
										<div class="form-group col-lg-12">
											<label>Jawaban</label>
											<textarea class="ckeditor" name="Isi" class="form-control" rows="4"><?php echo $Data['Isi']; ?></textarea>
										</div>
										<div class="col-lg-3">
											<button type="submit" class="btn btn-md btn-block btn-warning" name="EditData">Simpan Edit</button>
										</div>
									</form>
								</div>
                            </div>
                            <!-- /.row (nested) -->
							</div>
							<!-- ===================================== Edit ===================================== -->
							</div>
						</div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="komponen/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="komponen/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="komponen/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="komponen/vendor/raphael/raphael.min.js"></script>
    <script src="komponen/vendor/morrisjs/morris.min.js"></script>
    <script src="komponen/data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="komponen/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
		<!-- ckeditor JS -->
   <script type="text/javascript" src="../library/ckeditor/ckeditor.js"></script>
	<!-- DatePicker -->
<script type="text/javascript" src="../library/Datepicker/dist/zebra_datepicker.src.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#datepicker1').Zebra_DatePicker({format: 'Y-m-d H:i'});
		$('#datepicker2').Zebra_DatePicker({format: 'Y-m-d H:i'});
		$('#datepicker3').Zebra_DatePicker({format: 'Y-m-d H:i'});
		$('#datepicker4').Zebra_DatePicker({format: 'Y-m-d H:i'});
	});
</script>

<!-- membuat dropdown bertingkat -->
<script>
	var htmlobjek;
	$(document).ready(function(){
	  //apabila terjadi event onchange terhadap object <select id=nama_produk>
	  $("#KodeNegara").change(function(){
		var KodeNegara = $("#KodeNegara").val();
		$.ajax({
			url: "upload/ambil-menu.php",
			data: "KodeRM="+KodeNegara,
			cache: false,
			success: function(msg){
				//jika data sukses diambil dari server kita tampilkan
				//di <select id=kota>
				$("#KodeProvinsi").html(msg);
			}
		});
	  });
	});
</script>
<script>
	/* var htmlobjek;
	$(document).ready(function(){
	  //apabila terjadi event onchange terhadap object <select id=nama_produk>
	  $("#KodeNegara2").change(function(){
		var KodeNegara2 = $("#KodeNegara2").val();
		$.ajax({
			url: "upload/ambil-menu.php",
			data: "KodeRM="+KodeNegara2,
			cache: false,
			success: function(msg){
				//jika data sukses diambil dari server kita tampilkan
				//di <select id=kota>
				$("#KodeProvinsi2").html(msg);
			}
		});
	  });
	}); */
</script>
</body>

</html>
