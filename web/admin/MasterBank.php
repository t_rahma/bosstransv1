<?php
include ('akses.php');
$fitur_id = 4;
// include ('login/lock-menu.php');
//kode jadi untuk server log
date_default_timezone_set('Asia/Jakarta');
$DateTime = date('Y-m-d H:i:s a');

//tampilkan system setting
$QuerySetting=mysqli_query($koneksi,"SELECT * FROM systemsetting WHERE KodeCabang='$login_cabang' ORDER BY KodeSetting ASC");
while($DataSetting = mysqli_fetch_array($QuerySetting)){
	$KodeSetting[]	=$DataSetting['KodeSetting'];
	$ValueData[]	=$DataSetting['ValueData'];
}
//tampilkan master bank perusahaan
$QueryBank=mysqli_query($koneksi,"SELECT a.*, b.nama_bank FROM mstbank_perusahaan a JOIN mstbank b ON a.id_bank = b.id_bank WHERE a.is_aktif='1'");
while($DataBank = mysqli_fetch_array($QueryBank)){
	$NamaBank	=$DataBank['nama_bank'];
	$Atasnama	=$DataBank['atasnama'];
	$Norek		=$DataBank['norek'];
}

$Siang				= @$_POST['Siang'];
$Malam				= @$_POST['Malam'];
// $R4				= @$_POST['R4'];

// $R2_Malam		= @$_POST['R2_Malam'];
// $R3_Malam		= @$_POST['R3_Malam'];
// $R4_Malam		= @$_POST['R4_Malam'];

// $R2_KurangDari3	= @$_POST['R2_KurangDari3'];
// $R3_KurangDari3	= @$_POST['R3_KurangDari3'];
// $R4_KurangDari3	= @$_POST['R4_KurangDari3'];

// $R2_KurangDari3_Malam	= @$_POST['R2_KurangDari3_Malam'];
// $R3_KurangDari3_Malam	= @$_POST['R3_KurangDari3_Malam'];
// $R4_KurangDari3_Malam	= @$_POST['R4_KurangDari3_Malam'];

// $BookingR4		= @$_POST['BookingR4'];
// $RetribusiNominal= @$_POST['RetribusiNominal'];
// $RetribusiProsen = @$_POST['RetribusiProsen'];
// $JamPagi 		= @$_POST['JamPagi'];
// $JamMalam 		= @$_POST['JamMalam'];
// $TextBerjalan	= @$_POST['TextBerjalan'];

// $PersenPerusahaan= @$_POST['PersenPerusahaan'];
// $PersenPerusahaanCabang= @$_POST['PersenPerusahaanCabang'];
// $LelangKlien	= @$_POST['LelangKlien'];
// $SuspendKurir	= @$_POST['SuspendKurir'];
// $NamaBank		= @$_POST['NamaBank'];
// $NoRek			= @$_POST['NoRek'];
 
if(isset($_POST['submit_edit'])){
  if($IsServer=='1'){	
	if($Siang != $ValueData[0]){
		// include ('../library/kode-log-server.php');
		mysqli_query($koneksi,"UPDATE systemsetting SET ValueData='$R2' WHERE KodeSetting='".$KodeSetting[4]."' ");
		
		// mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
		// VALUES ('$kode_jadi_log','$DateTime','Edit Data','Sistem Setting : ".$KodeSetting[0]." : Tarif R2 Siang $R2','$login_id','$login_cabang')");
	}
	
	if($Malam != $ValueData[0]){
		include ('../library/kode-log-server.php');
		mysqli_query($koneksi,"UPDATE systemsetting SET ValueData4='$R2_Malam' WHERE KodeSetting='".$KodeSetting[2]."' ");
		
		// mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
		// VALUES ('$kode_jadi_log','$DateTime','Edit Data','Sistem Setting : ".$KodeSetting[0]." : Tarif R2 Malam $R2_Malam','$login_id','$login_cabang')");
	}
	
	if($R2_KurangDari3 != $ValueData2[0]){
		include ('../library/kode-log-server.php');
		mysqli_query($koneksi,"UPDATE systemsetting SET ValueData2='$R2_KurangDari3' WHERE KodeSetting='".$KodeSetting[0]."' ");
		
		mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
		VALUES ('$kode_jadi_log','$DateTime','Edit Data','Sistem Setting : ".$KodeSetting[0]." : Tarif R2 Siang < 3 KM $R2_KurangDari3','$login_id','$login_cabang')");
	}
	
	if($R2_KurangDari3_Malam != $ValueData3[0]){
		include ('../library/kode-log-server.php');
		mysqli_query($koneksi,"UPDATE systemsetting SET ValueData3='$R2_KurangDari3_Malam' WHERE KodeSetting='".$KodeSetting[0]."' ");
		
		mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
		VALUES ('$kode_jadi_log','$DateTime','Edit Data','Sistem Setting : ".$KodeSetting[0]." : Tarif R2 Malam < 3 KM $R2_KurangDari3_Malam','$login_id','$login_cabang')");
	}
	
	if($R3 != $ValueData[1]){
		include ('../library/kode-log-server.php');
		mysqli_query($koneksi,"UPDATE systemsetting SET ValueData='$R3' WHERE KodeSetting='".$KodeSetting[1]."' ");
		
		mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
		VALUES ('$kode_jadi_log','$DateTime','Edit Data','Sistem Setting : ".$KodeSetting[1]." : Tarif R3 Siang $R3','$login_id','$login_cabang')");
	}
	
	if($R3_Malam != $ValueData4[1]){
		include ('../library/kode-log-server.php');
		mysqli_query($koneksi,"UPDATE systemsetting SET ValueData4='$R3_Malam' WHERE KodeSetting='".$KodeSetting[1]."' ");
		
		mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
		VALUES ('$kode_jadi_log','$DateTime','Edit Data','Sistem Setting : ".$KodeSetting[1]." : Tarif R3 Malam $R3_Malam','$login_id','$login_cabang')");
	}
	
	if($R3_KurangDari3 != $ValueData2[1]){
		include ('../library/kode-log-server.php');
		mysqli_query($koneksi,"UPDATE systemsetting SET ValueData2='$R3_KurangDari3' WHERE KodeSetting='".$KodeSetting[1]."' ");
		
		mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
		VALUES ('$kode_jadi_log','$DateTime','Edit Data','Sistem Setting : ".$KodeSetting[1]." : Tarif R3 Siang < 3 KM $R3_KurangDari3','$login_id','$login_cabang')");
	}
	
	if($R3_KurangDari3_Malam != $ValueData3[1]){
		include ('../library/kode-log-server.php');
		mysqli_query($koneksi,"UPDATE systemsetting SET ValueData3='$R3_KurangDari3_Malam' WHERE KodeSetting='".$KodeSetting[1]."' ");
		
		mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
		VALUES ('$kode_jadi_log','$DateTime','Edit Data','Sistem Setting : ".$KodeSetting[1]." : Tarif R3 Malam < 3 KM $R3_KurangDari3_Malam','$login_id','$login_cabang')");
	}
	
	if($R4 != $ValueData[2]){
		include ('../library/kode-log-server.php');
		mysqli_query($koneksi,"UPDATE systemsetting SET ValueData='$R4' WHERE KodeSetting='".$KodeSetting[2]."' ");
		
		mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
		VALUES ('$kode_jadi_log','$DateTime','Edit Data','Sistem Setting : ".$KodeSetting[2]." : $R4','$login_id','$login_cabang')");
	}
	
	if($R4_Malam != $ValueData4[2]){
		include ('../library/kode-log-server.php');
		mysqli_query($koneksi,"UPDATE systemsetting SET ValueData4='$R4_Malam' WHERE KodeSetting='".$KodeSetting[2]."' ");
		
		mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
		VALUES ('$kode_jadi_log','$DateTime','Edit Data','Sistem Setting : ".$KodeSetting[2]." : Tarif R4 Malam $R4_Malam','$login_id','$login_cabang')");
	}
	
	if($R4_KurangDari3 != $ValueData2[2]){
		include ('../library/kode-log-server.php');
		mysqli_query($koneksi,"UPDATE systemsetting SET ValueData2='$R4_KurangDari3' WHERE KodeSetting='".$KodeSetting[2]."' ");
		
		mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
		VALUES ('$kode_jadi_log','$DateTime','Edit Data','Sistem Setting : ".$KodeSetting[2]." : Tarif R4 Siang < 3 KM $R4_KurangDari3','$login_id','$login_cabang')");
	}
	
	if($R4_KurangDari3_Malam != $ValueData3[2]){
		include ('../library/kode-log-server.php');
		mysqli_query($koneksi,"UPDATE systemsetting SET ValueData3='$R4_KurangDari3_Malam' WHERE KodeSetting='".$KodeSetting[2]."' ");
		
		mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
		VALUES ('$kode_jadi_log','$DateTime','Edit Data','Sistem Setting : ".$KodeSetting[2]." : Tarif R4 Malam < 3 KM $R4_KurangDari3_Malam','$login_id','$login_cabang')");
	}
  }
	
	if($BookingR4 != $ValueData[12]){
		include ('../library/kode-log-server.php');
		mysqli_query($koneksi,"UPDATE systemsetting SET ValueData='$BookingR4' WHERE KodeSetting='".$KodeSetting[12]."' AND KodeCabang='$login_cabang'");
		
		mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
		VALUES ('$kode_jadi_log','$DateTime','Edit Data','Sistem Setting : ".$KodeSetting[12]." : $BookingR4','$login_id','$login_cabang')");
	}
	
	if($RetribusiNominal != $ValueData[9]){
		include ('../library/kode-log-server.php');
		mysqli_query($koneksi,"UPDATE systemsetting SET ValueData='$RetribusiNominal' WHERE KodeSetting='".$KodeSetting[9]."' AND KodeCabang='$login_cabang'");
		
		mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
		VALUES ('$kode_jadi_log','$DateTime','Edit Data','Sistem Setting : ".$KodeSetting[9]." : $RetribusiNominal','$login_id','$login_cabang')");
	}
	
	if($RetribusiProsen != $ValueData[10]){
		include ('../library/kode-log-server.php');
		mysqli_query($koneksi,"UPDATE systemsetting SET ValueData='$RetribusiProsen' WHERE KodeSetting='".$KodeSetting[10]."' AND KodeCabang='$login_cabang'");
		
		mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
		VALUES ('$kode_jadi_log','$DateTime','Edit Data','Sistem Setting : ".$KodeSetting[10]." : $RetribusiProsen','$login_id','$login_cabang')");
	}
	
	if($TextBerjalan != $ValueData[11]){
		include ('../library/kode-log-server.php');
		mysqli_query($koneksi,"UPDATE systemsetting SET ValueData='$TextBerjalan' WHERE KodeSetting='".$KodeSetting[11]."' AND KodeCabang='$login_cabang'");
		
		mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
		VALUES ('$kode_jadi_log','$DateTime','Edit Data','Sistem Setting : ".$KodeSetting[11]." : $TextBerjalan','$login_id','$login_cabang')");
	}
	
	if($JamPagi != $ValueData[7]){
		include ('../library/kode-log-server.php');
		mysqli_query($koneksi,"UPDATE systemsetting SET ValueData='$JamPagi' WHERE KodeSetting='".$KodeSetting[7]."' AND KodeCabang='$login_cabang'");
		
		mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
		VALUES ('$kode_jadi_log','$DateTime','Edit Data','Sistem Setting : ".$KodeSetting[7]." : $JamPagi','$login_id','$login_cabang')");
	}
	
	if($JamMalam != $ValueData[8]){
		include ('../library/kode-log-server.php');
		mysqli_query($koneksi,"UPDATE systemsetting SET ValueData='$JamMalam' WHERE KodeSetting='".$KodeSetting[8]."' AND KodeCabang='$login_cabang'");
		
		mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
		VALUES ('$kode_jadi_log','$DateTime','Edit Data','Sistem Setting : ".$KodeSetting[8]." : $JamMalam','$login_id','$login_cabang')");
	}
	
	if($PersenPerusahaan != $ValueData[3]){
		include ('../library/kode-log-server.php');
		mysqli_query($koneksi,"UPDATE systemsetting SET ValueData='$PersenPerusahaan' WHERE KodeSetting='".$KodeSetting[3]."' AND KodeCabang='$login_cabang'");
		
		mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
		VALUES ('$kode_jadi_log','$DateTime','Edit Data','Sistem Setting : ".$KodeSetting[3]." : $PersenPerusahaan','$login_id','$login_cabang')");
	}
	
	if($LelangKlien != $ValueData[4]){
		include ('../library/kode-log-server.php');
		mysqli_query($koneksi,"UPDATE systemsetting SET ValueData='$LelangKlien' WHERE KodeSetting='".$KodeSetting[4]."' AND KodeCabang='$login_cabang'");
		
		mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
		VALUES ('$kode_jadi_log','$DateTime','Edit Data','Sistem Setting : ".$KodeSetting[4]." : $LelangKlien','$login_id','$login_cabang')");
	}
	
	if($SuspendKurir != $ValueData[5]){
		include ('../library/kode-log-server.php');
		mysqli_query($koneksi,"UPDATE systemsetting SET ValueData='$SuspendKurir' WHERE KodeSetting='".$KodeSetting[5]."' AND KodeCabang='$login_cabang'");
		
		mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
		VALUES ('$kode_jadi_log','$DateTime','Edit Data','Sistem Setting : ".$KodeSetting[5]." : $SuspendKurir','$login_id','$login_cabang')");
	}
	
	if($IsServer=='1'){
		if($NamaBank != $Title[6] OR $NoRek != $ValueData[6]){
			include ('../library/kode-log-server.php');
			mysqli_query($koneksi,"UPDATE systemsetting SET ValueData='$NoRek',Title='$NamaBank' WHERE KodeSetting='".$KodeSetting[6]."' AND KodeCabang='$login_cabang'");
			
			mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
			VALUES ('$kode_jadi_log','$DateTime','Edit Data','Sistem Setting : ".$KodeSetting[6]." : $NamaBank : $NoRek','$login_id','$login_cabang')");
		}
		
		if($PersenPerusahaanCabang != $ValueData[16]){
			include ('../library/kode-log-server.php');
			mysqli_query($koneksi,"UPDATE systemsetting SET ValueData='$PersenPerusahaanCabang' WHERE KodeSetting='".$KodeSetting[16]."'");
			
			mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
			VALUES ('$kode_jadi_log','$DateTime','Edit Data','Sistem Setting : ".$KodeSetting[16]." : Prosentase Bagi Hasil Cabang $PersenPerusahaanCabang','$login_id','$login_cabang')");
		}
	}

	echo '<script language="javascript">document.location="SistemSetting.php"; </script>';
 
 }
 

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <?php include 'view/title.php' ?>

    <!-- Bootstrap Core CSS -->
    <link href="komponen/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="komponen/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="komponen/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="komponen/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="komponen/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<!-- Datepcker -->
	<link rel="stylesheet" href="../library/Datepicker/dist/css/default/zebra_datepicker.min.css" type="text/css">

	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda Yakin Untuk Menghapus Data . . . ?")
			if (answer == true){
				window.location = "AccessLevel.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
	
	<script type="text/javascript">
		function confirmation2() {
			var answer = confirm("Apakah Anda Yakin Mengaktifkan Fitur ini . . . ?")
			if (answer == true){
				window.location = "AccessLevel.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
	
	<script type="text/javascript">
		function confirmation3() {
			var answer = confirm("Apakah Anda Yakin Non Aktifkan Fitur ini . . . ?")
			if (answer == true){
				window.location = "AccessLevel.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
		<?php include 'view/menu.php' ; ?>
		</nav>
		
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Sistem Setting</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
				<!-- ===================================== tabel ==================================== -->
				<label>Master Slider 
					<a href="SistemSetting.php?id=slider&aksi=<?php echo base64_encode('Tambah');?>" title='Tambah' ><span class='btn btn-success btn-sm'>Tambah Slider</span></a>
				</label>
				<div class="table-responsive">
				<table width="80%" class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>No</th>
							<th>Gambar</th>
							<th>Judul</th>
							<th>Deskripsi</th>
							<th>Status</th>
							<th>Viewer</th>
							<th>Aksi</th>
						</tr>
					</thead>

					<tbody>
					<?php 
					$select_slider = mysqli_query($koneksi,"select * from mstslider");
					$no_urut=0;
					while($data = mysqli_fetch_array($select_slider)){
					?>
					<tr class="odd gradeX">
						<td width="50px">
							<?php echo ++$no_urut;?> 
						</td>
						<td width = "150px">
							<img src="../../../andro/foto_slider/<?php echo $data ['Gambar']; ?>" class="img img-responsive img-thumbnail" width="100px">
						</td>
						<td>
							<strong><?php echo $data ['Judul']; ?></strong>
						</td>
						<td>
							<?php echo $data ['Isi']; ?>
						</td>
						<td>
							<?php
							if($data['IsAktif'] == 1){
								?>
								<a title='Aktif' ><span class='btn btn-info btn-sm'>Aktif</span></a>
								<?php
							}else{
								?>
								<a title='NonAktif' ><span class='btn btn-info btn-sm'>NonAktif</span></a>
								<?php
							}
							?>
						</td>
						<td>
							<?php
							if($data['IsDriver'] == 1){
								?>
								<a title='Driver' ><span class='btn btn-warning btn-sm'>Driver</span></a>
								<?php
							}else{
								?>
								<a title='Klien' ><span class='btn btn-success btn-sm'>Klien</span></a>
								<?php
							}
							?>
						</td>
						<td width="100px">											
							<?php if($IsServer=='1'){ ?>
							<a href="SistemSetting.php?id=slider&aksi=<?php echo base64_encode('Hapus');?>" title='Hapus' onclick='return confirmation()'><i class='btn btn-danger btn-sm'><span class='fa fa-trash'></span></i></a>
							<?php }else{
								echo '<i class="btn btn-danger btn-sm disabled"><span class="fa fa-trash"></span></i>';
							}?>
						</td>
					</tr>
					<?php
					}
					?>
						
					</tbody>
				</table>
				<!-- /.table-responsive -->
				<!-- ===================================== tabel ==================================== -->
				<label>Master Bank Perusahaan
					<a href="SistemSetting.php?id=bank&aksi=<?php echo base64_encode('Tambah');?>" title='Tambah' ><span class='btn btn-success btn-sm'>Tambah Slider</span></a>
				</label>
				<div class="table-responsive">
				<table width="80%" class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>No</th>
							<th>Gambar</th>
							<th>Judul</th>
							<th>Deskripsi</th>
							<th>Status</th>
							<th>Viewer</th>
							<th>Aksi</th>
						</tr>
					</thead>

					<tbody>
					<?php 
					$select_slider = mysqli_query($koneksi,"select * from mstslider");
					$no_urut=0;
					while($data = mysqli_fetch_array($select_slider)){
					?>
					<tr class="odd gradeX">
						<td width="50px">
							<?php echo ++$no_urut;?> 
						</td>
						<td width = "150px">
							<img src="../../../andro/foto_slider/<?php echo $data ['Gambar']; ?>" class="img img-responsive img-thumbnail" width="100px">
						</td>
						<td>
							<strong><?php echo $data ['Judul']; ?></strong>
						</td>
						<td>
							<?php echo $data ['Isi']; ?>
						</td>
						<td>
							<?php
							if($data['IsAktif'] == 1){
								?>
								<a title='Aktif' ><span class='btn btn-info btn-sm'>Aktif</span></a>
								<?php
							}else{
								?>
								<a title='NonAktif' ><span class='btn btn-info btn-sm'>NonAktif</span></a>
								<?php
							}
							?>
						</td>
						<td>
							<?php
							if($data['IsDriver'] == 1){
								?>
								<a title='Driver' ><span class='btn btn-warning btn-sm'>Driver</span></a>
								<?php
							}else{
								?>
								<a title='Klien' ><span class='btn btn-success btn-sm'>Klien</span></a>
								<?php
							}
							?>
						</td>
						<td width="100px">											
							<?php if($IsServer=='1'){ ?>
							<a href="SistemSetting.php?id=<?php echo base64_encode($data['KodePerson']);?>&aksi=<?php echo base64_encode('Hapus');?>" title='Hapus' onclick='return confirmation()'><i class='btn btn-danger btn-sm'><span class='fa fa-trash'></span></i></a>
							<?php }else{
								echo '<i class="btn btn-danger btn-sm disabled"><span class="fa fa-trash"></span></i>';
							}?>
						</td>
					</tr>
					<?php
					}
					?>
						
					</tbody>
				</table>
				<!-- /.table-responsive -->
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
							<form role="form" method="post" id="edit"  enctype="multipart/form-data">
								<div class="col-lg-4">
									<label>Tarif Kendaraan/Km</label>
									<div class="form-group input-group">
										<span class="input-group-addon">Siang</span>
										<input type="number" name="Siang" class="form-control" value="<?php echo $ValueData[4];?>" placeholder="Tarif Siang" required <?php if($IsServer!='1'){echo 'readonly';}?>>
									</div>
									<div class="form-group input-group">
										<span class="input-group-addon">Malam</span>
										<input type="number" name="Malam" class="form-control" value="<?php echo $ValueData[2];?>" placeholder="Tarif Malam" required <?php //if($IsServer!='1'){echo 'readonly';}?>>
									</div>
									<!--<div class="form-group input-group">
										<span class="input-group-addon">R4</span>
										<input type="number" name="R4" class="form-control" value="<?php //echo $ValueData[2];?>" placeholder="Tarif Roda 4" required <?php //if($IsServer!='1'){echo 'readonly';}?>>
									</div>-->
									
									<label>Tarif Minimum Order</label>
									<div class="form-group input-group">
										<span class="input-group-addon">Min</span>
										<input type="number" name="Min" class="form-control" value="<?php echo $ValueData[3];?>" placeholder="Tarif Minimum" required <?php if($IsServer!='1'){echo 'readonly';}?>>
									</div>
									<!--<div class="form-group input-group">
										<span class="input-group-addon">R3</span>
										<input type="number" name="R3_Malam" class="form-control" value="<?php //echo $ValueData4[1];?>" placeholder="Tarif Roda 3" required <?php //if($IsServer!='1'){echo 'readonly';}?>>
									</div>
									<div class="form-group input-group">
										<span class="input-group-addon">R4</span>
										<input type="number" name="R4_Malam" class="form-control" value="<?php //echo $ValueData4[2];?>" placeholder="Tarif Roda 4" required <?php //if($IsServer!='1'){echo 'readonly';}?>>
									</div>-->
									
									<label>Saldo Minimum Dompet Driver</label>
									<div class="form-group input-group">
										<span class="input-group-addon">Saldo</span>
										<input type="number" name="saldo_min" class="form-control" value="<?php echo $ValueData[5];?>" placeholder="Saldo Minimum" required <?php //if($IsServer!='1'){echo 'readonly';}?>>
									</div>
									<!--<div class="form-group input-group">
										<span class="input-group-addon">R3</span>
										<input type="number" name="R3_KurangDari3" class="form-control" value="<?php //echo $ValueData2[1];?>" placeholder="Tarif Roda 3" required <?php //if($IsServer!='1'){echo 'readonly';}?>>
									</div>
									<div class="form-group input-group">
										<span class="input-group-addon">R4</span>
										<input type="number" name="R4_KurangDari3" class="form-control" value="<?php //echo $ValueData2[2];?>" placeholder="Tarif Roda 4" required <?php //if($IsServer!='1'){echo 'readonly';}?>>
									</div>-->
									
									<label>Prosentase Bagi Hasil Perusahaan</label>
									<div class="form-group input-group">
										<span class="input-group-addon">%</span>
										<input type="number" name="bagi_hasil" class="form-control" value="<?php echo $ValueData[6];?>" placeholder="Prosentase bagi hasil" required <?php //if($IsServer!='1'){echo 'readonly';}?>>
									</div>
									<!--<div class="form-group input-group">
										<span class="input-group-addon">R3</span>
										<input type="number" name="R3_KurangDari3_Malam" class="form-control" value="<?php //echo $ValueData3[1];?>" placeholder="Tarif Roda 3" required <?php //if($IsServer!='1'){echo 'readonly';}?>>
									</div>
									<div class="form-group input-group">
										<span class="input-group-addon">R4</span>
										<input type="number" name="R4_KurangDari3_Malam" class="form-control" value="<?php //echo $ValueData3[2];?>" placeholder="Tarif Roda 4" required <?php //if($IsServer!='1'){echo 'readonly';}?>>
									</div>-->
								</div>
								<div class="col-lg-4">
									<label>Jarak Minimum</label>
									<div class="form-group input-group">
										<span class="input-group-addon">KM</span>
										<input type="number" name="JarakMinimum" class="form-control" value="<?php echo $ValueData[1];?>" placeholder="Jarak Minimum" required>
									</div>
									<label>Jarak Maksimum</label>
									<div class="form-group input-group">
										<span class="input-group-addon">KM</span>
										<input type="number" name="JarakMaksimum" class="form-control" value="<?php echo $ValueData[0];?>" placeholder="Jarak Maksimum" required>
									</div>
									<!--<label>Tarif Retribusi Penjual (%)</label>
									<div class="form-group input-group">
										<span class="input-group-addon">%</span>
										<input type="number" name="RetribusiProsen" class="form-control" value="<?php //echo $ValueData[10];?>" placeholder="Tarif Retribusi Prosen" required>
									</div>
									<label>Prosentase Perusahaan (Retribusi Kurir)</label>
									<div class="form-group input-group">
										<span class="input-group-addon">%</span>
										<input type="number" name="PersenPerusahaan" class="form-control" value="<?php //echo $ValueData[3];?>" placeholder="Persentase Perusahaan" required>
									</div>
								<?php //if($IsServer=='1'){ ?>
									<label>Prosentase Bagi Hasil (Cabang Usaha)</label>
									<div class="form-group input-group">
										<span class="input-group-addon">%</span>
										<input type="number" name="PersenPerusahaanCabang" class="form-control" value="<?php //echo $ValueData[16];?>" placeholder="Prosentase Bagi Hasil (Cabang Usaha)" required>
									</div>
								<?php//} ?>
									<label>Durasi Lelang Klien (Menit)</label>
									<div class="form-group input-group">
										<span class="input-group-addon">M</span>
										<input type="number" name="LelangKlien" class="form-control" value="<?php //echo $ValueData[4];?>" placeholder="Durasi Lelang Klien (Menit)" required>
									</div>
									<label>Durasi Suspend Kurir (Menit)</label>
									<div class="form-group input-group">
										<span class="input-group-addon">M</span>
										<input type="number" name="SuspendKurir" class="form-control" value="<?php //echo $ValueData[5];?>" placeholder="Durasi Suspend Kurir (Menit)" required>
									</div>-->
								</div>
								<div class="col-lg-4">
									<!--<label>Jam Pagi</label>
									<div class="form-group">
										<input type="text" name="JamPagi" class="form-control" value="<?php echo $ValueData[7];?>" placeholder="Jam Pagi" id="jam1" required>
									</div>
									<label>Jam Malam</label>
									<div class="form-group">
										<input type="text" name="JamMalam" class="form-control" value="<?php echo $ValueData[8];?>" placeholder="Jam Malam" id="jam2" required>
									</div>
									<label>Text Berjalan</label>
									<div class="form-group">
										<input type="text" name="TextBerjalan" class="form-control" value="<?php echo $ValueData[11];?>" placeholder="Text Berjalan" required>
									</div>-->
									<label>Nama Bank & Atas Nama Rek. Perusahaan</label>
									<div class="form-group">
										<input type="text" class="form-control" name="NamaBank" value="<?php echo $NamaBank;?>" placeholder="Nama Bank" required <?php if($IsServer!='1'){echo 'readonly';}?>>
									</div>
									<label>No. Rekening Perusahaan</label>
									<div class="form-group">
										<input type="number" class="form-control" name="NoRek" value="<?php echo $Norek;?>" placeholder="No. Rekening" required <?php if($IsServer!='1'){echo 'readonly';}?>>
									</div>
								</div>
								<div class="form-group col-lg-12 text-center">
									<button type="submit" name="submit_edit" value="simpan" class="btn btn-info">Simpan Sistem Setting</button>
									<button type="reset" name="reset" value="reset" class="btn btn-danger">Reset Sistem Setting</button>
								</div>
							</form>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="komponen/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="komponen/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="komponen/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="komponen/vendor/raphael/raphael.min.js"></script>
    <script src="komponen/vendor/morrisjs/morris.min.js"></script>
    <script src="komponen/data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="komponen/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
	
	<!-- ckeditor JS -->
   <script type="text/javascript" src="../library/ckeditor/ckeditor.js"></script>
	<!-- DatePicker -->
	<script type="text/javascript" src="../library/Datepicker/dist/zebra_datepicker.src.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			$('#jam1').Zebra_DatePicker({format: 'H:i'});
			$('#jam2').Zebra_DatePicker({format: 'H:i'});
		});
	</script>
</body>

</html>
