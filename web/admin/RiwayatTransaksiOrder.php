<?php
include ('akses.php');
$fitur_id = 21;
include ('login/lock-menu.php');

//kode jadi untuk server log
include ('../library/kode-log-server.php');
include ('../library/tgl-indo.php');
$DateTime = date('Y-m-d H:i:s');

include('../library/config.php');
date_default_timezone_set('Asia/Jakarta');
$id = base64_decode(@$_GET['id']);
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <?php include 'view/title.php' ?>

    <!-- Bootstrap Core CSS -->
    <link href="komponen/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="komponen/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="komponen/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="komponen/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="komponen/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda Yakin Untuk Mengganti Kurir Ini ?")
			if (answer == true){
				window.location = "MasterKurir.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
	
	<!-- Datepcker -->
	<link rel="stylesheet" href="../library/Datepicker/dist/css/default/zebra_datepicker.min.css" type="text/css">
	
	<!-- Select 2 Skin -->
	<link rel="stylesheet" href="../library/select2-master/dist/css/select2.css"/>
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
		<?php include 'view/menu.php' ; ?>
		</nav>
		
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Riwayat Transaksi Order</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
						<!-- Nav tabs -->
						<ul class="nav nav-pills">
							<li <?php if(@$id==null){echo 'class="active"';} ?>><a href="#home-pills" data-toggle="tab">Data Order</a></li>
							<?php if(@$id!=null){
								echo '<li class="active"><a href="#tambah-user" data-toggle="tab">Detil Order</a></li>';
							} ?>
						</ul>
						<div class="tab-content">
                            <div class="tab-pane fade <?php if(@$id==null){echo 'in active';} ?>" id="home-pills">
							<form method="post" action="">
								<div class="form-group input-group col-lg-4 col-lg-offset-8">
									<input type="text" name="keyword" class="form-control" value="<?php echo @$_REQUEST['keyword']; ?>" placeholder="Cari Nama Kurir/Klien/Penjual">
									<span class="input-group-btn">
										<button class="btn btn-large btn-info" type="submit">Cari</button>
									 </span>
								</div>
							</form>
							<!-- ===================================== tabel ==================================== -->
							<div class="table-responsive">
							<table width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal Kirim</th>
                                        <th>Nama Klien</th>
                                        <th>Nama Kurir</th>
                                        <th>Tempat Penjual</th>
                                        <th>Pembayaran</th>
                                        <th>Jenis Order</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
								<?php
									include '../library/pagination1.php';
									// mengatur variabel reload dan sql
									$kosong=null;
									if(isset($_REQUEST['keyword']) && $_REQUEST['keyword']<>""){
										// jika ada kata kunci pencarian (artinya form pencarian disubmit dan tidak kosong)pakai ini
										$keyword=$_REQUEST['keyword'];
										$reload = "RiwayatTransaksiOrder.php?pagination=true&keyword=$keyword";
										$sql =  "SELECT a.IsTrCariKurir,a.JamACCPenjual,a.NoTrOrder,a.TglKirim,a.StatusOrder, a.JenisPembayaran, a.KodePersonKurir,b.NamaPerson as Kurir,b.NoHP as HpKurir,c.NamaPerson as Pembeli,c.NoHP as HpPembeli,e.NamaRM,e.AlamatRM,e.TelpRM
										FROM trordermakanan a LEFT JOIN mstperson b ON a.KodePersonKurir=b.KodePerson 
										JOIN mstperson c ON a.KodePersonPembeli=c.KodePerson
										LEFT JOIN menuorder d ON a.NoTrOrder=d.NoTrOrder
										LEFT JOIN rm_penjual e ON (d.KodePerson,d.KodeRM)=(e.KodePerson,e.KodeRM)
										WHERE a.KodeCabang='$login_cabang' AND (a.StatusOrder='Failed' OR a.StatusOrder='Selesai') AND (b.NamaPerson LIKE '%$keyword%' || c.NamaPerson LIKE '%$keyword%' || e.NamaRM LIKE '%$keyword%') GROUP BY a.NoTrOrder ORDER BY a.TglKirim DESC";
										
										$result = mysqli_query($koneksi,$sql);
									}else{
									//jika tidak ada pencarian pakai ini
										$reload = "RiwayatTransaksiOrder.php?pagination=true";
										/* $sql =  "SELECT a.JamACCPenjual,a.NoTrOrder,a.TglKirim,a.StatusOrder,a.JenisPembayaran, a.KodePersonKurir,b.NamaPerson as Kurir,b.NoHP as HpKurir,c.NamaPerson as Pembeli,c.NoHP as HpPembeli,e.NamaRM,e.AlamatRM,e.TelpRM
										FROM trordermakanan a LEFT JOIN mstperson b ON a.KodePersonKurir=b.KodePerson 
										JOIN mstperson c ON a.KodePersonPembeli=c.KodePerson
										JOIN menuorder d ON a.NoTrOrder=d.NoTrOrder
										JOIN rm_penjual e ON (d.KodePerson,d.KodeRM)=(e.KodePerson,e.KodeRM)
										WHERE (StatusOrder!='Waiting' AND StatusOrder!='Failed' AND StatusOrder!='Selesai') GROUP BY a.NoTrOrder ORDER BY a.TglKirim ASC"; */
										$sql = "SELECT a.IsTrCariKurir,a.JamACCPenjual,a.NoTrOrder,a.TglKirim,a.StatusOrder, a.JenisPembayaran, a.KodePersonKurir,b.NamaPerson as Kurir,b.NoHP as HpKurir,c.NamaPerson as Pembeli,c.NoHP as HpPembeli,e.NamaRM,e.AlamatRM,e.TelpRM 
										FROM trordermakanan a LEFT JOIN mstperson b ON a.KodePersonKurir=b.KodePerson 
										JOIN mstperson c ON a.KodePersonPembeli=c.KodePerson 
										LEFT JOIN menuorder d ON a.NoTrOrder=d.NoTrOrder 
										LEFT JOIN rm_penjual e ON (d.KodePerson,d.KodeRM)=(e.KodePerson,e.KodeRM) 
										WHERE a.KodeCabang='$login_cabang' AND (a.StatusOrder='Failed' OR a.StatusOrder='Selesai') AND a.TanggalOrder is not null GROUP BY a.NoTrOrder ORDER BY a.TglKirim DESC";
										$result = mysqli_query($koneksi,$sql);
									}
									
									//pagination config start
									$rpp = 25; // jumlah record per halaman
									$page = intval(@$_GET["page"]);
									if($page<=0) $page = 1;  
									$tcount = mysqli_num_rows($result);
									$tpages = ($tcount) ? ceil($tcount/$rpp) : 1; // total pages, last page number
									$count = 0;
									$i = ($page-1)*$rpp;
									$no_urut = ($page-1)*$rpp;
									//pagination config end				
								?>
                                <tbody>
									<?php
									while(($count<$rpp) && ($i<$tcount)) {
										mysqli_data_seek($result,$i);
										$data = mysqli_fetch_array($result);
									?>
                                    <tr class="odd gradeX">
                                        <td width="50px">
											<?php echo ++$no_urut;?> 
										</td>
										<td>
											<?php 
												if($data ['TglKirim']==null){
													echo '-';
												}else{
													echo TanggalIndo($data ['TglKirim']); echo '<br/>';echo substr($data['TglKirim'],10,19);
												}
											?>
										</td>
										<td>
											<strong><?php echo $data ['Pembeli']; ?></strong><br/>
											Hp.<?php echo $data['HpPembeli'];?>
										</td>
										<td>
											<?php if($data ['Kurir']==null){
												echo '-';
											}else{
												echo '<strong>';echo $data ['Kurir']; echo '</strong>';
												echo '<br/>'; 
												echo $data['HpKurir'];
											}
											?>
										</td>
										<td>
											<?php 
												if($data['IsTrCariKurir']=='1'){
													echo '-';
												}else{
													echo $data ['NamaRM']; echo '<br/>';
													echo 'Hp.'; echo $data['TelpRM']; echo '<br/>';
													echo 'Alamat.'; echo $data['AlamatRM'];
												}
											?>
										</td>
										<td>
											<?php echo $data ['JenisPembayaran']; ?>
										</td>
										<td>
											<?php
											if($data['IsTrCariKurir']=='1'){
												echo 'Pengiriman Barang';
											}else{
												echo 'Order & Kirim Makanan';
											}
											?>
											
										</td>
										<td>
											<?php 
												if($data['StatusOrder']=='Failed'){
													echo '<font color="red">Gagal</font>';
												}elseif($data['StatusOrder']=='Selesai'){
													echo '<font color="green">Selesai</font>';
												}
											?>
										</td>
                                    </tr>
									<?php
										$i++; 
										$count++;
									}
									?>
                                </tbody>
                            </table>
							<div><?php echo paginate_one($reload, $page, $tpages); ?></div>
							</div>
							<!-- ===================================== tabel ==================================== -->
							</div>
							
						</div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="komponen/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="komponen/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="komponen/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="komponen/vendor/raphael/raphael.min.js"></script>
    <script src="komponen/vendor/morrisjs/morris.min.js"></script>
    <script src="komponen/data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="komponen/dist/js/sb-admin-2.js"></script>
	<!-- Select 2 -->
	<script src="../library/select2-master/dist/js/select2.min.js"></script>
	<script>
	$(document).ready(function () {
		$("#kota").select2({
		placeholder: "Cari Nama Kurir Pengganti"
		});
	});
	</script>
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>

<!-- DatePicker -->
<script type="text/javascript" src="../library/Datepicker/dist/zebra_datepicker.src.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#datepicker').Zebra_DatePicker();
	});
</script>
</body>
</html>
