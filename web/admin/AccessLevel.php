<?php
include ('akses.php');
$fitur_id = 11;
include ('login/lock-menu.php');

//kode jadi untuk server log
$DateTime = date('Y-m-d H:i:s a');

include('../library/config.php');
date_default_timezone_set('Asia/Jakarta');

//$LevelID 			= @$_POST['LevelID'];
$LevelName		 	= @$_POST['LevelName'];
$IsAktif		 	= @$_POST['IsAktif'];

$id 				= base64_decode(@$_GET['id']);

if($IsServer=='1'){
	$KodeCabang = base64_decode(@$_GET['cab']);
	$InputCabang= @$_POST['KodeCabang']; //dari form input
}else{
	$KodeCabang = $login_cabang;
	$InputCabang= $login_cabang;
} 

if(isset($_POST['submit'])){
	include ('../library/kode-log-server.php');
	//membuat id user
	$sql = mysqli_query($koneksi,"SELECT RIGHT(LevelID,6) AS kode FROM accesslevel WHERE KodeCabang='$InputCabang' ORDER BY LevelID DESC LIMIT 1");  
	$nums = mysqli_num_rows($sql);
	 
	if($nums <> 0){
	 $data = mysqli_fetch_array($sql);
	 $kode = $data['kode'] + 1;
	}else{
	 $kode = 1;
	}
	 
	//mulai bikin kode
	 $bikin_kode = str_pad($kode, 6, "0", STR_PAD_LEFT);
	 $kode_jadi = $bikin_kode;
	
	if($IsAktif=='1'){
		$query = mysqli_query($koneksi,"INSERT into accesslevel (LevelID,KodeCabang,LevelName,IsAktif) VALUES ('$kode_jadi','$InputCabang','$LevelName','1')");
		if($query){
			mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
			VALUES ('$kode_jadi_log','$DateTime','Tambah Data','Access Level : $LevelName','$login_id','$login_cabang')");
			echo '<script language="javascript">alert("Data berhasil disimpan !"); document.location="AccessLevel.php"; </script>';
		}else{
			echo '<script language="javascript">alert("Input data gagal !"); document.location="AccessLevel.php"; </script>';
		}
	}else{
		$query = mysqli_query($koneksi,"INSERT into accesslevel (LevelID,KodeCabang,LevelName,IsAktif) VALUES ('$kode_jadi','$InputCabang','$LevelName','0')");
		if($query){
			mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
			VALUES ('$kode_jadi_log','$DateTime','Tambah Data','Access Level : $LevelName','$login_id','$login_cabang')");
			echo '<script language="javascript">alert("Data berhasil disimpan !"); document.location="AccessLevel.php"; </script>';
		}else{
			echo '<script language="javascript">alert("Input data gagal !"); document.location="AccessLevel.php"; </script>';
		}
	}
}
 
 //===========procedure Update data=============//
 $edit = mysqli_query($koneksi,"select * from accesslevel where LevelID = '$id' AND KodeCabang='$KodeCabang'");
 while($row = mysqli_fetch_array($edit)){
	$level_id 	= $row['LevelID'];
	$level_name	= $row['LevelName'];
	$is_aktif	= $row['IsAktif'];
 }
 
 if(isset($_POST['submit_edit'])){
	 include ('../library/kode-log-server.php');
	$query = mysqli_query($koneksi,"UPDATE accesslevel SET LevelName ='$LevelName',IsAktif='$IsAktif' WHERE LevelID = '$id' AND KodeCabang='$KodeCabang'");
		if($query){
			mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
			VALUES ('$kode_jadi_log','$DateTime','Edit Data','Access Level : $LevelName : $IsAktif','$login_id','$login_cabang')");
			echo '<script language="javascript">alert("Edit Data berhasil disimpan !"); document.location="AccessLevel.php"; </script>';
		}else{
			echo '<script language="javascript">alert("Edit data gagal !"); document.location="AccessLevel.php?id='.base64_encode($id).'&cab='.base64_encode($KodeCabang).'"; </script>';
		}
 }
 
 //------------------------------------------------
 if(base64_decode(@$_GET['aksi'])=='Aktif'){
	include ('../library/kode-log-server.php');
	$query = mysqli_query($koneksi,"UPDATE accesslevel SET IsAktif='1' WHERE LevelID = '$id' AND KodeCabang='$KodeCabang'");
	if($query){
		mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
		VALUES ('$kode_jadi_log','$DateTime','Edit Data','Set Aktif Access Level : $id','$login_id','$login_cabang')");
		echo '<script language="javascript">document.location="AccessLevel.php"; </script>';
	}else{
		echo '<script language="javascript">alert("Set Aktif data gagal !"); document.location="AccessLevel.php?id='.base64_encode($id).'"; </script>';
	}
 }
 
 if(base64_decode(@$_GET['aksi'])=='NonAktif'){
	include ('../library/kode-log-server.php');
	$query = mysqli_query($koneksi,"UPDATE accesslevel SET IsAktif='0' WHERE LevelID = '$id' AND KodeCabang='$KodeCabang'");
	if($query){
		mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
		VALUES ('$kode_jadi_log','$DateTime','Edit Data','Set Non Aktif Access Level : $id','$login_id','$login_cabang')");
		echo '<script language="javascript">document.location="AccessLevel.php"; </script>';
	}else{
		echo '<script language="javascript">alert("Set Non Aktif data gagal !"); document.location="AccessLevel.php?id='.base64_encode($id).'"; </script>';
	}
 }
 
 if(base64_decode(@$_GET['aksi'])=='Hapus'){
	include ('../library/kode-log-server.php');
	$query = mysqli_query($koneksi,"DELETE FROM accesslevel WHERE LevelID = '$id' AND KodeCabang='$KodeCabang'");
	if($query){
		mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
		VALUES ('$kode_jadi_log','$DateTime','Hapus Data','Access Level : $id','$login_id','$login_cabang')");
		echo '<script language="javascript">document.location="AccessLevel.php"; </script>';
	}else{
		echo '<script language="javascript">alert("Hapus data gagal !"); document.location="AccessLevel.php"</script>';
	}
 }
 
 //----------------------------------
if(@$_REQUEST['keyword']!=null){
	$cabang = @$_REQUEST['keyword'];
}else{
	$cabang = $login_cabang;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <?php include 'view/title.php' ?>

    <!-- Bootstrap Core CSS -->
    <link href="komponen/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="komponen/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="komponen/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="komponen/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="komponen/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda Yakin Untuk Menghapus Data . . . ?")
			if (answer == true){
				window.location = "AccessLevel.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
	
	<script type="text/javascript">
		function confirmation2() {
			var answer = confirm("Apakah Anda Yakin Mengaktifkan Fitur ini . . . ?")
			if (answer == true){
				window.location = "AccessLevel.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
	
	<script type="text/javascript">
		function confirmation3() {
			var answer = confirm("Apakah Anda Yakin Non Aktifkan Fitur ini . . . ?")
			if (answer == true){
				window.location = "AccessLevel.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
		<?php include 'view/menu.php' ; ?>
		</nav>
		
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Access Level</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">					
					<div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
						<!-- Nav tabs -->
                            <ul class="nav nav-pills">
                                <li <?php if($id==null){echo 'class="active"';} ?>><a href="#home-pills" data-toggle="tab">Level ID</a>
                                </li>
                                <li><a href="#tambah-user" data-toggle="tab">Tambah Data</a>
                                </li>
								<li <?php if($id!=null){echo 'class="active"';} ?>><a href="#edit-user" data-toggle="tab">Edit Data</a>
                                </li>
                            </ul>
						<div class="tab-content">
                            <div class="tab-pane fade <?php if($id==null){echo 'in active';} ?>" id="home-pills">
							<!-- ===================================== tabel ==================================== -->
							<div class="table-responsive col-lg-8">
							<h2>Level ID</h2>
							<?php if(@$IsServer=='1'){ ?>
							<form method="post" action="">
								<div class="form-group input-group col-lg-5 col-lg-offset-7">
									<select name="keyword" class="form-control" required>	
										
										<?php
											echo '<option value="">-- Cabang Perusahaan --</option>';
											$menu = mysqli_query($koneksi,"SELECT a.*,b.NamaProvinsi,c.NamaKab FROM mstcabang a JOIN mstprovinsi b ON a.KodeProvinsi=b.KodeProvinsi JOIN mstkabupaten c ON a.KodeKab=c.KodeKab ORDER BY a.KodeCabang ASC");
											while($kode = mysqli_fetch_array($menu)){
												if($kode['KodeCabang']==$cabang){
													echo "<option value=\"".$kode['KodeCabang']."\" selected >".$kode['NamaProvinsi']." - ".$kode['NamaKab']."</option>\n";
												}else{
													echo "<option value=\"".$kode['KodeCabang']."\">".$kode['NamaProvinsi']." - ".$kode['NamaKab']."</option>\n";
												}
												
											}
										?>
									</select>
									<span class="input-group-btn">
										<button class="btn btn-large btn-info" type="submit">Cari</button>
									</span>
								</div>
							</form>
							<?php } ?>
							
							<table width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Level Name</th>
                                        <th>Status</th>
                                        <th>Hak Akses</th>
										<th>Aksi</th>
                                    </tr>
                                </thead>
								<?php
									include '../library/pagination1.php';
									// mengatur variabel reload dan sql
									$kosong=null;
									if(isset($_REQUEST['keyword']) && $_REQUEST['keyword']<>""){
										// jika ada kata kunci pencarian (artinya form pencarian disubmit dan tidak kosong)pakai ini
										$keyword=$_REQUEST['keyword'];
										$reload = "AccessLevel.php?pagination=true&keyword=$keyword";
										$sql =  "SELECT * FROM accesslevel WHERE KodeCabang='$keyword' ORDER BY LevelID ASC";
										$result = mysqli_query($koneksi,$sql);
									}else{
									//jika tidak ada pencarian pakai ini
										$reload = "AccessLevel.php?pagination=true";
										$sql =  "SELECT * FROM accesslevel WHERE KodeCabang='$login_cabang' ORDER BY LevelID ASC";
										$result = mysqli_query($koneksi,$sql);
									}
									
									//pagination config start
									$rpp = 10; // jumlah record per halaman
									$page = intval(@$_GET["page"]);
									if($page<=0) $page = 1;  
									$tcount = mysqli_num_rows($result);
									$tpages = ($tcount) ? ceil($tcount/$rpp) : 1; // total pages, last page number
									$count = 0;
									$i = ($page-1)*$rpp;
									$no_urut = ($page-1)*$rpp;
									//pagination config end				
								?>
                                <tbody>
									<?php
									while(($count<$rpp) && ($i<$tcount)) {
										mysqli_data_seek($result,$i);
										$data = mysqli_fetch_array($result);
									?>
                                    <tr class="odd gradeX">
                                        <td width="50px">
											<?php echo ++$no_urut;?> 
										</td>
										<td>
											<strong><?php echo $data ['LevelName']; ?></strong>
										</td>
										<td>
											<?php if($data ['IsAktif']==1): ?>
												<a href="#" onclick="window.location = 'AccessLevel.php?id=<?php echo base64_encode($data['LevelID']);?>&aksi=<?php echo base64_encode('NonAktif');?>&cab=<?php echo base64_encode($data['KodeCabang']);?>'; return false;" onclick='return confirmation3()'>
												<i class='btn btn-success btn-sm'><span class='fa fa-check'></span> Aktif</i></a>
											<?php else: ?>
												<a href="#" onclick="window.location = 'AccessLevel.php?id=<?php echo base64_encode($data['LevelID']);?>&aksi=<?php echo base64_encode('Aktif');?>&cab=<?php echo base64_encode($data['KodeCabang']);?>'; return false;" onclick='return confirmation2()'>
												
												<i class='btn btn-danger btn-sm'><span class='fa fa-times'></span> Non Aktif</i></a>
											<?php endif ?>
										</td>
										<td width="150px">
											<a href="FiturLevel.php?id=<?php echo base64_encode($data['LevelID']);?>&cab=<?php echo base64_encode($data['KodeCabang']);?>" title='Fitur Akses User'><i class='btn btn-warning btn-sm'><span class='fa fa-unlock-alt'></span> Set Fitur Akses</i></a> 
										</td>
                                        <td width="150px">
											<a href="AccessLevel.php?id=<?php echo base64_encode($data['LevelID']);?>&cab=<?php echo base64_encode($data['KodeCabang']);?>" title='Edit'><i class='btn btn-warning btn-sm'><span class='fa fa-edit'></span></i></a> 
											
											<a href="AccessLevel.php?id=<?php echo base64_encode($data['LevelID']);?>&aksi=<?php echo base64_encode('Hapus');?>&cab=<?php echo base64_encode($data['KodeCabang']);?>" title='Hapus' onclick='return confirmation()'><i class='btn btn-danger btn-sm'><span class='fa fa-trash'></span></i></a>
										</td>
                                    </tr>
									<?php
										$i++; 
										$count++;
									}
									?>
                                </tbody>
                            </table>
							<div><?php echo paginate_one($reload, $page, $tpages); ?></div>
                            <!-- /.table-responsive -->
							
							</div>
							<!-- ===================================== tabel ==================================== -->
							</div>
							<div class="tab-pane fade" id="tambah-user">
							<!-- ===================================== input ===================================== -->
							<div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
									<h2>Tambah Access Level </h2>
										<form role="form" method="post" id="edit"  enctype="multipart/form-data">
											<div class="form-group">
												<label>Nama</label>
												<input type="text" class="form-control" name="LevelName" required>
											</div>
											<?php if($IsServer=='1'){?>
											<div class="form-group">
												<label>Cabang Perusahaan</label>
												<select name="KodeCabang" class="form-control" required>	
													<?php
														echo '<option value="">-- Cabang Perusahaan --</option>';
														$menu = mysqli_query($koneksi,"SELECT a.*,b.NamaProvinsi,c.NamaKab FROM mstcabang a JOIN mstprovinsi b ON a.KodeProvinsi=b.KodeProvinsi JOIN mstkabupaten c ON a.KodeKab=c.KodeKab ORDER BY a.KodeCabang ASC");
														while($kode = mysqli_fetch_array($menu)){
															echo "<option value=\"".$kode['KodeCabang']."\">".$kode['NamaProvinsi']." - ".$kode['NamaKab']."</option>\n";
														}
													?>
												</select>
											</div>
											<?php } ?>
											<div class="form-group">
												<label class="checkbox-inline">
													<input type="checkbox" name="IsAktif" value="1"> Is Aktif
												</label>
											</div>
											<button type="submit" name="submit" value="simpan" class="btn btn-info">Simpan</button>
											<button type="reset" name="reset" value="reset" class="btn btn-danger">Reset</button>
										</form>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
							</div>
							<!-- ===================================== input ===================================== -->
							</div>
							<!-- ===================================== Start Edit ===================================== -->
							<div class="tab-pane fade  <?php if($id!=null){echo 'in active';} ?>" id="edit-user">
							<div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
								<!-- kondisi jika $id == null maka tambah data -->
								<?php if($id==null):?>
									<h2>Edit Access Level </h2>
										<form role="form" method="post" id="edit"  enctype="multipart/form-data">
											<div class="form-group">
												<label>Nama</label>
												<input type="text" class="form-control" name="LevelName" disabled>
											</div>
											<div class="form-group">
												<label class="checkbox-inline">
													<input type="checkbox" name="IsAktif" value="1" disabled> Is Aktif
												</label>
											</div>
											<button type="submit" name="submit" value="simpan" class="btn btn-info" disabled>Simpan</button>
											<button type="reset" name="reset" value="reset" class="btn btn-danger" disabled>Reset</button>
										</form>
								<?php elseif($id!=null):?>
									<h2>Edit Access Level </h2>
										<form role="form" method="post" id="edit"  enctype="multipart/form-data">
											<div class="form-group">
												<label>Nama</label>
												<input type="text" class="form-control" name="LevelName" value="<?php echo $level_name; ?>" required>
											</div>
											<div class="form-group">
												<label class="checkbox-inline">
													<input type="checkbox" name="IsAktif" value="1" <?php if($is_aktif=='1'){echo 'checked';}?>> Is Aktif
												</label>
											</div>
											<button type="submit" name="submit_edit" value="simpan" class="btn btn-info">Simpan</button>
											<button type="reset" name="reset" value="reset" class="btn btn-danger">Reset</button>
										</form>
								<?php endif?>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
							</div>
							</div>
							<!-- ===================================== End Edit ===================================== -->
						</div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="komponen/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="komponen/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="komponen/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="komponen/vendor/raphael/raphael.min.js"></script>
    <script src="komponen/vendor/morrisjs/morris.min.js"></script>
    <script src="komponen/data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="komponen/dist/js/sb-admin-2.js"></script>
    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
	
</body>

</html>
