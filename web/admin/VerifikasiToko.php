<?php
include ('akses.php');
$fitur_id = 1;
include ('login/lock-menu.php');
//ambil kode cabang 
$QueryCab = mysqli_query($koneksi,"SELECT * FROM mstcabang WHERE KodeCabang='$login_cabang'");
$RowCab = mysqli_fetch_assoc($QueryCab);
$Prov = $RowCab['KodeProvinsi'];
$Kab = $RowCab['KodeKab'];

include ('../library/kode-mstperson.php');
include ('../library/tgl-indo.php');
$DateTime = date('Y-m-d H:i:s');

include('../library/config.php');
date_default_timezone_set('Asia/Jakarta');

if(base64_decode(@$_GET['aksi'])=='Hapus'){
	//kode jadi untuk server log
	include ('../library/kode-log-server.php');
	
	$Hapus = mysqli_query($koneksi,"UPDATE rm_penjual SET IsDihapus='1' WHERE KodeRM='".base64_decode($_GET['id'])."' AND KodePerson='".base64_decode($_GET['prs'])."' ");
	if($Hapus){
		mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
		VALUES ('$kode_jadi_log','$DateTime','Update Data','Unverified Toko : ".base64_decode($_GET['id'])."-".base64_decode($_GET['prs'])."','$login_id','$login_cabang')");
		echo '<script language="javascript">document.location="VerifikasiToko.php"; </script>';
	}else{
		echo '<script language="javascript">alert("Unverified Gagal!"); document.location="VerifikasiToko.php"; </script>';
	}
}

if(base64_decode(@$_GET['aksi'])=='Verifikasi'){
	//kode jadi untuk server log
	include ('../library/kode-log-server.php');
	
	$Hapus = mysqli_query($koneksi,"UPDATE rm_penjual SET IsVerifiedCabang='1' WHERE KodeRM='".base64_decode($_GET['id'])."' AND KodePerson='".base64_decode($_GET['prs'])."'");
	if($Hapus){
		mysqli_query($koneksi,"INSERT into serverlog (LogID,DateTimeLog,Action,Description,UserName,KodeCabang) 
		VALUES ('$kode_jadi_log','$DateTime','Update Data','Verified Toko : ".base64_decode($_GET['id'])."-".base64_decode($_GET['prs'])."','$login_id','$login_cabang')");
		echo '<script language="javascript">document.location="VerifikasiToko.php"; </script>';
	}else{
		echo '<script language="javascript">alert("Verified Gagal!"); document.location="VerifikasiToko.php"; </script>';
	}
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <?php include 'view/title.php' ?>

    <!-- Bootstrap Core CSS -->
    <link href="komponen/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="komponen/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="komponen/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="komponen/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="komponen/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Jika Anda verifikasi Toko, pastikan anda sudah memeriksa bahwa toko ini benar adanya.")
			if (answer == true){
				window.location = "DepositKoin.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
	<script type="text/javascript">
		function Hapusconfirmation() {
			var answer = confirm("Transaksi Deposit dompet akan dihapus! ")
			if (answer == true){
				window.location = "DepositKoin.php";
				}
			else{
			alert("Terima Kasih . . . !");	return false; 	
				}
			}
	</script>
	<!-- Datepcker -->
	<link rel="stylesheet" href="../library/Datepicker/dist/css/default/zebra_datepicker.min.css" type="text/css">
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
		<?php include 'view/menu.php' ; ?>
		</nav>
		
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Verifikasi Toko</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
							<form method="post" action="">
								<div class="form-group input-group col-lg-4 col-lg-offset-8">
									<input type="text" name="keyword" class="form-control" value="<?php echo @$_REQUEST['keyword']; ?>" placeholder="Cari Nama">
									<span class="input-group-btn">
										<button class="btn btn-large btn-info" type="submit">Cari</button>
									 </span>
								</div>
							</form>
								<!-- ===================================== tabel ==================================== -->
							<div class="table-responsive">
								<table width="100%" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>No</th>
											<th>Nama Toko/Rumah Makan</th>
											<th>Kategory</th>
											<th>Jam Operasional</th>
											<th>Telp</th>
											<th>Konfirmasi</th>
										</tr>
									</thead>
									<?php
										include '../library/pagination1.php';
										// mengatur variabel reload dan sql
										$kosong=null;
										if(isset($_REQUEST['keyword']) && $_REQUEST['keyword']<>""){
											// jika ada kata kunci pencarian (artinya form pencarian disubmit dan tidak kosong)pakai ini
											$keyword=$_REQUEST['keyword'];
											$reload = "DepositKoin.php?pagination=true&keyword=$keyword";
											$sql =  "SELECT a.*,b.NamaProvinsi,c.NamaKab,d.NamaKec,e.NamaDesa FROM rm_penjual a JOIN mstprovinsi b ON a.KodeProvinsi=b.KodeProvinsi JOIN mstkabupaten c ON a.KodeKab=c.KodeKab JOIN mstkecamatan d ON a.KodeKec=d.KodeKec JOIN mstdesa e ON a.KodeDesa=e.KodeDesa JOIN mstperson f ON a.KodePerson=f.KodePerson WHERE a.NamaRM LIKE '%$keyword%' AND f.IsAktif='1' AND a.IsDihapus='0' AND a.KodeProvinsi='$Prov' AND a.KodeKab='$Kab' AND a.IsVerifiedCabang='0' ORDER BY a.NamaRM ASC";
											$result = mysqli_query($koneksi,$sql);
										}else{
										//jika tidak ada pencarian pakai ini
											$reload = "DepositKoin.php?pagination=true";
											$sql =  "SELECT a.*,b.NamaProvinsi,c.NamaKab,d.NamaKec,e.NamaDesa FROM rm_penjual a JOIN mstprovinsi b ON a.KodeProvinsi=b.KodeProvinsi JOIN mstkabupaten c ON a.KodeKab=c.KodeKab JOIN mstkecamatan d ON a.KodeKec=d.KodeKec JOIN mstdesa e ON a.KodeDesa=e.KodeDesa JOIN mstperson f ON a.KodePerson=f.KodePerson WHERE f.IsAktif='1' AND a.IsDihapus='0' AND a.KodeProvinsi='$Prov' AND a.KodeKab='$Kab' AND a.IsVerifiedCabang='0' ORDER BY a.NamaRM ASC";
											$result = mysqli_query($koneksi,$sql);
										}
										
										//pagination config start
										$rpp = 25; // jumlah record per halaman
										$page = intval(@$_GET["page"]);
										if($page<=0) $page = 1;  
										$tcount = mysqli_num_rows($result);
										$tpages = ($tcount) ? ceil($tcount/$rpp) : 1; // total pages, last page number
										$count = 0;
										$i = ($page-1)*$rpp;
										$no_urut = ($page-1)*$rpp;
										//pagination config end				
									?>
									<tbody>
										<?php
										while(($count<$rpp) && ($i<$tcount)) {
											mysqli_data_seek($result,$i);
											$data = mysqli_fetch_array($result);
										?>
										<tr class="odd gradeX">
											<td width="50px">
												<?php echo ++$no_urut;?> 
											</td>
											<td width="250px">
												<?php echo $data['NamaRM']; ?>
											</td>
											<td>
												<?php echo $data['KategoryRM']; ?>
											</td>
											<td>
												<?php echo $data['JamBuka']; echo' s/d '; echo $data['JamTutup'];?>
											</td>
											<td>
												<?php echo $data['TelpRM']; ?>
											</td>
											<td width="100px">											
												<?php $Alamat = "Prov.".$data['NamaProvinsi'].",Kab.".$data['NamaKab'].",Kec.".$data['NamaKec'].",Ds.".$data['NamaDesa']." ";?>
												
												<a href="#" class='open_modal' data-kode='<?php echo $data['KodeRM'];?>' data-nama='<?php echo $data['NamaRM'];?>' data-kategory='<?php echo $data['KategoryRM'];?>' data-ket='<?php echo $data['Keterangan'];?>' data-jam='<?php echo $data['JamBuka']; echo' s/d '; echo $data['JamTutup'];?>' data-foto='<?php echo $data['Foto'];?>' data-telp='<?php echo $data['TelpRM'];?>' data-alamat='<?php echo $Alamat;?>' data-kodeprs='<?php echo $data['KodePerson'];?>'><span class='btn btn-warning btn-sm'>Lihat Detil</span></a>
											</td>
										</tr>
										<?php
											$i++; 
											$count++;
										}
										?>
									</tbody>
								</table>
								<div><?php echo paginate_one($reload, $page, $tpages); ?></div>
								<!-- ===================================== tabel ==================================== -->
							</div>
							</div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
	<!-- Modal Popup untuk Edit--> 
	<div id="ModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	</div>
    <!-- jQuery -->
    <script src="komponen/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="komponen/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="komponen/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="komponen/vendor/raphael/raphael.min.js"></script>
    <script src="komponen/vendor/morrisjs/morris.min.js"></script>
    <script src="komponen/data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="komponen/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>

<!-- DatePicker -->
<script type="text/javascript" src="../library/Datepicker/dist/zebra_datepicker.src.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#datepicker').Zebra_DatePicker();
	});
</script>
<script type="text/javascript">
   $(document).ready(function () {
   $(".open_modal").click(function(e) {
	  var kode = $(this).data("kode");
	  var nama = $(this).data("nama");
	  var kategory 	= $(this).data("kategory");
	  var ket = $(this).data("ket");
	  var foto = $(this).data("foto");
	  var jam = $(this).data("jam");
	  var telp = $(this).data("telp");
	  var alamat = $(this).data("alamat");
	  var prs = $(this).data("kodeprs");
		   $.ajax({
				   url: "DetilVerifikasiToko.php",
				   type: "GET",
				   data : {KodeRM: kode,NamaRM: nama,Keterangan:ket,Kategory:kategory,Foto:foto,JamBuka:jam,Telp:telp,Alamat:alamat,KodePerson:prs},
				   success: function (ajaxData){
				   $("#ModalEdit").html(ajaxData);
				   $("#ModalEdit").modal('show',{backdrop: 'true'});
			   }
			});
		});
	});
</script>
</body>
</html>
