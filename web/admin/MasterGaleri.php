<?php
include ('akses.php');
/* $fitur_id = 13;
include ('login/lock-menu.php'); */

//kode jadi untuk server log
//include ('../library/kode-log-server.php');
include ('../library/tgl-indo.php');
$DateTime = date('Y-m-d H:i:s a');

include('../library/config.php');
date_default_timezone_set('Asia/Jakarta');

//Edit Data
if(@$_GET['metode']=='Edit'){
	$Tampil = mysqli_query($koneksi,"SELECT * FROM kontenweb WHERE JenisKonten='FAQ' AND KodeKonten='".base64_decode($_GET['id'])."'");
	$Data = mysqli_fetch_assoc($Tampil);
}

if(@$_GET['metode']=='Hapus'){
	$Hapus= mysqli_query($koneksi,"DELETE FROM kontenweb WHERE KodeKonten='".base64_decode($_GET['id'])."' AND JenisKonten='GALERI'");
	if($Hapus){
		echo '<script language="javascript">document.location="MasterGaleri.php"; </script>';
	}else{
		echo '<script language="javascript">alert("Hapus Data Gagal !"); document.location="MasterGaleri.php"; </script>';
	}
}

if(isset($_POST['EditData'])){
	$Edit = mysqli_query($koneksi,"UPDATE kontenweb SET Judul='".$_POST['Judul']."',Isi='".$_POST['Isi']."',UserName='$login_id' WHERE KodeKonten='".$_POST['KodeKonten']."' AND JenisKonten='FAQ' ");
	if($Edit){
		echo '<script language="javascript">document.location="MasterFaq.php"; </script>';
	}else{
		echo '<script language="javascript">alert("Edit Data Gagal !"); document.location="MasterFaq.php"; </script>';
	}
}

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <?php include 'view/title.php' ?>

    <!-- Bootstrap Core CSS -->
    <link href="komponen/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="komponen/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="komponen/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="komponen/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="komponen/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<!-- Datepcker -->
	<link rel="stylesheet" href="../library/Datepicker/dist/css/default/zebra_datepicker.min.css" type="text/css">
	
	<!-- css progress upload gambar   --> 
	<link href="../css/style.css" rel="stylesheet">
	<script type="text/javascript">
		function confirmation() {
			var answer = confirm("Apakah Anda Yakin Untuk Menghapus Data ?")
			if (answer == true){
				window.location = "MasterFaq.php";
				}
			else{
			alert("Terima Kasih !");	return false; 	
				}
			}
	</script>
</head>

<body>

    <div id="wrapper">
        <!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
		<?php include 'view/menu.php' ; ?>
		</nav>
		
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Galery</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
						<!-- Nav tabs -->
                            <ul class="nav nav-pills">
                                <li <?php if(@$_GET['metode']!='Edit'){ echo 'class="active"';}?>><a href="#home-pills" data-toggle="tab">Data Galery</a></li>
                                
								<li><a href="#tambah-iklan" data-toggle="tab">Tambah Data</a></li>
								
								<?php if(@$_GET['metode']=='Edit'){echo '<li class="active"><a href="#edit-iklan" data-toggle="tab">Edit Data</a></li>';}?>
                            </ul>
						<div class="tab-content">
                            <div class="tab-pane fade <?php if(@$_GET['metode']!='Edit'){ echo 'in active';}?>" id="home-pills"><br/>
							<!-- ===================================== tabel ==================================== -->
							<form method="post" action="">
								<div class="form-group input-group col-lg-4 col-lg-offset-8">
									<input type="text" name="keyword" class="form-control" value="<?php echo @$_REQUEST['keyword']; ?>" placeholder="Pencarian...">
									<span class="input-group-btn">
										<button class="btn btn-large btn-warning" type="submit">Cari</button>
									 </span>
								</div>
							</form>
							<div class="row">
								<?php
									include '../library/pagination1.php';
									// mengatur variabel reload dan sql
									$kosong=null;
									if(isset($_REQUEST['keyword']) && $_REQUEST['keyword']<>""){
										// jika ada kata kunci pencarian (artinya form pencarian disubmit dan tidak kosong)pakai ini
										$keyword=$_REQUEST['keyword'];
										$reload = "MasterGaleri.php?pagination=true&keyword=$keyword";
										$sql =  "SELECT * FROM kontenweb WHERE JenisKonten='GALERI' AND Judul LIKE '%$keyword%' ORDER BY KodeKonten ASC";
										$result = mysqli_query($koneksi,$sql);
									}else{
									//jika tidak ada pencarian pakai ini
										$reload = "MasterGaleri.php?pagination=true";
										$sql =  "SELECT * FROM kontenweb WHERE JenisKonten='GALERI' ORDER BY KodeKonten DESC";
										$result = mysqli_query($koneksi,$sql);
									}
									
									//pagination config start
									$rpp = 12; // jumlah record per halaman
									$page = intval(@$_GET["page"]);
									if($page<=0) $page = 1;  
									$tcount = mysqli_num_rows($result);
									$tpages = ($tcount) ? ceil($tcount/$rpp) : 1; // total pages, last page number
									$count = 0;
									$i = ($page-1)*$rpp;
									$no_urut = ($page-1)*$rpp;
									//pagination config end				
								?>
								
									<?php
									while(($count<$rpp) && ($i<$tcount)) {
										mysqli_data_seek($result,$i);
										$data = mysqli_fetch_array($result);
									?>
                                    <div class="col-lg-3">
										<div class="panel panel-yellow">
											<a href="#" class='open_modal' data-id='<?php echo $data['KodeKonten'];?>' data-judul='<?php echo $data['Judul'];?>' data-gambar='<?php echo $data['Gambar'];?>' data-user='<?php echo $data['UserName'];?>' data-tanggal='<?php echo $data['Date'];?>'>
												<div class="panel-heading">
													<img src="../../img/Galeri/thumb_<?php echo $data['Gambar'];?>" class="img img-responsive">
												</div>
											</a>
											<div class="panel-footer">
												<span class="pull-left"><?php echo $data ['Judul']; ?></span>
												<span class="pull-right">
													<a href="MasterGaleri.php?id=<?php echo base64_encode($data['KodeKonten']);?>&metode=Hapus" title='Hapus' onclick="return confirmation()">
													<i class="btn btn-danger fa fa-trash"></i>
													</a>
												</span>
												<div class="clearfix"></div>
											</div>
										</div>
									</div>
									<?php
										$i++; 
										$count++;
									}
									?>
							</div>
							<div><?php echo paginate_one($reload, $page, $tpages); ?></div>
							<!-- ===================================== tabel ==================================== -->
							</div>
							
							<div class="tab-pane fade" id="tambah-iklan">
							<!-- ===================================== Edit ===================================== -->
							<div class="panel-body">
                            <div class="row">
								<div class="form-group col-lg-6">
									<div id="upload-wrapper">
										<form action="upload/upload-galeri.php" onSubmit="return false" method="post" enctype="multipart/form-data" id="MyUploadForm">
											<label>Judul</label>
											<input type="text" class="form-control" name="Judul" required>
											<input type="hidden" class="form-control" name="User" value="<?php echo $login_id; ?>">
										
											<label>Upload gambar dengan type .jpg dengan ukuran max 3 mb</label>
												<input name="ImageFile" id="imageInput" type="file" /><br/>
												<input type="submit" class="btn btn-warning" id="submit-btn" value="Upload Foto" />
												<img src="../../andro/img/ajax-loader.gif" id="loading-img" style="display:none;" alt="Please Wait"/>
										</form>
										<div id="progressbox" style="display:none;"><div id="progressbar"></div ><div id="statustxt">0%</div></div>
										<div align="center" id="output"></div>
									</div>
								</div>
                            </div>
                            <!-- /.row (nested) -->
							</div>
							<!-- ===================================== Edit ===================================== -->
							</div>
							
							<div class="tab-pane fade <?php if(@$_GET['metode']=='Edit'){ echo 'in active';}?>" id="edit-iklan">
							<!-- ===================================== Edit ===================================== -->
							<div class="panel-body">
                            <div class="row">
								<div class="col-lg-12">
									<br/>
									<form method="post" action="">
										<div class="form-group col-lg-6">
											<label>Pertanyaan</label>
											<input type="text" class="form-control" name="Judul" value="<?php echo $Data['Judul']; ?>" required>
											<input type="hidden" class="form-control" name="KodeKonten" value="<?php echo $Data['KodeKonten']; ?>" required>
										</div>
										<div class="form-group col-lg-12">
											<label>Jawaban</label>
											<textarea class="ckeditor" name="Isi" class="form-control" rows="4"><?php echo $Data['Isi']; ?></textarea>
										</div>
										<div class="col-lg-3">
											<button type="submit" class="btn btn-md btn-block btn-warning" name="EditData">Simpan Edit</button>
										</div>
									</form>
								</div>
                            </div>
                            <!-- /.row (nested) -->
							</div>
							<!-- ===================================== Edit ===================================== -->
							</div>
						</div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
	<!-- Modal Popup untuk Edit--> 
	<div id="ModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	
	</div>
    <!-- jQuery -->
    <script src="komponen/vendor/jquery/jquery.min.js"></script>
 
    <!-- Bootstrap Core JavaScript -->
    <script src="komponen/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="komponen/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="komponen/vendor/raphael/raphael.min.js"></script>
    <script src="komponen/vendor/morrisjs/morris.min.js"></script>
    <script src="komponen/data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="komponen/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>
		<!-- ckeditor JS -->
   <script type="text/javascript" src="../library/ckeditor/ckeditor.js"></script>
	<!-- DatePicker -->
	<script type="text/javascript" src="../library/Datepicker/dist/zebra_datepicker.src.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			$('#datepicker1').Zebra_DatePicker({format: 'Y-m-d H:i'});
			$('#datepicker2').Zebra_DatePicker({format: 'Y-m-d H:i'});
			$('#datepicker3').Zebra_DatePicker({format: 'Y-m-d H:i'});
			$('#datepicker4').Zebra_DatePicker({format: 'Y-m-d H:i'});
		});
	</script>

<!-- membuat dropdown bertingkat -->
<script>
	var htmlobjek;
	$(document).ready(function(){
	  //apabila terjadi event onchange terhadap object <select id=nama_produk>
	  $("#KodeNegara").change(function(){
		var KodeNegara = $("#KodeNegara").val();
		$.ajax({
			url: "upload/ambil-menu.php",
			data: "KodeRM="+KodeNegara,
			cache: false,
			success: function(msg){
				//jika data sukses diambil dari server kita tampilkan
				//di <select id=kota>
				$("#KodeProvinsi").html(msg);
			}
		});
	  });
	});
</script>
	<!-- =============================================Progres Bar=================================-->
	<script type="text/javascript" src="../assets/js/jquery.form.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function() { 

		var progressbox     = $('#progressbox');
		var progressbar     = $('#progressbar');
		var statustxt       = $('#statustxt');
		var completed       = '0%';
		
		var options = { 
				target:   '#output',   // target element(s) to be updated with server response 
				beforeSubmit:  beforeSubmit,  // pre-submit callback 
				uploadProgress: OnProgress,
				success:       afterSuccess,  // post-submit callback 
				resetForm: true        // reset the form after successful submit 
			}; 
			
		 $('#MyUploadForm').submit(function() { 
				$(this).ajaxSubmit(options);  			
				// return false to prevent standard browser submit and page navigation 
				return false; 
			});
		
	//when upload progresses	
	function OnProgress(event, position, total, percentComplete)
	{
		//Progress bar
		progressbar.width(percentComplete + '%') //update progressbar percent complete
		statustxt.html(percentComplete + '%'); //update status text
		if(percentComplete>50)
			{
				statustxt.css('color','#fff'); //change status text to white after 50%
			}
	}

	//after succesful upload
	function afterSuccess()
	{
		$('#submit-btn').show(); //hide submit button
		$('#loading-img').hide(); //hide submit button

	}

	//function to check file size before uploading.
	function beforeSubmit(){
		//check whether browser fully supports all File API
	   if (window.File && window.FileReader && window.FileList && window.Blob)
		{

			if( !$('#imageInput').val()) //check empty input filed
			{
				$("#output").html("Masukkan gambar terlebih dahulu!");
				return false
			}
			
			var fsize = $('#imageInput')[0].files[0].size; //get file size
			var ftype = $('#imageInput')[0].files[0].type; // get file type
			
			//allow only valid image file types 
			switch(ftype)
			{
				case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
					break;
				default:
					$("#output").html("<b>"+ftype+"</b> File tidak didukung!");
					return false
			}
			
			//Allowed file size is less than 3 MB (1048576)
			if(fsize>3048576) 
			{
				$("#output").html("<b>"+bytesToSize(fsize) +"</b> File gambar terlalu besar! <br />Maksimal ukuran gambar 3 MB.");
				return false
			}
			
			//Progress bar
			progressbox.show(); //show progressbar
			progressbar.width(completed); //initial value 0% of progressbar
			statustxt.html(completed); //set status text
			statustxt.css('color','#000'); //initial color of status text

					
			$('#submit-btn').hide(); //hide submit button
			$('#loading-img').show(); //hide submit button
			$("#output").html("");  
		}
		else
		{
			//Output error to older unsupported browsers that doesn't support HTML5 File API
			$("#output").html("Browser tidak mendukung fitur ini, tolong upgrade browser anda!");
			return false;
		}
	}

	//function to format bites bit.ly/19yoIPO
	function bytesToSize(bytes) {
	   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	   if (bytes == 0) return '0 Bytes';
	   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
	}

	}); 
	</script>
	<script type="text/javascript">
	   $(document).ready(function () {
		  $(".open_modal").click(function(e) {
		  var gambar 	= $(this).data("gambar");
		  var judul 	= $(this).data("judul");
		  var user 		= $(this).data("user");
		  var tanggal 	= $(this).data("tanggal");
		  var id	 	= $(this).data("id");
			   $.ajax({
					   url: "DetilGaleri.php",
					   type: "GET",
					   data : {Gambar:gambar,Judul:judul,User:user,Tanggal:tanggal,Id:id},
					   success: function (ajaxData){
					   $("#ModalEdit").html(ajaxData);
					   $("#ModalEdit").modal('show',{backdrop: 'true'});
				   }
				});
			});
		});
	</script>
</body>

</html>
