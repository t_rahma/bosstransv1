<?php
include ('akses.php');
//ambil kode cabang 
$QueryCab = mysqli_query($koneksi,"SELECT * FROM mstcabang WHERE KodeCabang='$login_cabang'");
$RowCab = mysqli_fetch_assoc($QueryCab);
$Prov = $RowCab['KodeProvinsi'];
$Kab = $RowCab['KodeKab'];
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <?php include 'view/title.php' ?>

    <!-- Bootstrap Core CSS -->
    <link href="komponen/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="komponen/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="komponen/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="komponen/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="komponen/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	 <!-- jQuery -->
    <script src="komponen/vendor/jquery/jquery.min.js"></script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include 'view/menu.php'?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Dashboard</h1>
                </div>
                <!-- /.col-lg-12 
				<div id="time"></div>-->
            </div>
            <!-- /.row -->
     		<?php 
				// $Tarik = mysqli_query($koneksi,"SELECT NoTransaksi FROM dompetperson WHERE Keterangan='TOPUP' AND UserVerificator is null AND IsVerified='0'");
				// $num_Tarik= mysqli_num_rows($Tarik);
				
				// $Deposit = mysqli_query($koneksi,"SELECT NoTransaksi FROM dompetperson WHERE JenisMutasi='TARIK' AND UserVerificator ='Menunggu Verifikasi' AND IsVerified='0'");
				// $num_Deposit= mysqli_num_rows($Deposit);
				
				$Kurir = mysqli_query($koneksi,"SELECT KodePerson FROM mstperson WHERE IsVerified='1' AND TipePerson <> 'UMUM'");
				$num_Kurir= mysqli_num_rows($Kurir);
		
				
				$Pengguna = mysqli_query($koneksi,"SELECT KodePerson FROM mstperson WHERE IsVerified='1' AND TipePerson = 'UMUM'");
				$num_Pengguna= mysqli_num_rows($Pengguna);
				
				$DriverVerified = mysqli_query($koneksi,"SELECT KodePerson FROM mstperson WHERE IsVerified='0' AND TipePerson <> 'UMUM'");
				$num_DriverVerified= mysqli_num_rows($DriverVerified);
				
				$Topup = mysqli_query($koneksi,"SELECT NoTransaksi FROM dompetperson WHERE IsVerified='0' AND Keterangan='TOPUP'");
				$num_Topup= mysqli_num_rows($Topup);
				
				$Pencairan = mysqli_query($koneksi,"SELECT NoTransaksi FROM dompetperson WHERE IsVerified='0' AND Keterangan='TARIK' ");
				$num_Tarik= mysqli_num_rows($Pencairan);
			?>
            <div class="row">
				<?php if($IsServer=='1'){ ?>
				
					<div class="col-lg-3 col-md-6">
					<h4>Verifikasi Akun</h4>
						<div class="panel panel-danger">
							<div class="panel-heading">
								<div class="row">
									<div class="col-xs-3">
										<i class="fa fa-exclamation-triangle fa-5x"></i>
									</div>
									<div class="col-xs-9 text-right">
										<div class="huge"><?php echo $num_DriverVerified; ?></div>
										<div>Verifikasi Driver</div>
									</div>
								</div>
							</div>
							<a href="VerifikasiDriver.php">
								<div class="panel-footer">
									<span class="pull-left">Lihat Detail</span>
									<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
									<div class="clearfix"></div>
								</div>
							</a>
						</div>
					</div>
					<!--<div class="col-lg-3 col-md-6">
						<div class="panel panel-success">
							<div class="panel-heading">
								<div class="row">
									<div class="col-xs-3">
										<i class="fa fa-shopping-cart fa-5x"></i>
									</div>
									<div class="col-xs-9 text-right">
										<div class="huge"><?php //echo $num_Toko; ?></div>
										<div>Verifikasi Toko</div>
									</div>
								</div>
							</div>
							<a href="VerifikasiToko.php">
								<div class="panel-footer">
									<span class="pull-left">Lihat Detil</span>
									<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
									<div class="clearfix"></div>
								</div>
							</a>
						</div>
					</div>-->
				
				<div class="col-lg-6">
					<h4>Permintaan Verifikasi Transaksi Dompet</h4>
					<div class="col-lg-6 col-md-6">
						<div class="panel panel-red">
							<div class="panel-heading">
								<div class="row">
									<div class="col-xs-3">
										<i class="fa fa-credit-card fa-5x"></i>
									</div>
									<div class="col-xs-9 text-right">
										<div class="huge"><?php echo $num_Tarik; ?></div>
										<div>Pencairan</div>
									</div>
								</div>
							</div>
							<a href="VerifikasiPencairan.php">
								<div class="panel-footer">
									<span class="pull-left">Lihat Detil</span>
									<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
									<div class="clearfix"></div>
								</div>
							</a>
						</div>
					</div>
					<div class="col-lg-6 col-md-6">
						<div class="panel panel-green">
							<div class="panel-heading">
								<div class="row">
									<div class="col-xs-3">
										<i class="fa fa-list-alt fa-5x"></i>
									</div>
									<div class="col-xs-9 text-right">
										<div class="huge"><?php echo $num_Topup; ?></div>
										<div>TopUp</div>
									</div>
								</div>
							</div>
							<a href="VerifikasiTopup.php">
								<div class="panel-footer">
									<span class="pull-left">Lihat Detil</span>
									<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
									<div class="clearfix"></div>
								</div>
							</a>
						</div>
					</div>
                </div>
				<?php } ?>
				<!--<div class="col-lg-6">
					<h4>Transaksi Order Berlangsung</h4>
					<div class="col-lg-6 col-md-6">
						<div class="panel panel-yellow">
							<div class="panel-heading">
								<div class="row">
									<div class="col-xs-3">
										<i class="fa fa-compress fa-5x"></i>
									</div>
									<div class="col-xs-9 text-right">
										<div class="huge"><?php //echo $num_Transaksi; ?></div>
										<div>Transaksi</div>
									</div>
								</div>
							</div>
							<a href="TransaksiOrder.php">
								<div class="panel-footer">
									<span class="pull-left">Lihat Transaksi</span>
									<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
									<div class="clearfix"></div>
								</div>
							</a>
						</div>
					</div>
					<div class="col-lg-6 col-md-6">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<div class="row">
									<div class="col-xs-3">
										<i class="fa fa-tags fa-5x"></i>
									</div>
									<div class="col-xs-9 text-right">
										<div class="huge"><?php //echo $num_Promo; ?></div>
										<div>Verifikasi Promo</div>
									</div>
								</div>
							</div>
							<a href="VerifikasiPromo.php">
								<div class="panel-footer">
									<span class="pull-left">Verifikasi Promo Penjual</span>
									<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
									<div class="clearfix"></div>
								</div>
							</a>
						</div>
					</div>
                </div>-->
				<div class="col-lg-12">
					<h4>Statistik Data Pengguna Bosstrans</h4>
					<div class="col-lg-3 col-md-6">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<div class="row">
									<div class="col-xs-3">
										<i class="fa fa-car fa-5x"></i>
									</div>
									<div class="col-xs-9 text-right">
										<div class="huge"><?php echo $num_Kurir; ?></div>
										<div>Data Driver</div>
									</div>
								</div>
							</div>
							<a href="MasterDriver.php">
								<div class="panel-footer">
									<span class="pull-left">Lihat Detil</span>
									<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
									<div class="clearfix"></div>
								</div>
							</a>
						</div>
					</div>
					<div class="col-lg-3 col-md-6">
						<div class="panel panel-yellow">
							<div class="panel-heading">
								<div class="row">
									<div class="col-xs-3">
										<i class="fa fa-coffee fa-5x"></i>
									</div>
									<div class="col-xs-9 text-right">
										<div class="huge"><?php echo $num_Pengguna; ?></div>
										<div>Data Klien</div>
									</div>
								</div>
							</div>
							<a href="MasterKlien.php">
								<div class="panel-footer">
									<span class="pull-left">Lihat Detil</span>
									<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
									<div class="clearfix"></div>
								</div>
							</a>
						</div>
					</div>
					
					<!--<div class="col-lg-3 col-md-6">
						<div class="panel panel-green">
							<div class="panel-heading">
								<div class="row">
									<div class="col-xs-3">
										<i class="fa fa-users fa-5x"></i>
									</div>
									<div class="col-xs-9 text-right">
										<div class="huge"><?php //echo $num_Pengguna; ?></div>
										<div>Data Pengguna</div>
									</div>
								</div>
							</div>
							<a href="MasterKlien.php">
								<div class="panel-footer">
									<span class="pull-left">Lihat Detil</span>
									<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
									<div class="clearfix"></div>
								</div>
							</a>
						</div>
					</div>-->
				</div>
				
            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

   
    <!-- Bootstrap Core JavaScript -->
    <script src="komponen/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="komponen/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="komponen/vendor/raphael/raphael.min.js"></script>
    <script src="komponen/vendor/morrisjs/morris.min.js"></script>
    <script src="komponen/data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="komponen/dist/js/sb-admin-2.js"></script>

</body>

</html>
