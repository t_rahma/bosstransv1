<?php
include ("config.php");
date_default_timezone_set('Asia/Jakarta');
include 'kode-dompet.php';

//deposit
$KodeBankPerusahaan 	= @$_POST['KodeBankPerusahaan'];
$TransferKeRekDeposit 	= @$_POST['TransferKeRekDeposit'];
$NoRefTransferDeposit 	= @$_POST['NoRefTransferDeposit'];
$NominalDeposit	 		= @$_POST['NominalDeposit'];
//Penarikan
$TransferKeRekPenarikan	= @$_POST['TransferKeRekPenarikan'];
$KodeBankPersonPenarikan= @$_POST['KodeBankPersonPenarikan'];
$NominalPenarikan	 	= @$_POST['NominalPenarikan'];
//Transfer
$KodePersonTujuan 	= @$_POST['KodePersonTujuan'];
$NominalTransfer 	= @$_POST['NominalTransfer'];

$SaldoSaatIni	 	= @$_POST['SaldoSaatIni'];
$KodePerson		 	= @$_POST['KodePerson'];
$TanggalSekarang 	= date('Y-m-d H:i:s');
	//mendefinisikan folder upload
	define("UPLOAD_DIR", "../assets/img/BuktiTransfer/");

	if (!empty($_FILES["media"])) {
		$media	= $_FILES["media"];
		$ext	= pathinfo($_FILES["media"]["name"], PATHINFO_EXTENSION);
		$size	= $_FILES["media"]["size"];
		$tgl	= date("Y-m-d");

		if($size > (1024000*2)){ // maksimal 5 MB
			echo 'Upload Gagal ! Ukuran file maksimal 3 MB.';
			exit;
		}else{
			if ($media["error"] !== UPLOAD_ERR_OK) {
				echo 'Gagal upload file.';
				exit;
			}

			// filename yang aman
			$name = preg_replace("/[^A-Z0-9._-]/i", "_", $media["name"]);

			// mencegah overwrite filename
			$i = 0;
			$parts = pathinfo($name);
			while (file_exists(UPLOAD_DIR . $name)) {
				$i++;
				$name = $parts["filename"] . "_" . $kode_jadi . "_".$i."." . $parts["extension"];
			}

			$success = move_uploaded_file($media["tmp_name"], UPLOAD_DIR . $name);
			if ($success) {					
				if($ext=='jpg' OR $ext=='jpeg' OR $ext=='png'){
					//deposi saldo dompetperson
					$query = mysqli_query($koneksi,"INSERT INTO dompetperson (NoTransaksi,KodePerson,TanggalTransaksi,Debet,Keterangan,IsVerified, NoRefTransfer, TransferKeRek,JenisMutasi,FotoBuktiTransfer) VALUES('$kode_jadi_bank','$KodePerson','$TanggalSekarang','$NominalDeposit','Deposit Saldo Dompet, Transfer Ke- $KodeBankPerusahaan No.Rekening $TransferKeRekDeposit','0','$NoRefTransferDeposit','$TransferKeRekDeposit','Pembelian','$name')");
					if($query){
						echo "Data Berhasil Disimpan";
						exit;
					}else{
						unlink("../assets/img/BuktiTransfer/$name");
						echo "Data Gagal Disimpan";
						exit;
					}
				}else{
					unlink("../assets/img/BuktiTransfer/$name");
					echo "Bukti Transfer harus .jpeg atau .png";
					exit;
				}
			}
			chmod(UPLOAD_DIR . $name, 0644);
		}
	}
?>