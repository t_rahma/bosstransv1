<?php
session_start();
include('config.php');
include('kode-SesiLogin.php');
$jm_login=date("Y-m-d H:i:s");
//=========================login klien========================//
if(isset($_POST['submit_klien'])){
	if(empty($_POST['username']) || empty ($_POST ['password'])) {
		echo '<script language="javascript">alert("Username atau Password tidak valid. . . !"); document.location="../index.php"; </script>';
		session_destroy();
	}
	else{		
		// Variabel username dan password
		$username = $_POST['username'];
		$password = base64_encode($_POST['password']);
		$is_aktif = $_POST['is_aktif'];
		$is_klien = $_POST['is_klien'];
		$waktu    = time()+252000; //(GMT+7)
		$expired  = 3000;
			
		// Mencegah MySQL injection 
		$username = stripslashes($username);
		$password = stripslashes($password);
		$username = mysqli_real_escape_string($koneksi,$username);
		$password = mysqli_real_escape_string($koneksi,$password);
		
		$query = mysqli_query($koneksi,"select * from mstperson where UserName = '$username' AND Password = '$password' AND IsKlien='$is_klien'");				
		if(mysqli_num_rows($query) == 0){
			echo '<script language="javascript">alert("Login Gagal..! Periksa kembali username dan password anda."); document.location="../index.php"; </script>';
		}else{
			$row = mysqli_fetch_assoc($query);
			if($row['IsAktif'] == 1){
				$_SESSION['username']=$username;
				$_SESSION['is_aktif']=$is_aktif;
				$_SESSION['timeout']= $waktu + $expired; // Membuat Sesi Waktu
				//insert start durasi sesi login
				$kode_prs = $row['KodePerson'];
				//delete person yang login lebih dari 1.
				mysqli_query($koneksi,"DELETE FROM sesiperson WHERE KodePerson='$kode_prs' AND TanggalJamLogOut is NULL");
				
				mysqli_query($koneksi,"INSERT INTO sesiperson (IDSesi,KodePerson,TanggalJamLogin)values('$kode_jadi_sesi','$kode_prs','$jm_login')");
				
				header("Location: ../klien/index.php");
			}else{
				echo '<script language="javascript">alert("Login Klien Gagal..! Periksa kembali username dan password anda."); document.location="../index.php"; </script>';
			}
		}
	}
}

//======================login agen========================//
if(isset($_POST['submit_agen'])){
	if(empty($_POST['username']) || empty ($_POST ['password'])) {
		echo '<script language="javascript">alert("Username atau Password tidak valid. . . !"); document.location="../index.php"; </script>';
		session_destroy();
	}
	else{		
		// Variabel username dan password
		$username = $_POST['username'];
		$password = base64_encode($_POST['password']);
		$is_aktif = $_POST['is_aktif'];
		$is_agen  = $_POST['is_agen'];
		$waktu    = time()+252000; //(GMT+7)
		$expired  = 3000;
			
		// Mencegah MySQL injection 
		$username = stripslashes($username);
		$password = stripslashes($password);
		$username = mysqli_real_escape_string($koneksi,$username);
		$password = mysqli_real_escape_string($koneksi,$password);
		
		$query = mysqli_query($koneksi,"select * from mstperson where UserName = '$username' AND Password = '$password' AND IsAgen='$is_agen'");				
		if(mysqli_num_rows($query) == 0){
			echo '<script language="javascript">alert("Login Gagal..! Periksa kembali username dan password anda."); document.location="../index.php"; </script>';
		}else{
			$row = mysqli_fetch_assoc($query);
			if($row['IsAktif'] == 1){
				$_SESSION['username']=$username;
				$_SESSION['is_aktif']=$is_aktif;
				$_SESSION['timeout']= $waktu + $expired; // Membuat Sesi Waktu
				//insert start durasi sesi login
				$kode_prs = $row['KodePerson'];
				//delete person yang login lebih dari 1.
				mysqli_query($koneksi,"DELETE FROM sesiperson WHERE KodePerson='$kode_prs' AND TanggalJamLogOut is NULL");
				
				mysqli_query($koneksi,"INSERT INTO sesiperson (IDSesi,KodePerson,TanggalJamLogin)values('$kode_jadi_sesi','$kode_prs','$jm_login')");
				//cek apakah pin yang agen punya adalah Pin EO
				$JenisPIN = mysqli_query($koneksi,"SELECT KodeKategory FROM generatedpin WHERE KodePIN='".$row['KodePIN']."' AND KodeKategory='KTG-000014'");
				$num_JenisPIN = mysqli_num_rows($JenisPIN);
				if($num_JenisPIN > 0){
					header("Location: ../eo/index.php");
				}else{
					header("Location: ../agen/index.php");
				}
			}else{
				echo '<script language="javascript">alert("Login Agen Gagal..! Periksa kembali username dan password anda."); document.location="../index.php"; </script>';
			}
		}
	}
}

//======================login rekan========================//
if(isset($_POST['submit_rekan'])){
	if(empty($_POST['username']) || empty ($_POST ['password'])) {
		echo '<script language="javascript">alert("Username atau Password tidak valid. . . !"); document.location="../index.php"; </script>';
		session_destroy();
	}
	else{		
		// Variabel username dan password
		$username = $_POST['username'];
		$password = base64_encode($_POST['password']);
		$is_aktif = $_POST['is_aktif'];
		$is_rekan  = $_POST['is_rekan'];
		$waktu    = time()+252000; //(GMT+7)
		$expired  = 3000;
			
		// Mencegah MySQL injection 
		$username = stripslashes($username);
		$password = stripslashes($password);
		$username = mysqli_real_escape_string($koneksi,$username);
		$password = mysqli_real_escape_string($koneksi,$password);
		
		$query = mysqli_query($koneksi,"select * from mstperson where UserName = '$username' AND Password = '$password' AND IsVendor='$is_rekan'");				
		if(mysqli_num_rows($query) == 0){
				echo '<script language="javascript">alert("Login Gagal..! yo Periksa kembali username dan password anda."); document.location="../index.php"; </script>';
			}else{
				$row = mysqli_fetch_assoc($query);
				if($row['IsAktif'] == 1){
					$_SESSION['username']=$username;
					$_SESSION['is_aktif']=$is_aktif;
					$_SESSION['timeout']= $waktu + $expired; // Membuat Sesi Waktu
					//insert start durasi sesi login
					$kode_prs = $row['KodePerson'];
					//delete person yang login lebih dari 1.
					mysqli_query($koneksi,"DELETE FROM sesiperson WHERE KodePerson='$kode_prs' AND TanggalJamLogOut is NULL");
					mysqli_query($koneksi,"INSERT INTO sesiperson (IDSesi,KodePerson,TanggalJamLogin)values('$kode_jadi_sesi','$kode_prs','$jm_login')");
					header("Location: ../rekan/index.php");
				}else{
					echo '<script language="javascript">alert("Login Rekan Gagal..! Periksa kembali username dan password anda."); document.location="../index.php"; </script>';
				}
			}
	}
}

//======================login pakar========================//
if(isset($_POST['submit_pakar'])){
	if(empty($_POST['username']) || empty ($_POST ['password'])) {
		echo '<script language="javascript">alert("Username atau Password tidak valid. . . !"); document.location="../index.php"; </script>';
		session_destroy();
	}
	else{		
		// Variabel username dan password
		$username = $_POST['username'];
		$password = base64_encode($_POST['password']);
		$is_aktif = $_POST['is_aktif'];
		$is_pakar  = $_POST['is_pakar'];
		$waktu    = time()+252000; //(GMT+7)
		$expired  = 3000;
			
		// Mencegah MySQL injection 
		$username = stripslashes($username);
		$password = stripslashes($password);
		$username = mysqli_real_escape_string($koneksi,$username);
		$password = mysqli_real_escape_string($koneksi,$password);
		
		$query = mysqli_query($koneksi,"select * from mstperson where UserName = '$username' AND Password = '$password' AND IsPakar='$is_pakar'");				
		if(mysqli_num_rows($query) == 0){
				echo '<script language="javascript">alert("Login Gagal..! Periksa kembali username dan password anda."); document.location="../index.php"; </script>';
			}else{
				$row = mysqli_fetch_assoc($query);
				if($row['IsAktif'] == 1){
					$_SESSION['username']=$username;
					$_SESSION['is_aktif']=$is_aktif;
					$_SESSION['timeout']= $waktu + $expired; // Membuat Sesi Waktu
					//insert start durasi sesi login
					$kode_prs = $row['KodePerson'];
					//delete person yang login lebih dari 1.
					mysqli_query($koneksi,"DELETE FROM sesiperson WHERE KodePerson='$kode_prs' AND TanggalJamLogOut is NULL");
					mysqli_query($koneksi,"INSERT INTO sesiperson (IDSesi,KodePerson,TanggalJamLogin)values('$kode_jadi_sesi','$kode_prs','$jm_login')");
					header("Location: ../pakar/index.php");
				}else{
					echo '<script language="javascript">alert("Login Pakar Gagal..! Periksa kembali username dan password anda."); document.location="../index.php"; </script>';
				}
			}
	}
}

//======================login Leader========================//
if(isset($_POST['submit_leader'])){
	if(empty($_POST['username']) || empty ($_POST ['password'])) {
		echo '<script language="javascript">alert("Username atau Password tidak valid. . . !"); document.location="../index.php"; </script>';
		session_destroy();
	}
	else{		
		// Variabel username dan password
		$username = $_POST['username'];
		$password = base64_encode($_POST['password']);
		$is_aktif = $_POST['is_aktif'];
		$is_leader  = $_POST['is_leader'];
		$waktu    = time()+252000; //(GMT+7)
		$expired  = 3000;
			
		// Mencegah MySQL injection 
		$username = stripslashes($username);
		$password = stripslashes($password);
		$username = mysqli_real_escape_string($koneksi,$username);
		$password = mysqli_real_escape_string($koneksi,$password);
		
		$query = mysqli_query($koneksi,"select * from mstperson where UserName = '$username' AND Password = '$password' AND IsLeader='$is_leader'");				
		if(mysqli_num_rows($query) == 0){
				echo '<script language="javascript">alert("Login Gagal..! Periksa kembali username dan password anda."); document.location="../index.php"; </script>';
		}else{
			$row = mysqli_fetch_assoc($query);
			if($row['IsAktif'] == 1){
				$_SESSION['username']=$username;
				$_SESSION['is_aktif']=$is_aktif;
				$_SESSION['timeout']= $waktu + $expired; // Membuat Sesi Waktu
				//insert start durasi sesi login
				$kode_prs = $row['KodePerson'];
				//delete person yang login lebih dari 1.
				mysqli_query($koneksi,"DELETE FROM sesiperson WHERE KodePerson='$kode_prs' AND TanggalJamLogOut is NULL");
				mysqli_query($koneksi,"INSERT INTO sesiperson (IDSesi,KodePerson,TanggalJamLogin)values('$kode_jadi_sesi','$kode_prs','$jm_login')");
				header("Location: ../leader/index.php");
			}else{
				echo '<script language="javascript">alert("Login Koordinator Gagal..! Periksa kembali username dan password anda."); document.location="../index.php"; </script>';
			}
		}
	}
}

?>