<?php
include ("../library/config.php");
include ("../library/kode-mstperson.php");

$KodePerson		= $kode_jadi;
//$KodeSponsor	= $_POST['KodeSponsor']; 
$Username		= @$_POST['Username'];
$Password		= base64_encode(@$_POST['Password']);
$KonfirmasiPassword		= base64_encode(@$_POST['KonfirmasiPassword']);
$NamaPerson		= @$_POST['NamaPerson'];
$TglLahir		= @$_POST['TglLahir'];
$AlamatLengkap	= @$_POST['AlamatLengkap'];
$NomorKTP		= @$_POST['NomorKTP'];
$NoHP			= @$_POST['NoHP'];
$Email			= @$_POST['Email'];
$KodeBank		= @$_POST['KodeBank'];
$NoRek			= @$_POST['NoRek'];
$KodeProvinsi	= @$_POST['KodeProvinsi'];
$KodeKabupaten	= @$_POST['KodeKabupaten'];
$KodeKecamatan	= @$_POST['KodeKecamatan'];
$KodeDesa		= @$_POST['KodeDesa'];
$Latitude		= @$_POST['Latitude'];
$Longitude		= @$_POST['Longitude'];
$KodeBank		= @$_POST['KodeBank'];
$NoRek			= @$_POST['NoRek'];
$AtasNamaRek	= @$_POST['AtasNamaRek'];
$IsPenjual 		= @$_POST['IsPenjual'];
$IsPembeli 		= @$_POST['IsPembeli'];
$NomorID		= @$_POST['NomorID'];
//$TanggalDaftar	= date("Y-m-d H:i:s");

//============================================Simpan Data=============================================//
//cek konfirmasi password
if($Password==$KonfirmasiPassword){
	//cek apakah username ada yang sama atau tidak
	$cek_username = mysqli_query($koneksi,"select UserName from mstperson where UserName='$Username'");
	$num_username = mysqli_num_rows($cek_username);
	if($num_username == 1 ){
		echo "Maaf Username sudah ada, silahkan ganti Username Anda !";
		exit;
	}else{
		//cek apakah email ada yang sama atau tidak
		$cek_email = mysqli_query($koneksi,"select AlamatEmail from mstperson where AlamatEmail='$Email'");
		$num_email = mysqli_num_rows($cek_email);
		if($num_email != 0 ){
			echo "Maaf Email sudah ada, silahkan ganti Email Anda !";
			exit;
		}else{
			if($IsPenjual=='1'){
				//cek apakah provinsi dan kabupaten ada di kode cabang
				$QueryCab = mysqli_query($koneksi,"SELECT KodeCabang FROM mstcabang WHERE KodeProvinsi='$KodeProvinsi' AND KodeKab='$KodeKabupaten'");
				if(mysqli_num_rows($QueryCab) > 0){
					$RowCab = mysqli_fetch_assoc($QueryCab);
					$KodeCabang = $RowCab['KodeCabang'];
					
					$query = mysqli_query($koneksi,"INSERT into mstperson (KodePerson,NamaPerson,IDMember,TglLahir,UserName,Password,AlamatLengkap,NoID_KTP,NoHP, AlamatEmail,Koor_Long,Koor_Lat, IsPenjual,IsAktif,KodeBankPerson,NoRekPerson,AtasNama,KodeProvinsi,KodeKab,KodeKec,KodeDesa,KodeCabang) VALUES 
					('$KodePerson','$NamaPerson','$NomorID','$TglLahir','$Username','$Password','$AlamatLengkap','$NomorKTP','$NoHP','$Email',
					'$Longitude','$Latitude','1','0','$KodeBank','$NoRek','$AtasNamaRek','$KodeProvinsi','$KodeKabupaten','$KodeKecamatan','$KodeDesa','$KodeCabang')");				
					if($query){
						echo "Pendaftaran Berhasil, Silahkan Hubungi Administrator Garuda Delivery Untuk Mengaktifkan Akun Anda!";
						exit;
					}else{
						echo "Proses Pendaftaran gagal !";
						exit;
					}
				}else{
					echo "Belum Ada Cabang Garuda Delivery Di Kota Ini!";
					exit;
				}
			}else{
				$query = mysqli_query($koneksi,"INSERT into mstperson (KodePerson,NamaPerson,IDMember,TglLahir,UserName,Password,AlamatLengkap,NoID_KTP,NoHP, AlamatEmail,Koor_Long,Koor_Lat,IsPembeli,IsAktif,KodeBankPerson,NoRekPerson,AtasNama,KodeProvinsi,KodeKab,KodeKec,KodeDesa) VALUES 
				('$KodePerson','$NamaPerson','$NomorID','$TglLahir','$Username','$Password','$AlamatLengkap','$NomorKTP','$NoHP','$Email',
				'$Longitude','$Latitude','1','1','$KodeBank','$NoRek','$AtasNamaRek','$KodeProvinsi','$KodeKabupaten','$KodeKecamatan','$KodeDesa')");
				if($query){
					echo "Proses Pendaftaran berhasil!";
					exit;
				}else{
					echo "Proses Pendaftaran gagal !";
					exit;
				}
			}
		}
	}	
}else{
	echo "Maaf Password Anda Salah!";
	exit;
}	
//============================================End Simpan Data=============================================//
?>