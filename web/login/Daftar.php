<?php include '../library/config.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<?php //include 'title.php';?>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="" />
<meta name="author" content="http://bootstraptaste.com" />
<!-- css -->
<link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
<link href="../assets/css/fancybox/jquery.fancybox.css" rel="stylesheet">
<link href="../assets/css/jcarousel.css" rel="stylesheet" />
<link href="../assets/css/flexslider.css" rel="stylesheet" />
<link href="../assets/css/style.css" rel="stylesheet" />
<!-- Datepcker -->
<link rel="stylesheet" href="../library/Datepicker/dist/css/default/zebra_datepicker.min.css" type="text/css">
<!-- Theme skin -->
<link href="../assets/skins/default.css" rel="stylesheet" />

<!-- =======================================================
    Theme Name: Moderna
    Theme URL: https://bootstrapmade.com/free-bootstrap-template-corporate-moderna/
    Author: BootstrapMade
    Author URL: https://bootstrapmade.com
======================================================= -->
<style>
#description {
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
}

#infowindow-content .title {
  font-weight: bold;
}

#infowindow-content {
  display: none;
}

#map #infowindow-content {
  display: inline;
}

.pac-card {
  margin: 10px 10px 0 0;
  border-radius: 2px 0 0 2px;
  box-sizing: border-box;
  -moz-box-sizing: border-box;
  outline: none;
  box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
  background-color: #fff;
  font-family: Roboto;
}

#pac-container {
  padding-bottom: 12px;
  margin-right: 12px;
}

.pac-controls {
  display: inline-block;
  padding: 5px 11px;
}

.pac-controls label {
  font-family: Roboto;
  font-size: 13px;
  font-weight: 300;
}

#pacinput,#pacinputpengambilan {
  background-color: #fff;
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
  margin: 10px 12px;
  padding: 5px;
  text-overflow: ellipsis;
  width: 250px;
}

#pacinput:focus {
  border-color: #4d90fe;
}

#title {
  color: #fff;
  background-color: #4d90fe;
  font-size: 25px;
  font-weight: 500;
  padding: 6px 12px;
}
#target {
  width: 345px;
}
	</style>
</head>
<body>
<div id="wrapper">
	<!-- start header -->
	<?php //include 'header.php'; ?>	
	<section id="content">
	<div class="container">
		<!-- Portfolio Projects -->
		<div class="row">
			<div class="row">
				<section id="projects">
					<div class="col-md-6">
					<?php if(@$_GET['Aksi']=='Penjual'){ ?>
						<div class="cta-text">
							<h3><span>Pendaftaran</span> Penjual</h3>
						</div>
						<form method="post" id="form1" name='input' action="">
							<fieldset>
								<div class="form-group">
									<input name="IsPenjual" type="hidden" value="1" required>
									<input class="form-control" placeholder="Nama Lengkap" name="NamaPerson" type="text" required>
								</div>
								<!--<div class="form-group">
									<input class="form-control" placeholder="Nomor ID (Opsional)" name="NomorID" type="text">
								</div>
								<div class="form-group">
									<input class="form-control" placeholder="Nomor KTP" name="NomorKTP" type="text" required>
								</div>-->
								<div class="form-group">
									<input class="form-control" placeholder="Nomor Handphone" name="NoHP" type="text" required>
								</div>
								<div class="form-group">
									<input class="form-control" placeholder="Email" name="Email" type="email" required>
								</div>
								<!--<div class="form-group">
									<input class="form-control" id="datepicker" placeholder="Tanggal Lahir (YYYY-MM-DD)" name="TglLahir" type="text" required>
								</div>-->
								<div class="form-group">
									<input class="form-control" placeholder="Username" name="Username" type="text" required>
								</div>
								<div class="form-group">
									<input class="form-control" placeholder="Password" name="Password" type="password" required>
								</div>
								<div class="form-group">
									<input class="form-control" placeholder="Konfirmasi Password" name="KonfirmasiPassword" type="password" required>
								</div>
								<!--<div class="form-group">
									<input class="form-control" placeholder="Bank" name="KodeBank" type="text" required>
								</div>
								<div class="form-group">
									<input class="form-control" placeholder="Nomor Rekening" name="NoRek" type="text" required>
								</div>
								<div class="form-group">
									<input class="form-control" placeholder="Atas Nama Rekening" name="AtasNamaRek" type="text" required>
								</div>--><hr/>
								<div class="form-group">
									<select id="KodeProvinsi" name="KodeProvinsi" class="form-control" required>	
										<?php
											echo "<option value=''>--- Provinsi ---</option>";
											$menu = mysqli_query($koneksi,"SELECT * FROM mstprovinsi");
											while($kode = mysqli_fetch_array($menu)){
												echo "<option value=\"".$kode['KodeProvinsi']."\" >".$kode['NamaProvinsi']."</option>\n";
											}
										?>
									</select>
								</div>
								<div class="form-group">
									<select id="KodeKabupaten" name="KodeKabupaten" class="form-control" required>
									<option value=''>--- Kab/Kota ---</option>
									</select>
								</div>
								<div class="form-group">
									<select id="KodeKecamatan" class="form-control" name="KodeKecamatan" required>
									<option value=''>--- Kecamatan ---</option>
									</select>
								</div>
								<div class="form-group">
									<select id="KodeDesa" class="form-control" name="KodeDesa" required>
									<option value=''>--- Desa ---</option>
									</select>
								</div>
								<!--<div class="form-group">
									<input class="form-control" placeholder="Alamat Lengkap" name="AlamatLengkap" id="Alamat" type="text" required>
								</div>-->
								<hr/>
								<!-- ------------------------- Start Map Cari Lokasi ---------------------------------- -->			
								<div class="col-lg-12">
									<p><font color="red">Tandai lokasi rumah Anda dengan akurat.</font></p>
									<input id="pacinputpengambilan" type="text" placeholder="Tentukan lokasi rumah Anda" style="z-index: 0; position: absolute; left: 113px; top: 300px;" required>
									<div id="mappengambilan" style="height:350px;width:100%;margin-bottom:30px;"></div>
									
									<input id="latpengambilan" name="Latitude" type="hidden" value="-7.556032627191996">
									<input id="lngpengambilan" name="Longitude" type="hidden" value="112.221">
									<script>
									function initAutocomplete() {
									  var map_ambil = new google.maps.Map(document.getElementById('mappengambilan'), {
										center: {lat: -7.556032627191996, lng: 112.221},
										zoom: 12,
										//mapTypeId: 'satellite'
										mapTypeControl: false,
										mapTypeId: 'roadmap',
									  });
									
									  // Create the search box and link it to the UI element.
									  var input_ambil = document.getElementById('pacinputpengambilan');
									  var searchBox_ambil = new google.maps.places.SearchBox(input_ambil);
										  map_ambil.controls[google.maps.ControlPosition.TOP_LEFT].push(input_ambil);
									  // Bias the SearchBox results towards current map's viewport.
									  map_ambil.addListener('bounds_changed', function() {
										searchBox_ambil.setBounds(map_ambil.getBounds());
									  });

									  var markers_ambil = [];
									  // Listen for the event fired when the user selects a prediction and retrieve
									  // more details for that place.
									  searchBox_ambil.addListener('places_changed', function() {
										var places_ambil = searchBox_ambil.getPlaces();

										if (places_ambil.length == 0) {
										  return;
										}

										// Clear out the old markers.
										markers_ambil.forEach(function(marker_ambil) {
										  marker_ambil.setMap(null);
										});
										markers_ambil = [];

										// For each place, get the icon, name and location.
										var bounds_ambil = new google.maps.LatLngBounds();
										for (var j = 0, place_ambil; place_ambil = places_ambil[j]; j++) {
										  var image_ambil = {
											//url: place_ambil.icon,
											url: "https://g-delivery.com/kuliner/assets/img/placemaps2.png",
											size: new google.maps.Size(100, 100),
											origin: new google.maps.Point(0, 0),
											anchor: new google.maps.Point(17, 45),
											scaledSize: new google.maps.Size(34, 45)
										  };
										  var marker_ambil = new google.maps.Marker({
											draggable: true,
											map: map_ambil,
											icon: image_ambil,
											title: place_ambil.name,
											position: place_ambil.geometry.location
										  });

										  document.getElementById('latpengambilan').value = marker_ambil.getPosition().lat().toFixed(6);
										  document.getElementById('lngpengambilan').value = marker_ambil.getPosition().lng().toFixed(6);
										  // drag response
										  google.maps.event.addListener(marker_ambil, 'dragend', function(e) {
											displayPosition_ambil(this.getPosition());
										  });
										  // click response
										  google.maps.event.addListener(marker_ambil, 'click', function(e) {
											displayPosition_ambil(this.getPosition());
										  });
										  markers_ambil.push(marker_ambil);
										  bounds_ambil.extend(place_ambil.geometry.location);
										}

										map_ambil.fitBounds(bounds_ambil);  
									  });

									  google.maps.event.addListener(map_ambil, 'bounds_changed', function() {
										var bounds_ambil = map_ambil.getBounds();
										searchBox_ambil.setBounds(bounds_ambil);
									  });

									  // displays a position on two <input> elements
									  function displayPosition_ambil(post) {
										document.getElementById('latpengambilan').value = post.lat();
										document.getElementById('lngpengambilan').value = post.lng();
									  }
									  
									  
									}
									
									</script>
									<?php include '../library/MapsPencarian.php';?>
								</div>
								<!-- ------------------------- End Map Cari Lokasi ---------------------------------- -->	
								<!--<button type="submit" name="submit" class="btn btn-theme btn-block">Daftar</button>-->
								<input type="submit" class="btn btn-block btn-theme" value="Kirim">
							</fieldset>
						</form>
						<div class="msg alert alert-info text-left" style="display:none"></div>
						<div class="clearfix"></div>
					<?php }elseif(@$_GET['Aksi']=='Pengguna'){ ?>
						<div class="cta-text">
							<h3><span>Pendaftaran</span> Pengguna</h3>
						</div>
						<form method="post" id="form1" name='input' action="">
							<fieldset>
								<div class="form-group">
									<input class="form-control" placeholder="Nama Lengkap" name="NamaPerson" type="text" required>
								</div>
								<!--<div class="form-group">
									<input class="form-control" placeholder="Nomor ID (Opsional)" name="NomorID" type="text">
								</div>
								<div class="form-group">
									<input class="form-control" placeholder="Nomor KTP" name="NomorKTP" type="text" required>
								</div>-->
								<div class="form-group">
									<input class="form-control" placeholder="Nomor Handphone" name="NoHP" type="text" required>
								</div>
								<div class="form-group">
									<input class="form-control" placeholder="Email" name="Email" type="email" required>
								</div>
								<!--<div class="form-group">
									<input class="form-control" id="datepicker" placeholder="Tanggal Lahir (YYYY-MM-DD)" name="TglLahir" type="text"  required>
								</div>-->
								<div class="form-group">
									<input class="form-control" placeholder="Username" name="Username" type="text" required>
								</div>
								<div class="form-group">
									<input class="form-control" placeholder="Password" name="Password" type="password" required>
								</div>
								<div class="form-group">
									<input class="form-control" placeholder="Konfirmasi Password" name="KonfirmasiPassword" type="password" required>
								</div><hr/>
								<!--<div class="form-group">
									<input class="form-control" placeholder="Bank" name="KodeBank" type="text" required>
								</div>
								<div class="form-group">
									<input class="form-control" placeholder="Nomor Rekening" name="NoRek" type="text" required>
								</div>
								<div class="form-group">
									<input class="form-control" placeholder="Atas Nama Rekening" name="AtasNamaRek" type="text" required>
								</div><hr/>-->
								<div class="form-group">
									<select id="KodeProvinsi" name="KodeProvinsi" class="form-control" required>	
										<?php
											echo "<option value=''>--- Provinsi ---</option>";
											$menu = mysqli_query($koneksi,"SELECT * FROM mstprovinsi");
											while($kode = mysqli_fetch_array($menu)){
												echo "<option value=\"".$kode['KodeProvinsi']."\" >".$kode['NamaProvinsi']."</option>\n";
											}
										?>
									</select>
								</div>
								<div class="form-group">
									<select id="KodeKabupaten" name="KodeKabupaten" class="form-control" required>
									<option value=''>--- Kab/Kota ---</option>
									</select>
								</div>
								<div class="form-group">
									<select id="KodeKecamatan" class="form-control" name="KodeKecamatan" required>
									<option value=''>--- Kecamatan ---</option>
									</select>
								</div>
								<div class="form-group">
									<select id="KodeDesa" class="form-control" name="KodeDesa" required>
									<option value=''>--- Desa ---</option>
									</select>
								</div>
								<!--<div class="form-group">
									<input class="form-control" placeholder="Alamat Lengkap" id='Alamat' name="AlamatLengkap" type="text" required>
								</div>--><hr/>
								<!-- ------------------------- Start Map Cari Lokasi ---------------------------------- -->			
								<div class="col-lg-12">
									<p><font color="red">Tandai lokasi rumah Anda dengan akurat.</font></p>
									<input id="pacinputpengambilan" type="text" placeholder="Tentukan lokasi rumah Anda" style="z-index: 0; position: absolute; left: 113px; top: 300px;" required>
									<div id="mappengambilan" style="height:350px;width:100%;margin-bottom:30px;"></div>
									
									<input id="latpengambilan" name="Latitude" type="hidden" value="-7.556032627191996">
									<input id="lngpengambilan" name="Longitude" type="hidden" value="112.221">
									<script>
									function initAutocomplete() {
									  var map_ambil = new google.maps.Map(document.getElementById('mappengambilan'), {
										center: {lat: -7.556032627191996, lng: 112.221},
										zoom: 12,
										//mapTypeId: 'satellite'
										mapTypeControl: false,
									  });

									  // Create the search box and link it to the UI element.
									  var input_ambil = document.getElementById('pacinputpengambilan');
									  var searchBox_ambil = new google.maps.places.SearchBox(input_ambil);
										  map_ambil.controls[google.maps.ControlPosition.TOP_LEFT].push(input_ambil);
									  // Bias the SearchBox results towards current map's viewport.
									  map_ambil.addListener('bounds_changed', function() {
										searchBox_ambil.setBounds(map_ambil.getBounds());
									  });

									  var markers_ambil = [];
									  // Listen for the event fired when the user selects a prediction and retrieve
									  // more details for that place.
									  searchBox_ambil.addListener('places_changed', function() {
										var places_ambil = searchBox_ambil.getPlaces();

										if (places_ambil.length == 0) {
										  return;
										}

										// Clear out the old markers.
										markers_ambil.forEach(function(marker_ambil) {
										  marker_ambil.setMap(null);
										});
										markers_ambil = [];

										// For each place, get the icon, name and location.
										var bounds_ambil = new google.maps.LatLngBounds();
										for (var j = 0, place_ambil; place_ambil = places_ambil[j]; j++) {
										  var image_ambil = {
											//url: place_ambil.icon,
											url: "https://g-delivery.com/kuliner/assets/img/placemaps2.png",
											size: new google.maps.Size(100, 100),
											origin: new google.maps.Point(0, 0),
											anchor: new google.maps.Point(17, 45),
											scaledSize: new google.maps.Size(34, 45)
										  };
										  var marker_ambil = new google.maps.Marker({
											draggable: true,
											map: map_ambil,
											icon: image_ambil,
											title: place_ambil.name,
											position: place_ambil.geometry.location
										  });

										  document.getElementById('latpengambilan').value = marker_ambil.getPosition().lat().toFixed(6);
										  document.getElementById('lngpengambilan').value = marker_ambil.getPosition().lng().toFixed(6);
										  // drag response
										  google.maps.event.addListener(marker_ambil, 'dragend', function(e) {
											displayPosition_ambil(this.getPosition());
										  });
										  // click response
										  google.maps.event.addListener(marker_ambil, 'click', function(e) {
											displayPosition_ambil(this.getPosition());
										  });
										  markers_ambil.push(marker_ambil);
										  bounds_ambil.extend(place_ambil.geometry.location);
										}

										map_ambil.fitBounds(bounds_ambil);  
									  });

									  google.maps.event.addListener(map_ambil, 'bounds_changed', function() {
										var bounds_ambil = map_ambil.getBounds();
										searchBox_ambil.setBounds(bounds_ambil);
									  });

									  // displays a position on two <input> elements
									  function displayPosition_ambil(post) {
										document.getElementById('latpengambilan').value = post.lat();
										document.getElementById('lngpengambilan').value = post.lng();
									  }
									}
									
									</script>
									<?php include '../library/MapsPencarian.php';?>
								</div>
								<!-- ------------------------- End Map Cari Lokasi ---------------------------------- -->	
								<!--<button type="submit" name="submit" class="btn btn-theme btn-block">Daftar</button>-->
								<input type="submit" class="btn btn-block btn-theme" value="Kirim">
							</fieldset>
						</form>
						<div class="msg alert alert-info text-left" style="display:none"></div>
						<div class="clearfix"></div>
					<?php }else{?>
						<!--<div class="cta-text">
							<h2><span>Daftar</span> Sebagai</h2>
						</div>
						<div class="col-lg-6 text-center">
							<a href="Daftar.php?Aksi=Penjual"><img src="../assets/img/penjual.png" class="img" width="70%"></a>
							<h4>Penjual</h4>
						</div>
						<div class="col-lg-6 text-center">
							<a href="Daftar.php?Aksi=Pengguna"><img src="../assets/img/user.png" class="img" width="70%"></a>
							<h4>Pengguna</h4>
						</div>
						<div class="col-lg-6 text-center">
							<p>Sudah Punya Akun ? <a href="index.php">Login</a>
						</div>-->
					<?php } ?>
					</div>
				</section>
			</div>
		</div>

	</div>
	</section>
	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-body">
					<h4>Maaf, Belum Ada Cabang Garuda Delivery Di Kota yang Anda pilih!</h4>
					<label>Cabang Garuda Delivery Tersedia di :</label>
					<div class="table-responsive">
						<table class="table table-hover">
							<!--<thead>
								<tr align="center">
									<td>No</td>
									<td>Cabang</td>
								</tr>
							</thead>-->
							<tbody>
								<?php 
								$No = 1;
								$QueryCabang = mysqli_query($koneksi,"SELECT a.KodeCabang,b.NamaProvinsi,c.NamaKab FROM mstcabang a JOIN mstprovinsi b ON a.KodeProvinsi=b.KodeProvinsi JOIN mstkabupaten c ON a.KodeKab=c.KodeKab ORDER BY a.KodeCabang ASC");
								while($RowCab=mysqli_fetch_array($QueryCabang)){
								?>
								<tr>
									<td><font size="1.5px"><?php echo $No++; ?></font></td>
									<td><font size="1.5px"><?php echo $RowCab['NamaProvinsi']; echo ' - '; echo $RowCab['NamaKab'];?></font></td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
					<p><font color="red">Pilih ulang Provinsi & Kabupaten/Kota dari cabang yang tersedia.</font></p>
					<br/><br/>
					<button type="button" class="btn btn-theme" data-dismiss="modal">TUTUP</button>
				</div>
			</div>
		</div>
	</div>
	<?php //include 'footer.php'; ?>
</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../assets/js/jquery.js"></script>
<script src="../assets/js/jquery.easing.1.3.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/jquery.fancybox.pack.js"></script>
<script src="../assets/js/jquery.fancybox-media.js"></script>
<script src="../assets/js/google-code-prettify/prettify.js"></script>
<script src="../assets/js/portfolio/jquery.quicksand.js"></script>
<script src="../assets/js/portfolio/setting.js"></script>
<script src="../assets/js/jquery.flexslider.js"></script>
<script src="../assets/js/animate.js"></script>
<script src="../assets/js/custom.js"></script>
<!-- DatePicker -->
<script type="text/javascript" src="../library/Datepicker/dist/zebra_datepicker.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#datepicker').Zebra_DatePicker();
	});
</script>

<script>
	var htmlobjek;
	$(document).ready(function(){
	  //apabila terjadi event onchange terhadap object <select id=nama_produk>
	 $("#KodeProvinsi").change(function(){
		var KodeProvinsi = $("#KodeProvinsi").val();
		$.ajax({
			url: "../library/Dropdown/ambil-kabupaten.php",
			data: "KodeProvinsi="+KodeProvinsi,
			cache: false,
			success: function(msg){
				$("#KodeKabupaten").html(msg);
			}
		});
	  });
	  
	  $("#KodeKabupaten").change(function(){
		var KodeKabupaten = $("#KodeKabupaten").val();
		$.ajax({
			url: "../library/Dropdown/ambil-kecamatan.php",
			data: "KodeKabupaten="+KodeKabupaten,
			cache: false,
			success: function(msg){
				$("#KodeKecamatan").html(msg);
			}
		});
	  });
	  
	  $("#KodeKecamatan").change(function(){
		var KodeKecamatan = $("#KodeKecamatan").val();
		$.ajax({
			url: "../library/Dropdown/ambil-desa.php",
			data: "KodeKecamatan="+KodeKecamatan,
			cache: false,
			success: function(msg){
				$("#KodeDesa").html(msg);
			}
		});
	  });
	  
	});
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#form1').on('submit', function(event){
			event.preventDefault();
			var formData = new FormData($('#form1')[0]);

			$('.msg').hide();
			$('.progress').show();
			
			$.ajax({
				type : 'POST',
				url : 'SimpanPendaftaran.php',
				data : formData,
				processData : false,
				contentType : false,
				success : function(response){
					//$('#form1')[0].reset();
					$('.progress').hide();
					$('.msg').show();
					if(response == "Maaf Username sudah ada, silahkan ganti Username Anda !"){
						var msg = response;
						$('.msg').html(msg);
					}else if(response === "Maaf Email sudah ada, silahkan ganti Email Anda !"){
						var msg = response;
						$('.msg').html(msg);
					}else if(response === "Maaf Kode Sponsor Tidak Valid!"){
						var msg = response;
						$('.msg').html(msg);
					}else if(response === "Proses Pendaftaran gagal !"){
						var msg = response;
						$('.msg').html(msg);
					}else if(response === "Maaf Password Anda Salah!"){
						var msg = response;
						$('.msg').html(msg);
					}else if(response === "Belum Ada Cabang Garuda Delivery Di Kota Ini!"){
						var msg = response;
						//$('.msg').html(msg);
						$('#myModal').modal('show');
					}else{
						$('#form1')[0].reset();
						var msg = response;
						$('.msg').html(msg);
						//alert(msg);document.location="Library.php";
					}
				}
			});
		});
	});
	</script>
</body>
</html>