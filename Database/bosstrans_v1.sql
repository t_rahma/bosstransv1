-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 19, 2018 at 02:00 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bosstrans_v1`
--

-- --------------------------------------------------------

--
-- Table structure for table `accesslevel`
--

CREATE TABLE `accesslevel` (
  `LevelID` int(11) NOT NULL,
  `LevelName` varchar(255) DEFAULT NULL,
  `KodeCabang` varchar(255) NOT NULL,
  `IsAktif` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accesslevel`
--

INSERT INTO `accesslevel` (`LevelID`, `LevelName`, `KodeCabang`, `IsAktif`) VALUES
(1, 'Administrator', 'C0001', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `adminbulanan`
--

CREATE TABLE `adminbulanan` (
  `BulanTahun` date NOT NULL,
  `KodePerson` varchar(255) NOT NULL,
  `TglBayar` date NOT NULL,
  `Nominal` float NOT NULL,
  `Keterangan` varchar(255) NOT NULL,
  `NoRefTransaksi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `datakendaraan`
--

CREATE TABLE `datakendaraan` (
  `NoPol` varchar(20) NOT NULL,
  `MerkKendaraan` varchar(50) DEFAULT NULL,
  `TypeKendaraan` varchar(50) DEFAULT NULL,
  `CCKendaraan` int(11) DEFAULT NULL,
  `TahunPembuatan` int(11) DEFAULT NULL,
  `KapasitasPenumpang` int(11) DEFAULT NULL,
  `AtasNamaSTNK` varchar(150) DEFAULT NULL,
  `StatusKendaraan` varchar(20) DEFAULT NULL,
  `LastUjiKIR` date DEFAULT NULL,
  `AkhirMasaPajakSTNK` date DEFAULT NULL,
  `FotoKendaraan1` varchar(150) DEFAULT NULL,
  `FotoKendaraan2` varchar(150) DEFAULT NULL,
  `FotoKendaraan3` varchar(150) DEFAULT NULL,
  `FotoKendaraan4` varchar(255) NOT NULL,
  `FotoSTNK` varchar(255) NOT NULL,
  `Merk` varchar(255) NOT NULL,
  `Tipe` varchar(255) NOT NULL,
  `KetMerk` varchar(255) NOT NULL,
  `KetTipe` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `datakendaraan`
--

INSERT INTO `datakendaraan` (`NoPol`, `MerkKendaraan`, `TypeKendaraan`, `CCKendaraan`, `TahunPembuatan`, `KapasitasPenumpang`, `AtasNamaSTNK`, `StatusKendaraan`, `LastUjiKIR`, `AkhirMasaPajakSTNK`, `FotoKendaraan1`, `FotoKendaraan2`, `FotoKendaraan3`, `FotoKendaraan4`, `FotoSTNK`, `Merk`, `Tipe`, `KetMerk`, `KetTipe`) VALUES
('123', 'qww', NULL, 554, 5, 88, '2018-08-04 14:41:54', 'AKTIF', '2018-08-04', '2018-08-24', '1231.png', '1232.png', '1233.png', '', '', '', '', '', ''),
('3zdd33', 'honda', NULL, 150, 1995, 56, '2018-08-06 10:18:08', 'AKTIF', '2018-08-06', '2018-08-17', '3zdd33_1.png', '3zdd33_2.png', '3zdd33_3.png', '', '', '', '', '', ''),
('555', 'aaa', NULL, 7775, 556, 4994, '2018-08-04 14:59:41', 'AKTIF', '2018-08-04', '2018-08-04', '5551.png', '5552.png', '5553.png', '', '', '', '', '', ''),
('75863', 'vshshs', NULL, 888, 8888, 88, '2018-08-04 23:59:16', 'AKTIF', '2018-08-04', '2018-08-18', '758631.png', '758632.png', '758633.png', '', '', '', '', '', ''),
('h', 'MERK-00001', NULL, 9, 8, 5, '2018-08-12 23:50:36', 'AKTIF', '0000-00-00', '0000-00-00', 'h_1.png', 'h_2.png', 'h_3.png', 'h_4.png', 'h_7.png', 'MERK-00003', 'MERK-00003', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `dompetperson`
--

CREATE TABLE `dompetperson` (
  `NoTransaksi` char(25) NOT NULL,
  `TanggalTransaksi` datetime DEFAULT NULL,
  `Debet` float DEFAULT '0',
  `Kredit` float DEFAULT '0',
  `Keterangan` char(255) DEFAULT NULL,
  `BiayaAdmin` float DEFAULT NULL,
  `NoRefTransfer` char(100) DEFAULT NULL,
  `TransferKeRek` char(50) DEFAULT NULL,
  `IsVerified` bit(1) DEFAULT NULL,
  `UserVerificator` char(20) DEFAULT NULL,
  `KodePerson` varchar(50) NOT NULL,
  `BuktiTransfer` varchar(150) DEFAULT NULL,
  `IsMutasi` bit(1) DEFAULT NULL,
  `KodePersonTujuan` varchar(20) DEFAULT NULL,
  `JenisMutasi` varchar(10) DEFAULT NULL,
  `NoTrOrder` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dompetperson`
--

INSERT INTO `dompetperson` (`NoTransaksi`, `TanggalTransaksi`, `Debet`, `Kredit`, `Keterangan`, `BiayaAdmin`, `NoRefTransfer`, `TransferKeRek`, `IsVerified`, `UserVerificator`, `KodePerson`, `BuktiTransfer`, `IsMutasi`, `KodePersonTujuan`, `JenisMutasi`, `NoTrOrder`) VALUES
('TRD-2018-0000000001', '2018-09-01 11:40:38', 250000, 0, 'TOPUP', 0, NULL, NULL, b'1', 'admin', 'PRS-2018-0000000011', 'TRD-2018-0000000001.png', b'0', NULL, NULL, NULL),
('TRD-2018-0000000004', '2018-09-01 13:42:53', 0, 2500, 'TRANSFER', 0, NULL, NULL, b'1', NULL, 'PRS-2018-0000000011', NULL, b'0', 'PRS-2018-0000000001', NULL, NULL),
('TRD-2018-0000000005', '2018-09-01 14:30:32', 0, 150000, 'TRANSFER', 0, NULL, NULL, b'1', NULL, 'PRS-2018-0000000011', NULL, b'0', 'PRS-2018-0000000001', NULL, NULL),
('TRD-2018-0000000006', '2018-09-01 14:51:01', 76387.2, 0, 'ORDER', 19096.8, NULL, NULL, b'1', NULL, 'PRS-2018-0000000001', NULL, b'1', NULL, 'BAYAR', 'TRO-2018-0000000001'),
('TRD-2018-0000000007', '2018-09-01 14:51:01', 0, 95484, 'ORDER', 0, NULL, NULL, b'1', NULL, 'PRS-2018-0000000011', NULL, b'1', NULL, 'BAYAR', 'TRO-2018-0000000001'),
('TRD-2018-0000000008', '2018-09-03 14:24:46', 0, 1000, 'TARIK', 0, NULL, NULL, b'1', 'admin', 'PRS-2018-0000000011', NULL, b'0', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fcm_token`
--

CREATE TABLE `fcm_token` (
  `KodePerson` varchar(255) NOT NULL,
  `IDToken` varchar(255) NOT NULL,
  `Topik` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fcm_token`
--

INSERT INTO `fcm_token` (`KodePerson`, `IDToken`, `Topik`) VALUES
('PRS-2018-0000000011', 'cyrzc2QyQS4:APA91bHv_hz2gQ9pzcRVHr-FoTOjJ7n6Oo9K3hxZdJKS7DPbCF_tgv9Cf0xo-iUGdzGUT2tjLg5U-6-7jM8qot9CF8neRUixFmuMKAFxqKwbtwEVyFmZ3TIn8lYYwvEUB9lOdaLEsysZVqlxutzEG6zpKd5z7fM2ZA', 'User'),
('PRS-2018-0000000025', 'cnU_LIbOBaQ:APA91bFHOQ-Jq4v8BZ-2D_WZfPtc086ER1JcNK_102YoK_YoCg2ubKudKfdaTK9OMVk52b70igHNBWyk-eAPau34Q912eQ3E6h_GdxgqoboP8rVBR0heMQKQAFNmBIx-zozphCyP4P7Y', 'Driver');

-- --------------------------------------------------------

--
-- Table structure for table `fiturlevel`
--

CREATE TABLE `fiturlevel` (
  `LevelID` int(11) NOT NULL,
  `FiturID` int(11) NOT NULL,
  `ViewData` bit(1) DEFAULT NULL,
  `AddData` bit(1) DEFAULT NULL,
  `EditData` bit(1) DEFAULT NULL,
  `DeleteData` bit(1) DEFAULT NULL,
  `PrintData` bit(1) DEFAULT NULL,
  `KodeCabang` varchar(255) NOT NULL,
  `IsGranted` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fiturlevel`
--

INSERT INTO `fiturlevel` (`LevelID`, `FiturID`, `ViewData`, `AddData`, `EditData`, `DeleteData`, `PrintData`, `KodeCabang`, `IsGranted`) VALUES
(1, 1, NULL, NULL, NULL, NULL, NULL, 'C0001', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `kontenweb`
--

CREATE TABLE `kontenweb` (
  `KodeKonten` varchar(20) NOT NULL,
  `Judul` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Isi` longtext,
  `JenisKonten` varchar(100) NOT NULL,
  `Gambar` varchar(255) DEFAULT NULL,
  `UserName` varchar(50) NOT NULL,
  `Date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kontenweb`
--

INSERT INTO `kontenweb` (`KodeKonten`, `Judul`, `Title`, `Isi`, `JenisKonten`, `Gambar`, `UserName`, `Date`) VALUES
('KNT-0000001', 'Dengan BOSSTRANS Semua bisa menjadi BOSS', 'Bosstrans memudahkan namun tetap menjaga keamanan dan menjamin kepuasan konsumen.', '<p>Bosstrans selalu menjaga integritas sebagai penyedia jasa online.</p>\r\n\r\n<p>Perusahaan jasa transportasi, tersedia truk tronton, trailer, wingsbox untuk dalam / luar kota. Angkutan barang yang melayani pengiriman di Pulau Jawa, Sumatra, dan Bali.</p>\r\n\r\n<p>Perlu armada yang sesuai guna memudahkan menuju ke tempat yang dituju. Mengenai perusahaan jasa transportasi, menjadi bagian yang tidak dapat terpisahkan dari kebutuhan pelanggan saat ini. PT Indratma Trans sebagai&nbsp;sebuah jasa transportasi yang hadir untuk membantu pelanggan untuk mengirimkan segala jenis barang. Pengiriman barang untuk Pulau Jawa, Sumatra, dan Bali.</p>\r\n\r\n<p><strong>Perusahaan Jasa Transportasi&nbsp;Jawa, Sumatera, Bali</strong></p>\r\n\r\n<p>Fokus sebagai jasa transportasi darat. Perusahaan kami beralamat di Jalan Kedungmundu Raya 18 C-D Semarang. Kami mengoperasikan armada tronton dengan panjang 7 meter buka pintu belakang, tronton dengan panjang 9 meter buka pintu belakang, dan tronton dengan panjang 9,5 meter buka pintu samping dan belakang. Armada yang kami gunakan merupakan unit keluaran tahun 2014 keatas yang dapat memenuhi kebutuhan pelanggan sesuai dengan segmentasi kargo dengan kapasitas maksimal 30 sampai 35 ton. Unit kami dilengkapi dengan fasilitas GPS yang merupakan keunggulan kami dalam memberikan pelayanan terbaik kepada pelanggan.</p>\r\n', 'BERANDA', NULL, 'admin', NULL),
('KNT-0000001', 'Apakah Bosstrans Itu?', NULL, '<p>Bosstrans adalah penyedia jasa layanan antar</p>\r\n', 'FAQ', NULL, 'admin', NULL),
('KNT-0000001', 'gfhfg', NULL, NULL, 'GALERI', '20180917105318-KNT-0000001.jpg', 'admin', '2018-09-17'),
('KNT-0000001', 'Boss Car', 'Jasa Transportasi Penumpang', '<ol>\r\n	<li>Online Service</li>\r\n	<li>Offline Service</li>\r\n	<li>Keamanan Terjaga</li>\r\n	<li>Memberi Kemudahan</li>\r\n</ol>\r\n', 'LAYANAN', NULL, 'admin', NULL),
('KNT-0000001', 'Syarat & Ketentuan', NULL, '<p>ini adalah konten syarat &amp; Ketentuan</p>\r\n', 'S&K', NULL, 'admin', NULL),
('KNT-0000002', 'Jemput Dimana Saja', 'Beritahu kami dimana anda ingin dijemput.', NULL, 'BERANDA', NULL, 'admin', NULL),
('KNT-0000002', 'Bagaimana menjadi driver bosstrans ??', NULL, '<p>Iya wes</p>\r\n', 'FAQ', NULL, 'admin', NULL),
('KNT-0000002', 'okeee', NULL, NULL, 'GALERI', '20180917105433-KNT-0000002.jpg', 'admin', '2018-09-17'),
('KNT-0000002', 'Boss Travel', 'Jasa Transportasi Tour and Travel', NULL, 'LAYANAN', NULL, 'admin', NULL),
('KNT-0000003', 'Set Lokasi Penjemputan', 'Ubah lokasi penjemputan sesuai keinginan anda dengan mudah.', NULL, 'BERANDA', NULL, 'admin', NULL),
('KNT-0000003', 'Apa keuntungan dari penggunaan aplikasi BossTrans?', NULL, '<p>Keuntungan sangat banyak</p>\r\n', 'FAQ', NULL, 'admin', NULL),
('KNT-0000003', 'sip', NULL, NULL, 'GALERI', '20180917105444-KNT-0000003.jpg', 'admin', '2018-09-17'),
('KNT-0000003', 'Boss Rent', 'Jasa Transportasi Rent Car', NULL, 'LAYANAN', NULL, 'admin', NULL),
('KNT-0000004', 'Antar Kemana Saja', 'Kami mengantar kemanapun yang anda inginkan.', NULL, 'BERANDA', NULL, 'admin', NULL),
('KNT-0000004', 'sukksess', NULL, NULL, 'GALERI', '20180917105643-KNT-0000004.jpg', 'admin', '2018-09-17'),
('KNT-0000004', 'Boss Ku', 'Jasa Pengiriman Barang / Kurir', '<p>Oke</p>\r\n', 'LAYANAN', NULL, 'admin', NULL),
('KNT-0000005', 'siap ya', NULL, NULL, 'GALERI', '20180917111022-KNT-0000005.jpg', 'admin', '2018-09-17'),
('KNT-0000006', 'okee', NULL, NULL, 'GALERI', '20180918061306-KNT-0000006.jpg', 'admin', '2018-09-18');

-- --------------------------------------------------------

--
-- Table structure for table `mstbank`
--

CREATE TABLE `mstbank` (
  `id_bank` varchar(50) NOT NULL,
  `nama_bank` varchar(255) NOT NULL,
  `is_aktif` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mstbank`
--

INSERT INTO `mstbank` (`id_bank`, `nama_bank`, `is_aktif`) VALUES
('BNK-0001', 'Bank Negara Indonesia', b'1'),
('BNK-0002', 'Bank Rakyat Indonesia', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `mstbank_perusahaan`
--

CREATE TABLE `mstbank_perusahaan` (
  `id_bank` varchar(50) NOT NULL,
  `norek` varchar(255) NOT NULL,
  `atasnama` varchar(255) NOT NULL,
  `is_aktif` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mstbank_perusahaan`
--

INSERT INTO `mstbank_perusahaan` (`id_bank`, `norek`, `atasnama`, `is_aktif`) VALUES
('BNK-0001', '0089993333112', 'bosstrans', b'1'),
('BNK-0001', '1234', 'asdf', b'0'),
('BNK-0002', '0085634737', 'bank bri bosstrans', b'0'),
('BNK-0002', '12332432434', 'bri boss', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `mstcabang`
--

CREATE TABLE `mstcabang` (
  `KodeCabang` varchar(15) NOT NULL,
  `KodeKab` varchar(10) DEFAULT NULL,
  `KodeProvinsi` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mstcabang`
--

INSERT INTO `mstcabang` (`KodeCabang`, `KodeKab`, `KodeProvinsi`) VALUES
('C0001', '3517', '35');

-- --------------------------------------------------------

--
-- Table structure for table `mstdesa`
--

CREATE TABLE `mstdesa` (
  `KodeDesa` varchar(20) NOT NULL,
  `NamaDesa` varchar(100) DEFAULT NULL,
  `KodeKec` varchar(15) NOT NULL,
  `KodeKab` char(10) NOT NULL,
  `KodeProvinsi` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mstkabupaten`
--

CREATE TABLE `mstkabupaten` (
  `KodeKab` char(10) NOT NULL,
  `NamaKab` varchar(100) DEFAULT NULL,
  `KodeProvinsi` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mstkabupaten`
--

INSERT INTO `mstkabupaten` (`KodeKab`, `NamaKab`, `KodeProvinsi`) VALUES
('1101', 'KABUPATEN SIMEULUE', '11'),
('1102', 'KABUPATEN ACEH SINGKIL', '11'),
('1103', 'KABUPATEN ACEH SELATAN', '11'),
('1104', 'KABUPATEN ACEH TENGGARA', '11'),
('1105', 'KABUPATEN ACEH TIMUR', '11'),
('1106', 'KABUPATEN ACEH TENGAH', '11'),
('1107', 'KABUPATEN ACEH BARAT', '11'),
('1108', 'KABUPATEN ACEH BESAR', '11'),
('1109', 'KABUPATEN PIDIE', '11'),
('1110', 'KABUPATEN BIREUEN', '11'),
('1111', 'KABUPATEN ACEH UTARA', '11'),
('1112', 'KABUPATEN ACEH BARAT DAYA', '11'),
('1113', 'KABUPATEN GAYO LUES', '11'),
('1114', 'KABUPATEN ACEH TAMIANG', '11'),
('1115', 'KABUPATEN NAGAN RAYA', '11'),
('1116', 'KABUPATEN ACEH JAYA', '11'),
('1117', 'KABUPATEN BENER MERIAH', '11'),
('1118', 'KABUPATEN PIDIE JAYA', '11'),
('1171', 'KOTA BANDA ACEH', '11'),
('1172', 'KOTA SABANG', '11'),
('1173', 'KOTA LANGSA', '11'),
('1174', 'KOTA LHOKSEUMAWE', '11'),
('1175', 'KOTA SUBULUSSALAM', '11'),
('1201', 'KABUPATEN NIAS', '12'),
('1202', 'KABUPATEN MANDAILING NATAL', '12'),
('1203', 'KABUPATEN TAPANULI SELATAN', '12'),
('1204', 'KABUPATEN TAPANULI TENGAH', '12'),
('1205', 'KABUPATEN TAPANULI UTARA', '12'),
('1206', 'KABUPATEN TOBA SAMOSIR', '12'),
('1207', 'KABUPATEN LABUHAN BATU', '12'),
('1208', 'KABUPATEN ASAHAN', '12'),
('1209', 'KABUPATEN SIMALUNGUN', '12'),
('1210', 'KABUPATEN DAIRI', '12'),
('1211', 'KABUPATEN KARO', '12'),
('1212', 'KABUPATEN DELI SERDANG', '12'),
('1213', 'KABUPATEN LANGKAT', '12'),
('1214', 'KABUPATEN NIAS SELATAN', '12'),
('1215', 'KABUPATEN HUMBANG HASUNDUTAN', '12'),
('1216', 'KABUPATEN PAKPAK BHARAT', '12'),
('1217', 'KABUPATEN SAMOSIR', '12'),
('1218', 'KABUPATEN SERDANG BEDAGAI', '12'),
('1219', 'KABUPATEN BATU BARA', '12'),
('1220', 'KABUPATEN PADANG LAWAS UTARA', '12'),
('1221', 'KABUPATEN PADANG LAWAS', '12'),
('1222', 'KABUPATEN LABUHAN BATU SELATAN', '12'),
('1223', 'KABUPATEN LABUHAN BATU UTARA', '12'),
('1224', 'KABUPATEN NIAS UTARA', '12'),
('1225', 'KABUPATEN NIAS BARAT', '12'),
('1271', 'KOTA SIBOLGA', '12'),
('1272', 'KOTA TANJUNG BALAI', '12'),
('1273', 'KOTA PEMATANG SIANTAR', '12'),
('1274', 'KOTA TEBING TINGGI', '12'),
('1275', 'KOTA MEDAN', '12'),
('1276', 'KOTA BINJAI', '12'),
('1277', 'KOTA PADANGSIDIMPUAN', '12'),
('1278', 'KOTA GUNUNGSITOLI', '12'),
('1301', 'KABUPATEN KEPULAUAN MENTAWAI', '13'),
('1302', 'KABUPATEN PESISIR SELATAN', '13'),
('1303', 'KABUPATEN SOLOK', '13'),
('1304', 'KABUPATEN SIJUNJUNG', '13'),
('1305', 'KABUPATEN TANAH DATAR', '13'),
('1306', 'KABUPATEN PADANG PARIAMAN', '13'),
('1307', 'KABUPATEN AGAM', '13'),
('1308', 'KABUPATEN LIMA PULUH KOTA', '13'),
('1309', 'KABUPATEN PASAMAN', '13'),
('1310', 'KABUPATEN SOLOK SELATAN', '13'),
('1311', 'KABUPATEN DHARMASRAYA', '13'),
('1312', 'KABUPATEN PASAMAN BARAT', '13'),
('1371', 'KOTA PADANG', '13'),
('1372', 'KOTA SOLOK', '13'),
('1373', 'KOTA SAWAH LUNTO', '13'),
('1374', 'KOTA PADANG PANJANG', '13'),
('1375', 'KOTA BUKITTINGGI', '13'),
('1376', 'KOTA PAYAKUMBUH', '13'),
('1377', 'KOTA PARIAMAN', '13'),
('1401', 'KABUPATEN KUANTAN SINGINGI', '14'),
('1402', 'KABUPATEN INDRAGIRI HULU', '14'),
('1403', 'KABUPATEN INDRAGIRI HILIR', '14'),
('1404', 'KABUPATEN PELALAWAN', '14'),
('1405', 'KABUPATEN S I A K', '14'),
('1406', 'KABUPATEN KAMPAR', '14'),
('1407', 'KABUPATEN ROKAN HULU', '14'),
('1408', 'KABUPATEN BENGKALIS', '14'),
('1409', 'KABUPATEN ROKAN HILIR', '14'),
('1410', 'KABUPATEN KEPULAUAN MERANTI', '14'),
('1471', 'KOTA PEKANBARU', '14'),
('1473', 'KOTA D U M A I', '14'),
('1501', 'KABUPATEN KERINCI', '15'),
('1502', 'KABUPATEN MERANGIN', '15'),
('1503', 'KABUPATEN SAROLANGUN', '15'),
('1504', 'KABUPATEN BATANG HARI', '15'),
('1505', 'KABUPATEN MUARO JAMBI', '15'),
('1506', 'KABUPATEN TANJUNG JABUNG TIMUR', '15'),
('1507', 'KABUPATEN TANJUNG JABUNG BARAT', '15'),
('1508', 'KABUPATEN TEBO', '15'),
('1509', 'KABUPATEN BUNGO', '15'),
('1571', 'KOTA JAMBI', '15'),
('1572', 'KOTA SUNGAI PENUH', '15'),
('1601', 'KABUPATEN OGAN KOMERING ULU', '16'),
('1602', 'KABUPATEN OGAN KOMERING ILIR', '16'),
('1603', 'KABUPATEN MUARA ENIM', '16'),
('1604', 'KABUPATEN LAHAT', '16'),
('1605', 'KABUPATEN MUSI RAWAS', '16'),
('1606', 'KABUPATEN MUSI BANYUASIN', '16'),
('1607', 'KABUPATEN BANYU ASIN', '16'),
('1608', 'KABUPATEN OGAN KOMERING ULU SELATAN', '16'),
('1609', 'KABUPATEN OGAN KOMERING ULU TIMUR', '16'),
('1610', 'KABUPATEN OGAN ILIR', '16'),
('1611', 'KABUPATEN EMPAT LAWANG', '16'),
('1612', 'KABUPATEN PENUKAL ABAB LEMATANG ILIR', '16'),
('1613', 'KABUPATEN MUSI RAWAS UTARA', '16'),
('1671', 'KOTA PALEMBANG', '16'),
('1672', 'KOTA PRABUMULIH', '16'),
('1673', 'KOTA PAGAR ALAM', '16'),
('1674', 'KOTA LUBUKLINGGAU', '16'),
('1701', 'KABUPATEN BENGKULU SELATAN', '17'),
('1702', 'KABUPATEN REJANG LEBONG', '17'),
('1703', 'KABUPATEN BENGKULU UTARA', '17'),
('1704', 'KABUPATEN KAUR', '17'),
('1705', 'KABUPATEN SELUMA', '17'),
('1706', 'KABUPATEN MUKOMUKO', '17'),
('1707', 'KABUPATEN LEBONG', '17'),
('1708', 'KABUPATEN KEPAHIANG', '17'),
('1709', 'KABUPATEN BENGKULU TENGAH', '17'),
('1771', 'KOTA BENGKULU', '17'),
('1801', 'KABUPATEN LAMPUNG BARAT', '18'),
('1802', 'KABUPATEN TANGGAMUS', '18'),
('1803', 'KABUPATEN LAMPUNG SELATAN', '18'),
('1804', 'KABUPATEN LAMPUNG TIMUR', '18'),
('1805', 'KABUPATEN LAMPUNG TENGAH', '18'),
('1806', 'KABUPATEN LAMPUNG UTARA', '18'),
('1807', 'KABUPATEN WAY KANAN', '18'),
('1808', 'KABUPATEN TULANGBAWANG', '18'),
('1809', 'KABUPATEN PESAWARAN', '18'),
('1810', 'KABUPATEN PRINGSEWU', '18'),
('1811', 'KABUPATEN MESUJI', '18'),
('1812', 'KABUPATEN TULANG BAWANG BARAT', '18'),
('1813', 'KABUPATEN PESISIR BARAT', '18'),
('1871', 'KOTA BANDAR LAMPUNG', '18'),
('1872', 'KOTA METRO', '18'),
('1901', 'KABUPATEN BANGKA', '19'),
('1902', 'KABUPATEN BELITUNG', '19'),
('1903', 'KABUPATEN BANGKA BARAT', '19'),
('1904', 'KABUPATEN BANGKA TENGAH', '19'),
('1905', 'KABUPATEN BANGKA SELATAN', '19'),
('1906', 'KABUPATEN BELITUNG TIMUR', '19'),
('1971', 'KOTA PANGKAL PINANG', '19'),
('2101', 'KABUPATEN KARIMUN', '21'),
('2102', 'KABUPATEN BINTAN', '21'),
('2103', 'KABUPATEN NATUNA', '21'),
('2104', 'KABUPATEN LINGGA', '21'),
('2105', 'KABUPATEN KEPULAUAN ANAMBAS', '21'),
('2171', 'KOTA B A T A M', '21'),
('2172', 'KOTA TANJUNG PINANG', '21'),
('3101', 'KABUPATEN KEPULAUAN SERIBU', '31'),
('3171', 'KOTA JAKARTA SELATAN', '31'),
('3172', 'KOTA JAKARTA TIMUR', '31'),
('3173', 'KOTA JAKARTA PUSAT', '31'),
('3174', 'KOTA JAKARTA BARAT', '31'),
('3175', 'KOTA JAKARTA UTARA', '31'),
('3201', 'KABUPATEN BOGOR', '32'),
('3202', 'KABUPATEN SUKABUMI', '32'),
('3203', 'KABUPATEN CIANJUR', '32'),
('3204', 'KABUPATEN BANDUNG', '32'),
('3205', 'KABUPATEN GARUT', '32'),
('3206', 'KABUPATEN TASIKMALAYA', '32'),
('3207', 'KABUPATEN CIAMIS', '32'),
('3208', 'KABUPATEN KUNINGAN', '32'),
('3209', 'KABUPATEN CIREBON', '32'),
('3210', 'KABUPATEN MAJALENGKA', '32'),
('3211', 'KABUPATEN SUMEDANG', '32'),
('3212', 'KABUPATEN INDRAMAYU', '32'),
('3213', 'KABUPATEN SUBANG', '32'),
('3214', 'KABUPATEN PURWAKARTA', '32'),
('3215', 'KABUPATEN KARAWANG', '32'),
('3216', 'KABUPATEN BEKASI', '32'),
('3217', 'KABUPATEN BANDUNG BARAT', '32'),
('3218', 'KABUPATEN PANGANDARAN', '32'),
('3271', 'KOTA BOGOR', '32'),
('3272', 'KOTA SUKABUMI', '32'),
('3273', 'KOTA BANDUNG', '32'),
('3274', 'KOTA CIREBON', '32'),
('3275', 'KOTA BEKASI', '32'),
('3276', 'KOTA DEPOK', '32'),
('3277', 'KOTA CIMAHI', '32'),
('3278', 'KOTA TASIKMALAYA', '32'),
('3279', 'KOTA BANJAR', '32'),
('3301', 'KABUPATEN CILACAP', '33'),
('3302', 'KABUPATEN BANYUMAS', '33'),
('3303', 'KABUPATEN PURBALINGGA', '33'),
('3304', 'KABUPATEN BANJARNEGARA', '33'),
('3305', 'KABUPATEN KEBUMEN', '33'),
('3306', 'KABUPATEN PURWOREJO', '33'),
('3307', 'KABUPATEN WONOSOBO', '33'),
('3308', 'KABUPATEN MAGELANG', '33'),
('3309', 'KABUPATEN BOYOLALI', '33'),
('3310', 'KABUPATEN KLATEN', '33'),
('3311', 'KABUPATEN SUKOHARJO', '33'),
('3312', 'KABUPATEN WONOGIRI', '33'),
('3313', 'KABUPATEN KARANGANYAR', '33'),
('3314', 'KABUPATEN SRAGEN', '33'),
('3315', 'KABUPATEN GROBOGAN', '33'),
('3316', 'KABUPATEN BLORA', '33'),
('3317', 'KABUPATEN REMBANG', '33'),
('3318', 'KABUPATEN PATI', '33'),
('3319', 'KABUPATEN KUDUS', '33'),
('3320', 'KABUPATEN JEPARA', '33'),
('3321', 'KABUPATEN DEMAK', '33'),
('3322', 'KABUPATEN SEMARANG', '33'),
('3323', 'KABUPATEN TEMANGGUNG', '33'),
('3324', 'KABUPATEN KENDAL', '33'),
('3325', 'KABUPATEN BATANG', '33'),
('3326', 'KABUPATEN PEKALONGAN', '33'),
('3327', 'KABUPATEN PEMALANG', '33'),
('3328', 'KABUPATEN TEGAL', '33'),
('3329', 'KABUPATEN BREBES', '33'),
('3371', 'KOTA MAGELANG', '33'),
('3372', 'KOTA SURAKARTA', '33'),
('3373', 'KOTA SALATIGA', '33'),
('3374', 'KOTA SEMARANG', '33'),
('3375', 'KOTA PEKALONGAN', '33'),
('3376', 'KOTA TEGAL', '33'),
('3401', 'KABUPATEN KULON PROGO', '34'),
('3402', 'KABUPATEN BANTUL', '34'),
('3403', 'KABUPATEN GUNUNG KIDUL', '34'),
('3404', 'KABUPATEN SLEMAN', '34'),
('3471', 'KOTA YOGYAKARTA', '34'),
('3501', 'KABUPATEN PACITAN', '35'),
('3502', 'KABUPATEN PONOROGO', '35'),
('3503', 'KABUPATEN TRENGGALEK', '35'),
('3504', 'KABUPATEN TULUNGAGUNG', '35'),
('3505', 'KABUPATEN BLITAR', '35'),
('3506', 'KABUPATEN KEDIRI', '35'),
('3507', 'KABUPATEN MALANG', '35'),
('3508', 'KABUPATEN LUMAJANG', '35'),
('3509', 'KABUPATEN JEMBER', '35'),
('3510', 'KABUPATEN BANYUWANGI', '35'),
('3511', 'KABUPATEN BONDOWOSO', '35'),
('3512', 'KABUPATEN SITUBONDO', '35'),
('3513', 'KABUPATEN PROBOLINGGO', '35'),
('3514', 'KABUPATEN PASURUAN', '35'),
('3515', 'KABUPATEN SIDOARJO', '35'),
('3516', 'KABUPATEN MOJOKERTO', '35'),
('3517', 'KABUPATEN JOMBANG', '35'),
('3518', 'KABUPATEN NGANJUK', '35'),
('3519', 'KABUPATEN MADIUN', '35'),
('3520', 'KABUPATEN MAGETAN', '35'),
('3521', 'KABUPATEN NGAWI', '35'),
('3522', 'KABUPATEN BOJONEGORO', '35'),
('3523', 'KABUPATEN TUBAN', '35'),
('3524', 'KABUPATEN LAMONGAN', '35'),
('3525', 'KABUPATEN GRESIK', '35'),
('3526', 'KABUPATEN BANGKALAN', '35'),
('3527', 'KABUPATEN SAMPANG', '35'),
('3528', 'KABUPATEN PAMEKASAN', '35'),
('3529', 'KABUPATEN SUMENEP', '35'),
('3571', 'KOTA KEDIRI', '35'),
('3572', 'KOTA BLITAR', '35'),
('3573', 'KOTA MALANG', '35'),
('3574', 'KOTA PROBOLINGGO', '35'),
('3575', 'KOTA PASURUAN', '35'),
('3576', 'KOTA MOJOKERTO', '35'),
('3577', 'KOTA MADIUN', '35'),
('3578', 'KOTA SURABAYA', '35'),
('3579', 'KOTA BATU', '35'),
('3601', 'KABUPATEN PANDEGLANG', '36'),
('3602', 'KABUPATEN LEBAK', '36'),
('3603', 'KABUPATEN TANGERANG', '36'),
('3604', 'KABUPATEN SERANG', '36'),
('3671', 'KOTA TANGERANG', '36'),
('3672', 'KOTA CILEGON', '36'),
('3673', 'KOTA SERANG', '36'),
('3674', 'KOTA TANGERANG SELATAN', '36'),
('5101', 'KABUPATEN JEMBRANA', '51'),
('5102', 'KABUPATEN TABANAN', '51'),
('5103', 'KABUPATEN BADUNG', '51'),
('5104', 'KABUPATEN GIANYAR', '51'),
('5105', 'KABUPATEN KLUNGKUNG', '51'),
('5106', 'KABUPATEN BANGLI', '51'),
('5107', 'KABUPATEN KARANG ASEM', '51'),
('5108', 'KABUPATEN BULELENG', '51'),
('5171', 'KOTA DENPASAR', '51'),
('5201', 'KABUPATEN LOMBOK BARAT', '52'),
('5202', 'KABUPATEN LOMBOK TENGAH', '52'),
('5203', 'KABUPATEN LOMBOK TIMUR', '52'),
('5204', 'KABUPATEN SUMBAWA', '52'),
('5205', 'KABUPATEN DOMPU', '52'),
('5206', 'KABUPATEN BIMA', '52'),
('5207', 'KABUPATEN SUMBAWA BARAT', '52'),
('5208', 'KABUPATEN LOMBOK UTARA', '52'),
('5271', 'KOTA MATARAM', '52'),
('5272', 'KOTA BIMA', '52'),
('5301', 'KABUPATEN SUMBA BARAT', '53'),
('5302', 'KABUPATEN SUMBA TIMUR', '53'),
('5303', 'KABUPATEN KUPANG', '53'),
('5304', 'KABUPATEN TIMOR TENGAH SELATAN', '53'),
('5305', 'KABUPATEN TIMOR TENGAH UTARA', '53'),
('5306', 'KABUPATEN BELU', '53'),
('5307', 'KABUPATEN ALOR', '53'),
('5308', 'KABUPATEN LEMBATA', '53'),
('5309', 'KABUPATEN FLORES TIMUR', '53'),
('5310', 'KABUPATEN SIKKA', '53'),
('5311', 'KABUPATEN ENDE', '53'),
('5312', 'KABUPATEN NGADA', '53'),
('5313', 'KABUPATEN MANGGARAI', '53'),
('5314', 'KABUPATEN ROTE NDAO', '53'),
('5315', 'KABUPATEN MANGGARAI BARAT', '53'),
('5316', 'KABUPATEN SUMBA TENGAH', '53'),
('5317', 'KABUPATEN SUMBA BARAT DAYA', '53'),
('5318', 'KABUPATEN NAGEKEO', '53'),
('5319', 'KABUPATEN MANGGARAI TIMUR', '53'),
('5320', 'KABUPATEN SABU RAIJUA', '53'),
('5321', 'KABUPATEN MALAKA', '53'),
('5371', 'KOTA KUPANG', '53'),
('6101', 'KABUPATEN SAMBAS', '61'),
('6102', 'KABUPATEN BENGKAYANG', '61'),
('6103', 'KABUPATEN LANDAK', '61'),
('6104', 'KABUPATEN MEMPAWAH', '61'),
('6105', 'KABUPATEN SANGGAU', '61'),
('6106', 'KABUPATEN KETAPANG', '61'),
('6107', 'KABUPATEN SINTANG', '61'),
('6108', 'KABUPATEN KAPUAS HULU', '61'),
('6109', 'KABUPATEN SEKADAU', '61'),
('6110', 'KABUPATEN MELAWI', '61'),
('6111', 'KABUPATEN KAYONG UTARA', '61'),
('6112', 'KABUPATEN KUBU RAYA', '61'),
('6171', 'KOTA PONTIANAK', '61'),
('6172', 'KOTA SINGKAWANG', '61'),
('6201', 'KABUPATEN KOTAWARINGIN BARAT', '62'),
('6202', 'KABUPATEN KOTAWARINGIN TIMUR', '62'),
('6203', 'KABUPATEN KAPUAS', '62'),
('6204', 'KABUPATEN BARITO SELATAN', '62'),
('6205', 'KABUPATEN BARITO UTARA', '62'),
('6206', 'KABUPATEN SUKAMARA', '62'),
('6207', 'KABUPATEN LAMANDAU', '62'),
('6208', 'KABUPATEN SERUYAN', '62'),
('6209', 'KABUPATEN KATINGAN', '62'),
('6210', 'KABUPATEN PULANG PISAU', '62'),
('6211', 'KABUPATEN GUNUNG MAS', '62'),
('6212', 'KABUPATEN BARITO TIMUR', '62'),
('6213', 'KABUPATEN MURUNG RAYA', '62'),
('6271', 'KOTA PALANGKA RAYA', '62'),
('6301', 'KABUPATEN TANAH LAUT', '63'),
('6302', 'KABUPATEN KOTA BARU', '63'),
('6303', 'KABUPATEN BANJAR', '63'),
('6304', 'KABUPATEN BARITO KUALA', '63'),
('6305', 'KABUPATEN TAPIN', '63'),
('6306', 'KABUPATEN HULU SUNGAI SELATAN', '63'),
('6307', 'KABUPATEN HULU SUNGAI TENGAH', '63'),
('6308', 'KABUPATEN HULU SUNGAI UTARA', '63'),
('6309', 'KABUPATEN TABALONG', '63'),
('6310', 'KABUPATEN TANAH BUMBU', '63'),
('6311', 'KABUPATEN BALANGAN', '63'),
('6371', 'KOTA BANJARMASIN', '63'),
('6372', 'KOTA BANJAR BARU', '63'),
('6401', 'KABUPATEN PASER', '64'),
('6402', 'KABUPATEN KUTAI BARAT', '64'),
('6403', 'KABUPATEN KUTAI KARTANEGARA', '64'),
('6404', 'KABUPATEN KUTAI TIMUR', '64'),
('6405', 'KABUPATEN BERAU', '64'),
('6409', 'KABUPATEN PENAJAM PASER UTARA', '64'),
('6411', 'KABUPATEN MAHAKAM HULU', '64'),
('6471', 'KOTA BALIKPAPAN', '64'),
('6472', 'KOTA SAMARINDA', '64'),
('6474', 'KOTA BONTANG', '64'),
('6501', 'KABUPATEN MALINAU', '65'),
('6502', 'KABUPATEN BULUNGAN', '65'),
('6503', 'KABUPATEN TANA TIDUNG', '65'),
('6504', 'KABUPATEN NUNUKAN', '65'),
('6571', 'KOTA TARAKAN', '65'),
('7101', 'KABUPATEN BOLAANG MONGONDOW', '71'),
('7102', 'KABUPATEN MINAHASA', '71'),
('7103', 'KABUPATEN KEPULAUAN SANGIHE', '71'),
('7104', 'KABUPATEN KEPULAUAN TALAUD', '71'),
('7105', 'KABUPATEN MINAHASA SELATAN', '71'),
('7106', 'KABUPATEN MINAHASA UTARA', '71'),
('7107', 'KABUPATEN BOLAANG MONGONDOW UTARA', '71'),
('7108', 'KABUPATEN SIAU TAGULANDANG BIARO', '71'),
('7109', 'KABUPATEN MINAHASA TENGGARA', '71'),
('7110', 'KABUPATEN BOLAANG MONGONDOW SELATAN', '71'),
('7111', 'KABUPATEN BOLAANG MONGONDOW TIMUR', '71'),
('7171', 'KOTA MANADO', '71'),
('7172', 'KOTA BITUNG', '71'),
('7173', 'KOTA TOMOHON', '71'),
('7174', 'KOTA KOTAMOBAGU', '71'),
('7201', 'KABUPATEN BANGGAI KEPULAUAN', '72'),
('7202', 'KABUPATEN BANGGAI', '72'),
('7203', 'KABUPATEN MOROWALI', '72'),
('7204', 'KABUPATEN POSO', '72'),
('7205', 'KABUPATEN DONGGALA', '72'),
('7206', 'KABUPATEN TOLI-TOLI', '72'),
('7207', 'KABUPATEN BUOL', '72'),
('7208', 'KABUPATEN PARIGI MOUTONG', '72'),
('7209', 'KABUPATEN TOJO UNA-UNA', '72'),
('7210', 'KABUPATEN SIGI', '72'),
('7211', 'KABUPATEN BANGGAI LAUT', '72'),
('7212', 'KABUPATEN MOROWALI UTARA', '72'),
('7271', 'KOTA PALU', '72'),
('7301', 'KABUPATEN KEPULAUAN SELAYAR', '73'),
('7302', 'KABUPATEN BULUKUMBA', '73'),
('7303', 'KABUPATEN BANTAENG', '73'),
('7304', 'KABUPATEN JENEPONTO', '73'),
('7305', 'KABUPATEN TAKALAR', '73'),
('7306', 'KABUPATEN GOWA', '73'),
('7307', 'KABUPATEN SINJAI', '73'),
('7308', 'KABUPATEN MAROS', '73'),
('7309', 'KABUPATEN PANGKAJENE DAN KEPULAUAN', '73'),
('7310', 'KABUPATEN BARRU', '73'),
('7311', 'KABUPATEN BONE', '73'),
('7312', 'KABUPATEN SOPPENG', '73'),
('7313', 'KABUPATEN WAJO', '73'),
('7314', 'KABUPATEN SIDENRENG RAPPANG', '73'),
('7315', 'KABUPATEN PINRANG', '73'),
('7316', 'KABUPATEN ENREKANG', '73'),
('7317', 'KABUPATEN LUWU', '73'),
('7318', 'KABUPATEN TANA TORAJA', '73'),
('7322', 'KABUPATEN LUWU UTARA', '73'),
('7325', 'KABUPATEN LUWU TIMUR', '73'),
('7326', 'KABUPATEN TORAJA UTARA', '73'),
('7371', 'KOTA MAKASSAR', '73'),
('7372', 'KOTA PAREPARE', '73'),
('7373', 'KOTA PALOPO', '73'),
('7401', 'KABUPATEN BUTON', '74'),
('7402', 'KABUPATEN MUNA', '74'),
('7403', 'KABUPATEN KONAWE', '74'),
('7404', 'KABUPATEN KOLAKA', '74'),
('7405', 'KABUPATEN KONAWE SELATAN', '74'),
('7406', 'KABUPATEN BOMBANA', '74'),
('7407', 'KABUPATEN WAKATOBI', '74'),
('7408', 'KABUPATEN KOLAKA UTARA', '74'),
('7409', 'KABUPATEN BUTON UTARA', '74'),
('7410', 'KABUPATEN KONAWE UTARA', '74'),
('7411', 'KABUPATEN KOLAKA TIMUR', '74'),
('7412', 'KABUPATEN KONAWE KEPULAUAN', '74'),
('7413', 'KABUPATEN MUNA BARAT', '74'),
('7414', 'KABUPATEN BUTON TENGAH', '74'),
('7415', 'KABUPATEN BUTON SELATAN', '74'),
('7471', 'KOTA KENDARI', '74'),
('7472', 'KOTA BAUBAU', '74'),
('7501', 'KABUPATEN BOALEMO', '75'),
('7502', 'KABUPATEN GORONTALO', '75'),
('7503', 'KABUPATEN POHUWATO', '75'),
('7504', 'KABUPATEN BONE BOLANGO', '75'),
('7505', 'KABUPATEN GORONTALO UTARA', '75'),
('7571', 'KOTA GORONTALO', '75'),
('7601', 'KABUPATEN MAJENE', '76'),
('7602', 'KABUPATEN POLEWALI MANDAR', '76'),
('7603', 'KABUPATEN MAMASA', '76'),
('7604', 'KABUPATEN MAMUJU', '76'),
('7605', 'KABUPATEN MAMUJU UTARA', '76'),
('7606', 'KABUPATEN MAMUJU TENGAH', '76'),
('8101', 'KABUPATEN MALUKU TENGGARA BARAT', '81'),
('8102', 'KABUPATEN MALUKU TENGGARA', '81'),
('8103', 'KABUPATEN MALUKU TENGAH', '81'),
('8104', 'KABUPATEN BURU', '81'),
('8105', 'KABUPATEN KEPULAUAN ARU', '81'),
('8106', 'KABUPATEN SERAM BAGIAN BARAT', '81'),
('8107', 'KABUPATEN SERAM BAGIAN TIMUR', '81'),
('8108', 'KABUPATEN MALUKU BARAT DAYA', '81'),
('8109', 'KABUPATEN BURU SELATAN', '81'),
('8171', 'KOTA AMBON', '81'),
('8172', 'KOTA TUAL', '81'),
('8201', 'KABUPATEN HALMAHERA BARAT', '82'),
('8202', 'KABUPATEN HALMAHERA TENGAH', '82'),
('8203', 'KABUPATEN KEPULAUAN SULA', '82'),
('8204', 'KABUPATEN HALMAHERA SELATAN', '82'),
('8205', 'KABUPATEN HALMAHERA UTARA', '82'),
('8206', 'KABUPATEN HALMAHERA TIMUR', '82'),
('8207', 'KABUPATEN PULAU MOROTAI', '82'),
('8208', 'KABUPATEN PULAU TALIABU', '82'),
('8271', 'KOTA TERNATE', '82'),
('8272', 'KOTA TIDORE KEPULAUAN', '82'),
('9101', 'KABUPATEN FAKFAK', '91'),
('9102', 'KABUPATEN KAIMANA', '91'),
('9103', 'KABUPATEN TELUK WONDAMA', '91'),
('9104', 'KABUPATEN TELUK BINTUNI', '91'),
('9105', 'KABUPATEN MANOKWARI', '91'),
('9106', 'KABUPATEN SORONG SELATAN', '91'),
('9107', 'KABUPATEN SORONG', '91'),
('9108', 'KABUPATEN RAJA AMPAT', '91'),
('9109', 'KABUPATEN TAMBRAUW', '91'),
('9110', 'KABUPATEN MAYBRAT', '91'),
('9111', 'KABUPATEN MANOKWARI SELATAN', '91'),
('9112', 'KABUPATEN PEGUNUNGAN ARFAK', '91'),
('9171', 'KOTA SORONG', '91'),
('9401', 'KABUPATEN MERAUKE', '94'),
('9402', 'KABUPATEN JAYAWIJAYA', '94'),
('9403', 'KABUPATEN JAYAPURA', '94'),
('9404', 'KABUPATEN NABIRE', '94'),
('9408', 'KABUPATEN KEPULAUAN YAPEN', '94'),
('9409', 'KABUPATEN BIAK NUMFOR', '94'),
('9410', 'KABUPATEN PANIAI', '94'),
('9411', 'KABUPATEN PUNCAK JAYA', '94'),
('9412', 'KABUPATEN MIMIKA', '94'),
('9413', 'KABUPATEN BOVEN DIGOEL', '94'),
('9414', 'KABUPATEN MAPPI', '94'),
('9415', 'KABUPATEN ASMAT', '94'),
('9416', 'KABUPATEN YAHUKIMO', '94'),
('9417', 'KABUPATEN PEGUNUNGAN BINTANG', '94'),
('9418', 'KABUPATEN TOLIKARA', '94'),
('9419', 'KABUPATEN SARMI', '94'),
('9420', 'KABUPATEN KEEROM', '94'),
('9426', 'KABUPATEN WAROPEN', '94'),
('9427', 'KABUPATEN SUPIORI', '94'),
('9428', 'KABUPATEN MAMBERAMO RAYA', '94'),
('9429', 'KABUPATEN NDUGA', '94'),
('9430', 'KABUPATEN LANNY JAYA', '94'),
('9431', 'KABUPATEN MAMBERAMO TENGAH', '94'),
('9432', 'KABUPATEN YALIMO', '94'),
('9433', 'KABUPATEN PUNCAK', '94'),
('9434', 'KABUPATEN DOGIYAI', '94'),
('9435', 'KABUPATEN INTAN JAYA', '94'),
('9436', 'KABUPATEN DEIYAI', '94'),
('9471', 'KOTA JAYAPURA', '94');

-- --------------------------------------------------------

--
-- Table structure for table `mstkecamatan`
--

CREATE TABLE `mstkecamatan` (
  `KodeKec` varchar(15) NOT NULL,
  `NamaKec` varchar(100) DEFAULT NULL,
  `KodeKab` char(10) NOT NULL,
  `KodeProvinsi` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mstkecamatan`
--

INSERT INTO `mstkecamatan` (`KodeKec`, `NamaKec`, `KodeKab`, `KodeProvinsi`) VALUES
('1101010', 'TEUPAH SELATAN', '1101', '11'),
('1101020', 'SIMEULUE TIMUR', '1101', '11'),
('1101021', 'TEUPAH BARAT', '1101', '11'),
('1101022', 'TEUPAH TENGAH', '1101', '11'),
('1101030', 'SIMEULUE TENGAH', '1101', '11'),
('1101031', 'TELUK DALAM', '1101', '11'),
('1101032', 'SIMEULUE CUT', '1101', '11'),
('1101040', 'SALANG', '1101', '11'),
('1101050', 'SIMEULUE BARAT', '1101', '11'),
('1101051', 'ALAFAN', '1101', '11'),
('1102010', 'PULAU BANYAK', '1102', '11'),
('1102011', 'PULAU BANYAK BARAT', '1102', '11'),
('1102020', 'SINGKIL', '1102', '11'),
('1102021', 'SINGKIL UTARA', '1102', '11'),
('1102022', 'KUALA BARU', '1102', '11'),
('1102030', 'SIMPANG KANAN', '1102', '11'),
('1102031', 'GUNUNG MERIAH', '1102', '11'),
('1102032', 'DANAU PARIS', '1102', '11'),
('1102033', 'SURO', '1102', '11'),
('1102042', 'SINGKOHOR', '1102', '11'),
('1102043', 'KOTA BAHARU', '1102', '11'),
('1103010', 'TRUMON', '1103', '11'),
('1103011', 'TRUMON TIMUR', '1103', '11'),
('1103012', 'TRUMON TENGAH', '1103', '11'),
('1103020', 'BAKONGAN', '1103', '11'),
('1103021', 'BAKONGAN TIMUR', '1103', '11'),
('1103022', 'KOTA BAHAGIA', '1103', '11'),
('1103030', 'KLUET SELATAN', '1103', '11'),
('1103031', 'KLUET TIMUR', '1103', '11'),
('1103040', 'KLUET UTARA', '1103', '11'),
('1103041', 'PASIE RAJA', '1103', '11'),
('1103042', 'KLUET TENGAH', '1103', '11'),
('1103050', 'TAPAK TUAN', '1103', '11'),
('1103060', 'SAMA DUA', '1103', '11'),
('1103070', 'SAWANG', '1103', '11'),
('1103080', 'MEUKEK', '1103', '11'),
('1103090', 'LABUHAN HAJI', '1103', '11'),
('1103091', 'LABUHAN HAJI TIMUR', '1103', '11'),
('1103092', 'LABUHAN HAJI BARAT', '1103', '11'),
('1104010', 'LAWE ALAS', '1104', '11'),
('1104011', 'BABUL RAHMAH', '1104', '11'),
('1104012', 'TANOH ALAS', '1104', '11'),
('1104020', 'LAWE SIGALA-GALA', '1104', '11'),
('1104021', 'BABUL MAKMUR', '1104', '11'),
('1104022', 'SEMADAM', '1104', '11'),
('1104023', 'LAUSER', '1104', '11'),
('1104030', 'BAMBEL', '1104', '11'),
('1104031', 'BUKIT TUSAM', '1104', '11'),
('1104032', 'LAWE SUMUR', '1104', '11'),
('1104040', 'BABUSSALAM', '1104', '11'),
('1104041', 'LAWE BULAN', '1104', '11'),
('1104050', 'BADAR', '1104', '11'),
('1104051', 'DARUL HASANAH', '1104', '11'),
('1104052', 'KETAMBE', '1104', '11'),
('1104053', 'DELENG POKHKISEN', '1104', '11'),
('1105080', 'SERBA JADI', '1105', '11'),
('1105081', 'SIMPANG JERNIH', '1105', '11'),
('1105082', 'PEUNARON', '1105', '11'),
('1105090', 'BIREM BAYEUN', '1105', '11'),
('1105100', 'RANTAU SELAMAT', '1105', '11'),
('1105101', 'SUNGAI RAYA', '1105', '11'),
('1105110', 'PEUREULAK', '1105', '11'),
('1105111', 'PEUREULAK TIMUR', '1105', '11'),
('1105112', 'PEUREULAK BARAT', '1105', '11'),
('1105120', 'RANTO PEUREULAK', '1105', '11'),
('1105130', 'IDI RAYEUK', '1105', '11'),
('1105131', 'PEUDAWA', '1105', '11'),
('1105132', 'BANDA ALAM', '1105', '11'),
('1105133', 'IDI TUNONG', '1105', '11'),
('1105134', 'DARUL IHSAN', '1105', '11'),
('1105135', 'IDI TIMUR', '1105', '11'),
('1105140', 'DARUL AMAN', '1105', '11'),
('1105150', 'NURUSSALAM', '1105', '11'),
('1105151', 'DARUL FALAH', '1105', '11'),
('1105160', 'JULOK', '1105', '11'),
('1105161', 'INDRA MAKMUR', '1105', '11'),
('1105170', 'PANTE BIDARI', '1105', '11'),
('1105180', 'SIMPANG ULIM', '1105', '11'),
('1105181', 'MADAT', '1105', '11'),
('1106010', 'LINGE', '1106', '11'),
('1106011', 'ATU LINTANG', '1106', '11'),
('1106012', 'JAGONG JEGET', '1106', '11'),
('1106020', 'BINTANG', '1106', '11'),
('1106031', 'LUT TAWAR', '1106', '11'),
('1106032', 'KEBAYAKAN', '1106', '11'),
('1106040', 'PEGASING', '1106', '11'),
('1106041', 'BIES', '1106', '11'),
('1106050', 'BEBESEN', '1106', '11'),
('1106051', 'KUTE PANANG', '1106', '11'),
('1106060', 'SILIH NARA', '1106', '11'),
('1106061', 'KETOL', '1106', '11'),
('1106062', 'CELALA', '1106', '11'),
('1106063', 'RUSIP ANTARA', '1106', '11'),
('1107050', 'JOHAN PAHLAWAN', '1107', '11'),
('1107060', 'SAMATIGA', '1107', '11'),
('1107061', 'BUBON', '1107', '11'),
('1107062', 'ARONGAN LAMBALEK', '1107', '11'),
('1107070', 'WOYLA', '1107', '11'),
('1107071', 'WOYLA BARAT', '1107', '11'),
('1107072', 'WOYLA TIMUR', '1107', '11'),
('1107080', 'KAWAY XVI', '1107', '11'),
('1107081', 'MEUREUBO', '1107', '11'),
('1107082', 'PANTAI CEUREMEN', '1107', '11'),
('1107083', 'PANTON REU', '1107', '11'),
('1107090', 'SUNGAI MAS', '1107', '11'),
('1108010', 'LHOONG', '1108', '11'),
('1108020', 'LHOKNGA', '1108', '11'),
('1108021', 'LEUPUNG', '1108', '11'),
('1108030', 'INDRAPURI', '1108', '11'),
('1108031', 'KUTA COT GLIE', '1108', '11'),
('1108040', 'SEULIMEUM', '1108', '11'),
('1108041', 'KOTA JANTHO', '1108', '11'),
('1108042', 'LEMBAH SEULAWAH', '1108', '11'),
('1108050', 'MESJID RAYA', '1108', '11'),
('1108060', 'DARUSSALAM', '1108', '11'),
('1108061', 'BAITUSSALAM', '1108', '11'),
('1108070', 'KUTA BARO', '1108', '11'),
('1108080', 'MONTASIK', '1108', '11'),
('1108081', 'BLANG BINTANG', '1108', '11'),
('1108090', 'INGIN JAYA', '1108', '11'),
('1108091', 'KRUENG BARONA JAYA', '1108', '11'),
('1108100', 'SUKA MAKMUR', '1108', '11'),
('1108101', 'KUTA MALAKA', '1108', '11'),
('1108102', 'SIMPANG TIGA', '1108', '11'),
('1108110', 'DARUL IMARAH', '1108', '11'),
('1108111', 'DARUL KAMAL', '1108', '11'),
('1108120', 'PEUKAN BADA', '1108', '11'),
('1108130', 'PULO ACEH', '1108', '11'),
('1109010', 'GEUMPANG', '1109', '11'),
('1109011', 'MANE', '1109', '11'),
('1109070', 'GLUMPANG TIGA', '1109', '11'),
('1109071', 'GLUMPANG BARO', '1109', '11'),
('1109080', 'MUTIARA', '1109', '11'),
('1109081', 'MUTIARA TIMUR', '1109', '11'),
('1109090', 'TIRO/TRUSEB', '1109', '11'),
('1109100', 'TANGSE', '1109', '11'),
('1109111', 'KEUMALA', '1109', '11'),
('1109112', 'TITEUE', '1109', '11'),
('1109120', 'SAKTI', '1109', '11'),
('1109130', 'MILA', '1109', '11'),
('1109140', 'PADANG TIJI', '1109', '11'),
('1109150', 'DELIMA', '1109', '11'),
('1109151', 'GRONG GRONG', '1109', '11'),
('1109160', 'INDRAJAYA', '1109', '11'),
('1109170', 'PEUKAN BARO', '1109', '11'),
('1109180', 'KEMBANG TANJONG', '1109', '11'),
('1109190', 'SIMPANG TIGA', '1109', '11'),
('1109200', 'KOTA SIGLI', '1109', '11'),
('1109210', 'PIDIE', '1109', '11'),
('1109220', 'BATEE', '1109', '11'),
('1109230', 'MUARA TIGA', '1109', '11'),
('1110010', 'SAMALANGA', '1110', '11'),
('1110011', 'SIMPANG MAMPLAM', '1110', '11'),
('1110020', 'PANDRAH', '1110', '11'),
('1110030', 'JEUNIEB', '1110', '11'),
('1110031', 'PEULIMBANG', '1110', '11'),
('1110040', 'PEUDADA', '1110', '11'),
('1110050', 'JULI', '1110', '11'),
('1110060', 'JEUMPA', '1110', '11'),
('1110061', 'KOTA JUANG', '1110', '11'),
('1110062', 'KUALA', '1110', '11'),
('1110070', 'JANGKA', '1110', '11'),
('1110080', 'PEUSANGAN', '1110', '11'),
('1110081', 'PEUSANGAN SELATAN', '1110', '11'),
('1110082', 'PEUSANGAN SIBLAH KRUENG', '1110', '11'),
('1110090', 'MAKMUR', '1110', '11'),
('1110100', 'GANDA PURA', '1110', '11'),
('1110101', 'KUTA BLANG', '1110', '11'),
('1111010', 'SAWANG', '1111', '11'),
('1111020', 'NISAM', '1111', '11'),
('1111021', 'NISAM ANTARA', '1111', '11'),
('1111022', 'BANDA BARO', '1111', '11'),
('1111030', 'KUTA MAKMUR', '1111', '11'),
('1111031', 'SIMPANG KERAMAT', '1111', '11'),
('1111040', 'SYAMTALIRA BAYU', '1111', '11'),
('1111041', 'GEUREUDONG PASE', '1111', '11'),
('1111050', 'MEURAH MULIA', '1111', '11'),
('1111060', 'MATANGKULI', '1111', '11'),
('1111061', 'PAYA BAKONG', '1111', '11'),
('1111062', 'PIRAK TIMU', '1111', '11'),
('1111070', 'COT GIREK', '1111', '11'),
('1111080', 'TANAH JAMBO AYE', '1111', '11'),
('1111081', 'LANGKAHAN', '1111', '11'),
('1111090', 'SEUNUDDON', '1111', '11'),
('1111100', 'BAKTIYA', '1111', '11'),
('1111101', 'BAKTIYA BARAT', '1111', '11'),
('1111110', 'LHOKSUKON', '1111', '11'),
('1111120', 'TANAH LUAS', '1111', '11'),
('1111121', 'NIBONG', '1111', '11'),
('1111130', 'SAMUDERA', '1111', '11'),
('1111140', 'SYAMTALIRA ARON', '1111', '11'),
('1111150', 'TANAH PASIR', '1111', '11'),
('1111151', 'LAPANG', '1111', '11'),
('1111160', 'MUARA BATU', '1111', '11'),
('1111170', 'DEWANTARA', '1111', '11'),
('1112010', 'MANGGENG', '1112', '11'),
('1112011', 'LEMBAH SABIL', '1112', '11'),
('1112020', 'TANGAN-TANGAN', '1112', '11'),
('1112021', 'SETIA', '1112', '11'),
('1112030', 'BLANG PIDIE', '1112', '11'),
('1112031', 'JEUMPA', '1112', '11'),
('1112040', 'SUSOH', '1112', '11'),
('1112050', 'KUALA BATEE', '1112', '11'),
('1112060', 'BABAH ROT', '1112', '11'),
('1113010', 'KUTA PANJANG', '1113', '11'),
('1113011', 'BLANG JERANGO', '1113', '11'),
('1113020', 'BLANGKEJEREN', '1113', '11'),
('1113021', 'PUTRI BETUNG', '1113', '11'),
('1113022', 'DABUN GELANG', '1113', '11'),
('1113023', 'BLANG PEGAYON', '1113', '11'),
('1113030', 'PINING', '1113', '11'),
('1113040', 'RIKIT GAIB', '1113', '11'),
('1113041', 'PANTAN CUACA', '1113', '11'),
('1113050', 'TERANGUN', '1113', '11'),
('1113051', 'TRIPE JAYA', '1113', '11'),
('1114010', 'TAMIANG HULU', '1114', '11'),
('1114011', 'BANDAR PUSAKA', '1114', '11'),
('1114020', 'KEJURUAN MUDA', '1114', '11'),
('1114021', 'TENGGULUN', '1114', '11'),
('1114030', 'RANTAU', '1114', '11'),
('1114040', 'KOTA KUALA SIMPANG', '1114', '11'),
('1114050', 'SERUWAY', '1114', '11'),
('1114060', 'BENDAHARA', '1114', '11'),
('1114061', 'BANDA MULIA', '1114', '11'),
('1114070', 'KARANG BARU', '1114', '11'),
('1114071', 'SEKERAK', '1114', '11'),
('1114080', 'MANYAK PAYED', '1114', '11'),
('1115010', 'DARUL MAKMUR', '1115', '11'),
('1115011', 'TRIPA MAKMUR', '1115', '11'),
('1115020', 'KUALA', '1115', '11'),
('1115021', 'KUALA PESISIR', '1115', '11'),
('1115022', 'TADU RAYA', '1115', '11'),
('1115030', 'BEUTONG', '1115', '11'),
('1115031', 'BEUTONG ATEUH BANGGALANG', '1115', '11'),
('1115040', 'SEUNAGAN', '1115', '11'),
('1115041', 'SUKA MAKMUE', '1115', '11'),
('1115050', 'SEUNAGAN TIMUR', '1115', '11'),
('1116010', 'TEUNOM', '1116', '11'),
('1116011', 'PASIE RAYA', '1116', '11'),
('1116020', 'PANGA', '1116', '11'),
('1116030', 'KRUENG SABEE', '1116', '11'),
('1116040', 'SETIA BAKTI', '1116', '11'),
('1116050', 'SAMPOINIET', '1116', '11'),
('1116051', 'DARUL HIKMAH', '1116', '11'),
('1116060', 'JAYA', '1116', '11'),
('1116061', 'INDRA JAYA', '1116', '11'),
('1117010', 'TIMANG GAJAH', '1117', '11'),
('1117011', 'GAJAH PUTIH', '1117', '11'),
('1117020', 'PINTU RIME GAYO', '1117', '11'),
('1117030', 'BUKIT', '1117', '11'),
('1117040', 'WIH PESAM', '1117', '11'),
('1117050', 'BANDAR', '1117', '11'),
('1117051', 'BENER KELIPAH', '1117', '11'),
('1117060', 'SYIAH UTAMA', '1117', '11'),
('1117061', 'MESIDAH', '1117', '11'),
('1117070', 'PERMATA', '1117', '11'),
('1118010', 'MEUREUDU', '1118', '11'),
('1118020', 'MEURAH DUA', '1118', '11'),
('1118030', 'BANDAR DUA', '1118', '11'),
('1118040', 'JANGKA BUYA', '1118', '11'),
('1118050', 'ULIM', '1118', '11'),
('1118060', 'TRIENGGADENG', '1118', '11'),
('1118070', 'PANTERAJA', '1118', '11'),
('1118080', 'BANDAR BARU', '1118', '11'),
('1171010', 'MEURAXA', '1171', '11'),
('1171011', 'JAYA BARU', '1171', '11'),
('1171012', 'BANDA RAYA', '1171', '11'),
('1171020', 'BAITURRAHMAN', '1171', '11'),
('1171021', 'LUENG BATA', '1171', '11'),
('1171030', 'KUTA ALAM', '1171', '11'),
('1171031', 'KUTA RAJA', '1171', '11'),
('1171040', 'SYIAH KUALA', '1171', '11'),
('1171041', 'ULEE KARENG', '1171', '11'),
('1172010', 'SUKAJAYA', '1172', '11'),
('1172020', 'SUKAKARYA', '1172', '11'),
('1173010', 'LANGSA TIMUR', '1173', '11'),
('1173011', 'LANGSA LAMA', '1173', '11'),
('1173020', 'LANGSA BARAT', '1173', '11'),
('1173021', 'LANGSA BARO', '1173', '11'),
('1173030', 'LANGSA KOTA', '1173', '11'),
('1174010', 'BLANG MANGAT', '1174', '11'),
('1174020', 'MUARA DUA', '1174', '11'),
('1174021', 'MUARA SATU', '1174', '11'),
('1174030', 'BANDA SAKTI', '1174', '11'),
('1175010', 'SIMPANG KIRI', '1175', '11'),
('1175020', 'PENANGGALAN', '1175', '11'),
('1175030', 'RUNDENG', '1175', '11'),
('1175040', 'SULTAN DAULAT', '1175', '11'),
('1175050', 'LONGKIB', '1175', '11'),
('1201060', 'IDANO GAWO', '1201', '12'),
('1201061', 'BAWOLATO', '1201', '12'),
('1201062', 'ULUGAWO', '1201', '12'),
('1201070', 'GIDO', '1201', '12'),
('1201071', 'SOGAEADU', '1201', '12'),
('1201081', 'MA U', '1201', '12'),
('1201082', 'SOMOLO - MOLO', '1201', '12'),
('1201130', 'HILIDUHO', '1201', '12'),
('1201131', 'HILI SERANGKAI', '1201', '12'),
('1201132', 'BOTOMUZOI', '1201', '12'),
('1202010', 'BATAHAN', '1202', '12'),
('1202011', 'SINUNUKAN', '1202', '12'),
('1202020', 'BATANG NATAL', '1202', '12'),
('1202021', 'LINGGA BAYU', '1202', '12'),
('1202022', 'RANTO BAEK', '1202', '12'),
('1202030', 'KOTANOPAN', '1202', '12'),
('1202031', 'ULU PUNGKUT', '1202', '12'),
('1202032', 'TAMBANGAN', '1202', '12'),
('1202033', 'LEMBAH SORIK MARAPI', '1202', '12'),
('1202034', 'PUNCAK SORIK MARAPI', '1202', '12'),
('1202040', 'MUARA SIPONGI', '1202', '12'),
('1202041', 'PAKANTAN', '1202', '12'),
('1202050', 'PANYABUNGAN', '1202', '12'),
('1202051', 'PANYABUNGAN SELATAN', '1202', '12'),
('1202052', 'PANYABUNGAN BARAT', '1202', '12'),
('1202053', 'PANYABUNGAN UTARA', '1202', '12'),
('1202054', 'PANYABUNGAN TIMUR', '1202', '12'),
('1202055', 'HUTA BARGOT', '1202', '12'),
('1202060', 'NATAL', '1202', '12'),
('1202070', 'MUARA BATANG GADIS', '1202', '12'),
('1202080', 'SIABU', '1202', '12'),
('1202081', 'BUKIT MALINTANG', '1202', '12'),
('1202082', 'NAGA JUANG', '1202', '12'),
('1203010', 'BATANG ANGKOLA', '1203', '12'),
('1203011', 'SAYUR MATINGGI', '1203', '12'),
('1203012', 'TANO TOMBANGAN ANGKOLA', '1203', '12'),
('1203070', 'ANGKOLA TIMUR', '1203', '12'),
('1203080', 'ANGKOLA SELATAN', '1203', '12'),
('1203090', 'ANGKOLA  BARAT', '1203', '12'),
('1203091', 'ANGKOLA SANGKUNUR', '1203', '12'),
('1203100', 'BATANG TORU', '1203', '12'),
('1203101', 'MARANCAR', '1203', '12'),
('1203102', 'MUARA BATANG TORU', '1203', '12'),
('1203110', 'SIPIROK', '1203', '12'),
('1203120', 'ARSE', '1203', '12'),
('1203160', 'SAIPAR DOLOK HOLE', '1203', '12'),
('1203161', 'AEK BILAH', '1203', '12'),
('1204010', 'PINANG SORI', '1204', '12'),
('1204011', 'BADIRI', '1204', '12'),
('1204020', 'SIBABANGUN', '1204', '12'),
('1204021', 'LUMUT', '1204', '12'),
('1204022', 'SUKABANGUN', '1204', '12'),
('1204030', 'PANDAN', '1204', '12'),
('1204031', 'TUKKA', '1204', '12'),
('1204032', 'SARUDIK', '1204', '12'),
('1204040', 'TAPIAN NAULI', '1204', '12'),
('1204041', 'SITAHUIS', '1204', '12'),
('1204050', 'KOLANG', '1204', '12'),
('1204060', 'SORKAM', '1204', '12'),
('1204061', 'SORKAM BARAT', '1204', '12'),
('1204062', 'PASARIBU TOBING', '1204', '12'),
('1204070', 'BARUS', '1204', '12'),
('1204071', 'SOSOR GADONG', '1204', '12'),
('1204072', 'ANDAM DEWI', '1204', '12'),
('1204073', 'BARUS UTARA', '1204', '12'),
('1204080', 'MANDUAMAS', '1204', '12'),
('1204081', 'SIRANDORUNG', '1204', '12'),
('1205030', 'PARMONANGAN', '1205', '12'),
('1205040', 'ADIANKOTING', '1205', '12'),
('1205050', 'SIPOHOLON', '1205', '12'),
('1205060', 'TARUTUNG', '1205', '12'),
('1205061', 'SIATAS BARITA', '1205', '12'),
('1205070', 'PAHAE JULU', '1205', '12'),
('1205080', 'PAHAE JAE', '1205', '12'),
('1205081', 'PURBATUA', '1205', '12'),
('1205082', 'SIMANGUMBAN', '1205', '12'),
('1205090', 'PANGARIBUAN', '1205', '12'),
('1205100', 'GAROGA', '1205', '12'),
('1205110', 'SIPAHUTAR', '1205', '12'),
('1205120', 'SIBORONGBORONG', '1205', '12'),
('1205130', 'PAGARAN', '1205', '12'),
('1205180', 'MUARA', '1205', '12'),
('1206030', 'BALIGE', '1206', '12'),
('1206031', 'TAMPAHAN', '1206', '12'),
('1206040', 'LAGUBOTI', '1206', '12'),
('1206050', 'HABINSARAN', '1206', '12'),
('1206051', 'BORBOR', '1206', '12'),
('1206052', 'NASSAU', '1206', '12'),
('1206060', 'SILAEN', '1206', '12'),
('1206061', 'SIGUMPAR', '1206', '12'),
('1206070', 'PORSEA', '1206', '12'),
('1206071', 'PINTU POHAN MERANTI', '1206', '12'),
('1206072', 'SIANTAR NARUMONDA', '1206', '12'),
('1206073', 'PARMAKSIAN', '1206', '12'),
('1206080', 'LUMBAN JULU', '1206', '12'),
('1206081', 'ULUAN', '1206', '12'),
('1206082', 'AJIBATA', '1206', '12'),
('1206083', 'BONATUA LUNASI', '1206', '12'),
('1207050', 'BILAH HULU', '1207', '12'),
('1207070', 'PANGKATAN', '1207', '12'),
('1207080', 'BILAH BARAT', '1207', '12'),
('1207130', 'BILAH HILIR', '1207', '12'),
('1207140', 'PANAI HULU', '1207', '12'),
('1207150', 'PANAI TENGAH', '1207', '12'),
('1207160', 'PANAI HILIR', '1207', '12'),
('1207210', 'RANTAU SELATAN', '1207', '12'),
('1207220', 'RANTAU UTARA', '1207', '12'),
('1208010', 'BANDAR PASIR MANDOGE', '1208', '12'),
('1208020', 'BANDAR PULAU', '1208', '12'),
('1208021', 'AEK SONGSONGAN', '1208', '12'),
('1208022', 'RAHUNING', '1208', '12'),
('1208030', 'PULAU RAKYAT', '1208', '12'),
('1208031', 'AEK KUASAN', '1208', '12'),
('1208032', 'AEK LEDONG', '1208', '12'),
('1208040', 'SEI KEPAYANG', '1208', '12'),
('1208041', 'SEI KEPAYANG BARAT', '1208', '12'),
('1208042', 'SEI KEPAYANG TIMUR', '1208', '12'),
('1208050', 'TANJUNG BALAI', '1208', '12'),
('1208060', 'SIMPANG EMPAT', '1208', '12'),
('1208061', 'TELUK DALAM', '1208', '12'),
('1208070', 'AIR BATU', '1208', '12'),
('1208071', 'SEI DADAP', '1208', '12'),
('1208080', 'BUNTU PANE', '1208', '12'),
('1208081', 'TINGGI RAJA', '1208', '12'),
('1208082', 'SETIA JANJI', '1208', '12'),
('1208090', 'MERANTI', '1208', '12'),
('1208091', 'PULO BANDRING', '1208', '12'),
('1208092', 'RAWANG PANCA ARGA', '1208', '12'),
('1208100', 'AIR JOMAN', '1208', '12'),
('1208101', 'SILAU LAUT', '1208', '12'),
('1208160', 'KISARAN BARAT', '1208', '12'),
('1208170', 'KISARAN TIMUR', '1208', '12'),
('1209010', 'SILIMAKUTA', '1209', '12'),
('1209011', 'PEMATANG SILIMAHUTA', '1209', '12'),
('1209020', 'PURBA', '1209', '12'),
('1209021', 'HARANGGAOL HORISON', '1209', '12'),
('1209030', 'DOLOK PARDAMEAN', '1209', '12'),
('1209040', 'SIDAMANIK', '1209', '12'),
('1209041', 'PEMATANG SIDAMANIK', '1209', '12'),
('1209050', 'GIRSANG SIPANGAN BOLON', '1209', '12'),
('1209060', 'TANAH JAWA', '1209', '12'),
('1209061', 'HATONDUHAN', '1209', '12'),
('1209070', 'DOLOK PANRIBUAN', '1209', '12'),
('1209080', 'JORLANG HATARAN', '1209', '12'),
('1209090', 'PANEI', '1209', '12'),
('1209091', 'PANOMBEAN PANEI', '1209', '12'),
('1209100', 'RAYA', '1209', '12'),
('1209110', 'DOLOK SILAU', '1209', '12'),
('1209120', 'SILAU KAHEAN', '1209', '12'),
('1209130', 'RAYA KAHEAN', '1209', '12'),
('1209140', 'TAPIAN DOLOK', '1209', '12'),
('1209150', 'DOLOK BATU NANGGAR', '1209', '12'),
('1209160', 'SIANTAR', '1209', '12'),
('1209161', 'GUNUNG MALELA', '1209', '12'),
('1209162', 'GUNUNG MALIGAS', '1209', '12'),
('1209170', 'HUTABAYU RAJA', '1209', '12'),
('1209171', 'JAWA MARAJA BAH JAMBI', '1209', '12'),
('1209180', 'PEMATANG BANDAR', '1209', '12'),
('1209181', 'BANDAR HULUAN', '1209', '12'),
('1209190', 'BANDAR', '1209', '12'),
('1209191', 'BANDAR MASILAM', '1209', '12'),
('1209200', 'BOSAR MALIGAS', '1209', '12'),
('1209210', 'UJUNG PADANG', '1209', '12'),
('1210030', 'SIDIKALANG', '1210', '12'),
('1210031', 'BERAMPU', '1210', '12'),
('1210032', 'SITINJO', '1210', '12'),
('1210040', 'PARBULUAN', '1210', '12'),
('1210050', 'SUMBUL', '1210', '12'),
('1210051', 'SILAHI SABUNGAN', '1210', '12'),
('1210060', 'SILIMA PUNGGA-PUNGGA', '1210', '12'),
('1210061', 'LAE PARIRA', '1210', '12'),
('1210070', 'SIEMPAT NEMPU', '1210', '12'),
('1210080', 'SIEMPAT NEMPU HULU', '1210', '12'),
('1210090', 'SIEMPAT NEMPU HILIR', '1210', '12'),
('1210100', 'TIGA LINGGA', '1210', '12'),
('1210101', 'GUNUNG SITEMBER', '1210', '12'),
('1210110', 'PEGAGAN HILIR', '1210', '12'),
('1210120', 'TANAH PINEM', '1210', '12'),
('1211010', 'MARDINGDING', '1211', '12'),
('1211020', 'LAUBALENG', '1211', '12'),
('1211030', 'TIGA BINANGA', '1211', '12'),
('1211040', 'JUHAR', '1211', '12'),
('1211050', 'MUNTE', '1211', '12'),
('1211060', 'KUTA BULUH', '1211', '12'),
('1211070', 'PAYUNG', '1211', '12'),
('1211071', 'TIGANDERKET', '1211', '12'),
('1211080', 'SIMPANG EMPAT', '1211', '12'),
('1211081', 'NAMAN TERAN', '1211', '12'),
('1211082', 'MERDEKA', '1211', '12'),
('1211090', 'KABANJAHE', '1211', '12'),
('1211100', 'BERASTAGI', '1211', '12'),
('1211110', 'TIGAPANAH', '1211', '12'),
('1211111', 'DOLAT RAYAT', '1211', '12'),
('1211120', 'MEREK', '1211', '12'),
('1211130', 'BARUSJAHE', '1211', '12'),
('1212010', 'GUNUNG MERIAH', '1212', '12'),
('1212020', 'SINEMBAH TANJUNG MUDA HULU', '1212', '12'),
('1212030', 'SIBOLANGIT', '1212', '12'),
('1212040', 'KUTALIMBARU', '1212', '12'),
('1212050', 'PANCUR BATU', '1212', '12'),
('1212060', 'NAMO RAMBE', '1212', '12'),
('1212070', 'BIRU-BIRU', '1212', '12'),
('1212080', 'SINEMBAH TANJUNG MUDA HILIR', '1212', '12'),
('1212090', 'BANGUN PURBA', '1212', '12'),
('1212190', 'GALANG', '1212', '12'),
('1212200', 'TANJUNG MORAWA', '1212', '12'),
('1212210', 'PATUMBAK', '1212', '12'),
('1212220', 'DELI TUA', '1212', '12'),
('1212230', 'SUNGGAL', '1212', '12'),
('1212240', 'HAMPARAN PERAK', '1212', '12'),
('1212250', 'LABUHAN DELI', '1212', '12'),
('1212260', 'PERCUT SEI TUAN', '1212', '12'),
('1212270', 'BATANG KUIS', '1212', '12'),
('1212280', 'PANTAI LABU', '1212', '12'),
('1212290', 'BERINGIN', '1212', '12'),
('1212300', 'LUBUK PAKAM', '1212', '12'),
('1212310', 'PAGAR MERBAU', '1212', '12'),
('1213010', 'BOHOROK', '1213', '12'),
('1213011', 'SIRAPIT', '1213', '12'),
('1213020', 'SALAPIAN', '1213', '12'),
('1213021', 'KUTAMBARU', '1213', '12'),
('1213030', 'SEI BINGAI', '1213', '12'),
('1213040', 'KUALA', '1213', '12'),
('1213050', 'SELESAI', '1213', '12'),
('1213060', 'BINJAI', '1213', '12'),
('1213070', 'STABAT', '1213', '12'),
('1213080', 'WAMPU', '1213', '12'),
('1213090', 'BATANG SERANGAN', '1213', '12'),
('1213100', 'SAWIT SEBERANG', '1213', '12'),
('1213110', 'PADANG TUALANG', '1213', '12'),
('1213120', 'HINAI', '1213', '12'),
('1213130', 'SECANGGANG', '1213', '12'),
('1213140', 'TANJUNG PURA', '1213', '12'),
('1213150', 'GEBANG', '1213', '12'),
('1213160', 'BABALAN', '1213', '12'),
('1213170', 'SEI LEPAN', '1213', '12'),
('1213180', 'BRANDAN BARAT', '1213', '12'),
('1213190', 'BESITANG', '1213', '12'),
('1213200', 'PANGKALAN SUSU', '1213', '12'),
('1213201', 'PEMATANG JAYA', '1213', '12'),
('1214010', 'HIBALA', '1214', '12'),
('1214011', 'TANAH MASA', '1214', '12'),
('1214020', 'PULAU-PULAU BATU', '1214', '12'),
('1214021', 'PULAU-PULAU BATU TIMUR', '1214', '12'),
('1214022', 'SIMUK', '1214', '12'),
('1214023', 'PULAU-PULAU BATU BARAT', '1214', '12'),
('1214024', 'PULAU-PULAU BATU UTARA', '1214', '12'),
('1214030', 'TELUK DALAM', '1214', '12'),
('1214031', 'FANAYAMA', '1214', '12'),
('1214032', 'TOMA', '1214', '12'),
('1214033', 'MANIAMOLO', '1214', '12'),
('1214034', 'MAZINO', '1214', '12'),
('1214035', 'LUAHAGUNDRE MANIAMOLO', '1214', '12'),
('1214036', 'ONOLALU', '1214', '12'),
('1214040', 'AMANDRAYA', '1214', '12'),
('1214041', 'ARAMO', '1214', '12'),
('1214042', 'ULUSUSUA', '1214', '12'),
('1214050', 'LAHUSA', '1214', '12'),
('1214051', 'SIDUAORI', '1214', '12'),
('1214052', 'SOMAMBAWA', '1214', '12'),
('1214060', 'GOMO', '1214', '12'),
('1214061', 'SUSUA', '1214', '12'),
('1214062', 'MAZO', '1214', '12'),
('1214063', 'UMBUNASI', '1214', '12'),
('1214064', 'IDANOTAE', '1214', '12'),
('1214065', 'ULUIDANOTAE', '1214', '12'),
('1214066', 'BORONADU', '1214', '12'),
('1214070', 'LOLOMATUA', '1214', '12'),
('1214071', 'ULUNOYO', '1214', '12'),
('1214072', 'HURUNA', '1214', '12'),
('1214080', 'LOLOWA\'U', '1214', '12'),
('1214081', 'HILIMEGAI', '1214', '12'),
('1214082', 'OOU', '1214', '12'),
('1214083', 'ONOHAZUMBA', '1214', '12'),
('1214084', 'HILISALAWAAHE', '1214', '12'),
('1215010', 'PAKKAT', '1215', '12'),
('1215020', 'ONAN GANJANG', '1215', '12'),
('1215030', 'SIJAMA POLANG', '1215', '12'),
('1215040', 'DOLOK SANGGUL', '1215', '12'),
('1215050', 'LINTONG NIHUTA', '1215', '12'),
('1215060', 'PARANGINAN', '1215', '12'),
('1215070', 'BAKTI RAJA', '1215', '12'),
('1215080', 'POLLUNG', '1215', '12'),
('1215090', 'PARLILITAN', '1215', '12'),
('1215100', 'TARA BINTANG', '1215', '12'),
('1216010', 'SALAK', '1216', '12'),
('1216011', 'SITELLU TALI URANG JEHE', '1216', '12'),
('1216012', 'PAGINDAR', '1216', '12'),
('1216013', 'SITELLU TALI URANG JULU', '1216', '12'),
('1216014', 'PERGETTENG-GETTENG SENGKUT', '1216', '12'),
('1216020', 'KERAJAAN', '1216', '12'),
('1216021', 'TINADA', '1216', '12'),
('1216022', 'SIEMPAT RUBE', '1216', '12'),
('1217010', 'SIANJUR MULA MULA', '1217', '12'),
('1217020', 'HARIAN', '1217', '12'),
('1217030', 'SITIO-TIO', '1217', '12'),
('1217040', 'ONAN RUNGGU', '1217', '12'),
('1217050', 'NAINGGOLAN', '1217', '12'),
('1217060', 'PALIPI', '1217', '12'),
('1217070', 'RONGGUR NIHUTA', '1217', '12'),
('1217080', 'PANGURURAN', '1217', '12'),
('1217090', 'SIMANINDO', '1217', '12'),
('1218010', 'KOTARIH', '1218', '12'),
('1218011', 'SILINDA', '1218', '12'),
('1218012', 'BINTANG BAYU', '1218', '12'),
('1218020', 'DOLOK MASIHUL', '1218', '12'),
('1218021', 'SERBAJADI', '1218', '12'),
('1218030', 'SIPISPIS', '1218', '12'),
('1218040', 'DOLOK MERAWAN', '1218', '12'),
('1218050', 'TEBINGTINGGI', '1218', '12'),
('1218051', 'TEBING SYAHBANDAR', '1218', '12'),
('1218060', 'BANDAR KHALIPAH', '1218', '12'),
('1218070', 'TANJUNG BERINGIN', '1218', '12'),
('1218080', 'SEI RAMPAH', '1218', '12'),
('1218081', 'SEI BAMBAN', '1218', '12'),
('1218090', 'TELUK MENGKUDU', '1218', '12'),
('1218100', 'PERBAUNGAN', '1218', '12'),
('1218101', 'PEGAJAHAN', '1218', '12'),
('1218110', 'PANTAI CERMIN', '1218', '12'),
('1219010', 'SEI BALAI', '1219', '12'),
('1219020', 'TANJUNG TIRAM', '1219', '12'),
('1219030', 'TALAWI', '1219', '12'),
('1219040', 'LIMAPULUH', '1219', '12'),
('1219050', 'AIR PUTIH', '1219', '12'),
('1219060', 'SEI SUKA', '1219', '12'),
('1219070', 'MEDANG DERAS', '1219', '12'),
('1220010', 'BATANG ONANG', '1220', '12'),
('1220020', 'PADANG BOLAK JULU', '1220', '12'),
('1220030', 'PORTIBI', '1220', '12'),
('1220040', 'PADANG BOLAK', '1220', '12'),
('1220050', 'SIMANGAMBAT', '1220', '12'),
('1220060', 'HALONGONAN', '1220', '12'),
('1220070', 'DOLOK', '1220', '12'),
('1220080', 'DOLOK SIGOMPULON', '1220', '12'),
('1220090', 'HULU SIHAPAS', '1220', '12'),
('1221010', 'SOSOPAN', '1221', '12'),
('1221020', 'ULU BARUMUN', '1221', '12'),
('1221030', 'BARUMUN', '1221', '12'),
('1221031', 'BARUMUN SELATAN', '1221', '12'),
('1221040', 'LUBUK BARUMUN', '1221', '12'),
('1221050', 'SOSA', '1221', '12'),
('1221060', 'BATANG LUBU SUTAM', '1221', '12'),
('1221070', 'HUTA RAJA TINGGI', '1221', '12'),
('1221080', 'HURISTAK', '1221', '12'),
('1221090', 'BARUMUN TENGAH', '1221', '12'),
('1221091', 'AEK NABARA BARUMUN', '1221', '12'),
('1221092', 'SIHAPAS BARUMUN', '1221', '12'),
('1222010', 'SUNGAI KANAN', '1222', '12'),
('1222020', 'TORGAMBA', '1222', '12'),
('1222030', 'KOTA PINANG', '1222', '12'),
('1222040', 'SILANGKITANG', '1222', '12'),
('1222050', 'KAMPUNG RAKYAT', '1222', '12'),
('1223010', 'NA IX-X', '1223', '12'),
('1223020', 'MARBAU', '1223', '12'),
('1223030', 'AEK KUO', '1223', '12'),
('1223040', 'AEK NATAS', '1223', '12'),
('1223050', 'KUALUH SELATAN', '1223', '12'),
('1223060', 'KUALUH HILIR', '1223', '12'),
('1223070', 'KUALUH HULU', '1223', '12'),
('1223080', 'KUALUH LEIDONG', '1223', '12'),
('1224010', 'TUGALA OYO', '1224', '12'),
('1224020', 'ALASA', '1224', '12'),
('1224030', 'ALASA TALU MUZOI', '1224', '12'),
('1224040', 'NAMOHALU ESIWA', '1224', '12'),
('1224050', 'SITOLU ORI', '1224', '12'),
('1224060', 'TUHEMBERUA', '1224', '12'),
('1224070', 'SAWO', '1224', '12'),
('1224080', 'LOTU', '1224', '12'),
('1224090', 'LAHEWA TIMUR', '1224', '12'),
('1224100', 'AFULU', '1224', '12'),
('1224110', 'LAHEWA', '1224', '12'),
('1225010', 'SIROMBU', '1225', '12'),
('1225020', 'LAHOMI', '1225', '12'),
('1225030', 'ULU MORO O', '1225', '12'),
('1225040', 'LOLOFITU MOI', '1225', '12'),
('1225050', 'MANDREHE UTARA', '1225', '12'),
('1225060', 'MANDREHE', '1225', '12'),
('1225070', 'MANDREHE BARAT', '1225', '12'),
('1225080', 'MORO O', '1225', '12'),
('1271010', 'SIBOLGA UTARA', '1271', '12'),
('1271020', 'SIBOLGA KOTA', '1271', '12'),
('1271030', 'SIBOLGA SELATAN', '1271', '12'),
('1271031', 'SIBOLGA SAMBAS', '1271', '12'),
('1272010', 'DATUK BANDAR', '1272', '12'),
('1272011', 'DATUK BANDAR TIMUR', '1272', '12'),
('1272020', 'TANJUNG BALAI SELATAN', '1272', '12'),
('1272030', 'TANJUNG BALAI UTARA', '1272', '12'),
('1272040', 'SEI TUALANG RASO', '1272', '12'),
('1272050', 'TELUK NIBUNG', '1272', '12'),
('1273010', 'SIANTAR MARIHAT', '1273', '12'),
('1273011', 'SIANTAR MARIMBUN', '1273', '12'),
('1273020', 'SIANTAR SELATAN', '1273', '12'),
('1273030', 'SIANTAR BARAT', '1273', '12'),
('1273040', 'SIANTAR UTARA', '1273', '12'),
('1273050', 'SIANTAR TIMUR', '1273', '12'),
('1273060', 'SIANTAR MARTOBA', '1273', '12'),
('1273061', 'SIANTAR SITALASARI', '1273', '12'),
('1274010', 'PADANG HULU', '1274', '12'),
('1274011', 'TEBING TINGGI KOTA', '1274', '12'),
('1274020', 'RAMBUTAN', '1274', '12'),
('1274021', 'BAJENIS', '1274', '12'),
('1274030', 'PADANG HILIR', '1274', '12'),
('1275010', 'MEDAN TUNTUNGAN', '1275', '12'),
('1275020', 'MEDAN JOHOR', '1275', '12'),
('1275030', 'MEDAN AMPLAS', '1275', '12'),
('1275040', 'MEDAN DENAI', '1275', '12'),
('1275050', 'MEDAN AREA', '1275', '12'),
('1275060', 'MEDAN KOTA', '1275', '12'),
('1275070', 'MEDAN MAIMUN', '1275', '12'),
('1275080', 'MEDAN POLONIA', '1275', '12'),
('1275090', 'MEDAN BARU', '1275', '12'),
('1275100', 'MEDAN SELAYANG', '1275', '12'),
('1275110', 'MEDAN SUNGGAL', '1275', '12'),
('1275120', 'MEDAN HELVETIA', '1275', '12'),
('1275130', 'MEDAN PETISAH', '1275', '12'),
('1275140', 'MEDAN BARAT', '1275', '12'),
('1275150', 'MEDAN TIMUR', '1275', '12'),
('1275160', 'MEDAN PERJUANGAN', '1275', '12'),
('1275170', 'MEDAN TEMBUNG', '1275', '12'),
('1275180', 'MEDAN DELI', '1275', '12'),
('1275190', 'MEDAN LABUHAN', '1275', '12'),
('1275200', 'MEDAN MARELAN', '1275', '12'),
('1275210', 'MEDAN BELAWAN', '1275', '12'),
('1276010', 'BINJAI SELATAN', '1276', '12'),
('1276020', 'BINJAI KOTA', '1276', '12'),
('1276030', 'BINJAI TIMUR', '1276', '12'),
('1276040', 'BINJAI UTARA', '1276', '12'),
('1276050', 'BINJAI BARAT', '1276', '12'),
('1277010', 'PADANGSIDIMPUAN TENGGARA', '1277', '12'),
('1277020', 'PADANGSIDIMPUAN SELATAN', '1277', '12'),
('1277030', 'PADANGSIDIMPUAN BATUNADUA', '1277', '12'),
('1277040', 'PADANGSIDIMPUAN UTARA', '1277', '12'),
('1277050', 'PADANGSIDIMPUAN HUTAIMBARU', '1277', '12'),
('1277051', 'PADANGSIDIMPUAN ANGKOLA JULU', '1277', '12'),
('1278010', 'GUNUNGSITOLI IDANOI', '1278', '12'),
('1278020', 'GUNUNGSITOLI SELATAN', '1278', '12'),
('1278030', 'GUNUNGSITOLI BARAT', '1278', '12'),
('1278040', 'GUNUNG SITOLI', '1278', '12'),
('1278050', 'GUNUNGSITOLI ALO OA', '1278', '12'),
('1278060', 'GUNUNGSITOLI UTARA', '1278', '12'),
('1301011', 'PAGAI SELATAN', '1301', '13'),
('1301012', 'SIKAKAP', '1301', '13'),
('1301013', 'PAGAI UTARA', '1301', '13'),
('1301021', 'SIPORA SELATAN', '1301', '13'),
('1301022', 'SIPORA UTARA', '1301', '13'),
('1301030', 'SIBERUT SELATAN', '1301', '13'),
('1301031', 'SEBERUT BARAT DAYA', '1301', '13'),
('1301032', 'SIBERUT TENGAH', '1301', '13'),
('1301040', 'SIBERUT UTARA', '1301', '13'),
('1301041', 'SIBERUT BARAT', '1301', '13'),
('1302011', 'SILAUT', '1302', '13'),
('1302012', 'LUNANG', '1302', '13'),
('1302020', 'BASA AMPEK BALAI TAPAN', '1302', '13'),
('1302021', 'RANAH AMPEK HULU TAPAN', '1302', '13'),
('1302030', 'PANCUNG SOAL', '1302', '13'),
('1302031', 'AIRPURA', '1302', '13'),
('1302040', 'LINGGO SARI BAGANTI', '1302', '13'),
('1302050', 'RANAH PESISIR', '1302', '13'),
('1302060', 'LENGAYANG', '1302', '13'),
('1302070', 'SUTERA', '1302', '13'),
('1302080', 'BATANG KAPAS', '1302', '13'),
('1302090', 'IV JURAI', '1302', '13'),
('1302100', 'BAYANG', '1302', '13'),
('1302101', 'IV  NAGARI BAYANG UTARA', '1302', '13'),
('1302110', 'KOTO XI TARUSAN', '1302', '13'),
('1303040', 'PANTAI CERMIN', '1303', '13'),
('1303050', 'LEMBAH GUMANTI', '1303', '13'),
('1303051', 'HILIRAN GUMANTI', '1303', '13'),
('1303060', 'PAYUNG SEKAKI', '1303', '13'),
('1303061', 'TIGO LURAH', '1303', '13'),
('1303070', 'LEMBANG JAYA', '1303', '13'),
('1303071', 'DANAU KEMBAR', '1303', '13'),
('1303080', 'GUNUNG TALANG', '1303', '13'),
('1303090', 'BUKIT SUNDI', '1303', '13'),
('1303100', 'IX KOTO SUNGAI LASI', '1303', '13'),
('1303110', 'KUBUNG', '1303', '13'),
('1303120', 'X KOTO DIATAS', '1303', '13'),
('1303130', 'X KOTO SINGKARAK', '1303', '13'),
('1303140', 'JUNJUNG SIRIH', '1303', '13'),
('1304050', 'KAMANG BARU', '1304', '13'),
('1304060', 'TANJUNG GADANG', '1304', '13'),
('1304070', 'SIJUNJUNG', '1304', '13'),
('1304071', 'LUBUK TAROK', '1304', '13'),
('1304080', 'IV NAGARI', '1304', '13'),
('1304090', 'KUPITAN', '1304', '13'),
('1304100', 'KOTO TUJUH', '1304', '13'),
('1304110', 'SUMPUR KUDUS', '1304', '13'),
('1305010', 'SEPULUH KOTO', '1305', '13'),
('1305020', 'BATIPUH', '1305', '13'),
('1305021', 'BATIPUH SELATAN', '1305', '13'),
('1305030', 'PARIANGAN', '1305', '13'),
('1305040', 'RAMBATAN', '1305', '13'),
('1305050', 'LIMA KAUM', '1305', '13'),
('1305060', 'TANJUNG EMAS', '1305', '13'),
('1305070', 'PADANG GANTING', '1305', '13'),
('1305080', 'LINTAU BUO', '1305', '13'),
('1305081', 'LINTAU BUO UTARA', '1305', '13'),
('1305090', 'SUNGAYANG', '1305', '13'),
('1305100', 'SUNGAI TARAB', '1305', '13'),
('1305110', 'SALIMPAUNG', '1305', '13'),
('1305111', 'TANJUNG BARU', '1305', '13'),
('1306010', 'BATANG ANAI', '1306', '13'),
('1306020', 'LUBUK ALUNG', '1306', '13'),
('1306021', 'SINTUK TOBOH GADANG', '1306', '13'),
('1306030', 'ULAKAN TAPAKIS', '1306', '13'),
('1306040', 'NAN SABARIS', '1306', '13'),
('1306050', '2 X 11 ENAM LINGKUNG', '1306', '13'),
('1306051', 'ENAM LINGKUNG', '1306', '13'),
('1306052', '2 X 11 KAYU TANAM', '1306', '13'),
('1306060', 'VII KOTO SUNGAI SARIAK', '1306', '13'),
('1306061', 'PATAMUAN', '1306', '13'),
('1306062', 'PADANG SAGO', '1306', '13'),
('1306070', 'V KOTO KP DALAM', '1306', '13'),
('1306071', 'V KOTO TIMUR', '1306', '13'),
('1306080', 'SUNGAI LIMAU', '1306', '13'),
('1306081', 'BATANG GASAN', '1306', '13'),
('1306090', 'SUNGAI GERINGGING', '1306', '13'),
('1306100', 'IV KOTO AUR MALINTANG', '1306', '13'),
('1307010', 'TANJUNG MUTIARA', '1307', '13'),
('1307020', 'LUBUK BASUNG', '1307', '13'),
('1307021', 'AMPEK NAGARI', '1307', '13'),
('1307030', 'TANJUNG RAYA', '1307', '13'),
('1307040', 'MATUR', '1307', '13'),
('1307050', 'IV KOTO', '1307', '13'),
('1307051', 'MALALAK', '1307', '13'),
('1307061', 'BANUHAMPU', '1307', '13'),
('1307062', 'SUNGAI PUA', '1307', '13'),
('1307070', 'AMPEK ANGKEK', '1307', '13'),
('1307071', 'CANDUANG', '1307', '13'),
('1307080', 'BASO', '1307', '13'),
('1307090', 'TILATANG KAMANG', '1307', '13'),
('1307091', 'KAMANG MAGEK', '1307', '13'),
('1307100', 'PALEMBAYAN', '1307', '13'),
('1307110', 'PALUPUH', '1307', '13'),
('1308010', 'PAYAKUMBUH', '1308', '13'),
('1308011', 'AKABILURU', '1308', '13'),
('1308020', 'LUAK', '1308', '13'),
('1308021', 'LAREH SAGO HALABAN', '1308', '13'),
('1308022', 'SITUJUAH LIMO NAGARI', '1308', '13'),
('1308030', 'HARAU', '1308', '13'),
('1308040', 'GUGUAK', '1308', '13'),
('1308041', 'MUNGKA', '1308', '13'),
('1308050', 'SULIKI', '1308', '13'),
('1308051', 'BUKIK BARISAN', '1308', '13'),
('1308060', 'GUNUANG OMEH', '1308', '13'),
('1308070', 'KAPUR IX', '1308', '13'),
('1308080', 'PANGKALAN KOTO BARU', '1308', '13'),
('1309070', 'BONJOL', '1309', '13'),
('1309071', 'TIGO NAGARI', '1309', '13'),
('1309072', 'SIMPANG ALAHAN MATI', '1309', '13'),
('1309080', 'LUBUK SIKAPING', '1309', '13'),
('1309100', 'DUA KOTO', '1309', '13'),
('1309110', 'PANTI', '1309', '13'),
('1309111', 'PADANG GELUGUR', '1309', '13'),
('1309121', 'RAO', '1309', '13'),
('1309122', 'MAPAT TUNGGUL', '1309', '13'),
('1309123', 'MAPAT TUNGGUL SELATAN', '1309', '13'),
('1309124', 'RAO SELATAN', '1309', '13'),
('1309125', 'RAO UTARA', '1309', '13'),
('1310010', 'SANGIR', '1310', '13'),
('1310020', 'SANGIR JUJUAN', '1310', '13'),
('1310021', 'SANGIR BALAI JANGGO', '1310', '13'),
('1310030', 'SANGIR BATANG HARI', '1310', '13'),
('1310040', 'SUNGAI PAGU', '1310', '13'),
('1310041', 'PAUAH DUO', '1310', '13'),
('1310050', 'KOTO PARIK GADANG DIATEH', '1310', '13'),
('1311010', 'SUNGAI RUMBAI', '1311', '13'),
('1311011', 'KOTO BESAR', '1311', '13'),
('1311012', 'ASAM JUJUHAN', '1311', '13'),
('1311020', 'KOTO BARU', '1311', '13'),
('1311021', 'KOTO SALAK', '1311', '13'),
('1311022', 'TIUMANG', '1311', '13'),
('1311023', 'PADANG LAWEH', '1311', '13'),
('1311030', 'SITIUNG', '1311', '13'),
('1311031', 'TIMPEH', '1311', '13'),
('1311040', 'PULAU PUNJUNG', '1311', '13'),
('1311041', 'IX KOTO', '1311', '13'),
('1312010', 'SUNGAI BEREMAS', '1312', '13'),
('1312020', 'RANAH BATAHAN', '1312', '13'),
('1312030', 'KOTO BALINGKA', '1312', '13'),
('1312040', 'SUNGAI AUR', '1312', '13'),
('1312050', 'LEMBAH MALINTANG', '1312', '13'),
('1312060', 'GUNUNG TULEH', '1312', '13'),
('1312070', 'TALAMAU', '1312', '13'),
('1312080', 'PASAMAN', '1312', '13'),
('1312090', 'LUHAK NAN DUO', '1312', '13'),
('1312100', 'SASAK RANAH PASISIE', '1312', '13'),
('1312110', 'KINALI', '1312', '13'),
('1371010', 'BUNGUS TELUK KABUNG', '1371', '13'),
('1371020', 'LUBUK KILANGAN', '1371', '13'),
('1371030', 'LUBUK BEGALUNG', '1371', '13'),
('1371040', 'PADANG SELATAN', '1371', '13'),
('1371050', 'PADANG TIMUR', '1371', '13'),
('1371060', 'PADANG BARAT', '1371', '13'),
('1371070', 'PADANG UTARA', '1371', '13'),
('1371080', 'NANGGALO', '1371', '13'),
('1371090', 'KURANJI', '1371', '13'),
('1371100', 'PAUH', '1371', '13'),
('1371110', 'KOTO TANGAH', '1371', '13'),
('1372010', 'LUBUK SIKARAH', '1372', '13'),
('1372020', 'TANJUNG HARAPAN', '1372', '13'),
('1373010', 'SILUNGKANG', '1373', '13'),
('1373020', 'LEMBAH SEGAR', '1373', '13'),
('1373030', 'BARANGIN', '1373', '13'),
('1373040', 'TALAWI', '1373', '13'),
('1374010', 'PADANG PANJANG BARAT', '1374', '13'),
('1374020', 'PADANG PANJANG TIMUR', '1374', '13'),
('1375010', 'GUGUK PANJANG', '1375', '13'),
('1375020', 'MANDIANGIN KOTO SELAYAN', '1375', '13'),
('1375030', 'AUR BIRUGO TIGO BALEH', '1375', '13'),
('1376010', 'PAYAKUMBUH BARAT', '1376', '13'),
('1376011', 'PAYAKUMBUH SELATAN', '1376', '13'),
('1376020', 'PAYAKUMBUH TIMUR', '1376', '13'),
('1376030', 'PAYAKUMBUH UTARA', '1376', '13'),
('1376031', 'LAMPOSI TIGO NAGORI', '1376', '13'),
('1377010', 'PARIAMAN SELATAN', '1377', '13'),
('1377020', 'PARIAMAN TENGAH', '1377', '13'),
('1377021', 'PARIAMAN TIMUR', '1377', '13'),
('1377030', 'PARIAMAN UTARA', '1377', '13'),
('1401010', 'KUANTAN MUDIK', '1401', '14'),
('1401011', 'HULU KUANTAN', '1401', '14'),
('1401012', 'GUNUNG TOAR', '1401', '14'),
('1401013', 'PUCUK RANTAU', '1401', '14'),
('1401020', 'SINGINGI', '1401', '14'),
('1401021', 'SINGINGI HILIR', '1401', '14'),
('1401030', 'KUANTAN TENGAH', '1401', '14'),
('1401031', 'SENTAJO RAYA', '1401', '14'),
('1401040', 'BENAI', '1401', '14'),
('1401050', 'KUANTAN HILIR', '1401', '14'),
('1401051', 'PANGEAN', '1401', '14'),
('1401052', 'LOGAS TANAH DARAT', '1401', '14'),
('1401053', 'KUANTAN HILIR SEBERANG', '1401', '14'),
('1401060', 'CERENTI', '1401', '14'),
('1401061', 'INUMAN', '1401', '14'),
('1402010', 'PERANAP', '1402', '14'),
('1402011', 'BATANG PERANAP', '1402', '14'),
('1402020', 'SEBERIDA', '1402', '14'),
('1402021', 'BATANG CENAKU', '1402', '14'),
('1402022', 'BATANG GANSAL', '1402', '14'),
('1402030', 'KELAYANG', '1402', '14'),
('1402031', 'RAKIT KULIM', '1402', '14'),
('1402040', 'PASIR PENYU', '1402', '14'),
('1402041', 'LIRIK', '1402', '14'),
('1402042', 'SUNGAI LALA', '1402', '14'),
('1402043', 'LUBUK BATU JAYA', '1402', '14'),
('1402050', 'RENGAT BARAT', '1402', '14'),
('1402060', 'RENGAT', '1402', '14'),
('1402061', 'KUALA CENAKU', '1402', '14'),
('1403010', 'KERITANG', '1403', '14'),
('1403011', 'KEMUNING', '1403', '14'),
('1403020', 'RETEH', '1403', '14'),
('1403021', 'SUNGAI BATANG', '1403', '14'),
('1403030', 'ENOK', '1403', '14'),
('1403040', 'TANAH MERAH', '1403', '14'),
('1403050', 'KUALA INDRAGIRI', '1403', '14'),
('1403051', 'CONCONG', '1403', '14'),
('1403060', 'TEMBILAHAN', '1403', '14'),
('1403061', 'TEMBILAHAN HULU', '1403', '14'),
('1403070', 'TEMPULING', '1403', '14'),
('1403071', 'KEMPAS', '1403', '14'),
('1403080', 'BATANG TUAKA', '1403', '14'),
('1403090', 'GAUNG ANAK SERKA', '1403', '14'),
('1403100', 'GAUNG', '1403', '14'),
('1403110', 'MANDAH', '1403', '14'),
('1403120', 'KATEMAN', '1403', '14'),
('1403121', 'PELANGIRAN', '1403', '14'),
('1403122', 'TELUK BELENGKONG', '1403', '14'),
('1403123', 'PULAU BURUNG', '1403', '14'),
('1404010', 'LANGGAM', '1404', '14'),
('1404011', 'PANGKALAN KERINCI', '1404', '14'),
('1404012', 'BANDAR SEIKIJANG', '1404', '14'),
('1404020', 'PANGKALAN KURAS', '1404', '14'),
('1404021', 'UKUI', '1404', '14'),
('1404022', 'PANGKALAN LESUNG', '1404', '14'),
('1404030', 'BUNUT', '1404', '14'),
('1404031', 'PELALAWAN', '1404', '14'),
('1404032', 'BANDAR PETALANGAN', '1404', '14'),
('1404040', 'KUALA KAMPAR', '1404', '14'),
('1404041', 'KERUMUTAN', '1404', '14'),
('1404042', 'TELUK MERANTI', '1404', '14'),
('1405010', 'MINAS', '1405', '14'),
('1405011', 'SUNGAI MANDAU', '1405', '14'),
('1405012', 'KANDIS', '1405', '14'),
('1405020', 'SIAK', '1405', '14'),
('1405021', 'KERINCI KANAN', '1405', '14'),
('1405022', 'TUALANG', '1405', '14'),
('1405023', 'DAYUN', '1405', '14'),
('1405024', 'LUBUK DALAM', '1405', '14'),
('1405025', 'KOTO GASIB', '1405', '14'),
('1405026', 'MEMPURA', '1405', '14'),
('1405030', 'SUNGAI APIT', '1405', '14'),
('1405031', 'BUNGA RAYA', '1405', '14'),
('1405032', 'SABAK AUH', '1405', '14'),
('1405033', 'PUSAKO', '1405', '14'),
('1406010', 'KAMPAR KIRI', '1406', '14'),
('1406011', 'KAMPAR KIRI HULU', '1406', '14'),
('1406012', 'KAMPAR KIRI HILIR', '1406', '14'),
('1406013', 'GUNUNG SAHILAN', '1406', '14'),
('1406014', 'KAMPAR KIRI TENGAH', '1406', '14'),
('1406020', 'XIII KOTO KAMPAR', '1406', '14'),
('1406021', 'KOTO KAMPAR HULU', '1406', '14'),
('1406030', 'KUOK', '1406', '14'),
('1406031', 'SALO', '1406', '14'),
('1406040', 'TAPUNG', '1406', '14'),
('1406041', 'TAPUNG HULU', '1406', '14'),
('1406042', 'TAPUNG HILIR', '1406', '14'),
('1406050', 'BANGKINANG KOTA', '1406', '14'),
('1406051', 'BANGKINANG', '1406', '14'),
('1406060', 'KAMPAR', '1406', '14'),
('1406061', 'KAMPAR TIMUR', '1406', '14'),
('1406062', 'RUMBIO JAYA', '1406', '14'),
('1406063', 'KAMPAR UTARA', '1406', '14'),
('1406070', 'TAMBANG', '1406', '14'),
('1406080', 'SIAK HULU', '1406', '14'),
('1406081', 'PERHENTIAN RAJA', '1406', '14'),
('1407010', 'ROKAN IV KOTO', '1407', '14'),
('1407011', 'PENDALIAN IV KOTO', '1407', '14'),
('1407020', 'TANDUN', '1407', '14'),
('1407021', 'KABUN', '1407', '14'),
('1407022', 'UJUNG BATU', '1407', '14'),
('1407030', 'RAMBAH SAMO', '1407', '14'),
('1407040', 'RAMBAH', '1407', '14'),
('1407041', 'RAMBAH HILIR', '1407', '14'),
('1407042', 'BANGUN PURBA', '1407', '14'),
('1407050', 'TAMBUSAI', '1407', '14'),
('1407051', 'TAMBUSAI UTARA', '1407', '14'),
('1407060', 'KEPENUHAN', '1407', '14'),
('1407061', 'KEPENUHAN HULU', '1407', '14'),
('1407070', 'KUNTO DARUSSALAM', '1407', '14'),
('1407071', 'PAGARAN TAPAH DARUSSALAM', '1407', '14'),
('1407072', 'BONAI DARUSSALAM', '1407', '14'),
('1408010', 'MANDAU', '1408', '14'),
('1408011', 'PINGGIR', '1408', '14'),
('1408020', 'BUKIT BATU', '1408', '14'),
('1408021', 'SIAK KECIL', '1408', '14'),
('1408030', 'RUPAT', '1408', '14'),
('1408031', 'RUPAT UTARA', '1408', '14'),
('1408040', 'BENGKALIS', '1408', '14'),
('1408050', 'BANTAN', '1408', '14'),
('1409010', 'TANAH PUTIH', '1409', '14'),
('1409011', 'PUJUD', '1409', '14'),
('1409012', 'TANAH PUTIH TANJUNG MELAWAN', '1409', '14'),
('1409013', 'RANTAU KOPAR', '1409', '14'),
('1409014', 'TANJUNG MEDAN', '1409', '14'),
('1409020', 'BAGAN SINEMBAH', '1409', '14'),
('1409021', 'SIMPANG KANAN', '1409', '14'),
('1409022', 'BAGAN SINEMBAH RAYA', '1409', '14'),
('1409023', 'BALAI JAYA', '1409', '14'),
('1409030', 'KUBU', '1409', '14'),
('1409031', 'PASIR LIMAU KAPAS', '1409', '14'),
('1409032', 'KUBU BABUSSALAM', '1409', '14'),
('1409040', 'BANGKO', '1409', '14'),
('1409041', 'SINABOI', '1409', '14'),
('1409042', 'BATU HAMPAR', '1409', '14'),
('1409043', 'PEKAITAN', '1409', '14'),
('1409050', 'RIMBA MELINTANG', '1409', '14'),
('1409051', 'BANGKO PUSAKO', '1409', '14'),
('1410010', 'TEBING TINGGI BARAT', '1410', '14'),
('1410020', 'TEBING TINGGI', '1410', '14'),
('1410021', 'TEBING TINGGI TIMUR', '1410', '14'),
('1410030', 'RANGSANG', '1410', '14'),
('1410031', 'RANGSANG PESISIR', '1410', '14'),
('1410040', 'RANGSANG BARAT', '1410', '14'),
('1410050', 'MERBAU', '1410', '14'),
('1410051', 'PULAU MERBAU', '1410', '14'),
('1410052', 'TASIK PUTRI PUYU', '1410', '14'),
('1471010', 'TAMPAN', '1471', '14'),
('1471011', 'PAYUNG SEKAKI', '1471', '14'),
('1471020', 'BUKIT RAYA', '1471', '14'),
('1471021', 'MARPOYAN DAMAI', '1471', '14'),
('1471022', 'TENAYAN RAYA', '1471', '14'),
('1471030', 'LIMAPULUH', '1471', '14'),
('1471040', 'SAIL', '1471', '14'),
('1471050', 'PEKANBARU KOTA', '1471', '14'),
('1471060', 'SUKAJADI', '1471', '14'),
('1471070', 'SENAPELAN', '1471', '14'),
('1471080', 'RUMBAI', '1471', '14'),
('1471081', 'RUMBAI PESISIR', '1471', '14'),
('1473010', 'BUKIT KAPUR', '1473', '14'),
('1473011', 'MEDANG KAMPAI', '1473', '14'),
('1473012', 'SUNGAI SEMBILAN', '1473', '14'),
('1473020', 'DUMAI BARAT', '1473', '14'),
('1473021', 'DUMAI SELATAN', '1473', '14'),
('1473030', 'DUMAI TIMUR', '1473', '14'),
('1473031', 'DUMAI KOTA', '1473', '14'),
('1501010', 'GUNUNG RAYA', '1501', '15'),
('1501011', 'BUKIT KERMAN', '1501', '15'),
('1501020', 'BATANG MERANGIN', '1501', '15'),
('1501030', 'KELILING DANAU', '1501', '15'),
('1501040', 'DANAU KERINCI', '1501', '15'),
('1501050', 'SITINJAU LAUT', '1501', '15'),
('1501070', 'AIR HANGAT', '1501', '15'),
('1501071', 'AIR HANGAT TIMUR', '1501', '15'),
('1501072', 'DEPATI VII', '1501', '15'),
('1501073', 'AIR HANGAT BARAT', '1501', '15'),
('1501080', 'GUNUNG KERINCI', '1501', '15'),
('1501081', 'SIULAK', '1501', '15'),
('1501082', 'SIULAK MUKAI', '1501', '15'),
('1501090', 'KAYU ARO', '1501', '15'),
('1501091', 'GUNUNG TUJUH', '1501', '15'),
('1501092', 'KAYU ARO BARAT', '1501', '15'),
('1502010', 'JANGKAT', '1502', '15'),
('1502011', 'SUNGAI TENANG', '1502', '15'),
('1502020', 'MUARA SIAU', '1502', '15'),
('1502021', 'LEMBAH MASURAI', '1502', '15'),
('1502022', 'TIANG PUMPUNG', '1502', '15'),
('1502030', 'PAMENANG', '1502', '15'),
('1502031', 'PAMENANG BARAT', '1502', '15'),
('1502032', 'RENAH PAMENANG', '1502', '15'),
('1502033', 'PAMENANG SELATAN', '1502', '15'),
('1502040', 'BANGKO', '1502', '15'),
('1502041', 'BANGKO BARAT', '1502', '15'),
('1502042', 'NALO TANTAN', '1502', '15'),
('1502043', 'BATANG MASUMAI', '1502', '15'),
('1502050', 'SUNGAI MANAU', '1502', '15'),
('1502051', 'RENAH PEMBARAP', '1502', '15'),
('1502052', 'PANGKALAN JAMBU', '1502', '15'),
('1502060', 'TABIR', '1502', '15'),
('1502061', 'TABIR ULU', '1502', '15'),
('1502062', 'TABIR SELATAN', '1502', '15'),
('1502063', 'TABIR ILIR', '1502', '15'),
('1502064', 'TABIR TIMUR', '1502', '15'),
('1502065', 'TABIR LINTAS', '1502', '15'),
('1502066', 'MARGO TABIR', '1502', '15'),
('1502067', 'TABIR BARAT', '1502', '15'),
('1503010', 'BATANG ASAI', '1503', '15'),
('1503020', 'LIMUN', '1503', '15'),
('1503021', 'CERMIN NAN GEDANG', '1503', '15'),
('1503030', 'PELAWAN', '1503', '15'),
('1503031', 'SINGKUT', '1503', '15'),
('1503040', 'SAROLANGUN', '1503', '15'),
('1503041', 'BATHIN VIII', '1503', '15'),
('1503050', 'PAUH', '1503', '15'),
('1503051', 'AIR HITAM', '1503', '15'),
('1503060', 'MANDIANGIN', '1503', '15'),
('1504010', 'MERSAM', '1504', '15'),
('1504011', 'MARO SEBO ULU', '1504', '15'),
('1504020', 'BATIN XXIV', '1504', '15'),
('1504030', 'MUARA TEMBESI', '1504', '15'),
('1504040', 'MUARA BULIAN', '1504', '15'),
('1504041', 'BAJUBANG', '1504', '15'),
('1504042', 'MARO SEBO ILIR', '1504', '15'),
('1504050', 'PEMAYUNG', '1504', '15'),
('1505010', 'MESTONG', '1505', '15'),
('1505011', 'SUNGAI BAHAR', '1505', '15'),
('1505012', 'BAHAR SELATAN', '1505', '15'),
('1505013', 'BAHAR UTARA', '1505', '15'),
('1505020', 'KUMPEH ULU', '1505', '15'),
('1505021', 'SUNGAI GELAM', '1505', '15'),
('1505030', 'KUMPEH', '1505', '15'),
('1505040', 'MARO SEBO', '1505', '15'),
('1505041', 'TAMAN RAJO', '1505', '15'),
('1505050', 'JAMBI LUAR KOTA', '1505', '15'),
('1505060', 'SEKERNAN', '1505', '15'),
('1506010', 'MENDAHARA', '1506', '15'),
('1506011', 'MENDAHARA ULU', '1506', '15'),
('1506012', 'GERAGAI', '1506', '15'),
('1506020', 'DENDANG', '1506', '15'),
('1506031', 'MUARA SABAK BARAT', '1506', '15'),
('1506032', 'MUARA SABAK TIMUR', '1506', '15'),
('1506033', 'KUALA JAMBI', '1506', '15'),
('1506040', 'RANTAU RASAU', '1506', '15'),
('1506041', 'BERBAK', '1506', '15'),
('1506050', 'NIPAH PANJANG', '1506', '15'),
('1506060', 'SADU', '1506', '15'),
('1507010', 'TUNGKAL ULU', '1507', '15'),
('1507011', 'MERLUNG', '1507', '15'),
('1507012', 'BATANG ASAM', '1507', '15'),
('1507013', 'TEBING TINGGI', '1507', '15'),
('1507014', 'RENAH MENDALUH', '1507', '15'),
('1507015', 'MUARA PAPALIK', '1507', '15'),
('1507020', 'PENGABUAN', '1507', '15'),
('1507021', 'SENYERANG', '1507', '15'),
('1507030', 'TUNGKAL ILIR', '1507', '15'),
('1507031', 'BRAM ITAM', '1507', '15'),
('1507032', 'SEBERANG KOTA', '1507', '15'),
('1507040', 'BETARA', '1507', '15'),
('1507041', 'KUALA BETARA', '1507', '15'),
('1508010', 'TEBO ILIR', '1508', '15'),
('1508011', 'MUARA TABIR', '1508', '15'),
('1508020', 'TEBO TENGAH', '1508', '15'),
('1508021', 'SUMAY', '1508', '15'),
('1508022', 'TENGAH ILIR', '1508', '15'),
('1508030', 'RIMBO BUJANG', '1508', '15'),
('1508031', 'RIMBO ULU', '1508', '15'),
('1508032', 'RIMBO ILIR', '1508', '15'),
('1508040', 'TEBO ULU', '1508', '15'),
('1508041', 'VII KOTO', '1508', '15'),
('1508042', 'SERAI SERUMPUN', '1508', '15'),
('1508043', 'VII KOTO ILIR', '1508', '15'),
('1509010', 'PELEPAT', '1509', '15'),
('1509011', 'PELEPAT ILIR', '1509', '15'),
('1509021', 'BATHIN II BABEKO', '1509', '15'),
('1509022', 'RIMBO TENGAH', '1509', '15'),
('1509023', 'BUNGO DANI', '1509', '15'),
('1509024', 'PASAR MUARA BUNGO', '1509', '15'),
('1509025', 'BATHIN III', '1509', '15'),
('1509030', 'RANTAU PANDAN', '1509', '15'),
('1509031', 'MUKO-MUKO BATHIN VII', '1509', '15'),
('1509032', 'BATHIN III ULU', '1509', '15'),
('1509040', 'TANAH SEPENGGAL', '1509', '15'),
('1509041', 'TANAH SEPENGGAL LINTAS', '1509', '15'),
('1509050', 'TANAH TUMBUH', '1509', '15'),
('1509051', 'LIMBUR LUBUK MENGKUANG', '1509', '15'),
('1509052', 'BATHIN II PELAYANG', '1509', '15'),
('1509060', 'JUJUHAN', '1509', '15'),
('1509061', 'JUJUHAN ILIR', '1509', '15'),
('1571010', 'KOTA BARU', '1571', '15'),
('1571011', 'ALAM BARAJO', '1571', '15'),
('1571020', 'JAMBI SELATAN', '1571', '15'),
('1571021', 'PAAL MERAH', '1571', '15'),
('1571030', 'JELUTUNG', '1571', '15'),
('1571040', 'PASAR JAMBI', '1571', '15'),
('1571050', 'TELANAIPURA', '1571', '15'),
('1571051', 'DANAU SIPIN', '1571', '15'),
('1571060', 'DANAU TELUK', '1571', '15'),
('1571070', 'PELAYANGAN', '1571', '15'),
('1571080', 'JAMBI TIMUR', '1571', '15'),
('1572010', 'TANAH KAMPUNG', '1572', '15'),
('1572020', 'KUMUN DEBAI', '1572', '15'),
('1572030', 'SUNGAI PENUH', '1572', '15'),
('1572031', 'PONDOK TINGGGI', '1572', '15'),
('1572032', 'SUNGAI BUNGKAL', '1572', '15'),
('1572040', 'HAMPARAN RAWANG', '1572', '15'),
('1572050', 'PESISIR BUKIT', '1572', '15'),
('1572051', 'KOTO BARU', '1572', '15'),
('1601052', 'LENGKITI', '1601', '16'),
('1601070', 'SOSOH BUAY RAYAP', '1601', '16'),
('1601080', 'PENGANDONAN', '1601', '16'),
('1601081', 'SEMIDANG AJI', '1601', '16'),
('1601082', 'ULU OGAN', '1601', '16'),
('1601083', 'MUARA JAYA', '1601', '16'),
('1601090', 'PENINJAUAN', '1601', '16'),
('1601091', 'LUBUK BATANG', '1601', '16'),
('1601092', 'SINAR PENINJAUAN', '1601', '16'),
('1601093', 'KEDATON PENINJAUAN RAYA', '1601', '16'),
('1601130', 'BATU RAJA TIMUR', '1601', '16'),
('1601131', 'LUBUK RAJA', '1601', '16'),
('1601140', 'BATU RAJA BARAT', '1601', '16'),
('1602010', 'LEMPUING', '1602', '16'),
('1602011', 'LEMPUING JAYA', '1602', '16'),
('1602020', 'MESUJI', '1602', '16'),
('1602021', 'SUNGAI MENANG', '1602', '16'),
('1602022', 'MESUJI MAKMUR', '1602', '16'),
('1602023', 'MESUJI RAYA', '1602', '16'),
('1602030', 'TULUNG SELAPAN', '1602', '16'),
('1602031', 'CENGAL', '1602', '16'),
('1602040', 'PEDAMARAN', '1602', '16'),
('1602041', 'PEDAMARAN TIMUR', '1602', '16'),
('1602050', 'TANJUNG LUBUK', '1602', '16'),
('1602051', 'TELUK GELAM', '1602', '16'),
('1602060', 'KOTA KAYU AGUNG', '1602', '16'),
('1602120', 'SIRAH PULAU PADANG', '1602', '16'),
('1602121', 'JEJAWI', '1602', '16'),
('1602130', 'PAMPANGAN', '1602', '16');

-- --------------------------------------------------------

--
-- Table structure for table `mstmerk`
--

CREATE TABLE `mstmerk` (
  `id_merk` varchar(255) NOT NULL,
  `merk` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mstmerk`
--

INSERT INTO `mstmerk` (`id_merk`, `merk`) VALUES
('MERK-00001', 'Avanza'),
('MERK-00002', 'Innova'),
('MERK-00003', 'MERK-00001');

-- --------------------------------------------------------

--
-- Table structure for table `mstperson`
--

CREATE TABLE `mstperson` (
  `KodePerson` varchar(50) NOT NULL,
  `KodeSponsor` varchar(50) DEFAULT NULL,
  `NamaPerson` varchar(150) DEFAULT NULL,
  `NIKPerson` varchar(50) DEFAULT NULL,
  `FotoPerson` varchar(150) DEFAULT NULL,
  `TglLahir` date DEFAULT NULL,
  `UserName` varchar(20) DEFAULT NULL,
  `Password` varchar(150) DEFAULT NULL,
  `Alamat` varchar(255) DEFAULT NULL,
  `NoHP` varchar(20) DEFAULT NULL,
  `AlamatEmail` varchar(50) DEFAULT NULL,
  `Keterangan` varchar(255) DEFAULT NULL,
  `Koor_Long` varchar(50) DEFAULT NULL,
  `Koor_Lat` varchar(50) DEFAULT NULL,
  `TipePerson` varchar(25) DEFAULT NULL,
  `KodeBankPerson` varchar(10) DEFAULT NULL,
  `NoRekPerson` varchar(50) DEFAULT NULL,
  `AtasNama` varchar(100) DEFAULT NULL,
  `KodeDesa` varchar(20) DEFAULT NULL,
  `KodeKec` varchar(15) DEFAULT NULL,
  `KodeKab` char(10) DEFAULT NULL,
  `KodeProvinsi` varchar(5) DEFAULT NULL,
  `RealPosisiLong` varchar(50) DEFAULT NULL,
  `RealPosisiLat` varchar(50) DEFAULT NULL,
  `TglAkhirSuspend` datetime DEFAULT NULL,
  `StatusPerson` varchar(25) DEFAULT NULL,
  `KodeCabang` varchar(15) DEFAULT NULL,
  `NoPol` varchar(20) DEFAULT NULL,
  `FotoKTP` varchar(255) NOT NULL,
  `FotoSIM` varchar(255) NOT NULL,
  `FotoSKCK` varchar(255) NOT NULL,
  `IsVerified` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mstperson`
--

INSERT INTO `mstperson` (`KodePerson`, `KodeSponsor`, `NamaPerson`, `NIKPerson`, `FotoPerson`, `TglLahir`, `UserName`, `Password`, `Alamat`, `NoHP`, `AlamatEmail`, `Keterangan`, `Koor_Long`, `Koor_Lat`, `TipePerson`, `KodeBankPerson`, `NoRekPerson`, `AtasNama`, `KodeDesa`, `KodeKec`, `KodeKab`, `KodeProvinsi`, `RealPosisiLong`, `RealPosisiLat`, `TglAkhirSuspend`, `StatusPerson`, `KodeCabang`, `NoPol`, `FotoKTP`, `FotoSIM`, `FotoSKCK`, `IsVerified`) VALUES
('PRS-2018-0000000001', NULL, 'Driver1', NULL, 'dv_profil.png', NULL, 'aa', 'YWE=', NULL, '085706719932', 'aa', NULL, NULL, NULL, 'DRIVER NON BRANDING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '112.2220857', '-7.5661449', NULL, 'AKTIF ON DUTY', 'CAB-001', 'S1234W', '', '', '', b'0'),
('PRS-2018-0000000002', NULL, '	\r\nDriver2', NULL, NULL, NULL, 'bb', 'YmI=', NULL, '88', 'bb', NULL, NULL, NULL, 'DRIVER NON BRANDING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '112.2220537', '-7.5661887', NULL, 'AKTIF FREE', 'CAB-001', '123', '', '', '', b'0'),
('PRS-2018-0000000003', NULL, '	\r\nDriver3', NULL, NULL, NULL, 'cc', 'Y2M=', NULL, '99', 'cc', NULL, NULL, NULL, 'DRIVER NON BRANDING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '112.223339', '-7.564862', NULL, 'AKTIF FREE', 'CAB-001', '0', '', '', '', b'0'),
('PRS-2018-0000000004', NULL, '	\r\nDriver4', NULL, NULL, NULL, 'dd', 'ZGQ=', NULL, '55', 'dd', NULL, NULL, NULL, 'DRIVER NON BRANDING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '112.224301', '-7.565658', NULL, 'AKTIF FREE', 'CAB-001', '555', '', '', '', b'0'),
('PRS-2018-0000000006', NULL, 'driver', NULL, NULL, NULL, 'driver', 'ZHJpdmVy', NULL, '123', 'driver', NULL, NULL, NULL, 'DRIVER NON BRANDING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '112.233952', '-7.556017', NULL, 'AKTIF FREE', 'CAB-001', '3zdd33', '', '', '', b'0'),
('PRS-2018-0000000008', NULL, 'ramawa', NULL, NULL, NULL, 'ra', 'Y25oNmIwMA==', NULL, '123456', 'rama', NULL, NULL, NULL, 'UMUM', NULL, NULL, NULL, NULL, NULL, '3517', '35', '0', '0', NULL, 'AKTIF FREE', 'CAB-001', '0', '', '', '', b'1'),
('PRS-2018-0000000010', NULL, 'ramawati', NULL, 'ramawati_profil.png', NULL, 'ramawati', 'MTIzNDU2', NULL, '123', 'rama2@gmail.com', NULL, NULL, NULL, 'UMUM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 'CAB-001', '0', 'ramawati_ktp.png', '', '', b'1'),
('PRS-2018-0000000011', NULL, 'aku jaya', NULL, 'aku_profil.png', NULL, 'aku', 'YWt1', NULL, '79785555', 'aku', NULL, NULL, NULL, 'UMUM', 'BNK-0001', '00825334456789', 'Bank Aku', NULL, NULL, NULL, NULL, 'Not available', 'Not available', NULL, '', 'CAB-001', '0', 'aku_ktp.png', '', '', b'1'),
('PRS-2018-0000000012', NULL, 'dv', NULL, 'dv_profil.png', NULL, 'dv', 'ZHY=', NULL, '85', 'dv', NULL, NULL, NULL, 'DRIVER NON BRANDING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ONLINE NOT VERIFIED', 'CAB-001', 'h', 'dv_ktp.png', 'h_5.png', 'h_6.png', b'1'),
('PRS-2018-0000000013', NULL, 'kamu', NULL, 'kamu_profil.png', NULL, 'kamu', 'a2FtdQ==', NULL, '49779', 'kamu', NULL, NULL, NULL, 'UMUM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 'CAB-001', '0', 'kamu_ktp.png', '', '', b'1'),
('PRS-2018-0000000014', NULL, 'driv', NULL, 'driv_profil.png', NULL, 'driv', 'ZHJpdg==', NULL, '8', 'driv', NULL, NULL, NULL, 'DRIVER NON BRANDING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ONLINE NOT VERIFIED', 'CAB-001', '0', 'driv_ktp.png', '', '', b'1'),
('PRS-2018-0000000015', NULL, 'usertes', NULL, 'usertes_profil.png', NULL, 'usertes', 'dXNlcnRlcw==', NULL, '65', 'usertes', NULL, NULL, NULL, 'UMUM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 'CAB-001', '0', 'usertes_ktp.png', '', '', b'1'),
('PRS-2018-0000000016', NULL, 'drivertes', NULL, 'drivertes_profil.png', NULL, 'drivertes', 'ZHJpdmVydGVz', NULL, '8454', 'drivertes', NULL, NULL, NULL, 'DRIVER NON BRANDING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ONLINE NOT VERIFIED', 'CAB-001', '0', 'drivertes_ktp.png', '', '', b'1'),
('PRS-2018-0000000017', NULL, 'saya', NULL, 'saya_profil.png', NULL, 'saya', 'c2F5YQ==', NULL, '477', 'saya', NULL, NULL, NULL, 'UMUM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 'CAB-001', '0', 'saya_ktp.png', '', '', b'1'),
('PRS-2018-0000000018', NULL, 'dr', '12345678936464', 'dr_profil.png', NULL, 'dr', 'ZHI=', NULL, '885', 'dr', NULL, NULL, NULL, 'DRIVER NON BRANDING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ONLINE NOT VERIFIED', 'CAB-001', '0', 'dr_ktp.png', '', '', b'1'),
('PRS-2018-0000000019', NULL, 'www', NULL, 'shsis_profil.png', NULL, 'shsis', 'eXN1cw==', NULL, '646', 'usisi', NULL, NULL, NULL, 'DRIVER NON BRANDING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ONLINE NOT VERIFIED', 'CAB-001', '0', 'shsis_ktp.png', '', '', b'1'),
('PRS-2018-0000000020', NULL, 'gue', NULL, 'jeuei_profil.png', NULL, 'jeuei', 'Z2VnZWhl', NULL, '4646', 'hejei', NULL, NULL, NULL, 'DRIVER NON BRANDING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ONLINE NOT VERIFIED', 'CAB-001', '0', 'jeuei_ktp.png', '', '', b'1'),
('PRS-2018-0000000021', NULL, 'nJK', NULL, 'zjsjjs_profil.png', NULL, 'zjsjjs', 'c2hocw==', NULL, '9797', 'jsjsjs', NULL, NULL, NULL, 'DRIVER NON BRANDING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ONLINE NOT VERIFIED', 'CAB-001', '0', 'zjsjjs_ktp.png', '', '', b'1'),
('PRS-2018-0000000022', NULL, 'Putri Rachael Trijanuarti', NULL, 'Rachael_profil.png', NULL, 'Rachael', 'NzE1NTAwMjU=', NULL, '081330716052', 'rachael_greeseldo@yahoo.com', NULL, NULL, NULL, 'UMUM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', 'CAB-001', '0', 'Rachael_ktp.png', '', '', b'1'),
('PRS-2018-0000000023', NULL, 'widya dwijanu saputra', NULL, 'wdjs22_profil.png', NULL, 'wdjs22', 'OHMyaXU4Ng==', NULL, '082233209999', 'wdjs22@gmail.com', NULL, NULL, NULL, 'UMUM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', 'CAB-001', '0', 'wdjs22_ktp.png', '', '', b'1'),
('PRS-2018-0000000024', NULL, 'linda priskila rianti', NULL, 'lindapri_profil.png', NULL, 'lindapri', 'bGluZGEyMTAzODY=', NULL, '085697123379', 'lindapriskila@gmail.com', NULL, NULL, NULL, 'UMUM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, '', 'CAB-001', '0', 'lindapri_ktp.png', '', '', b'1'),
('PRS-2018-0000000025', NULL, 'fatihdriver', NULL, 'fatihdriver_profil.png', NULL, 'fatihdriver', 'MTIzNDU2', NULL, '08123324932', 'fatih@driver.com', NULL, NULL, NULL, 'DRIVER NON BRANDING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '112.2220837', '-7.566264', NULL, 'ONLINE NOT VERIFIED', 'CAB-001', 'S 1882 WC', 'fatihdriver_ktp.png', 'S 1882 WC_5.png', 'S 1882 WC_6.png', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `mstprovinsi`
--

CREATE TABLE `mstprovinsi` (
  `KodeProvinsi` varchar(5) NOT NULL,
  `NamaProvinsi` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mstprovinsi`
--

INSERT INTO `mstprovinsi` (`KodeProvinsi`, `NamaProvinsi`) VALUES
('11', 'ACEH'),
('12', 'SUMATERA UTARA'),
('13', 'SUMATERA BARAT'),
('14', 'RIAU'),
('15', 'JAMBI'),
('16', 'SUMATERA SELATAN'),
('17', 'BENGKULU'),
('18', 'LAMPUNG'),
('19', 'KEPULAUAN BANGKA BELITUNG'),
('21', 'KEPULAUAN RIAU'),
('31', 'DKI JAKARTA'),
('32', 'JAWA BARAT'),
('33', 'JAWA TENGAH'),
('34', 'DI YOGYAKARTA'),
('35', 'JAWA TIMUR'),
('36', 'BANTEN'),
('51', 'BALI'),
('52', 'NUSA TENGGARA BARAT'),
('53', 'NUSA TENGGARA TIMUR'),
('61', 'KALIMANTAN BARAT'),
('62', 'KALIMANTAN TENGAH'),
('63', 'KALIMANTAN SELATAN'),
('64', 'KALIMANTAN TIMUR'),
('65', 'KALIMANTAN UTARA'),
('71', 'SULAWESI UTARA'),
('72', 'SULAWESI TENGAH'),
('73', 'SULAWESI SELATAN'),
('74', 'SULAWESI TENGGARA'),
('75', 'GORONTALO'),
('76', 'SULAWESI BARAT'),
('81', 'MALUKU'),
('82', 'MALUKU UTARA'),
('91', 'PAPUA BARAT'),
('94', 'PAPUA');

-- --------------------------------------------------------

--
-- Table structure for table `mstslider`
--

CREATE TABLE `mstslider` (
  `ID` int(50) NOT NULL,
  `Judul` varchar(255) NOT NULL,
  `Isi` varchar(255) NOT NULL,
  `Gambar` varchar(255) NOT NULL,
  `IsAktif` bit(1) NOT NULL,
  `IsDriver` bit(1) NOT NULL,
  `IsKlien` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mstslider`
--

INSERT INTO `mstslider` (`ID`, `Judul`, `Isi`, `Gambar`, `IsAktif`, `IsDriver`, `IsKlien`) VALUES
(1, 'Slider 1', 'Isi Slider 1', 'slider1.jpg', b'0', b'0', NULL),
(2, 'Slider 2', 'Isi Slider 2', 'slider2.jpg', b'0', b'0', NULL),
(3, 'gh', 'ggg', 'blurrytwo.jpg', b'0', b'0', NULL),
(4, 'Slider4', 'Deskripsi Slider 4', 'beauty.png', b'1', b'1', b'0'),
(5, 'Tes baru', 'oke', '5-Makanan-Murah-Yang-Bisa-Dicicipin-Waktu-Nge-mall.jpg', b'1', b'1', b'1'),
(6, 'fggfh', 'gfhgfhfg', '8d9aa092-dffc-4fe4-b060-38449285e7d3.jpg', b'1', b'0', b'1'),
(7, 'dfdsds', 'sdfsdd', '1280px-Ice_Milk_and_Lemon_Teas_-_Chilli_Cafe.jpg', b'1', b'1', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `msttipe`
--

CREATE TABLE `msttipe` (
  `id_merk` varchar(255) NOT NULL,
  `id_tipe` varchar(255) NOT NULL,
  `tipe` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `msttipe`
--

INSERT INTO `msttipe` (`id_merk`, `id_tipe`, `tipe`) VALUES
('MERK-00001', 'TIPE-00001', 'Tipe1'),
('MERK-00002', 'TIPE-00002', 'Tipe2'),
('MERK-00003', 'MERK-00003', 'TIPE-00001');

-- --------------------------------------------------------

--
-- Table structure for table `pesanuser`
--

CREATE TABLE `pesanuser` (
  `IdPesan` varchar(255) NOT NULL,
  `TanggalKirim` datetime DEFAULT NULL,
  `Nama` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Pesan` varchar(255) NOT NULL,
  `IsReaded` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pesanuser`
--

INSERT INTO `pesanuser` (`IdPesan`, `TanggalKirim`, `Nama`, `Email`, `Pesan`, `IsReaded`) VALUES
('KONTAK-2018-0000001', '2018-09-19 12:56:32', 'Billy', 'billy@gmail.com', 'oke ya', b'0'),
('KONTAK-2018-0000002', '2018-09-19 12:56:57', 'dsfsdsdfs', 'iya@gmail.com', 'dsfsdfsd', b'1'),
('KONTAK-2018-0000003', '2018-09-19 13:34:15', 'angga', 'billyanggaramukti@gmail.com', 'oke', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `serverfitur`
--

CREATE TABLE `serverfitur` (
  `FiturID` int(11) NOT NULL,
  `FiturName` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `serverfitur`
--

INSERT INTO `serverfitur` (`FiturID`, `FiturName`) VALUES
(1, 'Data Penjual'),
(2, 'Data Kurir'),
(3, 'Data Klien'),
(4, 'Sistem Setting'),
(5, 'Transaksi Bank Tarik Tunai'),
(6, 'Transaksi Bank Deposit'),
(7, 'Riwayat Tarik Tunai'),
(8, 'Riwayat Transaksi Deposit'),
(9, 'Lap. Laba Perusahaan'),
(10, 'User Login'),
(11, 'Akses Level'),
(12, 'Server Log'),
(13, 'Transaksi Sewa Iklan'),
(14, 'Transaksi Kas Masuk'),
(15, 'Transaksi Kas Keluar'),
(16, 'Lap. Arus Kas'),
(17, 'Lap. Bulanan'),
(18, 'Pengganti Kurir'),
(19, 'Verifikasi Promo Penjual'),
(20, 'Transaksi Order'),
(21, 'Riwayat Transaksi Order'),
(22, 'Transaksi Mutasi Dana'),
(23, 'Riwayat Transaksi Mutasi Dana'),
(24, 'Tampilkan User & Password Kurir');

-- --------------------------------------------------------

--
-- Table structure for table `systemsetting`
--

CREATE TABLE `systemsetting` (
  `KodeSetting` varchar(25) NOT NULL,
  `ValueData` varchar(255) DEFAULT NULL,
  `KodeCabang` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `systemsetting`
--

INSERT INTO `systemsetting` (`KodeSetting`, `ValueData`, `KodeCabang`) VALUES
('1_KM_MAKSIMUM', '77', 'C0001'),
('2_KM_MINIMUM', '5', 'C0001'),
('3_TRF_MALAM', '4500', 'C0001'),
('4_TRF_MINIMUM', '20000', 'C0001'),
('5_TRF_SIANG', '2500', 'C0001'),
('6_SaldoMin', '25000', 'C0001'),
('7_ProsentasePerusahaan', '30', 'C0001'),
('Alamat', 'Jl.Prof M.Yamin No.8 Pandanwangi', 'C0001'),
('Email', 'bosstran@gmail.com', 'C0001'),
('Telepon', '085645308456', 'C0001');

-- --------------------------------------------------------

--
-- Table structure for table `transaksikas`
--

CREATE TABLE `transaksikas` (
  `NoTrKas` varchar(25) NOT NULL,
  `UserName` varchar(50) NOT NULL,
  `TanggalTransaksi` date DEFAULT NULL,
  `IsMutasiDana` bit(1) DEFAULT NULL,
  `KodeCabangTujuan` varchar(15) DEFAULT NULL,
  `Debet` float DEFAULT NULL,
  `Kredit` float DEFAULT NULL,
  `KelompokKas` varchar(25) DEFAULT NULL,
  `Uraian` varchar(255) DEFAULT NULL,
  `KodeCabang` varchar(15) NOT NULL,
  `NoTrOrder` varchar(50) DEFAULT NULL,
  `KodeIklan` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trorderkendaraan`
--

CREATE TABLE `trorderkendaraan` (
  `NoTrOrder` varchar(50) NOT NULL,
  `JenisOrder` varchar(20) DEFAULT NULL,
  `JenisPembayaran` varchar(20) DEFAULT NULL,
  `TanggalOrder` datetime DEFAULT NULL,
  `KodePersonClient` varchar(50) DEFAULT NULL,
  `JamACCDriver` datetime DEFAULT NULL,
  `KodePersonDriver` varchar(50) DEFAULT NULL,
  `KeteranganUtkDriver` varchar(255) DEFAULT NULL,
  `AlamatAsal` varchar(255) NOT NULL,
  `AlamatTujuan` varchar(255) NOT NULL,
  `Koor_LongAwal` varchar(50) DEFAULT NULL,
  `Koor_LatAwal` varchar(50) DEFAULT NULL,
  `TotalBiaya` float DEFAULT NULL,
  `Koor_LongTujuan` varchar(50) DEFAULT NULL,
  `Koor_LatTujuan` varchar(50) DEFAULT NULL,
  `JarakDriver` float DEFAULT NULL,
  `BiayaDriver` float DEFAULT NULL,
  `PorsiPerusahaan` float DEFAULT NULL,
  `PorsiDriver` float DEFAULT NULL,
  `StatusOrder` varchar(20) DEFAULT NULL,
  `KetStatus` varchar(255) DEFAULT NULL,
  `RateDriver` float DEFAULT NULL,
  `TestimoniDriver` char(18) DEFAULT NULL,
  `RateClient` float DEFAULT NULL,
  `TestimoniClient` char(18) DEFAULT NULL,
  `NoPol` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trorderkendaraan`
--

INSERT INTO `trorderkendaraan` (`NoTrOrder`, `JenisOrder`, `JenisPembayaran`, `TanggalOrder`, `KodePersonClient`, `JamACCDriver`, `KodePersonDriver`, `KeteranganUtkDriver`, `AlamatAsal`, `AlamatTujuan`, `Koor_LongAwal`, `Koor_LatAwal`, `TotalBiaya`, `Koor_LongTujuan`, `Koor_LatTujuan`, `JarakDriver`, `BiayaDriver`, `PorsiPerusahaan`, `PorsiDriver`, `StatusOrder`, `KetStatus`, `RateDriver`, `TestimoniDriver`, `RateClient`, `TestimoniClient`, `NoPol`) VALUES
('TRO-2018-0000000001', 'SEKARANG', 'DOMPET', '2018-09-01 14:28:36', 'PRS-2018-0000000011', '2018-09-01 14:51:01', 'PRS-2018-0000000001', '', 'Jl. Cendrawasih, Plosogeneng, Kec. Jombang, Kabupaten Jombang, Jawa Timur 61419, Indonesia', 'Jalan Sunan Ampel III, Rejomulyo, Kec. Kota Kediri, Kediri, Jawa Timur 64129, Indonesia', '112.22597882151602', '-7.524736332953532', 95484, '112.0178286', '-7.848015599999999', NULL, NULL, 19096.8, 76387.2, 'SUCCESS', NULL, NULL, NULL, NULL, 'Sipp', 'S1234W'),
('TRO-2018-0000000002', 'SEKARANG', 'TUNAI', '2018-09-03 13:39:24', 'PRS-2018-0000000024', NULL, 'PRS-2018-0000000001', '', 'Jl. Dukuh Menanggal III No.26, RT.004/RW.06, Dukuh Menanggal, Gayungan, Kota SBY, Jawa Timur 60234, Indonesia', 'Graha Pena Jawa Pos Lobby, Ketintang, Gayungan, Kota SBY, Jawa Timur, Indonesia', '112.72067639976741', '-7.342347778081414', 10000, '112.73230339999999', '-7.3205151', NULL, NULL, 2000, 8000, 'SUCCESS', NULL, NULL, NULL, NULL, 'Mantap', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `userfitur`
--

CREATE TABLE `userfitur` (
  `UserName` varchar(50) NOT NULL,
  `FiturID` int(11) NOT NULL,
  `ViewData` bit(1) DEFAULT NULL,
  `AddData` bit(1) DEFAULT NULL,
  `EditData` bit(1) DEFAULT NULL,
  `DeleteData` bit(1) DEFAULT NULL,
  `PrintData` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `userlogin`
--

CREATE TABLE `userlogin` (
  `UserName` varchar(50) NOT NULL,
  `UserPsw` varchar(255) DEFAULT NULL,
  `ActualName` varchar(255) DEFAULT NULL,
  `Foto` varchar(255) NOT NULL,
  `Address` varchar(255) DEFAULT NULL,
  `Phone` varchar(50) DEFAULT NULL,
  `HPNo` varchar(50) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `UserStatus` int(11) DEFAULT '1',
  `LevelID` int(11) DEFAULT NULL,
  `IsAktif` bit(1) DEFAULT NULL,
  `KodeCabang` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userlogin`
--

INSERT INTO `userlogin` (`UserName`, `UserPsw`, `ActualName`, `Foto`, `Address`, `Phone`, `HPNo`, `Email`, `UserStatus`, `LevelID`, `IsAktif`, `KodeCabang`) VALUES
('admin', 'YWRtaW4=', 'admin', '20180908065139-admin.png', 'jombang', '0811', NULL, 'admin@gmail.com', 1, NULL, b'1', 'C0001');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accesslevel`
--
ALTER TABLE `accesslevel`
  ADD PRIMARY KEY (`LevelID`),
  ADD UNIQUE KEY `PrimaryKey` (`LevelID`);

--
-- Indexes for table `adminbulanan`
--
ALTER TABLE `adminbulanan`
  ADD PRIMARY KEY (`BulanTahun`,`KodePerson`),
  ADD KEY `KodePerson` (`KodePerson`);

--
-- Indexes for table `datakendaraan`
--
ALTER TABLE `datakendaraan`
  ADD PRIMARY KEY (`NoPol`),
  ADD UNIQUE KEY `XPKDataKendaraan` (`NoPol`);

--
-- Indexes for table `dompetperson`
--
ALTER TABLE `dompetperson`
  ADD PRIMARY KEY (`NoTransaksi`,`KodePerson`),
  ADD UNIQUE KEY `XPKDompetPerson` (`NoTransaksi`,`KodePerson`),
  ADD KEY `NoTrOrder` (`NoTrOrder`),
  ADD KEY `KodePerson` (`KodePerson`);

--
-- Indexes for table `fcm_token`
--
ALTER TABLE `fcm_token`
  ADD PRIMARY KEY (`KodePerson`);

--
-- Indexes for table `fiturlevel`
--
ALTER TABLE `fiturlevel`
  ADD PRIMARY KEY (`LevelID`,`FiturID`),
  ADD UNIQUE KEY `PrimaryKey` (`LevelID`,`FiturID`),
  ADD KEY `FiturID` (`FiturID`);

--
-- Indexes for table `kontenweb`
--
ALTER TABLE `kontenweb`
  ADD PRIMARY KEY (`KodeKonten`,`JenisKonten`),
  ADD KEY `UserName` (`UserName`);

--
-- Indexes for table `mstbank`
--
ALTER TABLE `mstbank`
  ADD PRIMARY KEY (`id_bank`);

--
-- Indexes for table `mstbank_perusahaan`
--
ALTER TABLE `mstbank_perusahaan`
  ADD PRIMARY KEY (`id_bank`,`norek`);

--
-- Indexes for table `mstcabang`
--
ALTER TABLE `mstcabang`
  ADD PRIMARY KEY (`KodeCabang`),
  ADD UNIQUE KEY `XPKMstCabang` (`KodeCabang`);

--
-- Indexes for table `mstdesa`
--
ALTER TABLE `mstdesa`
  ADD PRIMARY KEY (`KodeDesa`,`KodeKec`,`KodeKab`,`KodeProvinsi`),
  ADD UNIQUE KEY `XPKMstDesa` (`KodeDesa`,`KodeKec`,`KodeKab`,`KodeProvinsi`),
  ADD KEY `KodeProvinsi` (`KodeProvinsi`),
  ADD KEY `KodeKab` (`KodeKab`),
  ADD KEY `KodeKec` (`KodeKec`);

--
-- Indexes for table `mstkabupaten`
--
ALTER TABLE `mstkabupaten`
  ADD PRIMARY KEY (`KodeKab`,`KodeProvinsi`),
  ADD UNIQUE KEY `XPKMstKabupaten` (`KodeKab`,`KodeProvinsi`),
  ADD KEY `KodeProvinsi` (`KodeProvinsi`);

--
-- Indexes for table `mstkecamatan`
--
ALTER TABLE `mstkecamatan`
  ADD PRIMARY KEY (`KodeKec`,`KodeKab`,`KodeProvinsi`),
  ADD UNIQUE KEY `XPKMstKecamatan` (`KodeKec`,`KodeKab`,`KodeProvinsi`),
  ADD KEY `KodeProvinsi` (`KodeProvinsi`),
  ADD KEY `KodeKab` (`KodeKab`);

--
-- Indexes for table `mstmerk`
--
ALTER TABLE `mstmerk`
  ADD PRIMARY KEY (`id_merk`);

--
-- Indexes for table `mstperson`
--
ALTER TABLE `mstperson`
  ADD PRIMARY KEY (`KodePerson`),
  ADD UNIQUE KEY `XPKMstPerson` (`KodePerson`);

--
-- Indexes for table `mstprovinsi`
--
ALTER TABLE `mstprovinsi`
  ADD PRIMARY KEY (`KodeProvinsi`),
  ADD UNIQUE KEY `XPKMstProvinsi` (`KodeProvinsi`);

--
-- Indexes for table `mstslider`
--
ALTER TABLE `mstslider`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `msttipe`
--
ALTER TABLE `msttipe`
  ADD PRIMARY KEY (`id_merk`,`id_tipe`);

--
-- Indexes for table `pesanuser`
--
ALTER TABLE `pesanuser`
  ADD PRIMARY KEY (`IdPesan`);

--
-- Indexes for table `serverfitur`
--
ALTER TABLE `serverfitur`
  ADD PRIMARY KEY (`FiturID`),
  ADD UNIQUE KEY `PrimaryKey` (`FiturID`);

--
-- Indexes for table `systemsetting`
--
ALTER TABLE `systemsetting`
  ADD PRIMARY KEY (`KodeSetting`,`KodeCabang`),
  ADD UNIQUE KEY `XPKSystemSetting` (`KodeSetting`,`KodeCabang`);

--
-- Indexes for table `transaksikas`
--
ALTER TABLE `transaksikas`
  ADD PRIMARY KEY (`NoTrKas`,`KodeCabang`),
  ADD UNIQUE KEY `XPKTransaksiKAS` (`NoTrKas`,`KodeCabang`);

--
-- Indexes for table `trorderkendaraan`
--
ALTER TABLE `trorderkendaraan`
  ADD PRIMARY KEY (`NoTrOrder`),
  ADD UNIQUE KEY `XPKTrOrderKendaraan` (`NoTrOrder`);

--
-- Indexes for table `userfitur`
--
ALTER TABLE `userfitur`
  ADD PRIMARY KEY (`UserName`,`FiturID`),
  ADD UNIQUE KEY `XPKUserFitur` (`UserName`,`FiturID`);

--
-- Indexes for table `userlogin`
--
ALTER TABLE `userlogin`
  ADD PRIMARY KEY (`UserName`),
  ADD UNIQUE KEY `PrimaryKey` (`UserName`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mstslider`
--
ALTER TABLE `mstslider`
  MODIFY `ID` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `adminbulanan`
--
ALTER TABLE `adminbulanan`
  ADD CONSTRAINT `adminbulanan_ibfk_1` FOREIGN KEY (`KodePerson`) REFERENCES `mstperson` (`KodePerson`);

--
-- Constraints for table `dompetperson`
--
ALTER TABLE `dompetperson`
  ADD CONSTRAINT `dompetperson_ibfk_1` FOREIGN KEY (`NoTrOrder`) REFERENCES `trorderkendaraan` (`NoTrOrder`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `dompetperson_ibfk_2` FOREIGN KEY (`KodePerson`) REFERENCES `mstperson` (`KodePerson`);

--
-- Constraints for table `fiturlevel`
--
ALTER TABLE `fiturlevel`
  ADD CONSTRAINT `fiturlevel_ibfk_1` FOREIGN KEY (`LevelID`) REFERENCES `accesslevel` (`LevelID`),
  ADD CONSTRAINT `fiturlevel_ibfk_2` FOREIGN KEY (`FiturID`) REFERENCES `serverfitur` (`FiturID`);

--
-- Constraints for table `kontenweb`
--
ALTER TABLE `kontenweb`
  ADD CONSTRAINT `kontenweb_ibfk_1` FOREIGN KEY (`UserName`) REFERENCES `userlogin` (`UserName`);

--
-- Constraints for table `mstbank_perusahaan`
--
ALTER TABLE `mstbank_perusahaan`
  ADD CONSTRAINT `mstbank_perusahaan_ibfk_1` FOREIGN KEY (`id_bank`) REFERENCES `mstbank` (`id_bank`);

--
-- Constraints for table `mstdesa`
--
ALTER TABLE `mstdesa`
  ADD CONSTRAINT `mstdesa_ibfk_1` FOREIGN KEY (`KodeProvinsi`) REFERENCES `mstprovinsi` (`KodeProvinsi`),
  ADD CONSTRAINT `mstdesa_ibfk_2` FOREIGN KEY (`KodeKab`) REFERENCES `mstkabupaten` (`KodeKab`),
  ADD CONSTRAINT `mstdesa_ibfk_3` FOREIGN KEY (`KodeKec`) REFERENCES `mstkecamatan` (`KodeKec`);

--
-- Constraints for table `mstkabupaten`
--
ALTER TABLE `mstkabupaten`
  ADD CONSTRAINT `mstkabupaten_ibfk_1` FOREIGN KEY (`KodeProvinsi`) REFERENCES `mstprovinsi` (`KodeProvinsi`);

--
-- Constraints for table `mstkecamatan`
--
ALTER TABLE `mstkecamatan`
  ADD CONSTRAINT `mstkecamatan_ibfk_1` FOREIGN KEY (`KodeProvinsi`) REFERENCES `mstprovinsi` (`KodeProvinsi`),
  ADD CONSTRAINT `mstkecamatan_ibfk_2` FOREIGN KEY (`KodeKab`) REFERENCES `mstkabupaten` (`KodeKab`);

--
-- Constraints for table `msttipe`
--
ALTER TABLE `msttipe`
  ADD CONSTRAINT `msttipe_ibfk_1` FOREIGN KEY (`id_merk`) REFERENCES `mstmerk` (`id_merk`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
