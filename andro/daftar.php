<?php 
require_once('config.php'); 
date_default_timezone_set('Asia/Jakarta');
if($_SERVER['REQUEST_METHOD']=='POST'){ 
	$nama 			= $_POST['nama'];
	$telepon 		= $_POST['telepon'];
	$email 			= $_POST['email']; 
	$username 		= $_POST['username']; 
	$pass			= $_POST['password']; 
	$status			= $_POST['status'];
	$foto_ktp		= $_POST['foto_ktp']; 
	$foto_profil	= $_POST['foto_profil'];
	$tanggal 		= date('Y-m-d H:i:s');
	$year	 		= date('Y');
	
	$fotoktp = $username."_ktp.png";	
	$fotoprofil = $username."_profil.png";	
	$path1 = "foto_ktp/".$username."_ktp.png";
	$path2 = "foto_profil/".$username."_profil.png";
	
	$password   = str_replace("\n", '', $pass);
	
	if($status == 'Driver'){
		$tipe = 'DRIVER NON BRANDING';
		$statusperson = 'ONLINE NOT VERIFIED';
	}else{
		$tipe = 'UMUM';
		$statusperson = '';
	}

	//cek email
	$cek_email = mysqli_query($con, "SELECT AlamatEmail FROM mstperson WHERE replace(AlamatEmail,' ','') =replace('$email',' ','')");
	$row_cek_email = mysqli_num_rows($cek_email);
	if($row_cek_email > 0){
			echo "email_ada";
	}else{
			//cek username
			$cek_username = mysqli_query($con, "SELECT UserName FROM mstperson WHERE replace(UserName,' ','') =replace('$username',' ','')");
			$row_cek_user = mysqli_num_rows($cek_username);
			if($row_cek_user > 0){
				echo "username_ada";
			}else{

				//generate kode_person
				$sql = mysqli_query($con,"SELECT RIGHT(MAX(KodePerson),10) AS kode FROM mstperson WHERE LEFT (KodePerson,8) = 'PRS-".$year."'"); 
				$nums	= mysqli_num_rows($sql); 
				while($data = mysqli_fetch_array($sql)){
					if($nums === 0)
					{ 
						$kode = 1; 
					}else{ 
						$kode = $data['kode'] + 1;
					} 
				}		
				$bikin_kode 	= str_pad($kode, 10, "0", STR_PAD_LEFT);
				$kode_jadi	 	= "PRS-".$year."-".$bikin_kode;
					
				//insert data
				$daftar = mysqli_query($con, "INSERT INTO mstperson 
				(KodePerson, UserName, Password, NamaPerson, NoHP, AlamatEmail, TipePerson, StatusPerson, KodeCabang, NoPol, FotoPerson, FotoKTP, IsVerified) 
				VALUES 
				('$kode_jadi','$username', '$password', '$nama','$telepon', '$email', '$tipe', '$statusperson','CAB-001','0','$fotoprofil','$fotoktp','0')");

				if($daftar){
					file_put_contents($path1,base64_decode($foto_ktp));
					file_put_contents($path2,base64_decode($foto_profil));
					echo "success";
				}else{
					echo "failure";
				}
			}
		}	
}
 ?>