<?php  
require_once('config.php'); 
$lati  = $_GET['lati'];
$longi = $_GET['longi'];

//radius 10 km
// $xa = $lati-0.1;
// $xb = $lati+0.1;
// $ya = $longi-0.1;
// $yb = $longi+0.1;
 
// $hasil         = mysqli_query($con,"SELECT KodePerson FROM mstperson where
// '$xa' < RealPosisiLat < '$xb' AND  '$ya' < RealPosisiLong < '$yb' ");
$hasil = mysqli_query($con, 
	"SELECT
	  KodePerson, RealPosisiLat,RealPosisiLong,(
		6371 * acos (
		  cos ( radians($lati) )
		  * cos( radians( RealPosisiLat ) )
		  * cos( radians( RealPosisiLong ) - radians($longi) )
		  + sin ( radians($lati) )
		  * sin( radians( RealPosisiLat ) )
		)
	  ) AS distance
	FROM mstperson
	HAVING distance < 10
	ORDER BY KodePerson"
);



 $json_response = array();

 while ($row = mysqli_fetch_assoc($hasil)) {
     $json_response[] = $row;
 }
 echo json_encode(array('latlong' => $json_response));
//AND TipePerson LIKE '%DRIVER%'
?>