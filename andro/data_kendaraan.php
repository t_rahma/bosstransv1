<?php 
require_once('config.php'); 
date_default_timezone_set('Asia/Jakarta');
if($_SERVER['REQUEST_METHOD']=='POST'){ 
	$username	= $_POST['username'];
	$merk 		= $_POST['merk'];
	$nopol 		= $_POST['nopol'];
	$tipe 		= $_POST['tipe']; 
	$cc 		= $_POST['cc']; 
	$tahun		= $_POST['tahun']; 
	$kapasitas	= $_POST['kapasitas'];
	$foto1 		= $_POST['foto1']; 
	$foto2		= $_POST['foto2']; 
	$foto3		= $_POST['foto3'];
	$foto4		= $_POST['foto4'];
	$foto5		= $_POST['fotosim']; 
	$foto6		= $_POST['fotoskck'];
	$foto7		= $_POST['fotostnk'];
	$atasnama	= date('Y-m-d H:i:s');
	$year	 	= date('Y');
	
	$namafile1 = $nopol."_1.png";	
	$namafile2 = $nopol."_2.png";	
	$namafile3 = $nopol."_3.png";	
	$namafile4 = $nopol."_4.png";
	$namafile5 = $nopol."_5.png";	
	$namafile6 = $nopol."_6.png";	
	$namafile7 = $nopol."_7.png";	
	$path1 = "foto_kendaraan/".$nopol."_1.png";
	$path2 = "foto_kendaraan/".$nopol."_2.png";
	$path3 = "foto_kendaraan/".$nopol."_3.png";
	$path4 = "foto_kendaraan/".$nopol."_4.png";
	$path5 = "foto_sim/".$nopol."_5.png";
	$path6 = "foto_skck/".$nopol."_6.png";
	$path7 = "foto_stnk/".$nopol."_7.png";
	
	//cek nopol
	$cek_nopol = mysqli_query($con, "SELECT NoPol FROM DataKendaraan WHERE replace(NoPol,' ','') = replace('$nopol',' ','')");
	$row_cek_nopol = mysqli_num_rows($cek_nopol);
	if($row_cek_nopol > 0){
		echo "data_ada";
	}else{
		//cek merk kendaraan 
		$merk_kendaraan = mysqli_query($con, "select id_merk from mstmerk where merk = '$merk'");
		$count_merk = mysqli_num_rows($merk_kendaraan);
		$row_merk = mysqli_fetch_array($merk_kendaraan);
		
		if($row_merk > 0){
			$id_merk = $row_merk['id_merk'];
			//cek tipe kendaraan
			$tipe_kendaran = mysqli_query($con, "select id_tipe from msttipe where tipe ='$tipe'");
			$row_tipe = mysqli_fetch_array($tipe_kendaraan);
			$id_tipe = $row_tipe['id_tipe'];
		}else{
				//generate id_merk
				$sql = mysqli_query($con,"SELECT RIGHT(MAX(id_merk),5) AS kode FROM mstmerk WHERE LEFT (id_merk,5) = 'MERK-'"); 
				$nums	= mysqli_num_rows($sql); 
				while($data = mysqli_fetch_array($sql)){
					if($nums === 0)
					{ 
						$kode = 1; 
					}else{ 
						$kode = $data['kode'] + 1;
					} 
				}		
				$bikin_kode 	= str_pad($kode, 5, "0", STR_PAD_LEFT);
				$id_merk	 	= "MERK-".$bikin_kode;
				
				//generate id_tipe
				$sql2 = mysqli_query($con,"SELECT RIGHT(MAX(id_tipe),5) AS kode FROM msttipe WHERE LEFT (id_tipe,5) = 'TIPE-'"); 
				$nums2	= mysqli_num_rows($sql2); 
				while($data2 = mysqli_fetch_array($sql2)){
					if($nums2 === 0)
					{ 
						$kode2 = 1; 
					}else{ 
						$kode2 = $data2['kode'] + 1;
					} 
				}		
				$bikin_kode2 	= str_pad($kode2, 5, "0", STR_PAD_LEFT);
				$id_tipe	 	= "MERK-".$bikin_kode2;
				

				//insert mst_merk
				$insert_merk = mysqli_query($con, "insert into mstmerk (id_merk, merk) values ('$id_merk', '$merk')");
				//insert mst_tipe
				$insert_tipe = mysqli_query($con, "insert into msttipe (id_merk, id_tipe, tipe) values ('$id_merk', '$id_tipe', '$tipe')");
		}
		
		//insert data
		$daftar = mysqli_query($con, "INSERT INTO DataKendaraan 
		(NoPol, MerkKendaraan, CCKendaraan, TahunPembuatan, KapasitasPenumpang, AtasNamaSTNK, StatusKendaraan, LastUjiKIR, AkhirMasaPajakSTNK, FotoKendaraan1, FotoKendaraan2, FotoKendaraan3, FotoKendaraan4, FotoSTNK, Merk, Tipe) 
		VALUES 
		('$nopol', '$merk', '$cc', '$tahun', '$kapasitas', '$atasnama', 'AKTIF', '$kir', '$stnk', '$namafile1', '$namafile2', '$namafile3','$namafile4','$namafile7','$id_merk', '$id_tipe')");
		//update mstperson
		$update_person = mysqli_query($con, "UPDATE mstperson SET NoPol='$nopol', FotoSIM='$namafile5', FotoSKCK='$namafile6' WHERE UserName ='$username'");
	}
		if ($update_person){
			file_put_contents($path1,base64_decode($foto1));
			file_put_contents($path2,base64_decode($foto2));
			file_put_contents($path3,base64_decode($foto3));
			file_put_contents($path4,base64_decode($foto4));
			file_put_contents($path5,base64_decode($foto5));
			file_put_contents($path6,base64_decode($foto6));
			file_put_contents($path7,base64_decode($foto7));
			
			echo 'success';
		} else{ 
			echo 'failure';
		}
}
 ?>