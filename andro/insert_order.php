<?php 
require_once('config.php'); 
if($_SERVER['REQUEST_METHOD']=='POST'){
date_default_timezone_set('Asia/Jakarta');
$user 		= $_POST['username'];
$pembayaran = $_POST['pembayaran'];
$biaya	 	= $_POST['biaya'];
$pesan		= $_POST['pesan'];
$lat_asal	= $_POST['lat_asal'];
$lat_tujuan	= $_POST['lat_tujuan'];
$long_asal	= $_POST['long_asal'];
$long_tujuan= $_POST['long_tujuan'];
$alamat_asal	= $_POST['alamat_asal'];
$alamat_tujuan= $_POST['alamat_tujuan'];
// $jarakdriver= $_POST['jarakdriver'];
// $biayadriver= $_POST['biayadriver'];
$tanggal 	= date('Y-m-d H:i:s');
$year	 	= date('Y');

//firebase
$path_fcm		= 'https://fcm.googleapis.com/fcm/send';
$server_key		= 'AAAAM7CV3P0:APA91bHmIZEEofqSrMfKORF9xPQWpZe6BfMWlLXLgmHU84fNwxetmXPTmY3srOWFxkCkkaar5NQu6z7-e-Wha6LfoxFn_msdq8ercldYVWg7PeKBMBw9kSkMQxWBxEMaGmoWBi4uja5nI_4X8FTbiegY5SPNvtRr5g';
$title = 'Order Masuk';
$message = 'Ada order masuk, silahkan dilayani.';

// //radius 10 km
// $xa = $lat_asal-0.1;
// $xb = $lat_asal+0.1;
// $ya = $long_asal-0.1;
// $yb = $long_asal+0.1;

//select kode person user
$cek_user = mysqli_query($con, "select KodePerson from mstperson where UserName='$user'");
$row_person = mysqli_fetch_array($cek_user);
$kode_person = $row_person['KodePerson'];

//generate notrorder
	$sql = mysqli_query($con,"SELECT RIGHT(MAX(NoTrOrder),10) AS kode FROM trorderkendaraan WHERE LEFT (NoTrOrder,8) = 'PRS-".$year."'"); 
	$nums	= mysqli_num_rows($sql); 
	while($data = mysqli_fetch_array($sql)){
		if($nums === 0)
		{ 
			$kode = 1; 
		}else{ 
			$kode = $data['kode'] + 1;
		} 
	}		
		$bikin_kode 	= str_pad($kode, 10, "0", STR_PAD_LEFT);
		$kode_jadi	 	= "PRS-".$year."-".$bikin_kode;
		
//hitung bagi hasil
$porsiperusahaan = $biaya*0.8;
$porsidriver = $biaya*0.2;

//insert order
$insert_order = mysqli_query($con, "insert into trorderkendaraan (NoTrOrder, JenisOrder, JenisPembayaran, TanggalOrder, KodePersonClient, KeteranganUtkDriver, Koor_LongAwal, Koor_LatAwal, Koor_LongTujuan, Koor_LatTujuan, TotalBiaya, PorsiPerusahaan, PorsiDriver, StatusOrder, AlamatAsal, AlamatTujuan) values ('$kode_jadi', 'Sekarang', '$pembayaran', '$tanggal', '$kode_person', '$pesan', '$long_asal', '$lat_asal', '$long_tujuan', '$lat_tujuan', '$biaya', '$porsiperusahaan', '$porsidriver', 'LELANG','$alamat_asal','$alamat_tujuan')");

if($insert_order){
	echo 'success';
	//--------send_notification--------//
	
	//cek driver di dalam radius 10 km
	$hasil         = mysqli_query($con,"SELECT
	  KodePerson, RealPosisiLat,RealPosisiLong,(
		6371 * acos (
		  cos ( radians($lat_asal) )
		  * cos( radians( RealPosisiLat ) )
		  * cos( radians( RealPosisiLong ) - radians($long_asal) )
		  + sin ( radians($lat_asal) )
		  * sin( radians( RealPosisiLat ) )
		)
	  ) AS distance
	FROM mstperson
	HAVING distance < 10
	ORDER BY distance AND TipePerson LIKE '%DRIVER%' AND StatusPerson='AKTIF FREE'");
	
	$kode_driver = "";
	while($row_kode = mysqli_fetch_array($hasil)){
		if ($kode_driver == ""){
		  $kode_driver = "(KodePerson = '".$row_kode['KodePerson']."'"; 
		  
		}else{
		   $kode_driver = $kode_driver." OR KodePerson = '".$row_kode['KodePerson']."'";
	  }
	}
	$kode_driver = $kode_driver.")";

	//ambil token
	$token = mysqli_query($con, "select IDToken from fcm_token where " .$kode_driver);
	$fields = array();
	// $num=0;
	while($row_token = mysqli_fetch_array($token)){
	$key = $row_token['IDToken'];
		$headers = array('Authorization:key='.$server_key, 'Content-Type:application/json');
	
		$fields = array('to'=>$key, 'notification'=>array('title'=>$title, 'body'=>$message, 'sound'=>'default'));
		// $fields[$num]['kirim'] = array('to'=>$key, 'notification'=>array('title'=>$title, 'body'=>$message));
		// $num++;
		$payload = json_encode($fields);
		$curl_session = curl_init();
		curl_setopt($curl_session, CURLOPT_URL, $path_fcm);
		curl_setopt($curl_session, CURLOPT_POST, true);
		curl_setopt($curl_session, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl_session, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl_session, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
		curl_setopt($curl_session, CURLOPT_POSTFIELDS, $payload);
		$result = curl_exec($curl_session);
		curl_close($curl_session);
	}
	
}else{
	echo 'failure';
}

//Closing the database 
sqlsrv_close($con); 
}
?>
