<?php
session_start();
date_default_timezone_set('Asia/Jakarta');

include ('web/library/config.php');
$_SESSION = array();
include("web/library/captcha/simple-php-captcha.php");
$_SESSION['captcha'] = simple_php_captcha();

if(isset($_POST['Kirim'])){
	if($_POST['captcha']==$_POST['KodeCaptcha']){
		//membuat id user
		$year	 = date('Y');
		$sql 	 = mysqli_query($koneksi,'SELECT RIGHT(IdPesan,7) AS kode FROM pesanuser WHERE IdPesan LIKE "%'.$year.'%" ORDER BY IdPesan DESC LIMIT 1');  
		$num	 = mysqli_num_rows($sql);
		 
		if($num <> 0)
		 {
		 $data = mysqli_fetch_array($sql);
		 $kode = $data['kode'] + 1;
		 }else
		 {
		 $kode = 1;
		 }
		 
		//mulai bikin kode
		 $bikin_kode = str_pad($kode, 7, "0", STR_PAD_LEFT);
		 $kode_jadi_kontak	 = "KONTAK-".$year."-".$bikin_kode;
		
		$Simpan = mysqli_query($koneksi,"INSERT INTO pesanuser (IdPesan,TanggalKirim,Nama,Email,Pesan,IsReaded)VALUES('$kode_jadi_kontak','".date('Y-m-d H:i:s')."','".str_replace("'"," ",$_POST['Nama'])."','".str_replace("'"," ",$_POST['Email'])."','".str_replace("'"," ",$_POST['Pesan'])."',b'0')");
		
		if($Simpan){
			echo '<script language="javascript">alert("Pesan Berhasil Dikirim, Pesan akan dibalas melalui Email Anda!"); document.location="index.php"; </script>';
		}else{
			echo '<script language="javascript">alert(Maaf, Pesan Gagal Dikirim!"); document.location="index.php"; </script>';
		}
	}else{
		echo '<script language="javascript">alert("Kode Captcha Salah!"); document.location="index.php"; </script>';
	}
}


$query1 = mysqli_query($koneksi, "select ValueData from systemsetting where KodeSetting = 'Alamat'");
$row1 = mysqli_fetch_array($query1);
$Alamat	= $row1['ValueData'];

$query2 = mysqli_query($koneksi, "select ValueData from systemsetting where  KodeSetting = 'Telepon'");
$row2 = mysqli_fetch_array($query2);
$Telepon	= $row2['ValueData'];

$query3 = mysqli_query($koneksi, "select ValueData from systemsetting where KodeSetting = 'Email'");
$row3 = mysqli_fetch_array($query3);
$Email	= $row3['ValueData'];

//testimonia
$testi = mysqli_query($koneksi, "select a.TestimoniClient, b.FotoPerson, b.NamaPerson from trorderkendaraan a LEFT JOIN mstperson b ON a.KodePersonClient = b.KodePerson LIMIT 3");
$array_testi = array();

function Konten($JenisKonten,$KodeKonten,$koneksi){
	$AmbilData = mysqli_query($koneksi,"SELECT * FROM kontenweb WHERE KodeKonten='$KodeKonten' AND JenisKonten='$JenisKonten'");
	$Data = mysqli_fetch_assoc($AmbilData);
	
	return array($Data['Judul'], $Data['Title'], $Data['Isi']);
}
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bosstrans</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Slick slider css -->
    <link href="css/skdslider.css" rel="stylesheet">
    <!-- Font awesome css -->
	<link rel="stylesheet" href="web/admin/komponen/vendor/font-awesome/css/font-awesome.min.css">
    <!--<link rel="stylesheet" href="css/font-awesome.min.css">-->
    <!-- smooth animate css file -->
    <link rel="stylesheet" href="css/animate.css"> 
    <!-- Main style css -->
    <link rel="stylesheet" href="style.css">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="img/favicon.png"/>
    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100' rel='stylesheet' type='text/css'>     

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<style>
	#featuresSection {
		background-color: #FFFFFF;
		display: inline;
		float: left;
		padding-top: 50px;
		width: 100%;
		margin-top: 0px;
	}
	.features_productarea {
		border-top: 1px solid #DCDCDC;
		display: inline;
		float: left;
		margin-top: 0px;
		padding-top: 0px;
		position: relative;
		width: 100%;
	}
	
	#footer {
		float: left;
		display: inline;
		width: 100%;
		background-color: #333;
		padding: 25px 0px;
	}
	
	.single_pricelist {
		border: 1px solid #DCDCDC;
		display: inline;
		float: left;
		height: 300px;
		padding-bottom: 20px;
		width: 100%;
		margin-left: 0px;
		-webkit-transition: all .5s ease-out;
		-moz-transition: all .5s ease-out;
		-o-transition: all .5s ease-out;
		transition: all .5s ease-out;
	}
	
	.client_slider li:last-child {
		margin-left: 0px;
	}
	
	.client_slider li {
		float: left;
		list-style: none outside none;
		margin-right: 20px;
	}
	
	.modal-content {
		position: relative;
		background-color: #6e6b6b;
		border: 1px solid #999;
		border: 1px solid rgba(0,0,0,0.2);
		border-radius: 6px;
		outline: 0;
		-webkit-box-shadow: 0 3px 9px rgba(0,0,0,0.5);
		box-shadow: 0 3px 9px rgba(0,0,0,0.5);
		background-clip: padding-box;
	}
	</style>
  </head>

  <body>
  <!-- BEGAIN PRELOADER -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

  <!-- START HEADER SECTION -->
  <header id="headerArea">
    <a href="#" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="slider_area">           
          <div class="menuarea"> 
            <div class="navbar navbar-default navbar-fixed-top" role="navigation">
              <div class="container">
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <!-- For Text Logo -->
                 <a class="navbar-brand logo" href="#"><span>Boss</span>Trans</a>
                 <!-- For Img Logo -->
                  <!--  <a class="navbar-brand logo" href="#"><img src="img/logo.png" alt="logo"></a> -->
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                  <ul class="nav navbar-nav navbar-right custom_nav mobnav" id="top-menu">
                    <li class="active"><a href="#headerArea">Beranda</a></li>
                    <!--<li><a href="#featuresSection">Gallery</a></li>-->
                    <li><a href="#priceList">Layanan</a></li>
                    <li><a href="#clients">Gallery</a></li>
                    <!--<li><a href="#testimonila">Testimoni</a></li>-->
                    <li><a href="#contact">Hubungi Kami</a></li>
                  </ul>
                </div><!--/.nav-collapse -->
              </div>
            </div>
          </div>
          <ul id="demo1" class="slides">
            <li>
              <img src="img/slider/asfalt.png" />
              <!--Slider Description example-->
              <div class="slide-desc">
                <div class="slide_descleft">
                  <img src="img/slider1.png" alt="img">
                </div>
                <div class="slide_descright">
                  <h1><?php $Row = Konten('BERANDA','KNT-0000001',$koneksi); echo $Row[0];?></h1>
                  <p><?php echo $Row[1]; ?></p>
                  <div class="header_btnarea">
                    <a href="DetilLayanan.php?id=<?php echo base64_encode('KNT-0000001');?>&jenis=<?php echo base64_encode('BERANDA');?>" class="learnmore_btn" target="_blank">Selengkapnya</a>
                    <a href="#" class="download_btn">Download</a>
                  </div>
                </div>
              </div>
            </li>
            <li>
              <img src="img/slider/dark_wall.png" />
              <div class="slide-desc">
                <div class="slide_descleft">
                 <img src="img/slider2.png" alt="img">
                </div>
                <div class="slide_descright">
                 <h1><?php $Row2 = Konten('BERANDA','KNT-0000002',$koneksi); echo $Row2[0];?></h1>
                  <p><?php echo $Row2[1]; ?></p>
                  <div class="header_btnarea">
                   <a href="DetilLayanan.php?id=<?php echo base64_encode('KNT-0000002');?>&jenis=<?php echo base64_encode('BERANDA');?>" class="learnmore_btn" target="_blank">Selengkapnya</a>
                    <a href="#" class="download_btn">Download</a>
                  </div>
                </div>
              </div>                 
            </li>  
			<li>
              <img src="img/slider/dark_wood.png" />
              <div class="slide-desc">
                <div class="slide_descleft">
                   <img src="img/slider3.png" alt="img">
                </div>
                <div class="slide_descright">
                  <h1><?php $Row3 = Konten('BERANDA','KNT-0000003',$koneksi); echo $Row3[0];?></h1>
                  <p><?php echo $Row3[1]; ?></p>
                  <div class="header_btnarea">
                    <a href="DetilLayanan.php?id=<?php echo base64_encode('KNT-0000003');?>&jenis=<?php echo base64_encode('BERANDA');?>" class="learnmore_btn" target="_blank">Selengkapnya</a>
                    <a href="#" class="download_btn">Download</a>
                  </div>
                </div>
              </div>              
            </li>			
            <li>
              <img src="img/slider/stardust.png" />
              <div class="slide-desc">
               <div class="slide_descleft">
                 <img src="img/slider4.png" alt="img">
               </div>
               <div class="slide_descright">
                  <h1><?php $Row4 = Konten('BERANDA','KNT-0000004',$koneksi); echo $Row4[0];?></h1>
                  <p><?php echo $Row4[1]; ?></p>
                <div class="header_btnarea">
                  <a href="DetilLayanan.php?id=<?php echo base64_encode('KNT-0000004');?>&jenis=<?php echo base64_encode('BERANDA');?>" class="learnmore_btn" target="_blank">Selengkapnya</a>
                  <a href="#" class="download_btn">Download</a>
                </div>
               </div>
              </div>               
            </li>
          </ul>           
        </div>
      </div>  
    </div>      
  </header>
  <!-- END HEADER SECTION -->  

  <!-- START PRICE LIST SECTION -->
  <section id="priceList">
    <h1>Layanan Bosstrans</h1>
    <p>Tersedia banyak layanan yang memudahkan namun keamanan tetap terjaga, dengan BOSSTRANS semua bisa menjadi BOSS.</p>
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12">
          <div class="pricelist_area">            
          <div class="single_pricelist_area">
            <div class="row">
			<?php 
				$QueryLayanan = mysqli_query($koneksi,"SELECT * FROM kontenweb WHERE JenisKonten='LAYANAN' ORDER BY KodeKonten ASC ");
				while($DataLayanan = mysqli_fetch_array($QueryLayanan)){
			?>
			  <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="single_pricelist wow fadeInUp">
                 <div class="single_pricelist_box">
					<?php if($DataLayanan['Judul']=='Boss Car'){
						echo '<img src="img/boscar.png" class="img img-responsive">';
					}elseif($DataLayanan['Judul']=='Boss Travel'){
						echo '<img src="img/bostravel.png" class="img img-responsive">';
					}elseif($DataLayanan['Judul']=='Boss Rent'){
						echo '<img src="img/bosrent.png" class="img img-responsive">';
					}elseif($DataLayanan['Judul']=='Boss Ku'){
						echo '<img src="img/bosku.png" class="img img-responsive">';
					} ?>
                 </div>
                 <div class="single_pricelist_content">
                   <h1><?php echo $DataLayanan['Judul']; ?></h1>
                   <h2><?php echo $DataLayanan['Title']; ?></h2>
                   <a class="price_select" href="DetilLayanan.php?id=<?php echo base64_encode($DataLayanan['KodeKonten']);?>&jenis=<?php echo base64_encode('LAYANAN');?>" target='_blank'>Detil</a>
                 </div>
                </div>
              </div>
				<?php } ?>
            </div>
          </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- END PRICE LIST SECTION -->

  <!-- START CLIENTS SECTION -->
  <section id="clients">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12">
          <div class="clients_area">
            <h1>Gallery</h1>
            <p>Our Activity Bosstrans, <a href="DetilLayanan.php?jenis=<?php echo base64_encode('GALERI');?>" style="color:#ffd200;" target="_blank">More...</a></p>
            <div class="client_slider">
              <!-- Carousel
                ================================================== -->
              <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                  <div class="item active">
                    <ul>
                      <?php 
					  $GambarDefault = mysqli_query($koneksi,"SELECT * FROM kontenweb WHERE JenisKonten='GALERI' ORDER BY KodeKonten ASC LIMIT 0,5");
					  while($DataGambar= mysqli_fetch_array($GambarDefault)){
					  
						echo '
						<li>
							<a href="#" class="open_modal" data-id="'.$DataGambar['KodeKonten'].'" data-judul="'.$DataGambar['Judul'].'" data-gambar="'.$DataGambar['Gambar'].'" data-user="'.$DataGambar['UserName'].'" data-tanggal="'.$DataGambar['Date'].'">
							<img src="img/Galeri/thumb_'.$DataGambar['Gambar'].'" alt="img" class="img img-responsive">
							</a>
						</li>';
					  
					  } ?>
                    </ul>
                  </div>
                  <div class="item">
                    <ul>
                     <?php 
					  $GambarDefault = mysqli_query($koneksi,"SELECT * FROM kontenweb WHERE JenisKonten='GALERI' ORDER BY KodeKonten ASC LIMIT 5,9");
					  while($DataGambar= mysqli_fetch_array($GambarDefault)){
					  
						echo '
						<li>
							<a href="#" class="open_modal" data-id="'.$DataGambar['KodeKonten'].'" data-judul="'.$DataGambar['Judul'].'" data-gambar="'.$DataGambar['Gambar'].'" data-user="'.$DataGambar['UserName'].'" data-tanggal="'.$DataGambar['Date'].'">
							<img src="img/Galeri/thumb_'.$DataGambar['Gambar'].'" alt="img" class="img img-responsive">
							</a>
						</li>';
					  
					  } ?>
                    </ul>
                  </div>
                </div>
              </div><!-- /.carousel -->

              <a class="clientSlider_left" href="#myCarousel" data-slide="prev"><i class="fa fa-angle-left"></i></a>
              <a class="clientSlider_right" href="#myCarousel" data-slide="next"><i class="fa fa-angle-right"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- END CLIENTS SECTION -->

  <!-- START NEWS LETTER -->
  <!--<section id="newsletter">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12">
          <div class="newsletter_area">
            <div class="row">
              <div class="col-lg-5 col-md-5 col-sm-12">
                <div class="newsletter_left wow fadeInLeft">
                  <h2>AppStation<span>Newsletter</span></h2>
                  <p>Sign up to our newsletter to stay in the loop!</p>
                </div>
              </div>
              <div class="col-lg-7 col-md-7 col-sm-12">
                <div class="newsletter_right wow fadeInRight">
                  <form action="">
                    <div class="form-group">
                      <input type="email" class="" placeholder="Email Address">
                      <input class="" type="submit" value="Subscribe">
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- END NEWS LETTER -->

  <!-- START CLIENTS SECTION -->
  <!--<section id="clients">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12">
          <div class="clients_area">
            <h1>Some of our happy clients</h1>
            <p>Here are a few firms that trust our services, day after day.</p>
            <div class="client_slider">
              <!-- Carousel
                ================================================== -->
              <!--<div id="myCarousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                  <div class="item active">
                    <ul>
                      <li><a href="#"><img src="img/microsoft.png" alt="img"></a></li>
                      <li><a href="#"><img src="img/hp.png" alt="img"></a></li>
                      <li><a href="#"><img src="img/android.png" alt="img"></a></li>
                      <li><a href="#"><img src="img/intel.png" alt="img"></a></li>
                    </ul>
                  </div>
                  <div class="item">
                    <ul>
                      <li><a href="#"><img src="img/microsoft.png" alt="img"></a></li>
                      <li><a href="#"><img src="img/hp.png" alt="img"></a></li>
                      <li><a href="#"><img src="img/android.png" alt="img"></a></li>
                      <li><a href="#"><img src="img/intel.png" alt="img"></a></li>
                    </ul>
                  </div>
                </div>
              </div><!-- /.carousel -->

              <!--<a class="clientSlider_left" href="#myCarousel" data-slide="prev"><i class="fa fa-angle-left"></i></a>
              <a class="clientSlider_right" href="#myCarousel" data-slide="next"><i class="fa fa-angle-right"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- END CLIENTS SECTION -->

  <!-- START CONTACT SECTION -->
  <section id="contact">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12">
          <div class="contact_area">
            <h1>Hubungi Kami</h1>
            <p>Anda dapat menanyakan seputar Bosstrans lebih detil dalam isian form dibawah ini.</p>
            <div class="row">
              <div class="col-lg-6 col-md-6">
                <div class="contact_left wow fadeInLeft">
                  <h1>Kontak</h1>
                  <div class="media contact_media">
                    <i class="fa fa-phone"></i>
                    <div class="media-body contact_media_body">
                      <h4>Phone: <?php echo $Telepon?></h4>
                    </div>
                  </div>
                    <div class="media contact_media">
                    <i class="fa fa-envelope"></i>
                    <div class="media-body contact_media_body">
                      <h4>Email: <?php echo $Email?></h4>
                    </div>
                  </div>
                    <div class="media contact_media">
                    <i class="fa fa-map-marker"></i>
                    <div class="media-body contact_media_body">
                      <h4>Address: <?php echo $Alamat?></h4>
                    </div>
                  </div>
                  <!--<div class="contact_social">
                    <h1>Social</h1>
                    <a class="fb" href="#"><i class="fa fa-facebook"></i></a>
                    <a class="twitter" href="#"><i class="fa fa-twitter"></i></a>
                    <a class="gplus" href="#"><i class="fa fa-google-plus"></i></a>
                    <a class="pinterest" href="#"><i class="fa fa-pinterest"></i></a>
                  </div>-->
                </div>
                </div>
              <div class="col-lg-6 col-md-6">
                <div class="contact_right wow fadeInRight">
                <div id="form-messages"></div>
                  <form class="footer_form" method="post">
                    <div class="row">
                      <div class="col-lg-6 col-md-6">
                        <div class="form-grooup">
                          <input type="text" class="form-control" placeholder="Name" maxlength='50' name="Nama" required>
                        </div>
                      </div>
                      <div class="col-lg-6 col-md-6">
                        <div class="form-group">
                        <input type="email" class="form-control" placeholder="Email (Required)" name="Email" required>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-12 col-md-12">
                        <div class="form-group">
                          <textarea class="form-control"  cols="30" rows="8" placeholder="Message" name="Pesan" maxlength='150'></textarea>
                        </div>
						<div class="col-md-6">
							<?php
							echo '<img src="' . $_SESSION['captcha']['image_src'] . '" alt="CAPTCHA code">';
							//echo $_SESSION['captcha']['code'];
							?>
						</div>
						<div class="col-md-6">
							<input type="hidden" name="KodeCaptcha" value="<?php echo $_SESSION['captcha']['code']; ?>">
							<input placeholder="Masukkan Captcha*" type="text" name="captcha" maxlength="5" class="form-control input-lg" required>
						</div>
                      </div>
                    </div>
                    <input class="contact_sendbtn" type="submit" name="Kirim" value="Send">
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12">
          <!-- BEGAIN GOOGLE MAP -->
          <!--<div class="google_map wow fadeInUp">                         
            <div id="map_canvas"></div>-->
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- END CONTACT SECTION -->

  <!-- START FOOTER SECTION -->
   <?php include 'Footer.php';?>
  <!-- END FOOTER SECTION -->

  <!-- JQuery Files -->
 <!-- Modal Popup untuk Edit--> 
  <div id="ModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	
  </div>
  <!-- Initialize jQuery Library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <!-- Google map -->
  <script src="https://maps.googleapis.com/maps/api/js"></script>
  <script src="js/jquery.ui.map.js"></script>  
  <!-- Skds slider -->
  <script src="js/skdslider.min.js"></script>
  <!-- Bootstrap js  -->
  <script src="js/bootstrap.min.js"></script>
  <!-- For smooth animatin  -->
  <script src="js/wow.min.js"></script> 

  <!-- Custom js -->
  <script type="text/javascript" src="js/custom.js"></script>
	<script type="text/javascript">
	   $(document).ready(function () {
		  $(".open_modal").click(function(e) {
		  var gambar 	= $(this).data("gambar");
		  var judul 	= $(this).data("judul");
		  var user 		= $(this).data("user");
		  var tanggal 	= $(this).data("tanggal");
		  var id	 	= $(this).data("id");
			   $.ajax({
					   url: "DetilGaleri.php",
					   type: "GET",
					   data : {Gambar:gambar,Judul:judul,User:user,Tanggal:tanggal,Id:id},
					   success: function (ajaxData){
					   $("#ModalEdit").html(ajaxData);
					   $("#ModalEdit").modal('show',{backdrop: 'true'});
				   }
				});
			});
		});
	</script>
  </body>
</html>