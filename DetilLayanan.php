<?php
include ('web/library/config.php');

$AmbilData = mysqli_query($koneksi,"SELECT * FROM kontenweb WHERE KodeKonten='".base64_decode(@$_GET['id'])."' AND JenisKonten='".base64_decode(@$_GET['jenis'])."'");
$Data = mysqli_fetch_assoc($AmbilData);
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bosstrans</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Slick slider css -->
    <link href="css/skdslider.css" rel="stylesheet">
    <!-- Font awesome css -->
	<link rel="stylesheet" href="web/admin/komponen/vendor/font-awesome/css/font-awesome.min.css">
    <!--<link rel="stylesheet" href="css/font-awesome.min.css">-->
    <!-- smooth animate css file -->
    <link rel="stylesheet" href="css/animate.css"> 
    <!-- Main style css -->
    <link rel="stylesheet" href="style.css">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="img/favicon.png"/>
    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100' rel='stylesheet' type='text/css'>     

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<style>
	#featuresSection {
		background-color: #FFFFFF;
		display: inline;
		float: left;
		padding-top: 50px;
		width: 100%;
		margin-top: 0px;
	}
	.features_productarea {
		border-top: 1px solid #DCDCDC;
		display: inline;
		float: left;
		margin-top: 0px;
		padding-top: 0px;
		position: relative;
		width: 100%;
	}
	
	.panel-footer {
		padding: 10px 15px;
		background-color: #f89628e6;
		border-top: 1px solid #ddd;
		border-bottom-right-radius: 3px;
		border-bottom-left-radius: 3px;
	}
	
	.modal-content {
		position: relative;
		background-color: #6e6b6b;
		border: 1px solid #999;
		border: 1px solid rgba(0,0,0,0.2);
		border-radius: 6px;
		outline: 0;
		-webkit-box-shadow: 0 3px 9px rgba(0,0,0,0.5);
		box-shadow: 0 3px 9px rgba(0,0,0,0.5);
		background-clip: padding-box;
	}
	</style>
	<!-- <style>
   #footer {
		float: left;
		display: inline;
		width: 100%;
		background-color: #333;
		padding: 25px 0px;
	}
   </style>-->
   <style>
	 #footer {
		display: inline;
		left: 0;
		bottom: 0;
		width: 100%;
		background-color: #333;
		color: white;
		text-align: center;
	}
	</style>

  </head>

  <body>
  <!-- BEGAIN PRELOADER --
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>
  <!-- END PRELOADER -->

<?php if(base64_decode($_GET['jenis'])=='LAYANAN'){ ?>
  <section id="featuresSection">
	<!-- START FEATURES PRODUCT AREA -->
    <div class="features_productarea">
      <div class="container">
        <!-- START FIRST FEATURES PRODUCT -->
        <div class="row">
          <div class="col-lg-6 col-md-6">
            <div class="featprodcs_img wow fadeInLeft">
			<?php 
			if($Data['Judul']=='Boss Car'){
				echo '<img class="img-responsive" src="img/boscar.png"  width="80%" alt="img">';
			}elseif($Data['Judul']=='Boss Travel'){
				echo '<img class="img-responsive" src="img/bostravel.png"  width="80%" alt="img">';
			}elseif($Data['Judul']=='Boss Rent'){
				echo '<img class="img-responsive" src="img/bosrent.png"  width="80%" alt="img">';
			}elseif($Data['Judul']=='Boss Ku'){
				echo '<img class="img-responsive" src="img/bosku.png" width="80%"  alt="img">';
			}
			?>
            </div>
          </div>
          <div class="col-lg-6 col-md-6">
            <div class="featprodcs_content wow fadeInRight">
              <h1><?php echo $Data['Judul']; ?></h1>
              <p><?php echo $Data['Title']; ?></p>
              <div class="media features_widget">
                <a class="pull-left" href="#">
                  <span class="fa fa-lightbulb-o clock_icon"></span>
                </a>
                <div class="media-body media_content">
                  <h4>Deskripsi</h4>
                  <?php echo $Data['Isi']; ?>
                </div>
              </div>
            </div>
          </div>
        </div>
        
      </div>
    </div>
	
	</section>
  
<?php }elseif(base64_decode($_GET['jenis'])=='S&K'){ ?>  
  <!-- START FEATURES SECTION -->
  <section id="featuresSection">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12">
          <div class="">
            <h1><?php echo $Data['Judul'];?></h1>
			<h4><?php echo $Data['Title'];?> | <i class="fa fa-user"></i> Author <?php echo $Data['UserName'];?></h4>
            <p><?php echo $Data['Isi'];?></p>
           </div>
        </div>
      </div>
    </div>  
    </div>     
  </section>
<?php }elseif(base64_decode($_GET['jenis'])=='BERANDA'){ ?>
	<section id="featuresSection">
	<!-- START FEATURES PRODUCT AREA -->
    <div class="features_productarea">
      <div class="container">
        <!-- START FIRST FEATURES PRODUCT -->
        <div class="row">
          <div class="col-lg-6 col-md-6">
            <div class="featprodcs_img wow fadeInLeft">
			<?php 
			if($Data['KodeKonten']=='KNT-0000001'){
				echo '<img class="img-responsive" src="img/slider1.png" alt="img">';
			}elseif($Data['KodeKonten']=='KNT-0000002'){
				echo '<img class="img-responsive" src="img/slider2.png" alt="img">';
			}elseif($Data['KodeKonten']=='KNT-0000003'){
				echo '<img class="img-responsive" src="img/slider3.png" alt="img">';
			}elseif($Data['KodeKonten']=='KNT-0000004'){
				echo '<img class="img-responsive" src="img/slider4.png" alt="img">';
			}
			?>
            </div>
          </div>
          <div class="col-lg-6 col-md-6">
            <div class="featprodcs_content wow fadeInRight">
              <h1><?php echo $Data['Judul']; ?></h1>
              <p><?php echo $Data['Title']; ?></p>
              <div class="media features_widget">
                <a class="pull-left" href="#">
                  <span class="fa fa-lightbulb-o clock_icon"></span>
                </a>
                <div class="media-body media_content">
                  <h4>Deskripsi</h4>
                  <?php echo $Data['Isi']; ?>
                </div>
              </div>
            </div>
          </div>
        </div>
        
      </div>
    </div>
	
	</section>
	
<?php }elseif(base64_decode($_GET['jenis'])=='FAQ'){ ?> 
	<section id="videoSection">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="video_area">
            <h1>Frequently Asked Question</h1>
            <!--<p>Lorem ipsum dolor sit amet. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming.</p>-->
            <div class="col-md-12 ">
                    <div class="product-content-right">
                        <div class="woocommerce">
                             <div id="customer_details" class="col2-set">
                                    <div class="col-md-12">
                                        <div class="woocommerce-billing-fields">
											<div class="bs-example">
												<div class="panel-group" id="accordion">
													<?php 
													$edit = mysqli_query($koneksi, "select * from kontenweb WHERE JenisKonten='FAQ' order by KodeKonten ASC");
													while($row = mysqli_fetch_array($edit)){ ?>
														
													<div class="panel panel-info">
														<div class="panel-heading">
															<h4 class="panel-title">
																<a data-toggle="collapse" data-parent="#accordion" href="#<?php echo $row['KodeKonten']; ?>"><?php echo ucwords($row['Judul']); ?></a>
															</h4>
														</div>
														<div id="<?php echo $row['KodeKonten']; ?>" class="panel-collapse collapse">
															<div class="panel-body">
																<p><?php echo ucwords($row['Isi']); ?></p>
															</div>
														</div>
													</div>
													
													<?php } ?>
													
												</div>
												<p><strong>Note:</strong> Klik Pada Setiap Pertanyaan Untuk Detil FAQ</p>
											</div>

                                        </div>
                                    </div>

                                   
                                </div>

                                
                                        
                            </form>

                        </div>                       
                    </div>                    
                </div>
          </div>
        </div>
      </div>
    </div>      
  </section>
<?php }elseif(base64_decode($_GET['jenis'])=='GALERI'){ ?>
<!-- START CLIENTS SECTION -->
  <section id="clients">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12">
          <div class="clients_area">
            <h1>Gallery</h1>
            <div class="client_slider">
				<div class="row">
				<?php
					include 'web/library/pagination1.php';
					// mengatur variabel reload dan sql
					$kosong=null;
					if(isset($_REQUEST['keyword']) && $_REQUEST['keyword']<>""){
						// jika ada kata kunci pencarian (artinya form pencarian disubmit dan tidak kosong)pakai ini
						$keyword=$_REQUEST['keyword'];
						$reload = "MasterGaleri.php?pagination=true&keyword=$keyword";
						$sql =  "SELECT * FROM kontenweb WHERE JenisKonten='GALERI' AND Judul LIKE '%$keyword%' ORDER BY KodeKonten ASC";
						$result = mysqli_query($koneksi,$sql);
					}else{
					//jika tidak ada pencarian pakai ini
						$reload = "DetilLayanan.php?pagination=true&jenis=".$_GET['jenis']."";
						$sql =  "SELECT * FROM kontenweb WHERE JenisKonten='GALERI' ORDER BY KodeKonten DESC";
						$result = mysqli_query($koneksi,$sql);
					}
					
					//pagination config start
					$rpp = 12; // jumlah record per halaman
					$page = intval(@$_GET["page"]);
					if($page<=0) $page = 1;  
					$tcount = mysqli_num_rows($result);
					$tpages = ($tcount) ? ceil($tcount/$rpp) : 1; // total pages, last page number
					$count = 0;
					$i = ($page-1)*$rpp;
					$no_urut = ($page-1)*$rpp;
					//pagination config end				
				?>
				
					<?php
					while(($count<$rpp) && ($i<$tcount)) {
						mysqli_data_seek($result,$i);
						$data = mysqli_fetch_array($result);
					?>
					<div class="col-lg-3">
						<div class="panel panel-yellow">
							<a href="#" class='open_modal' data-id='<?php echo $data['KodeKonten'];?>' data-judul='<?php echo $data['Judul'];?>' data-gambar='<?php echo $data['Gambar'];?>' data-user='<?php echo $data['UserName'];?>' data-tanggal='<?php echo $data['Date'];?>'>
								<div class="panel-heading">
									<img src="img/Galeri/thumb_<?php echo $data['Gambar'];?>" class="img img-responsive">
								</div>
							</a>
							<div class="panel-footer">
								<?php echo $data ['Judul']; ?>
							</div>
						</div>
					</div>
					<?php
						$i++; 
						$count++;
					}
					?>
			</div>
			
			</div>
          </div>
		  <div><?php echo paginate_one($reload, $page, $tpages); ?></div>
        </div>
      </div>
    </div>
  </section>
  <!-- END CLIENTS SECTION -->
<?php } ?>
  <!-- START FOOTER SECTION -->
 <?php include 'Footer.php';?>
  <!-- END FOOTER SECTION -->

  <!-- JQuery Files -->
  <!-- Modal Popup untuk Edit--> 
  <div id="ModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	
  </div>
  <!-- Initialize jQuery Library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <!-- Google map -->
  <script src="https://maps.googleapis.com/maps/api/js"></script>
  <script src="js/jquery.ui.map.js"></script>  
  <!-- Skds slider -->
  <script src="js/skdslider.min.js"></script>
  <!-- Bootstrap js  -->
  <script src="js/bootstrap.min.js"></script>
  <!-- For smooth animatin  -->
  <script src="js/wow.min.js"></script> 

  <!-- Custom js -->
  <script type="text/javascript" src="js/custom.js"></script>
	<script type="text/javascript">
	   $(document).ready(function () {
		  $(".open_modal").click(function(e) {
		  var gambar 	= $(this).data("gambar");
		  var judul 	= $(this).data("judul");
		  var user 		= $(this).data("user");
		  var tanggal 	= $(this).data("tanggal");
		  var id	 	= $(this).data("id");
			   $.ajax({
					   url: "DetilGaleri.php",
					   type: "GET",
					   data : {Gambar:gambar,Judul:judul,User:user,Tanggal:tanggal,Id:id},
					   success: function (ajaxData){
					   $("#ModalEdit").html(ajaxData);
					   $("#ModalEdit").modal('show',{backdrop: 'true'});
				   }
				});
			});
		});
	</script>
  </body>
</html>